from Step_3_illustrator import plot_balance, calc_res, plot_grid, plot_energy
import os

name_model = 'urban'
# automatic search of scenario name, if not working, you can identify the "scenario" by yourself
# cwd = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))
# files = os.listdir(cwd+'/'+folder_name+'/'+res_folder)
# folder in which  results are saved
folder_name = 'new_test'  # please give the folder name if "check_latest_file" is False
scenario = 'S15'
calc_type = 'RB'


# please give season or the start_day and duration, only when season is "na", the start_day is valid
# season: show results of a certain season
# start_day: choose the start_day (0-8760) to show the results
season = 'na'  # [winter, transition, summer, na], choice of season only applicable for 1-year-simulation
start_day = 182
duration = 10  # in days
# community show the electrical and thermal balance of the whole district
# numbers give the heat balance of the 1st or 2nd zone (now only save data of the first two zones)
show_object = 'community'  # 'community or number in integer such as 1, 2, 3 representing the number of buildings

# # show thermal and electrical balance
plot_balance(name_model=name_model, scenario=scenario, calc_type=calc_type, season=season,
             startday=start_day, sim_object=show_object, days=duration, folder_name=folder_name, latest_file=False)

# # # calculate the indicators: emission, energy cost, peak load and other indicators
# res = calc_res(name_model=name_model, scenario=scenario, calc_type=calc_type, season=season,
#                startday=start_day, sim_object=show_object, days=duration, folder_name=folder_name, latest_file=False)
# print(res)
#
# # plot grid related indicators (load matching and grid interaction)
# plot_grid(name_model=name_model, scenario=scenario, calc_type=calc_type, season=season,
#           startday=start_day, sim_object=show_object, days=duration, folder_name=folder_name, latest_file=False)
#
# # plot energy conumsption for each month and energy flow
# plot_energy(name_model=name_model, scenario=scenario, calc_type=calc_type, season=season,
#             startday=start_day, sim_object=show_object, days=duration, folder_name=folder_name, latest_file=False)
