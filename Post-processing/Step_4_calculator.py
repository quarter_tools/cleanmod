import os
import math
import time
import pandas as pd
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
# import config_cleanmod as config
import Scripts.scenario_plotter as scenario_plot
from datetime import datetime
from ruamel.yaml import YAML

os.chdir('../')
# import the results
with open("Step_0_config.yaml") as config_file:
    config = YAML().load(config_file)
name_model = config['project_settings']['name_model']  # "Example"
calc_type = config['simulation_settings']['calc_type']  # 'Temp', 'peak_load', 'elec_bought', 'Qhc'
# name_model = "Example_manuel_simple"   # "Example"
# calc_type = 'cost'    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'

# if the latest results should be checked
check_latest_file = True

# if you want to open a specific results folder
folder_name = 'Sommer_cost'  # please give the folder name if "check_latest_file" is False

if check_latest_file:
    mydir = os.path.join(os.getcwd(), 'Models/{0}/Results'.format(name_model))  # find the results folder


    def latest_dir(path):
        files = os.listdir(path)
        paths = [os.path.join(path, basename) for basename in files]
        return max(paths, key=os.path.getctime)


    res_dir = latest_dir(mydir)
else:
    mydir = os.path.join(os.getcwd(), 'Models/{0}/Results'.format(name_model))
    res_dir = os.path.join(mydir, folder_name)

# load the latest file of results in mainrun
res_path = os.path.join(res_dir, 'mainrun/district_timeseries.csv')
res_path_powergrid = os.path.join(res_dir, 'mainrun/power_grid_timeseries.csv')

res = pd.read_csv(res_path, index_col=0)
res_powergrid = pd.read_csv(res_path_powergrid, index_col=0)

######################################################
# plot the power balance

# scaling by 1000, change unit from W to kW because the standard unit in the optimization is W
res /= 1000

# obtain the number of buildings
n_building = int(len(res_powergrid.columns) / 2)


# sort the dataset/ time series
# function to split the positive and negative values
def split_val(input_value):
    value_pos = np.array([val if val >= 0 else 0 for val in input_value])
    value_neg = np.array([val if val < 0 else 0 for val in input_value])

    return value_pos, value_neg


# read the data from simulation results with regard to positive and negative values
# electricity imported from or fed into external power grid
p_elec_extern_pos, p_elec_extern_neg = split_val(res['import_elec'])
# split the values
p_import, p_export = split_val(res['import_elec'])
# household demand: electricity
p_demand_fix_pos, p_demand_fix_neg = split_val(-res['consum_elec'])
# electrical consumption of heat pump
p_hp_pos, p_hp_neg = split_val(-res['pow_elec_hp'])
# electrical consumption of cooling devices
p_cool_pos, p_cool_neg = split_val(-res['consum_cold'])
# pv production
p_pv_pos, p_pv_neg = split_val(res['gen_pv'])
# storage system for electricity/ battery
p_sto_pos, p_sto_neg = split_val(res['pow_elec_bat'])
# ev data
p_ev_neg = res['consum_ev_charge']
p_ev_pos = np.zeros(len(p_sto_pos))

# check the power balance (now not implemented because the transmission losses cause the unbalance)
# alternative: check the power balance not for the whole community, instead for each building
sum_p = p_import + p_export + p_demand_fix_neg + p_hp_neg + p_cool_neg + p_pv_pos + p_sto_pos + p_sto_neg + p_ev_pos \
        + p_ev_neg
# if max(abs(sum)) > 0.01:
#     raise ValueError('balance not equal')


# 1: calculate the flexibility index
# low tariff: 22Uhr - 6Uhr, high tariff: other time

start_time = config['simulation_settings']['starthour']
end_time = config['simulation_settings']['endhour']
duration = end_time - start_time

start_hour = start_time % 24
days = duration // 24
tp_rel = 7 - start_hour  # high tariff starts from 7 o'clock
p_ht = []
p_lt = []
for i in range(days):
    p_ht += list(p_import[i * 24 + tp_rel:i * 24 + tp_rel + 16])
    p_lt += list(p_import[i * 24 + 7:i * 24 + 22])

# all demand during high price: -1, all demand during low price: 1
flex_idx = (sum(p_lt) - sum(p_ht)) / (sum(p_lt) + sum(p_ht))
print('\n Flexibility Indes is:  ' + str(flex_idx))

# 2: capacity factor for the transformer
cap_factor = sum(p_elec_extern_pos)/n_building * 110 / (630 * len(p_elec_extern_pos))
print('\n Capacity Factor of transformer is:  ' + str(cap_factor))

