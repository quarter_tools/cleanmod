import os
import pandas as pd
import matplotlib.pyplot as plt
from Plots_def_AN import plot_scatter
from scipy.spatial import KDTree
import numpy as np
import warnings
warnings.filterwarnings("ignore", category=UserWarning)


name_model = 'Rural'
mydir = os.path.join(os.getcwd(), '../Models/{0}'.format(name_model))
save_dir = os.path.join(mydir, 'Plots')
isExist = os.path.exists(save_dir)
if not isExist:
   os.makedirs(save_dir)


# This part should be identical in script Step_10_Runme_Plots.py
# ----------------------------------------------------------------------------------------------------------------------
# Read data from CSV file
csv_path = os.path.join(mydir, 'Output', 'overview_table.csv')
data = pd.read_csv(csv_path, encoding='latin-1')

# add column scenario_nr
scenario_nr_list = [int(sc.split('S')[1]) for sc in data['scenario'].tolist()]
data['scenario_nr'] = scenario_nr_list
# Sort the data based on the numerical part of scenario names in ascending order
data.sort_values(by=['scenario_nr'], inplace=True)

# Scale the data
data['co2_emission'] /= 1000  # Convert Kg to tons
data['primary_energy_consum'] /= 1000  # Convert KWh to MWh
data['cost_EAC'] /= 1000  # Convert Euros to k€
data['cost_EAC_temporary'] /= 1000  # Convert Euros to k€
data['cost_operating'] /= 1000  # Convert Euros to k€
data['cost_operating_discounted'] /= 1000  # Convert Euros to k€
# Neighbourhood Load Indicators (internal)
data['self_consumption'] = data['self_consumption'] * 100  # Convert Euros to % (the higher the better)
data['self_sufficiency'] = data['self_sufficiency'] * 100  # Convert Euros to % (the higher the better)
# Grid Load Indicators (external)
data['flexible_load_ratio'] = data['flexible_load_ratio'] * 100  # Convert Euros to % (the higher the better)
data['grid_interaction_index'] = data['grid_interaction_index'] * 100  # Convert Euros to % (the lower the better)
data['high_load_rate'] = data['high_load_rate'] * 100  # Convert Euros to % (the lower the better)

# Add inverted data
# Neighbourhood Load Indicators (internal)
data['1 - self_consumption'] = 100 - data['self_consumption']  # in % | Inverted data: the lower the better
data['1 - self_sufficiency'] = 100 - data['self_sufficiency']  # in % | Inverted data: the lower the better
# Grid Load Indicators (external)
data['1 - flexible_load_ratio'] = 100 - data['flexible_load_ratio']  # in % | Inverted data: the lower the better
# ----------------------------------------------------------------------------------------------------------------------
# Adapt scenario names
adaptation_dict = {'S71': 'S0',
                   'S72': 'S1',
                   'S87': 'S12'}
for sc_ori in list(adaptation_dict.keys()):
    try:
        sc_ori_i = data.index[data['scenario'] == sc_ori].tolist()[0]
        data.at[sc_ori_i, 'scenario'] = adaptation_dict[sc_ori]
    except:
        pass

# ----------------------------------------------------------------------------------------------------------------------
# save copy of processed data
data_ori = data.copy()
# ----------------------------------------------------------------------------------------------------------------------
# Overview analysis
analysis_runs_dict = {0: 'Where are we today?',
                      1: 'What could happen in the future if we do nothing?',
                      2: 'What we achieve in the future focusing only on reducing energy consumption?',
                      3: 'What could we achieve in the future scenario if we would additionally consider flexibility?',
                      4: 'What benefit could provide a mix of several building standards for diversifying flexibility?',
                      5: 'What would be the best pathway to achieve the best scenario in the future?',
                      6: 'No circles, no pathways',
                      7: 'S115',
                      8: 'Extreme cases',
                      9: 'Combinations',
                      10: 'EV',
                      11: 'No circles, top 3 pathways'
                      }

analysis_run_list = list(analysis_runs_dict.keys())

analysis_run_list = [analysis_run_list[11]]  # analysis_run_list[7:]  # analysis_run_list[0:5]  # [analysis_run_list[5]]

# ======================================================================================================================
# PLOTS SECTION
# ======================================================================================================================
# set additional setting
process_ideal_scenarios = False
plot_rectangle = False
plot_optimum_corner = True
#plot_encircle = True
#list_scenarios_to_encircle = ['S0', 'S115', 'S116']
# Set the plot style using the style file
plt.style.use('style.mplstyle')
# ----------------------------------------------------------------------------------------------------------------------


# Functions
def identify_pareto(scores):
    tree = KDTree(scores)
    pareto_front = []
    distances = []  # List to keep track of distances to origin
    for i, point in enumerate(scores):
        indices = tree.query_ball_point(point, 12)
        is_dominated = False
        for j in indices:
            if i != j and all(scores[j] <= point) and any(scores[j] < point):
                is_dominated = True
                break
        if not is_dominated:
            pareto_front.append(True)
            distances.append(np.linalg.norm(point))  # Compute Euclidean distance to origin
        else:
            pareto_front.append(False)
            distances.append(None)

    # Now, select only 12 nearest points to the origin from the Pareto front
    if len(pareto_front) > 12:
        pareto_distances = [distances[i] for i in range(len(distances)) if pareto_front[i]]
        top_indices = sorted(range(len(pareto_distances)), key=lambda i: pareto_distances[i])[:12]
        for i in range(len(pareto_front)):
            if pareto_front[i] and i not in top_indices:
                pareto_front[i] = False

    return np.array(pareto_front)


def top_pareto_based_on_x(scores, top_n=12):
    # Identify pareto points
    is_pareto = identify_pareto(scores)
    pareto_points = scores[is_pareto]

    # Sort based on x-values
    sorted_indices = np.argsort(pareto_points[:, 0])

    # Select top_n solutions
    top_indices = sorted_indices[:top_n]
    top_points = pareto_points[top_indices]

    # Get their indices in the original array
    original_indices = np.where(np.all(np.isin(scores, top_points), axis=1))[0]

    return original_indices


# Function to Normalize the x and y axis values
def normalize_value(value, min_val, max_val):
    return 100 * (value - min_val) / (max_val - min_val)


# Function to adjust the limits by 5%
def adjust_limit(limit_tuple):
    range_ = limit_tuple[1] - limit_tuple[0]
    lower_limit = limit_tuple[0] - 0.05 * range_
    upper_limit = limit_tuple[1] + 0.05 * range_
    return (lower_limit, upper_limit)


def filter_for_ideal(data, plot_spec):
    x_range = data[plot_spec['x']].max() - data[plot_spec['x']].min()
    y_range = data[plot_spec['y']].max() - data[plot_spec['y']].min()

    threshold = 0.1
    if data[plot_spec['x']].min() > threshold * x_range:
        x_min_for_ideal = data[plot_spec['x']].nsmallest(2).iloc[-1]
    else:
        x_min_for_ideal = data[plot_spec['x']].min()

    if data[plot_spec['y']].min() > threshold * y_range:
        y_min_for_ideal = data[plot_spec['y']].nsmallest(2).iloc[-1]
    else:
        y_min_for_ideal = data[plot_spec['y']].min()

    ideal_rectangle = {
        'x_min': x_min_for_ideal - 0.01 * x_range,
        'x_max': x_min_for_ideal + 0.4 * x_range,
        'y_min': y_min_for_ideal - 0.01 * y_range,
        'y_max': y_min_for_ideal + 0.4 * y_range
    }

    filtered_data = data[
        (data[plot_spec['x']] >= ideal_rectangle['x_min']) &
        (data[plot_spec['x']] <= ideal_rectangle['x_max']) &
        (data[plot_spec['y']] >= ideal_rectangle['y_min']) &
        (data[plot_spec['y']] <= ideal_rectangle['y_max'])
    ]
    return filtered_data, ideal_rectangle


# Plot each graph individually
def plot_and_show(plot_spec, idx, ideal_only=False, max_x=None, max_y=None):
    fig, ax = plt.subplots(figsize=(18, 16))

    # Set xlim and ylim
    #xlim = adjust_limit((0, data[plot_spec['x']].max()))
    #ylim = adjust_limit((0, data[plot_spec['y']].max()))

    #xlim = (0, data[plot_spec['x']].max())
    #ylim = (0, data[plot_spec['y']].max())

    if plot_spec['x_lim'][1] == 'max':
        xlim = (0, data[plot_spec['x']].max())
    else:
        xlim = (0, plot_spec['x_lim'][1])  # (0, 100)

    if plot_spec['y_lim'][1] == 'max':
        ylim = (0, data[plot_spec['y']].max())
    else:
        ylim = (0, plot_spec['y_lim'][1])  # (0, 100)

    # define dimensions of "ideal" rectangle
    x_range = data[plot_spec['x']].max() - data[plot_spec['x']].min()
    y_range = data[plot_spec['y']].max() - data[plot_spec['y']].min()

    if plot_spec['optimum_corner'] == 'bottom-left':
        ideal_rectangle = {
            'x_min': data[plot_spec['x']].min() - 0.01 * x_range,
            'x_max': data[plot_spec['x']].min() + 0.5 * x_range,
            'y_min': data[plot_spec['y']].min() - 0.01 * y_range,
            'y_max': data[plot_spec['y']].min() + 0.5 * y_range
        }
    elif plot_spec['optimum_corner'] == 'bottom-right':
        ideal_rectangle = {
            'x_min': data[plot_spec['x']].max() - 0.5 * x_range,
            'x_max': data[plot_spec['x']].max() + 0.01 * x_range,
            'y_min': data[plot_spec['y']].min() - 0.01 * y_range,
            'y_max': data[plot_spec['y']].min() + 0.5 * y_range
        }

    if ideal_only:
        filtered_data = data[
            (data[plot_spec['x']] >= ideal_rectangle['x_min']) &
            (data[plot_spec['x']] <= ideal_rectangle['x_max']) &
            (data[plot_spec['y']] >= ideal_rectangle['y_min']) &
            (data[plot_spec['y']] <= ideal_rectangle['y_max'])
            ]
        xlim = (ideal_rectangle['x_min'], ideal_rectangle['x_max'])
        ylim = (ideal_rectangle['y_min'], ideal_rectangle['y_max'])
    else:
        filtered_data = data.copy()

    plot_scatter(plot_spec['x'], plot_spec['y'], plot_spec['x_label'], plot_spec['y_label'], plot_spec['title'],
                 filtered_data,data, name_model, ax=ax, xlim=xlim, ylim=ylim,
                 encircle=plot_encircle, scenarios_to_encircle=list_scenarios_to_encircle,
                 print_pathway=plot_pathway, pathways_to_print=list_pw_to_print)

    # Detect Pareto front for the filtered_data
    is_pareto = identify_pareto(filtered_data[[plot_spec['x'], plot_spec['y']]].values)
    pareto_data = filtered_data[is_pareto]

    # This line should come before you set any values on pareto_data
    pareto_data = pareto_data.copy()
    pareto_data['distance_to_origin'] = np.sqrt(pareto_data[plot_spec['x']] ** 2 + pareto_data[plot_spec['y']] ** 2)

    pareto_indices = top_pareto_based_on_x(filtered_data[[plot_spec['x'], plot_spec['y']]].values)
    pareto_data = filtered_data.iloc[pareto_indices]

    # Sort the pareto_data based on x-values to ensure the line doesn't crisscross
    pareto_data = pareto_data.sort_values(by=plot_spec['x'])

    # Highlight Pareto front scenarios by connecting them with a line
    #ax.plot(pareto_data[plot_spec['x']], pareto_data[plot_spec['y']], color='red', marker='o', linestyle='-')

    if not ideal_only:

        # Prepare secondary axis showing variation with respect to basis scenario S0

        x_s0 = data[data['scenario'] == 'S0'][plot_spec['x']].tolist()[0]
        y_s0 = data[data['scenario'] == 'S0'][plot_spec['y']].tolist()[0]

        x_min = data[plot_spec['x']].min()
        x_max = data[plot_spec['x']].max()

        y_min = data[plot_spec['y']].min()
        y_max = data[plot_spec['y']].max()

        ax2 = ax.twinx()
        ax3 = ax.twiny()

        # ax2_ylim
        if plot_spec['y_lim'][1] == 'max':
            if y_max == y_s0:
                ax2.set_ylim(-100, 0)
            elif y_s0 != 0:
                actual_ylim_max = (y_max / y_s0) * 100 - 100
                ax2.set_ylim(-100, actual_ylim_max)
            else:
                ax2.set_ylim(0, 100)
        else:
            if y_max == y_s0:
                ax2.set_ylim(-100, 0)
            elif y_s0 != 0:
                actual_ylim_max = (y_max / y_s0) * 100 - 100
                ax2.set_ylim(-100, actual_ylim_max)
            else:
                ax2.set_ylim(0, 100)

        # ax3_xlim
        if plot_spec['x_lim'][1] == 'max':
            if x_max == x_s0:
                ax3.set_xlim(-100, 0)
            elif x_s0 != 0:
                actual_xlim_max = (x_max / x_s0) * 100 - 100
                ax3.set_xlim(-100, actual_xlim_max)
            else:
                ax3.set_xlim(0, 100)
        else:
            if x_max == x_s0:
                actual_xlim_min = (0 - x_s0)
                actual_xlim_max = (100 - x_s0)
                ax3.set_xlim(actual_xlim_min, actual_xlim_max)
            elif x_s0 != 0:
                actual_xlim_max = 100 - x_s0
                actual_xlim_min = actual_xlim_max - 100
                ax3.set_xlim(actual_xlim_min, actual_xlim_max)
            else:
                ax3.set_xlim(0, 100)

        ax2.set_ylabel('y-axis Variation with respect to S0 (%)', rotation=-90, va="bottom", fontsize=20)
        ax3.set_xlabel('x-axis Variation with respect to S0 (%)', fontsize=20)

        ax2.grid(which='major', linestyle='-') #, linewidth='0.5', color='red')
        #ax2.grid(which='minor', linestyle=':')  # , linewidth='0.5', color='black')
        #ax2.minorticks_on()

        ax3.grid(which='major', linestyle='-') #, linewidth='0.5', color='red')
        #ax3.grid(which='minor', linestyle=':')  # , linewidth='0.5', color='black')
        #ax3.minorticks_on()


    # Use max_x and max_y for normalization if they are provided
    if max_x is None:
        max_x = data[plot_spec['x']].max()
    if max_y is None:
        max_y = data[plot_spec['y']].max()

    def update_xticks(axis, xlim):
        values = np.linspace(xlim[0], xlim[1], len(axis.get_xticks()))
        axis.set_xticklabels([f"{normalize_value(v, 0, max_x):.0f}" for v in values])

    def update_yticks(axis, ylim):
        values = np.linspace(ylim[0], ylim[1], len(axis.get_yticks()))
        axis.set_yticklabels([f"{normalize_value(v, 0, max_y):.0f}" for v in values])

    def on_xlim_changed(axis):
        xlim = axis.get_xlim()
        update_xticks(ax3, xlim)

    def on_ylim_changed(axis):
        ylim = axis.get_ylim()
        update_yticks(ax2, ylim)

    ax.callbacks.connect("xlim_changed", on_xlim_changed)
    ax.callbacks.connect("ylim_changed", on_ylim_changed)

    if plot_rectangle:
        rect = plt.Rectangle((ideal_rectangle['x_min'], ideal_rectangle['y_min']),
                             ideal_rectangle['x_max'] - ideal_rectangle['x_min'],
                             ideal_rectangle['y_max'] - ideal_rectangle['y_min'],
                             edgecolor='black', linewidth=2, fill=False)
        ax.add_patch(rect)

    plt.subplots_adjust(left=0.1, right=0.7, top=0.9, bottom=0.1)

    # Plot indication of optimum direction
    if plot_optimum_corner and not ideal_only:
        if plot_spec['optimum_corner'] == 'bottom-left':
            opt_x, opt_y, opt_va, opt_ha, opt_xytext = xlim[0], ylim[0], 'bottom', 'left', (5, 5)
            ax.scatter(opt_x, opt_y, color='black', marker='o', s=200, edgecolors='black', alpha=1)
            ax.annotate('optimum\ncorner',
                        xy=(opt_x,opt_y), xycoords='data',
                        xytext=opt_xytext, textcoords='offset points', va=opt_va, ha=opt_ha, size=15)
        if plot_spec['optimum_corner'] == 'bottom-right':
            opt_x, opt_y, opt_va, opt_ha, opt_xytext = xlim[1], ylim[0], 'bottom', 'right', (-5, 5)
            ax.scatter(opt_x, opt_y, color='black', marker='o', s=200, edgecolors='black', alpha=1)
            ax.annotate('optimum\ncorner',
                        xy=(opt_x,opt_y), xycoords='data',
                        xytext=opt_xytext, textcoords='offset points', va=opt_va, ha=opt_ha, size=15)

    # Adjust size of the secondary x & y ticks
    ax2.tick_params(axis='y', labelsize=14, length=6)
    ax3.tick_params(axis='x', labelsize=14, length=6)

    # Save
    if not os.path.exists(save_dir_A):
        os.makedirs(save_dir_A, exist_ok=True)

    save_path = os.path.join(save_dir_A, plot_spec['title'] + ('_ideal' if ideal_only else '') + '.png')
    ax.tick_params(axis='x', labelsize=14, length=6)  # Increase x-tick label size and length of tick marks
    ax.tick_params(axis='y', labelsize=14, length=6)  # Increase y-tick label size and length of tick marks
    plt.tight_layout()
    plt.savefig(save_path, bbox_inches='tight', pad_inches = 1, dpi=300)
    #plt.show()

    if ideal_only:
        return []

    # Return max_x, max_y, and scenarios for the non-zoomed plot
    return max_x, max_y, filtered_data['scenario'].tolist()


########################################################################################################################
for analysis in analysis_run_list:
    # create subfolder
    save_dir_A = os.path.join(save_dir, 'A' + str(analysis))
    isExist = os.path.exists(save_dir_A)
    if not isExist:
        os.makedirs(save_dir_A)
    # use only selected scenarios
    only_selection = True
    if only_selection:
        selection = [0, 116, 36, 22, 40, 26, 122, 123, 113, 115, 101, 117, 118, 119, 120, 121]
        selection_str = ['S' + str(i) for i in selection]
        data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 0:
        plot_encircle = True
        list_scenarios_to_encircle = ['S0']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 1:
        plot_encircle = True
        # list_scenarios_to_encircle = ['S0', 'S116']
        list_scenarios_to_encircle = ['S116']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 2:
        plot_encircle = True
        # list_scenarios_to_encircle = ['S0', 'S116', 'S36', 'S22']
        list_scenarios_to_encircle = ['S36', 'S22']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 3:
        plot_encircle = True
        # list_scenarios_to_encircle = ['S0', 'S116', 'S36', 'S22', 'S40', 'S26']
        list_scenarios_to_encircle = ['S40', 'S26']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 4:
        plot_encircle = True
        # list_scenarios_to_encircle = ['S0', 'S116', 'S36', 'S22', 'S40', 'S26', 'S122', 'S123', 'S113']
        list_scenarios_to_encircle = ['S122', 'S123', 'S113']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 5:
        plot_encircle = True
        #list_scenarios_to_encircle = ['S0', 'S116', 'S36', 'S22', 'S40', 'S26', 'S122', 'S123', 'S113']
        plot_pathway = True
        if name_model == 'suburban':
            list_pw_to_print = ['S0-S119-S113',
                                'S0-S121-S113',
                                'S0-S121-S40']
        elif name_model == 'rural':
            list_pw_to_print = ['S0-S119-S40',
                                'S0-S121-S40',
                                'S0-S119-S122']
        list_scenarios_to_encircle = []
        for pw in list_pw_to_print:
            scs = pw.split('-')
            for sc in scs:
                if sc not in list_scenarios_to_encircle:
                    list_scenarios_to_encircle.append(sc)
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 6:
        plot_encircle = False
        # list_scenarios_to_encircle = ['S0', 'S116', 'S36', 'S22', 'S40', 'S26', 'S122', 'S123', 'S113']
        list_scenarios_to_encircle = ['S122', 'S123', 'S113']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 7:
        plot_encircle = True
        list_scenarios_to_encircle = ['S115']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 8:
        plot_encircle = True
        list_scenarios_to_encircle = ['S101', 'S120', 'S117']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 9:
        plot_encircle = True
        list_scenarios_to_encircle = ['S118', 'S119', 'S121']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 10:
        plot_encircle = False
        list_scenarios_to_encircle = ['S118', 'S119', 'S121']
        plot_pathway = False
        list_pw_to_print = []
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 11:
        plot_encircle = False
        list_scenarios_to_encircle = []
        plot_pathway = True
        if name_model == 'Urban' or name_model == 'Suburban' or name_model == 'Rural':
            list_pw_to_print = ['S0-S119-S40',
                                'S0-S118-S40']
        # elif name_model == 'rural':
        #     list_pw_to_print = ['S0-S119-S40',
        #                         'S0-S121-S40',
        #                         'S0-S119-S122']
        list_scenarios_to_encircle = []
        for pw in list_pw_to_print:
            scs = pw.split('-')
            for sc in scs:
                if sc not in list_scenarios_to_encircle:
                    list_scenarios_to_encircle.append(sc)
    # ------------------------------------------------------------------------------------------------------------------


    # Define the plots specifications
    plots = [
    {'id': 0, 'title': 'Primary Energy Demand vs CO2-Emissions',
     'x_lim': (0, 'max'), 'x': 'primary_energy_consum', 'x_label': 'Primary Energy Demand [MWh]',
     'y_lim': (0, 'max'),  'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-left'},
    {'id': 1, 'title': 'Primary Energy Demand vs CO2-Emissions',
     'x_lim': (0, 'max'), 'x': 'primary_energy_consum', 'x_label': 'Primary Energy Demand [MWh]',
     'y_lim': (0, 'max'), 'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-left'},
    {'id': 2, 'title': 'Operating Costs vs CO2-Emissions',
     'x_lim': (0, 'max'), 'x': 'cost_operating', 'x_label': 'Operating costs [k€]',
     'y_lim': (0, 'max'), 'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-left'},
    {'id': 3, 'title': 'Equivalent Annual Cost vs CO2-Emissions',
     'x_lim': (0, 'max'), 'x': 'cost_EAC', 'x_label': 'Equivalent Annual Cost [k€]',
     'y_lim': (0, 'max'), 'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-left'},
    {'id': 4, 'title': 'Self-Consumption Rate vs CO2-Emissions',
     'x_lim': (0,   100), 'x': 'self_consumption', 'x_label': 'Self-Consumption Rate [%]',
     'y_lim': (0, 'max'), 'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-right'},
    {'id': 5, 'title': 'Self-Sufficiency Rate vs CO2-Emissions',
     'x_lim': (0,   100), 'x': 'self_sufficiency', 'x_label': 'Self-Sufficiency Rate [%]',
     'y_lim': (0, 'max'), 'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-right'},
    {'id': 6,  'title': 'Flexible Load Rate vs CO2-Emissions',
     'x_lim': (0,   100), 'x': 'flexible_load_ratio', 'x_label': 'Flexible Load Rate [%]',
     'y_lim': (0, 'max'), 'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-right'},
    {'id': 7, 'title': 'Grid Interaction Index vs CO2-Emissions',
     'x_lim': (0,   100), 'x': 'grid_interaction_index', 'x_label': 'Grid Interaction Index [%]',
     'y_lim': (0, 'max'), 'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-left'},
    {'id': 8, 'title': 'High Load Rate vs CO2-Emissions',
     'x_lim': (0,   100), 'x': 'high_load_rate', 'x_label': 'High Load Rate [%]',
     'y_lim': (0, 'max'), 'y': 'co2_emission', 'y_label': 'CO2-Emissions [t]',
     'optimum_corner': 'bottom-left'}
    ]

    # ----------------------------------------------------------------------------------------------------------------------
    # Plotting and saving

    ideal_scenarios_dict = {}

    # Call the function like:
    for idx, plot_spec in enumerate(plots):

        print('\n', plot_spec['title'])

        # Get the filtered data and the ideal rectangle
        filtered_data, ideal_rectangle = filter_for_ideal(data, plot_spec)

        # The function now returns the max_x and max_y when ideal_only is False
        max_x, max_y, _ = plot_and_show(plot_spec, idx, ideal_only=False)

        if process_ideal_scenarios:
            # Pass max_x and max_y to the function when ideal_only is True
            plot_and_show(plot_spec, idx, ideal_only=True, max_x=max_x, max_y=max_y)

            # Save the scenario names of the filtered data
            ideal_scenarios_dict[plot_spec['title']] = filtered_data['scenario'].tolist()

    if process_ideal_scenarios:
        # Convert dictionary to DataFrame
        ideal_scenarios_df = pd.DataFrame.from_dict(ideal_scenarios_dict, orient='index').transpose()

        # Process each column of the ideal_scenarios_df
        for column in ideal_scenarios_df.columns:
            # Loop through the rows and replace each scenario with its modified version
            for index, scenario in ideal_scenarios_df[column].items():
                if pd.notna(scenario):  # Check for non-NaN values
                    calc_type = data.loc[data['scenario'] == scenario, 'calc_type'].values[0]
                    maxPV_value = data.loc[data['scenario'] == scenario, 'maxPV'].values[0]
                    # Translate the maxPV value to the desired string
                    no_pv_scenarios = ['S0', 'S115', 'S116']
                    if scenario in no_pv_scenarios:
                        maxPV_str = "nonPV"
                    else:
                        maxPV_str = "maxPV" if maxPV_value == 1 else "minPV"

                    ideal_scenarios_df.at[index, column] = scenario + "_" + calc_type + "_" + maxPV_str

        # Save the dataframe to a CSV file
        ideal_scenarios_df.to_csv(os.path.join(save_dir_A, 'Ideal_Scenarios.csv'), index=False)