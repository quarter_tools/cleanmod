import os
import math
import time
import pandas as pd
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
# import config_cleanmod as config
import Cleanvelope.CleanMod.Scripts.scenario_plotter as scenario_plot
from datetime import datetime
from ruamel.yaml import YAML
from matplotlib.sankey import Sankey


def plot_balance(name_model, scenario, sim_object, calc_type, season, startday, days, latest_file, folder_name):
    # sort the dataset/ time series
    # function to split the positive and negative values

    def split_val(input_value, option='both'):
        # scale down by 1000 to change unit from W to kW
        value_pos = np.array([val / 1000 if val >= 0 else 0 for val in input_value])
        value_neg = np.array([val / 1000 if val < 0 else 0 for val in input_value])
        if option == 'both':
            _res = (value_pos, value_neg)
        elif option == 'pos':
            _res = value_pos
        else:
            _res = value_neg
        return _res

    def latest_dir(path):
        files = os.listdir(path)
        paths = [os.path.join(path, basename) for basename in files]
        return max(paths, key=os.path.getmtime)

    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'
    # name_model = "Example_manuel_simple"   # "Example"
    # calc_type = 'cost'    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'

    # if the latest results should be checked
    check_latest_file = latest_file

    if check_latest_file:
        mydir = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))  # find the results folder

        res_dir = latest_dir(mydir)
    else:
        mydir = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))
        res_dir = os.path.join(mydir, folder_name)

    # choose to show the whole community or only one building
    # options: 'community', building id (string) or nummer of building (int)
    month = 3.55
    if sim_object == 'community':
        # option 1: load the latest file of results in mainrun
        res_path = os.path.join(res_dir, f'{scenario}/{calc_type}/mainrun/district_timeseries.csv')

        # option 2: load the latest file of results in precalc
        # res_path = os.path.join(res_dir, 'precalc/district_timeseries.csv')

        # option 3: specify the folder of which the results are loaded
        # res_spec = mydir + '/220822_180224_temp'
        # res_path = os.path.join(res_spec, 'mainrun/district_timeseries.csv')

        res = pd.read_csv(res_path, index_col=0)
        # res = res.iloc[int(8500*3.15/4):int(8500*3.2/4)]
        if season == 'summer':
            start_ts = 3600
            startday = int(start_ts / 24)
        elif season == 'transition':
            start_ts = 1440
            startday = int(start_ts / 24)
        elif season == 'winter':
            start_ts = 7200
            startday = int(start_ts / 24)
        else:
            start_ts = startday*24
        res = res.iloc[int(start_ts):int(start_ts + 24 * days)]

        ######################################################
        # electricity imported from or fed into external power grid
        p_import, p_export = split_val(res['import_elec'])
        p_demand_fix_neg = split_val(-res['consum_elec'], 'neg')  # household demand: electricity
        p_hp_neg = split_val(-res['pow_elec_hp'], 'neg')  # electrical consumption of heat pump
        p_gb_pos = split_val(res['pow_th_gb'], 'pos')  # thermal output of gas boiler
        p_gb_gas_neg = split_val(res['pow_gas_gb'], 'neg')  # gas consumption of gas boiler
        p_cool_neg = split_val(-res['consum_cold'], 'neg')  # electrical consumption of cooling devices
        p_pv_pos = split_val(res['gen_pv'], 'pos')   # pv production
        p_sto_pos, p_sto_neg = split_val(res['pow_elec_bat'])  # storage system for electricity/ battery
        p_eh_neg = split_val(-res['pow_elec_eh'], 'neg')  # consumption of electrical heater
        p_eh_th_in_neg = split_val(-res['pow_th_eh_in'], 'neg')
        p_eh_th_out_pos = split_val(res['pow_th_eh_out'], 'pos')
        p_chp_elec_pos = split_val(res['pow_elec_chp'], 'pos')
        p_chp_th_pos = split_val(res['pow_th_chp'], 'pos')
        p_ev_neg = res['consum_ev_charge'] / 1000
        temp = res["temp_zone_1"]
        # p_ev_neg = np.zeros(len(p_sto_pos))
        # charge and discharge of thermal energy system
        p_dischar, p_char = split_val(res['pow_th_tes'])
        # thermal power of heat pump
        p_hp_th_pos = split_val(res['pow_th_hp'], 'pos')
        # consumption of electrical heater

        # space heating
        p_Qh_neg = split_val(-res['consum_th'], 'neg')
        # domestic hot water consumption
        p_dhw_neg = split_val(-res['consum_dhw'], 'neg')
        # district heating
        p_district_pos = split_val(res['import_th'], 'pos')
        p_demand_th_sh = split_val(res['consum_th'], 'pos')
        p_demand_th_dhw = split_val(res['consum_dhw'], 'pos')

        # check the power balance (now not implemented because the transmission losses cause the unbalance)
        # alternative: check the power balance not for the whole community, instead for each building
        sum_ = p_import + p_export + p_demand_fix_neg + p_hp_neg + p_cool_neg + p_pv_pos + p_sto_pos + p_sto_neg \
               + p_ev_neg
        # if max(abs(sum)) > 0.01:
        #     raise ValueError('balance not equal')

        # plot the electricity balance
        scplotter = scenario_plot.ScenarioPlotter()
        colors = ["0.2"]  # color of the power imported / exported
        colors_opt = ["#FCBF49", "#9DA2AB", "#2F84B1", "#BA4940", "#63bbd6", "#00FFCA",
                      "black", 'pink']  # color back-up which can be added
        colors_pos = []
        colors_neg = []
        n_items = 7  # the number of items excluding the power grid
        for i in range(n_items):
            colors.append(colors_opt[i])
        labels_opt = ["PV", "Grid", "Battery", "HP", "Cooling", 'EH', 'EV',
                      'CHP']  # corresponding labels for the devices
        labels_pos = []
        labels_neg = []

        # fill the dataframe for yvalues of plots
        df_pos = [p_pv_pos, p_import, p_sto_pos]
        df_neg = [p_export, p_sto_neg]

        # select the colors for positive and negative time series
        # pv
        if p_pv_pos.max() > 0:
            labels_pos.append(labels_opt[0])
            colors_pos.append(colors_opt[0])

        # grid import and export
        labels_pos.append(labels_opt[1])
        colors_pos.append(colors_opt[1])
        labels_neg.append(labels_opt[1])
        colors_neg.append(colors_opt[1])

        # battery charging and discharing
        if p_sto_pos.max() > 0:
            labels_pos.append(labels_opt[2])
            colors_pos.append(colors_opt[2])
            labels_neg.append(labels_opt[2])
            colors_neg.append(colors_opt[2])

        # heat pump
        if p_hp_neg.min() < 0:
            labels_neg.append(labels_opt[3])
            colors_neg.append(colors_opt[3])
            df_neg.append(p_hp_neg)
        # air conditioning
        if p_cool_neg.min() < 0:
            labels_neg.append(labels_opt[4])
            colors_neg.append(colors_opt[4])


        # electrical heater
        if p_eh_neg.min() < 0:
            labels_neg.append(labels_opt[5])
            colors_neg.append(colors_opt[5])
            df_neg.append(p_eh_neg)

        # electrical vehicle
        if p_ev_neg.min() < 0:
            labels_neg.append(labels_opt[6])
            colors_neg.append(colors_opt[6])
            df_neg.append(p_ev_neg)

        # chp
        if p_chp_elec_pos.max() > 0:
            df_pos.append(p_chp_elec_pos)
            labels_pos.append(labels_opt[7])
            colors_pos.append(colors_opt[7])

        # set up the xticks/dates, start from 2022 1.Jan 00:00 (unix time: 1577833200)
        time_start_unix = 1577833200
        num_xvalues = len(p_import)  # obtain the total time steps
        range_xvalues = range(num_xvalues)
        xvalues_date = [time_start_unix + startday * 24 * 3600 + idx * 3600 for idx in range_xvalues]  # timestamp
        # x_labels = [datetime.fromtimestamp(x).strftime("%H:%M") for x in xvalues_date]  # change time format

        # Line plot of electrical load
        yvalues = -p_demand_fix_neg
        scplotter.ax.plot(xvalues_date, yvalues, linewidth=1.5, color=colors[0], label='Electrical load')
        # helper line: y=0
        yvalues_line = np.zeros(num_xvalues)
        scplotter.ax.plot(xvalues_date, yvalues_line, linewidth=0.8, color=colors[0], label='_nolegend_')
        # plot the positive values
        yvalues = df_pos
        scplotter.ax.stackplot(xvalues_date, yvalues, baseline="zero", colors=colors_pos, labels=labels_pos, alpha=0.85)
        # plot the negative values
        yvalues = df_neg
        scplotter.ax.stackplot(xvalues_date, yvalues, baseline="zero", colors=colors_neg, labels=labels_neg, alpha=0.85)
        # Figure setup
        xlims = [min(xvalues_date), max(xvalues_date)]
        handles, labels = scplotter.ax.get_legend_handles_labels()
        idx_duplicate = [i for i, x in enumerate(labels) if x in ['Grid', 'Battery']]
        idx_duplicate_last = idx_duplicate[-int(len(idx_duplicate) / 2):]
        idx_duplicate_last.reverse()
        for idx in idx_duplicate_last:
            handles.pop(idx)
            labels.pop(idx)
        scplotter.figure_setup(ylabel='Electrical power [kW]', handles=handles, legend_labels=labels, xlims=xlims,
                               xticks_style="date")
        (ymin, ymax) = scplotter.ax.get_ylim()
        ylim = max(-ymin, ymax)
        scplotter.ax.set_ylim(-ylim - 10, ylim + 10)
        # post-processing
        # scenario_plot.save_figure(name="Power Balance", res_dir=res_dir, calc_type=calc_type)  # saved in mainrun/{calctype}/..
        plt.show()
        newfolder = f'{res_dir}/{scenario}/{calc_type}/figures'
        if not os.path.exists(newfolder):
            os.makedirs(newfolder)
        scplotter.fig.savefig(f'{res_dir}/{scenario}/{calc_type}/figures/power_balance.png', dpi=600)
        ######################################################
        # plot the heat balance

        # check the thermal power balance (now not implemented because the transmission losses cause the unbalance)
        # alternative: check the thermal power balance not for the whole community, instead for each building
        # sum_th = p_dischar + p_char + p_hp_pos + p_hp_neg + p_Qh_pos + p_Qh_neg + p_dhw_pos + p_dhw_neg + p_district_pos \
        #          + p_district_neg
        # if max(abs(sum_th)) > 0.01:
        #     raise ValueError('thermal balance not equal')

        # start the plot
        scplotter = scenario_plot.ScenarioPlotter()
        color_load = ["#050505"]  # color for the district heating
        colors_opt = ["#BA4940", "#2F84B1", "#9DA2AB", "#00FFCA", "#FF6969", 'pink']
        n_items = 5  # the number of items excluding the district heating
        colors_pos = []
        colors_neg = []
        for i in range(n_items):
            colors.append(colors_opt[i])
        labels_opt = ["HP", "TES", "DH", 'EH', 'GB', 'CHP']  # corresponding labels for the devices
        for i in range(n_items):
            labels.append(labels_opt[i])
        labels_pos = []
        labels_neg = []
        # prepare the yvalues for the plots
        df_pos = []
        df_neg = []

        # select the colors for positive and negative time series
        # hp
        if p_hp_th_pos.max() > 0:
            df_pos.append(p_hp_th_pos)
            labels_pos.append(labels_opt[0])
            colors_pos.append(colors_opt[0])

        # thermal energy storage
        labels_pos.append(labels_opt[1])
        colors_pos.append(colors_opt[1])
        labels_neg.append(labels_opt[1])
        colors_neg.append(colors_opt[1])
        df_pos.append(p_dischar)
        df_neg.append(p_char)

        # district heating
        if p_district_pos.max() > 0:
            labels_pos.append(labels_opt[2])
            colors_pos.append(colors_opt[2])
            df_pos.append(p_district_pos)
        # electrical heater
        if p_eh_th_out_pos.max() > 0:
            df_pos.append(-p_eh_neg)
            labels_pos.append(labels_opt[3])
            colors_pos.append(colors_opt[3])

        # gas boiler
        if p_gb_pos.max() > 0:
            labels_pos.append(labels_opt[4])
            colors_pos.append(colors_opt[4])
            df_pos.append(p_gb_pos)

        # chp
        if p_chp_th_pos.max() > 0:
            df_pos.append(p_chp_th_pos)
            labels_pos.append(labels_opt[5])
            colors_pos.append(colors_opt[5])

        # set up the xticks/dates, start from 2022 1.Jan 00:00
        num_xvalues = len(p_dischar)
        range_xvalues = range(num_xvalues)
        xvalues_date = [time_start_unix + startday * 24 * 3600 + idx * 3600 for idx in range_xvalues]  # time stamp
        # x_labels = [datetime.fromtimestamp(x).strftime("%H:%M") for x in xvalues_date]  # change the time format

        # Line plot of main meter
        if sim_object == 'community':
            yvalues = (p_demand_th_sh + p_demand_th_dhw)
        else:
            yvalues = res['load_th'] / 1000
        yvalues_line = np.zeros(num_xvalues)
        scplotter.ax.plot(xvalues_date, yvalues, linewidth=1.5, color=color_load[0], label='Heat load')
        scplotter.ax.plot(xvalues_date, yvalues_line, linewidth=0.8, color=colors[0],
                          label='_nolegend_')  # helpfer for horizontal line
        # plot the positive values
        yvalues = df_pos
        scplotter.ax.stackplot(xvalues_date, yvalues, baseline="zero", labels=labels_pos, colors=colors_pos, alpha=0.85)
        # plot the negative values
        yvalues = df_neg
        scplotter.ax.stackplot(xvalues_date, yvalues, baseline="zero", labels=labels_neg, colors=colors_neg, alpha=0.85)
        # Figure setup
        xlims = [min(xvalues_date), max(xvalues_date)]
        handles, labels = scplotter.ax.get_legend_handles_labels()
        idx_duplicate = [i for i, x in enumerate(labels) if x in ['TES']]
        idx_duplicate_last = idx_duplicate[-int(len(idx_duplicate) / 2):]
        idx_duplicate_last.reverse()
        for idx in idx_duplicate_last:
            handles.pop(idx)
            labels.pop(idx)
        scplotter.figure_setup(ylabel='Thermal Power [kW]', handles=handles, legend_labels=labels, xlims=xlims,
                               xticks_style="date")
        (ymin, ymax) = scplotter.ax.get_ylim()
        ylim = max(-ymin, ymax)
        scplotter.ax.set_ylim(-ylim - 10, ylim + 10)
        # post-processing
        # scenario_plot.save_figure(name="Heat Balance", res_dir=res_dir, calc_type=calc_type)
        plt.show()
        scplotter.fig.savefig(f'{res_dir}/{scenario}/{calc_type}/figures/heat_balance.png', dpi=600)


    else:
        # TODO: plots for single building need to be corrected
        if isinstance(sim_object, str):
            res_path = os.path.join(res_dir, f'{scenario}/{calc_type}/mainrun/time_series_{sim_object}.csv')
            res = pd.read_csv(res_path, index_col=0)
        else:
            # find the building id
            res_path = os.path.join(res_dir, f'{scenario}/{calc_type}/mainrun/power_grid_timeseries.csv')
            res = pd.read_csv(res_path, index_col=0)
            building_id = eval(res.columns[(sim_object - 1) * 2])[0]
            # read the building time series from csv
            sim_object = building_id
            res_path = os.path.join(res_dir, f'{scenario}/{calc_type}/mainrun/time_series_{sim_object}.csv')
            res = pd.read_csv(res_path, index_col=0)
        if season == 'summer':
            start_ts = 3600
            startday = int(start_ts / 24)
        elif season == 'transition':
            start_ts = 1440
            startday = int(start_ts / 24)
        elif season == 'winter':
            start_ts = 7200
            startday = int(start_ts / 24)
        else:
            start_ts = 0
        res = res.iloc[int(start_ts):int(start_ts + 24 * days)]

        for idx in [1, 2]:
            p_th_conduc_pos, p_th_conduc_neg = split_val(res[f'Qconduc_{idx}'])
            p_th_vent_pos, p_th_vent_neg = split_val(res[f'Qvent_{idx}'])
            p_th_solar_pos, p_th_solar_neg = split_val(res[f'Qsolar_{idx}'])
            p_th_convInt_pos, p_th_convInt_neg = split_val(res[f'QconvInt_{idx}'])
            p_th_radInt_pos, p_th_radInt_neg = split_val(res[f'QradInt_{idx}'])
            p_th_heating_pos, p_th_heating_neg = split_val(res[f'Qheating_{idx}'])
            temp_zone = np.array(res[f'temp_zone_{idx}'])
            temp_aggregated = np.array(res[f'temp_aggregate_{idx}'])
            temp_wall = np.array(res[f'temp_wall_{idx}'])
            # start the plot
            scplotter = scenario_plot.ScenarioPlotter()
            colors = ["#ACB1D6", "#2F84B1", "#9DA2AB", "#9E6F21", "#FF6969", "#19A7CE", 'red']
            labels = ['conduc', 'venti', 'solar', 'int. conv', 'int. rad', 'heating', 'wall']

            # prepare the yvalues for the plots
            df_pos = [p_th_conduc_pos, p_th_vent_pos, p_th_solar_pos, p_th_convInt_pos, p_th_radInt_pos,
                      p_th_heating_pos]
            df_neg = [p_th_conduc_neg, p_th_vent_neg, p_th_solar_neg, p_th_convInt_neg, p_th_radInt_neg,
                      p_th_heating_neg]

            # set up the xticks/dates, start from 2022 1.Jan 00:00 (unix time: 1577833200)
            time_start_unix = 1577833200
            # set up the xticks/dates, start from 2022 1.Jan 00:00
            num_xvalues = len(p_th_conduc_pos)
            range_xvalues = range(num_xvalues)
            xvalues_date = [time_start_unix + startday * 24 * 3600 + idx * 3600 for idx in range_xvalues]
            temp_ts = temp_zone[1:]
            temp_ts = np.append(temp_ts, temp_zone[0])
            scplotter.ax.plot(xvalues_date, temp_ts, linewidth=1.5, color='black',
                              label='Zone temperature')
            scplotter.ax.plot(xvalues_date, temp_wall, linewidth=1.5, color='red',
                              label='Wall temperature')
            scplotter.ax.plot(xvalues_date, res['temp_amb'], linewidth=1.5, ls='dashed', color='red', label='Ambient',
                              alpha=0.5)
            scplotter.ax.plot(xvalues_date, temp_aggregated, linewidth=1.5, ls='dashed', color='green',
                              label='Previous temp',
                              alpha=0.5)
            # plot the positive values
            yvalues = df_pos
            scplotter.ax.stackplot(xvalues_date, yvalues, baseline="zero", labels=labels, colors=colors, alpha=0.85)
            # plot the negative values
            yvalues = df_neg
            scplotter.ax.stackplot(xvalues_date, yvalues, baseline="zero", colors=colors, alpha=0.85)
            # Figure setup
            xlims = [min(xvalues_date), max(xvalues_date)]
            handles, labels = scplotter.ax.get_legend_handles_labels()
            scplotter.figure_setup(ylabel='Thermal Power [kW]', handles=handles, legend_labels=labels, xlims=xlims,
                                   xticks_style="date")
            (ymin, ymax) = scplotter.ax.get_ylim()
            ylim = max(-ymin, ymax)
            scplotter.ax.set_ylim(-ylim - 10, ylim + 10)
            # post-processing
            # scenario_plot.save_figure(name="Heat Balance", res_dir=res_dir, calc_type=calc_type)
            plt.show()
            newfolder = f'{res_dir}/{scenario}/{calc_type}/figures'
            if not os.path.exists(newfolder):
                os.makedirs(newfolder)
            scplotter.fig.savefig(f'{res_dir}/{scenario}/{calc_type}/figures/'
                                  f'response test (10 kW) for {days} days in {season}.png', dpi=600)

        # electricity imported from or fed into external power grid
        # p_import, p_export = split_val(res['grid'])
        # p_demand_fix_pos, p_demand_fix_neg = split_val(-res['load_elec'])  # household demand: electricity
        # p_hp_pos, p_hp_neg = split_val(-res['hp_elec'])  # electrical consumption of heat pump
        # p_cool_pos, p_cool_neg = split_val(-res['cool'])  # electrical consumption of cooling devices
        # p_pv_pos, p_pv_neg = split_val(res['pv'])  # pv production
        # p_sto_pos, p_sto_neg = split_val(res['bat_pow'])  # storage system for electricity/ battery
        # p_ev_neg = res['ev'] / 1000 if res['ev'] else np.zeros(len(p_sto_pos))
        # p_ev_pos = np.zeros(len(p_sto_pos))
        # # p_ev_neg = np.zeros(len(p_sto_pos))
        # # charge and discharge of thermal energy system
        # p_dischar, p_char = split_val(res['tes_pow'])
        # # thermal power of heat pump
        # p_hp_th_pos, p_hp_th_neg = split_val(res['hp_th'])
        # # district heating
        # p_district_pos, p_district_neg = split_val(res['dh'])
    return 'no results when plotting figures'

def calc_res(name_model, scenario, sim_object, calc_type, season, startday, days, latest_file, folder_name):
    def split_val(input_value, option='both'):
        # scale down by 1000 to change unit from W to kW
        value_pos = np.array([val / 1000 if val >= 0 else 0 for val in input_value])
        value_neg = np.array([val / 1000 if val < 0 else 0 for val in input_value])
        if option == 'both':
            _res = (value_pos, value_neg)
        elif option == 'pos':
            _res = value_pos
        else:
            _res = value_neg
        return _res

    def latest_dir(path):
        files = os.listdir(path)
        paths = [os.path.join(path, basename) for basename in files]
        return max(paths, key=os.path.getmtime)

    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'
    # name_model = "Example_manuel_simple"   # "Example"
    # calc_type = 'cost'    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'

    # if the latest results should be checked
    check_latest_file = latest_file

    if check_latest_file:
        mydir = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))  # find the results folder

        res_dir = latest_dir(mydir)
    else:
        mydir = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))
        res_dir = os.path.join(mydir, folder_name)

    # choose to show the whole community or only one building
    # options: 'community', building id (string) or nummer of building (int)
    month = 3.55

    # option 1: load the latest file of results in mainrun
    res_path = os.path.join(res_dir, f'{scenario}/{calc_type}/mainrun/district_timeseries.csv')

    # option 2: load the latest file of results in precalc
    # res_path = os.path.join(res_dir, 'precalc/district_timeseries.csv')

    # option 3: specify the folder of which the results are loaded
    # res_spec = mydir + '/220822_180224_temp'
    # res_path = os.path.join(res_spec, 'mainrun/district_timeseries.csv')

    res = pd.read_csv(res_path, index_col=0)
    # res = res.iloc[int(8500*3.15/4):int(8500*3.2/4)]
    if season == 'summer':
        start_ts = 3600
        startday = int(start_ts / 24)
    elif season == 'transition':
        start_ts = 1440
        startday = int(start_ts / 24)
    elif season == 'winter':
        start_ts = 7200
        startday = int(start_ts / 24)
    else:
        start_ts = startday
    res = res.iloc[int(start_ts):int(start_ts + 24 * days)]


    ######################################################
    # electricity imported from or fed into external power grid
    p_import, p_export = split_val(res['import_elec'])
    temp = res["temp_zone_1"]
    p_district_pos = split_val(res['import_th'], 'pos')

    # show the results of indicators
    obj_cost = (res['obj_cost']).sum()  # the total energy costs, unit in Euro
    obj_co2 = (res['obj_co2']).sum() / 1000  # the total CO2 emission, unit in kg
    # the average value of maximum power flow from external grid of each time step, unit in kW
    obj_peak = np.max(res['obj_peak'] / 1000)  # kW
    temp_avg = temp.mean()

    print(f'costs == {obj_cost}')
    print(f'CO2-emission == {obj_co2}')
    print(f'Peak_power == {obj_peak}')
    print(f'Avg. temperature == {temp_avg}')

    # summarize the consumption of end energy consumption
    endenergie_elec = p_import.sum() + p_export.sum()
    print(f'Endenergie Elektrisch == {endenergie_elec}')
    endenergie_th = p_district_pos.sum()
    print(f'Endenergie Thermisch == {endenergie_th}\n')

    res_dict = {'cost': obj_cost,
                'emission': obj_co2,
                'peak_load': obj_peak,
                'temp_avg': temp_avg,
                'consum_elec': endenergie_elec,
                'consum_dh': endenergie_th}

    return res_dict

def plot_grid(name_model, scenario, sim_object, calc_type, season, startday, days, latest_file, folder_name):
    # sort the dataset/ time series
    # function to split the positive and negative values

    def split_val(input_value, option='both'):
        # scale down by 1000 to change unit from W to kW
        value_pos = np.array([val / 1000 if val >= 0 else 0 for val in input_value])
        value_neg = np.array([val / 1000 if val < 0 else 0 for val in input_value])
        if option == 'both':
            _res = (value_pos, value_neg)
        elif option == 'pos':
            _res = value_pos
        else:
            _res = value_neg
        return _res

    def latest_dir(path):
        files = os.listdir(path)
        paths = [os.path.join(path, basename) for basename in files]
        return max(paths, key=os.path.getmtime)

    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'
    # name_model = "Example_manuel_simple"   # "Example"
    # calc_type = 'cost'    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'

    # if the latest results should be checked
    check_latest_file = latest_file

    if check_latest_file:
        mydir = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))  # find the results folder

        res_dir = latest_dir(mydir)
    else:
        mydir = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))
        res_dir = os.path.join(mydir, folder_name)

    # choose to show the whole community or only one building
    # options: 'community', building id (string) or nummer of building (int)
    month = 3.55
    if sim_object == 'community':
        # option 1: load the latest file of results in mainrun
        res_path = os.path.join(res_dir, f'{scenario}/{calc_type}/mainrun/district_timeseries.csv')

        # option 2: load the latest file of results in precalc
        # res_path = os.path.join(res_dir, 'precalc/district_timeseries.csv')

        # option 3: specify the folder of which the results are loaded
        # res_spec = mydir + '/220822_180224_temp'
        # res_path = os.path.join(res_spec, 'mainrun/district_timeseries.csv')

        res = pd.read_csv(res_path, index_col=0)
        # res = res.iloc[int(8500*3.15/4):int(8500*3.2/4)]
        if season == 'summer':
            start_ts = 3600
            startday = int(start_ts / 24)
        elif season == 'transition':
            start_ts = 1440
            startday = int(start_ts / 24)
        elif season == 'winter':
            start_ts = 7200
            startday = int(start_ts / 24)
        else:
            start_ts = startday
        res = res.iloc[int(start_ts):int(start_ts + 24 * days)]

        ######################################################
        # electricity imported from or fed into external power grid
        p_import, p_export = split_val(res['import_elec'])
        p_demand_fix_neg = split_val(-res['consum_elec'], 'neg')  # household demand: electricity
        p_hp_neg = split_val(-res['pow_elec_hp'], 'neg')  # electrical consumption of heat pump
        p_gb_pos = split_val(res['pow_th_gb'], 'pos')  # thermal output of gas boiler
        p_gb_gas_neg = split_val(res['pow_gas_gb'], 'neg')  # gas consumption of gas boiler
        p_cool_neg = split_val(-res['consum_cold'], 'neg')  # electrical consumption of cooling devices
        p_pv_pos = split_val(res['gen_pv'], 'pos')   # pv production
        p_sto_pos, p_sto_neg = split_val(res['pow_elec_bat'])  # storage system for electricity/ battery
        p_eh_neg = split_val(-res['pow_elec_eh'], 'neg')  # consumption of electrical heater
        p_eh_th_in_neg = split_val(-res['pow_th_eh_in'], 'neg')
        p_eh_th_out_pos = split_val(res['pow_th_eh_out'], 'pos')
        p_chp_elec_pos = split_val(res['pow_elec_chp'], 'pos')
        p_chp_th_pos = split_val(res['pow_th_chp'], 'pos')
        p_ev_neg = np.array(res['consum_ev_charge'] / 1000)
        temp = res["temp_zone_1"]
        # p_ev_neg = np.zeros(len(p_sto_pos))
        # charge and discharge of thermal energy system
        p_dischar, p_char = split_val(res['pow_th_tes'])
        # thermal power of heat pump
        p_hp_th_pos = split_val(res['pow_th_hp'], 'pos')
        # consumption of electrical heater

        # space heating
        p_Qh_neg = split_val(-res['consum_th'], 'neg')
        # domestic hot water consumption
        p_dhw_neg = split_val(-res['consum_dhw'], 'neg')
        # district heating
        p_district_pos = split_val(res['import_th'], 'pos')
        p_demand_th_sh = split_val(res['consum_th'], 'pos')
        p_demand_th_dhw = split_val(res['consum_dhw'], 'pos')

        # calculation of load cover factor / self-sufficiency rate
        gen_sum = p_chp_elec_pos + p_pv_pos + p_sto_pos
        load_sum = gen_sum - p_sto_neg - p_ev_neg - p_hp_neg - p_eh_neg
        min_gen_load = np.array([min(gen_sum[i], gen_sum[i]) for i, value in enumerate(gen_sum)])
        f_ss = min_gen_load.sum()/load_sum.sum()

        # calculate supply cover factor / self-consumption rate
        f_sc = min_gen_load.sum() / gen_sum.sum()

        # calculate loss of load probability (LOLP): the fraction of
        # time on-site generation is insufficient and energy must be imported from the grid
        count_import = np.array([1 if value > 0 else 0 for value in p_import]).sum()
        count_export = np.array([1 if value < 0 else 0 for value in p_export]).sum()
        f_lolp = count_import/(count_export+count_import)

        # calculate grid interaction index:
        # the variability of the energy flow with the grid over the net-zero balancing period, normalized
        # with the maximum net-flow of electricity
        max_power_grid = max(p_import.max(), -p_export.min())
        import_kw = res['import_elec']/1000
        f_gii = np.std(import_kw/max_power_grid)

        indicators = [f_ss, f_sc, f_lolp, f_gii]
        labels = ['self-sufficiency', 'self-consumption', 'LOLP', 'grid interaction index']
        fig = plt.figure()
        x_pos = [1, 3, 5, 7]
        plt.bar(x_pos, indicators, align='center', edgecolor='black')
        plt.xticks(x_pos, labels)
        plt.grid(color='grey', linestyle='--', linewidth=1, alpha=0.5)
        plt.ylabel('Grid Indicator [-]')
        plt.tight_layout()
        plt.show()


def plot_energy(name_model, scenario, sim_object, calc_type, season, startday, days, latest_file, folder_name):
    # sort the dataset/ time series
    # function to split the positive and negative values

    def split_val(input_value, option='both'):
        # scale down by 1000 to change unit from W to kW
        value_pos = np.array([val / 1000 if val >= 0 else 0 for val in input_value])
        value_neg = np.array([val / 1000 if val < 0 else 0 for val in input_value])
        if option == 'both':
            _res = (value_pos, value_neg)
        elif option == 'pos':
            _res = value_pos
        else:
            _res = value_neg
        return _res

    def latest_dir(path):
        files = os.listdir(path)
        paths = [os.path.join(path, basename) for basename in files]
        return max(paths, key=os.path.getmtime)

    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'
    # name_model = "Example_manuel_simple"   # "Example"
    # calc_type = 'cost'    # 'Temp', 'peak_load', 'elec_bought', 'Qhc'

    # if the latest results should be checked
    check_latest_file = latest_file

    if check_latest_file:
        mydir = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))  # find the results folder

        res_dir = latest_dir(mydir)
    else:
        mydir = os.path.join(os.getcwd(), '../Models/{0}/Output'.format(name_model))
        res_dir = os.path.join(mydir, folder_name)

    # choose to show the whole community or only one building
    # options: 'community', building id (string) or nummer of building (int)
    month = 3.55
    if sim_object == 'community':
        # option 1: load the latest file of results in mainrun
        res_path = os.path.join(res_dir, f'{scenario}/{calc_type}/mainrun/district_timeseries.csv')

        # option 2: load the latest file of results in precalc
        # res_path = os.path.join(res_dir, 'precalc/district_timeseries.csv')

        # option 3: specify the folder of which the results are loaded
        # res_spec = mydir + '/220822_180224_temp'
        # res_path = os.path.join(res_spec, 'mainrun/district_timeseries.csv')

        res = pd.read_csv(res_path, index_col=0)

        ######################################################
        # electricity imported from or fed into external power grid

        p_demand_fix_neg = split_val(-res['consum_elec'], 'neg')  # household demand: electricity
        p_pv_pos = split_val(res['gen_pv'], 'pos')  # pv production
        p_ev_neg = res['consum_ev_charge'] / 1000
        p_demand_th_sh = split_val(res['consum_th'], 'pos')
        p_demand_th_dhw = split_val(res['consum_dhw'], 'pos')

        p_demand_elec = np.zeros(12)
        p_demand_sh = np.zeros(12)
        p_demand_dhw = np.zeros(12)
        p_pv = np.zeros(12)
        p_ev = np.zeros(12)

        for month in range(12):
            # unit in MWh
            p_demand_elec[month] = abs(p_demand_fix_neg[month * 720:(month + 1) * 720].sum() / 1000)
            p_demand_sh[month] = abs(p_demand_th_sh[month * 720:(month + 1) * 720].sum() / 1000)
            p_demand_dhw[month] = abs(p_demand_th_dhw[month * 720:(month + 1) * 720].sum() / 1000)
            p_pv[month] = abs(p_pv_pos[month * 720:(month + 1) * 720].sum() / 1000)
            p_ev[month] = abs(p_ev_neg[month * 720:(month + 1) * 720].sum() / 1000)

        res_df = pd.DataFrame({'Space heating': p_demand_sh,
                               'Hot water': p_demand_dhw,
                               'A & L': p_demand_elec,
                               'EVs': p_ev},
                              index=['Jan', 'Feb', 'Mar',
                                     'Apr', 'May', 'Jun',
                                     'Jul', 'Aug', 'Sep',
                                     'Oct', 'Nov', 'Dec'])
        res_df_pv = pd.DataFrame({'PV': p_pv},
                                 index=['Jan', 'Feb', 'Mar',
                                        'Apr', 'May', 'Jun',
                                        'Jul', 'Aug', 'Sep',
                                        'Oct', 'Nov', 'Dec'])
        # grouped barplot
        fig, ax1 = plt.subplots(1, 1, figsize=(8, 5))

        res_df.plot(kind='bar', stacked=True, color=['#B70404', '#E8AA42', '#068DA9', '#DBDFAA'], edgecolor='black',
                    fontsize=15, ax=ax1)
        res_df_pv.plot(kind='line', color=['black'], marker='o', lw=2, fontsize=15, ax=ax1)
        # labels for x & y axis
        # plt.xlabel('Months')
        plt.ylabel('Secondary energy consumption \n'
                   'and PV generation [MWh]', fontsize=15)
        legend = ax1.legend(loc='upper center', ncol=3, frameon=True, fontsize=14)
        frame = legend.get_frame()
        frame.set_facecolor('white')
        frame.set_edgecolor('black')
        # ax1.set_ylim([0, 28])
        # title of plot
        # plt.title('Monthly Temperatures in a year')
        plt.grid(color='grey', linestyle='--', linewidth=1, alpha=0.5)
        plt.tight_layout()
        plt.show()
        # scplotter.fig.savefig(f'{res_dir}/{scenario}/{calc_type}/figures/heat_balance.png', dpi=600)
        # fig.savefig('figures/seasonal_consumption.png', dpi=400)

        # Pie chart
        labels = ['Space heating', 'Hot Water', 'A & L', 'Electric vehicle']
        sh_sum = np.sum(p_demand_sh)
        dhw_sum = np.sum(p_demand_dhw)
        al_sum = np.sum(p_demand_elec)
        ev_sum = np.sum(p_ev)
        total_sum = sh_sum + dhw_sum + al_sum + ev_sum

        size_sh = int(sh_sum / total_sum * 100)
        size_dhw = int(dhw_sum / total_sum * 100)
        size_al = int(al_sum / total_sum * 100)
        size_ev = int(ev_sum / total_sum * 100)

        sizes = [size_sh, size_dhw, size_al, size_ev]

        # colors
        sh_col = '#B70404'
        dhw_col = '#E8AA42'
        al_col = '#068DA9'
        ev_col = '#DBDFAA'

        colors = [sh_col, dhw_col, al_col, ev_col]
        # explsion
        explode = (0.1, 0.05, 0.05, 0.05)

        fig1, ax2 = plt.subplots(figsize=(8, 5))
        plt.pie(sizes, colors=colors, labels=labels, autopct='%1.1f%%', startangle=90, pctdistance=0.75,
                explode=explode,
                wedgeprops={"alpha": 0.8, "linewidth": 2, "edgecolor": "black", 'antialiased': True},
                textprops={'fontsize': 15})
        # draw circle
        centre_circle = plt.Circle((0, 0), 0.50, fc='white')
        fig = plt.gcf()
        # fig.gca().add_artist(centre_circle)
        # Equal aspect ratio ensures that pie is drawn as a circle
        ax1.axis('equal')
        plt.grid(color='grey', linestyle='--', linewidth=1, alpha=0.5)
        plt.tight_layout()
        plt.show()

        ######################################################
        # plot sankey diagram
        # p_import, p_export = split_val(res['import_elec'])
        # p_demand_fix_neg = split_val(-res['consum_elec'], 'neg')  # household demand: electricity
        # p_pv_pos = split_val(res['gen_pv'], 'pos')  # pv production
        # p_ev_neg = res['consum_ev_charge'] / 1000
        # p_district_pos = split_val(res['import_th'], 'pos')
        # p_demand_th_sh = split_val(res['consum_th'], 'pos')
        # p_demand_th_dhw = split_val(res['consum_dhw'], 'pos')
        # p_gas_in = split_val(res['pow_elec_chp'], 'pos') / 0.25 + split_val(res['pow_th_gb'], 'pos') / 0.98
        #
        # # import
        # p_import = int(p_import.sum() / 1000)
        # p_pv = int(p_pv_pos.sum() / 1000)
        # p_dh = int(p_district_pos.sum() / 1000)
        # p_gas = int(p_gas_in.sum() / 1000)
        #
        # # export or consumption
        #
        # export = int(p_export.sum() / 1000)
        # demand_elec = int(p_demand_fix_neg.sum() / 1000)
        # demand_th = int(-(p_demand_th_sh.sum() + p_demand_th_dhw.sum()) / 1000)
        #
        # # residual
        # sum = int(p_import + p_pv + p_dh + p_gas)
        # residual = int(- (p_import + p_pv + p_dh + p_gas + export + demand_th + demand_elec))
        #
        # # basic sankey chart
        # fig = plt.figure(figsize=(8, 6), frameon=False)
        # sankey = Sankey(
        #     # format='%.5G',
        #     head_angle=120,
        #     unit='MWh',
        #     margin=65,
        #     offset=20,
        #     gap=20, shoulder=0)
        #
        # sankey.add(flows=[p_pv, p_import, p_gas, -sum],
        #            # flows=[p_import, p_pv, p_dh, p_gas, export, demand_elec, demand_th, residual],
        #            labels=['pv', 'Gird Import', 'Natural Gas', None],
        #            pathlengths=[30, 30, 10, 30],
        #            orientations=[-1, 0, 1, 0], trunklength=1500, facecolor='#C1D0B5')
        #
        # sankey.add(flows=[sum, demand_elec, export, demand_th, residual],
        #            # flows=[p_import, p_pv, p_dh, p_gas, export, demand_elec, demand_th, residual],
        #            labels=['', 'Electrical demand', 'Grid Export', 'Thermal Demand', 'losses'],
        #            pathlengths=[30, 20, 50, 10, 30],
        #            orientations=[0, -1, -1, 0, 1], trunklength=1500, prior=0, connect=(3, 0), facecolor='#C4DFDF')
        # sankey.finish()
        #
        # # plt.title("Sankey diagram with default settings")
        # # plt.tight_layout()
        # plt.show()


if __name__ == "__main__":
    name_model = 'suburban'
    scenario = 'Suburban_RED2_1'
    calc_type = 'RB'
    folder_name = 'Results_230622_183806'

    # please give season or the start_day and duration, only when season is "na", the start_day is valid
    # season: show results of a certain season
    # start_day: choose the start_day (0-8760) to show the results
    season = 'na'  # [winter, transition, summer, na], choice of season only applicable for 1-year-simulation
    start_day = 0
    duration = 365  # in days
    # community show the electrical and thermal balance of the whole district
    # numbers give the heat balance of the 1st or 2nd zone (now only save data of the first two zones)
    show_object = 'community'  # 'community or number in integer such as 1, 2, 3 representing the number of buildings

    # scenario = f'{res_folder}/' + scenario
    # # show thermal and electrical balance
    plot_balance(name_model=name_model, scenario=scenario, calc_type=calc_type, season=season,
                 startday=start_day, sim_object=show_object, days=duration, folder_name=folder_name, latest_file=False)

    # # calculate the indicators: emission, energy cost, peak load and other indicators
    res = calc_res(name_model=name_model, scenario=scenario, calc_type=calc_type, season=season,
                   startday=start_day, sim_object=show_object, days=duration, folder_name=folder_name, latest_file=False)
    print(res)

    # plot grid related indicators (load matching and grid interaction)
    plot_grid(name_model=name_model, scenario=scenario, calc_type=calc_type, season=season,
              startday=start_day, sim_object=show_object, days=duration, folder_name=folder_name, latest_file=False)

    # plot energy conumsption for each month and energy flow
    plot_energy(name_model=name_model, scenario=scenario, calc_type=calc_type, season=season,
                startday=start_day, sim_object=show_object, days=duration, folder_name=folder_name, latest_file=False)




