import os
from Step_3_illustrator import calc_res
from ruamel.yaml import YAML
import pandas as pd

name_model = 'rural'
# scenario = 'S2_1_1_b'
test_folder = 'success_month'
path_res_folder = f"../Models/{name_model}/Output/{test_folder}"
files = os.listdir(path_res_folder)
folder_list = [basename for basename in files]
path_list = [os.path.join(path_res_folder, basename) for basename in files]
path_list.sort(key=os.path.getctime)
# check the error state of each simulation
#try:
error_log_read = []
for path in path_list:
    error_df = pd.read_csv(f'{path}/error_logging.csv')
    error_log_read.append(error_df)

# analyze the number of warnings
num_warnings = []
for error_row in error_log_read:
    try:
        num_warnings.append(error_row.shape[0] - error_row['0'].value_counts()['ok'])
    except:
        num_warnings.append(error_row['0'].value_counts()['warning'])

print(num_warnings)

#except:
#    pass

# obtain the indicators from simulations
# import the config file
# with open("../Step_0_config.yaml") as config_file:
#     config = YAML().load(config_file)
# # if you want to open a specific results folder
# season = 'na'  # [winter, transition, summer, na], choice of season only applicable for 1-year-simulation
# show_object = 'community'  # 'community or number in integer such as 1, 2, 3 representing the number of buildings
# days = 365
#
# res_list = []
# scenario_extended = ''
#
# for i, path in enumerate(path_list):
#     path = path.split('\\')[-1]
#     scenario_extended = path + '/' + scenario
#     res = calc_res(name_model=name_model, scenario=scenario_extended, calc_type='emi', season=season,
#                    sim_object=show_object, days=days, folder_name=test_folder, latest_file=False, startday=0)
#     res_list.append(res)

