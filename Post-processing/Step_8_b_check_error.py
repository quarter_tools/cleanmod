import os
import csv
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

name_model = 'rural_ev_improved'
folder_name = 'Output'
mydir = os.path.join(os.getcwd(), '../Models/{0}'.format(name_model))


def find_logging_errors(root_folder):
    error_messages = []

    for foldername, subfolders, filenames in os.walk(root_folder):
        # Skip the folder named "Archives"
        if 'Archive' in subfolders:
            subfolders.remove('Archive')

        for filename in filenames:
            if filename == 'logging_error.txt':
                error_file_path = os.path.join(foldername, filename)
                with open(error_file_path, 'r') as error_file:
                    error_message = error_file.read().strip()
                    grandparent_folder = os.path.basename(os.path.dirname(foldername))
                    error_messages.append((grandparent_folder, error_message))

    return error_messages


def save_to_csv(error_messages, csv_filename):
    # Ensure the directory exists
    os.makedirs(os.path.dirname(csv_filename), exist_ok=True)

    with open(csv_filename, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Scenario', 'Error Message'])
        csv_writer.writerows(error_messages)
    print(f'Successfully saved error messages to {csv_filename}')


csv_filename = os.path.join(mydir, 'error_messages.csv')

error_messages = find_logging_errors(mydir)
save_to_csv(error_messages, csv_filename)

# Visualization part
df = pd.read_csv(csv_filename)

df['Error State'] = df['Error Message'].apply(lambda x: int(x.split()[2]) if pd.notnull(x) else float('nan'))
df = df.dropna(subset=['Error State'])
df['Error State'] = df['Error State'].astype(int)
df['Scenario Index'] = df['Scenario'].apply(lambda x: int(x[1:]))

fig, axs = plt.subplots(2, 1, figsize=(20, 20))

error_counts = df['Error State'].value_counts()
axs[0].pie(error_counts, labels=error_counts.index, autopct='%1.1f%%', startangle=90, colors=['green', 'red'])
axs[0].set_title(name_model + ': Distribution of Error States')

scatter_plot = sns.scatterplot(x='Scenario Index', y='Error State', hue='Error State', palette=['green', 'red'],
                               data=df, s=100, ax=axs[1])
axs[1].set_title(name_model + ': Scatterplot of Error States over Scenarios')
axs[1].set_ylim([-0.2, 1.2])  # Set y-axis limits to be between 0 and 1

for index, row in df.iterrows():
    scatter_plot.text(row['Scenario Index'] + 0.2, row['Error State'], row['Scenario'], horizontalalignment='left',
                      size='small', color='black')

plt.savefig(mydir + '/error_messages.png')
