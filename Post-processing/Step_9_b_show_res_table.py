import os
import math
import pandas as pd
import numpy as np

def split_val(input_value, option='both'):
    """
    Splits the positive and negative values from the input and divides by 1000.

    Parameters:
    - input_value (float): Value to be split.
    - option (str, optional): Determines what to return. Defaults to 'both'. Options are 'both', 'pos', or 'neg'.

    Returns:
    - tuple: Returns a tuple of positive and negative values if option is 'both'.
            Returns positive values if option is 'pos', negative values if option is 'neg'.
    """
    pos_values = np.maximum(input_value, 0) / 1000
    neg_values = np.minimum(input_value, 0) / 1000
    return (pos_values, neg_values) if option == 'both' else (pos_values if option == 'pos' else neg_values)


def process_scenario(res_file_dir, calc_type, cost_operating_ref):
    """
    Process scenario results from the given directory.

    Parameters:
    - res_file_dir (str): Directory containing the result files.
    - calc_type (str): Type of calculation for selecting the file.

    Returns:
    - pd.Series: A series of processed results.
    """
    res_path = os.path.join(res_file_dir, f'{calc_type}/mainrun/district_timeseries.csv')
    try:
        res = pd.read_csv(res_path, index_col=0)
    except pd.errors.EmptyDataError:
        print("The file is empty or not formatted correctly.")
        return None

    name_model = res_file_dir.split("/")[-3].split('\\')[0]
    # Splitting the data into positive and negative values for calculations
    p_import, p_export = split_val(res['import_elec'])
    temp = res["temp_zone_1"]
    p_district_pos = split_val(res['import_th'], 'pos')
    p_pv_pos = split_val(res['gen_pv'], 'pos')
    # show the results of indicators
    obj_cost = (res['obj_cost']).sum()  # the total energy costs, unit in Euro
    obj_co2 = (res['obj_co2']).sum() / 1000  # the total CO2 emission, unit in kg
    # the average value of maximum power flow from external grid of each time step, unit in kW
    obj_peak = np.max(res['obj_peak'] / 1000)  # kW
    temp_avg = temp.mean()
    p_hp_neg = split_val(-res['pow_elec_hp'], 'neg')
    p_sto_pos, p_sto_neg = split_val(res['pow_elec_bat'])  # storage system for electricity/ battery
    p_eh_neg = split_val(-res['pow_elec_eh'], 'neg')  # consumption of electrical heater
    p_chp_elec_pos = split_val(res['pow_elec_chp'], 'pos')
    p_ev_neg = np.array(res['consum_ev_charge'] / 1000)
    if math.isnan(p_ev_neg[0]):
        p_ev_neg = [0] * len(p_chp_elec_pos)
    p_demand_fix_neg = split_val(-res['consum_elec'], 'neg')
    gen_sum = p_chp_elec_pos + p_pv_pos
    load_sum = - p_demand_fix_neg - p_sto_neg - p_ev_neg - p_hp_neg - p_eh_neg
    min_gen_load = np.array([min(gen_sum[i], load_sum[i]) for i, value in enumerate(gen_sum)])

    # calculation of load cover factor / self-sufficiency rate
    f_ss = min_gen_load.sum() / load_sum.sum()
    p_ss = max((f_ss - 0.15)/0.35*100, 0)  # 15%-50%
    # calculate supply cover factor / self-consumption rate
    f_sc = min_gen_load.sum() / gen_sum.sum()  # 50% - 100%
    p_sc = max((f_sc - 0.5) / 0.5 * 100, 0)
    # calculate loss of load probability (LOLP): the fraction of
    # time on-site generation is insufficient and energy must be imported from the grid
    count_import = np.array([1 if value > 0 else 0 for value in p_import]).sum()
    count_export = np.array([1 if value <= 0 else 0 for value in p_export]).sum()
    f_lolp = count_import / (count_export + count_import)  # 30% - 50%
    p_lolp = max((0.5-f_lolp) / 0.2 * 100, 0)
    # calculate grid interaction index:
    # the variability of the energy flow with the grid over the net-zero balancing period, normalized
    # with the maximum net-flow of electricity
    # max_power_grid = max(p_import[50:].max(), -p_export[50:].min())
    import_kw = res['import_elec'] / 1000
    # import_avg = import_kw.mean()
    f_gii = np.std(import_kw / 600)  # 0% - 40%
    p_gii = max((0.4 - f_gii) / 0.4 * 100, 0)
    # print(f_gii)
    # calculate the flexible load ratio
    pow_flex = - p_sto_neg - p_ev_neg - p_hp_neg - p_eh_neg
    f_flr = pow_flex.sum()/load_sum.sum()  # 50% - 80%
    p_flr = max((f_flr-0.5) / 0.3 * 100, 0)
    # print(f_flr)
    # calculate high loading ratio
    threshold = 600 * 0.6  # 60% of maximum load/transformer capacity
    count_high_load = np.array([1 if abs(value) >= threshold else 0 for value in p_import]).sum()
    count_low_load = np.array([1 if abs(value) < threshold else 0 for value in p_import]).sum()
    f_hlr = count_high_load / (count_high_load + count_low_load)
    p_hlr = max((0.1-f_hlr) / 0.1 * 100, 0)  # 0% - 10%
    print(p_hlr)
    # summarize all the indicators -> "Netzdienlichkeit"
    f_ndl = (f_ss + f_sc + f_flr + (1-f_gii) + (1-f_hlr))/5
    p_ndl_local = (p_ss + p_sc)/2
    p_ndl_ext = (p_flr + p_gii + p_hlr)/3
    # print(f_ndl)
    # f_ndl = (f_ss + f_sc) / 2
    p_demand_sh = split_val(res['consum_th'], 'pos').sum()
    p_demand_dhw = split_val(res['consum_dhw'], 'pos').sum()
    elec_consum_grid = p_import.sum()
    elec_consum_grid_ev = -np.array(res['consum_ev_charge'] / 1000).sum()
    elec_consum_grid_building = elec_consum_grid - elec_consum_grid_ev
    elec_consum_grid_total = elec_consum_grid
    elec_consum_pv = (p_pv_pos + p_export).clip(lower=0).sum()
    gas_consum = split_val(res['pow_gas_gb'], 'pos').sum()
    primary_energy_consum = 1.8 * elec_consum_grid + 1.1 * gas_consum
    self_sufficiency = f_ss
    self_consumption = f_sc
    flexible_load_ratio = f_flr
    #loss_of_load_probability = f_lolp
    grid_interaction_index = f_gii
    high_load_rate = f_hlr
    index_netzdienlichkeit_local = p_ndl_local
    index_netzdienlichkeit_external = p_ndl_ext
    cost_ts = res['obj_cost']
    # cost_share_ev_ts = -np.array(res['consum_ev_charge'] / 1000)
    # cost_share_bulding_ts = (p_import - cost_share_ev_ts).clip(0, 10e9)
    cost_share_ev_ts = sum(-np.array(res['consum_ev_charge'] / 1000))
    cost_share_bulding_ts = sum(p_import) - cost_share_ev_ts
    # for value in cost_ts:
    #     cost_ts_ev =
    #     cost_ts_building =
    # cost_operating_ev = cost_ts * cost_share_ev_ts / (cost_share_ev_ts + cost_share_bulding_ts + 0.1)
    # cost_operating_building = cost_ts * cost_share_bulding_ts / (cost_share_ev_ts + cost_share_bulding_ts + 0.1)
    # cost_operating_ev = cost_operating_ev.sum()
    # cost_operating_building = cost_operating_building.sum()
    if name_model == 'urban':
        nr_hh = 403
    elif name_model == 'rural':
        nr_hh = 165
    elif name_model == 'suburban':
        nr_hh = 336
    else:
        raise ValueError('District type not defined')
    cost_operating_per_year = obj_cost  # euro
    cost_operating_ev = obj_cost * cost_share_ev_ts / (cost_share_ev_ts + cost_share_bulding_ts)
    cost_operating_building = obj_cost * cost_share_bulding_ts / (cost_share_ev_ts + cost_share_bulding_ts)
    cost_renovation_construct = res['renovation_cost_construction'][0]
    cost_renovation_hp = res['renovation_cost_hp'][0]
    cost_renovation_pv = res['renovation_cost_pv'][0]
    cost_renovation_gb = 3200 * nr_hh
    cost_renovation = res['renovation_cost_total'][0] + cost_renovation_gb  # euro
    # calculation of annual factor: construction for 35 years, pv and hp for 20 years, discount rate 3.5%
    # annu_factor_construction = (1-(1+0.035)**(-35))/0.035
    # annu_factor_devices = (1 - (1 + 0.035) ** (-20)) / 0.035
    Kbase = cost_operating_ref  # operating cost of the base scenario
    r = 0.0147  # discount rate
    d = (1-1/(1+r))  # derived value from discount rate for simplifying calculation
    KC = cost_renovation_construct  # total cost of constructive renovation
    KD = cost_renovation_hp+cost_renovation_pv+cost_renovation_gb  # total cost of rechnical renovation
    Kop = cost_operating_per_year  # yearly operating cost
    a10 = (1 - (1 + r) ** (-10)) / r * (1+r)
    a25 = (1 - (1 + r) ** (-25)) / r * (1+r)
    a35 = (1 - (1 + r) ** (-35)) / r * (1+r)
    cost_eac = ((KC+KD)/25*a25 +
                Kbase/d -
                Kop*(1/(1+r))**25/d +
                (Kop-Kbase)/24*(1/(1+r))*(1-(1/(1+r))**24)/d**2+
                Kop*a10/(1+r)**25)/a35

    cost_eac_temp = (KC+KD)/10*a10/a35 +\
                    (Kbase/d -
                    Kop*(1/(1+r))**10/d +
                    (Kop-Kbase)/9*(1/(1+r))*(1-(1/(1+r))**9)/d**2)/a10

    cost_operating_per_year_discounted = cost_operating_per_year/(1+r)**25

    co2_emission = obj_co2  # kg

    # Creating a list of results for each parameter of interest
    res_list = [p_demand_sh, p_demand_dhw, elec_consum_grid_building,
                elec_consum_grid_ev, elec_consum_grid_total,
                elec_consum_pv, gas_consum, primary_energy_consum,
                index_netzdienlichkeit_local, index_netzdienlichkeit_external,
                cost_operating_ev, cost_operating_building, cost_operating_per_year, cost_operating_per_year_discounted,
                cost_renovation, cost_eac, cost_eac_temp, co2_emission, self_sufficiency,
                self_consumption, flexible_load_ratio, grid_interaction_index,
                high_load_rate]


    # Name for each result in res_list
    list_res = ['heat_demand_sh',
        'heat_demand_dhw',
        'elec_consum_grid_building',
        'elec_consum_grid_ev',
        'elec_consum_grid_total',
        'elec_consum_pv',
        'gas_consum',
        'primary_energy_consum',
        'grid_index_local',
        'grid_index_external',
        'cost_operating_ev',
        'cost_operating_building',
        'cost_operating',
        'cost_operating_discounted',
        'cost_renovation',
        'cost_EAC',
        'cost_EAC_temporary',
        'co2_emission',
        'self_sufficiency',
        'self_consumption',
        'flexible_load_ratio',
        'grid_interaction_index',
        'high_load_rate'
    ]

    return pd.Series(res_list, index=list_res)


def process_results(mydir, folder_name, cost_operating_ref):
    """
    Processes and aggregates results from all scenarios.

    Parameters:
    - mydir (str): Base directory containing result folders.
    - folder_name (str): Name of the folder with result data.

    Returns:
    - None: Saves processed results as CSV and Excel files in the given directory.
    """

    # Load the Excel file to extract maxPV from scenarios.xlsx
    file_path = os.path.join(mydir, 'Input', 'Scenarios.xlsx')
    # Read the "Scenarios" sheet, considering only columns D to BV
    scenario_df = pd.read_excel(file_path, sheet_name='Scenarios', usecols='D:DW')

    # Extract the column headers
    headers = scenario_df.columns

    # Extract the second row of each column
    second_row_values = scenario_df.iloc[0].values

    # Iterate over the second row values
    pv_values = []
    renvar_values = []
    year_values = []
    for value in second_row_values:
        # Split the value by `_`
        parts = value.split('_')

        # Check for 'nonPV', 'minPV', or 'maxPV' and assign values
        if 'nonPV' in parts:
            pv_values.append(0)
        elif 'minPV' in parts:
            pv_values.append(1)
        elif 'maxPV' in parts:
            pv_values.append(2)
        else:
            pv_values.append(None)  # Or handle this case as needed

        # Extract the value between the first and second `_`
        try:
            year = parts[1]
        except IndexError:
            year = None
        year_values.append(year)

        # Extract the value between the second and third `_`
        try:
            renvar = parts[2]
        except IndexError:
            renvar = None
        renvar_values.append(renvar)

    # Construct the result DataFrame
    df_maxPV = pd.DataFrame({
        'scenario': headers,
        'year': year_values,
        'maxPV': pv_values,  # Use pv_values instead of maxPV_presence.values
        'RenVar': renvar_values
    })

    if all(v is None for v in renvar_values):
        print("RenVar values not found!")

    # -----------
    res_dir = os.path.join(mydir, folder_name)
    res_files = sorted(os.listdir(res_dir), key=lambda x: int(x[1:]))
    res_array_list = []
    scenario_list = []
    calc_type_list = []  # List to store calc_type values

    # Iterate over each result file and process the scenario
    for res_file in res_files:
        print(f'analyze {res_file}')
        res_file_dir = os.path.join(res_dir, f'{res_file}/')
        res_type = os.listdir(res_file_dir)
        calc_type = res_type[0]
        res = process_scenario(res_file_dir, calc_type, cost_operating_ref)
        if res is not None:
            res_array_list.append(res)
            scenario_list.append(res_file)
            calc_type_list.append(calc_type)  # Append calc_type value

    # Creating a DataFrame from processed results
    res_df = pd.DataFrame(res_array_list)
    res_df['scenario'] = scenario_list
    res_df['calc_type'] = calc_type_list  # Add new column for calc_type

    # Merge the res_df with df_maxPV to add maxPV column
    res_df = pd.merge(res_df, df_maxPV, on='scenario', how='left')

    # Reordering columns to have 'scenario', 'calc_type' and 'maxPV' at the beginning
    cols = ['scenario', 'year', 'calc_type', 'maxPV', 'RenVar'] + [col for col in res_df.columns if
                                                 col not in ['scenario', 'year', 'calc_type', 'maxPV', 'RenVar']]
    res_df = res_df[cols]
    res_df.set_index('scenario', inplace=True)

    # Define save paths
    save_path_csv = os.path.join(res_dir, 'overview_table.csv')
    save_path_xlsx = os.path.join(res_dir, 'overview_table.xlsx')

    # Saving the results to CSV and Excel files
    res_df.to_csv(save_path_csv)
    res_df.to_excel(save_path_xlsx)


if __name__ == '__main__':
    # Model name and results folder for processing
    name_model = 'rural'
    folder_name = 'Output/'
    mydir = os.path.join(os.getcwd(), '../Models/{0}'.format(name_model))
    cost_operating_ref = 929718.3  # in Euro from column cost_operating of S0
    process_results(mydir, folder_name, cost_operating_ref)
