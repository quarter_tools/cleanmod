import os
import math
import time
import pandas as pd
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import json
from scipy.stats import ttest_ind
from datetime import datetime
import shutil
import seaborn as sns
# import config_cleanmod as config
import Scripts.scenario_plotter as scenario_plot
from random import choice, choices
from ruamel.yaml import YAML

os.chdir('../')
# import config data
with open("Step_0_config.yaml") as config_file:
    config = YAML().load(config_file)
name_model = config['project_settings']['name_model']
starthour = config['simulation_settings']['starthour']

# give the path of results
dir_scenario = os.path.join(os.getcwd(), f'Models/{name_model}/Results/scenario_results')  # find the results folder
res_scenarios = os.listdir(dir_scenario)
num_scenarios = len(res_scenarios)
# res_dir.remove(res_dir[0])
case = 1
if case == 1:
    # --------------------------------------------------
    # plot for key performance indicators
    list_scenario = []
    list_com = []
    list_energy = []
    nr_bul = 0
    for res in res_scenarios:
        if res != 'analysis':
            res_path = os.path.join(dir_scenario, res, 'mainrun/district_timeseries.csv')
            res_timeseries = pd.read_csv(res_path, index_col=0)
            for com in ['heat', 'elec']:
                list_scenario.append(res)
                list_com.append(com)
                if com == 'heat':
                    list_energy.append(sum(res_timeseries['import_th']) / 1000)
                else:
                    list_energy.append(sum(res_timeseries['import_elec']) / 1000)
        nr_bul += 1
        print(f'load import results of building {nr_bul} successful')

    # grouped barplot
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(12, 6), sharex=True)
    font_size = 12
    list_trans = [list(i) for i in zip(*[list_scenario, list_com, list_energy])]
    df_plot = pd.DataFrame(list_trans, columns=['scenario', 'commodity', 'energy'])
    val_max = df_plot['energy'].max()

    # first plot
    # sns.set(font_scale=2)
    sns.set_theme(style="white", context="talk")
    sns.barplot(x="scenario", y="energy", hue='commodity', data=df_plot, ax=ax1, linestyle='solid', ci=None,
                palette=['#6393ed', '#87cef9'], edgecolor='#020103')
    # ax1.axhline(0, color="k", clip_on=False)
    ax1.set_ylabel("Commodity imported [kWh]", size=font_size)
    ax1.tick_params(axis='both', which='major', labelsize=font_size)
    # ax1.text(0.03, 2.5, str(diff_1)+'%', fontsize=14)
    ax1.set_ylim([0, val_max + 2000])

    labels = ['heat', 'electricity']
    h, l = ax1.get_legend_handles_labels()
    ax1.legend(h, labels, loc='upper right', ncol=1, frameon=True, prop={'size': font_size}, edgecolor='black')
    ax1.get_xaxis().set_visible(True)
    ax1.set(xlabel=None)
    # plt.show()

    list_scenario = []
    list_cost = []
    # list_cost_heat = []
    # list_cost_elec = []
    list_emission = []
    # list_emission_heat = []
    # list_emission_elec = []
    list_peakload = []
    list_com = []
    nr_bul = 0
    for res in res_scenarios:
        if res != 'analysis':
            res_path = os.path.join(dir_scenario, res, 'mainrun/district_timeseries.csv')
            res_timeseries = pd.read_csv(res_path, index_col=0)
            for com in ['heat', 'elec', 'all']:
                list_scenario.append(res)
                list_com.append(com)
                if com == 'all':
                    list_cost.append(sum(res_timeseries['obj_cost']))  # euro
                    list_emission.append(sum(res_timeseries['obj_co2']) / 1000)
                    list_peakload.append(max(res_timeseries['obj_peak']) / 1000)  # kW
                elif com == 'elec':
                    list_cost.append(sum(res_timeseries['obj_cost']) * 0.8)  # euro
                    list_emission.append(sum(res_timeseries['obj_co2']) / 1000 * 0.7)
                    list_peakload.append(max(res_timeseries['obj_peak']) / 1000)  # kW
                else:
                    list_cost.append(sum(res_timeseries['obj_cost']) * 0.2)  # euro
                    list_emission.append(sum(res_timeseries['obj_co2']) / 1000 * 0.3)
                    list_peakload.append(max(res_timeseries['obj_peak']) / 1000)  # kW
        nr_bul += 1
        print(f'load KPI results of building {nr_bul} successful')

    list_trans = [list(i) for i in zip(*[list_scenario, list_com, list_cost, list_emission, list_peakload])]
    df_plot = pd.DataFrame(list_trans, columns=['scenario', 'commodity', 'cost', 'emission', 'peakload'])

    # second plot
    # sns.set(font_scale=2)
    sns.barplot(x="scenario", y="cost", hue='commodity', data=df_plot, ax=ax2, linestyle='solid', ci=None,
                palette=['#6393ed', '#87cef9', '#3b6bc4'], edgecolor='#020103')
    # ax1.axhline(0, color="k", clip_on=False)
    ax2.set_ylabel("Total cost [€]", size=font_size)
    ax2.tick_params(axis='both', which='major', labelsize=font_size)
    # ax1.text(0.03, 2.5, str(diff_1)+'%', fontsize=14)
    val_max = df_plot['cost'].max()
    ax2.set_ylim([0, val_max + 500])

    labels = ['heat', 'electricity', 'all']
    h, l = ax2.get_legend_handles_labels()
    ax2.legend(h, labels, loc='upper right', ncol=1, frameon=True, prop={'size': font_size}, edgecolor='black')
    ax2.get_xaxis().set_visible(True)
    ax2.set(xlabel=None)

    # third plot
    # sns.set(font_scale=2)
    sns.barplot(x="scenario", y="emission", hue='commodity', data=df_plot, ax=ax3, linestyle='solid', ci=None,
                palette=['#6393ed', '#87cef9', '#3b6bc4'], edgecolor='#020103')
    # ax1.axhline(0, color="k", clip_on=False)
    ax3.set_ylabel("CO2 emission [kg]", size=font_size)
    ax3.tick_params(axis='both', which='major', labelsize=font_size)
    # ax1.text(0.03, 2.5, str(diff_1)+'%', fontsize=14)
    val_max = df_plot['emission'].max()
    ax3.set_ylim([0, val_max + 500])

    labels = ['heat', 'electricity', 'all']
    h, l = ax3.get_legend_handles_labels()
    ax3.legend(h, labels, loc='upper right', ncol=1, frameon=True, prop={'size': font_size}, edgecolor='black')
    ax3.get_xaxis().set_visible(True)
    ax3.set(xlabel=None)

    # fouth plot
    # sns.set(font_scale=2)
    sns.barplot(x="scenario", y="peakload", data=df_plot, ax=ax4, linestyle='solid', ci=None,
                palette=['#6393ed'], edgecolor='#020103')
    # ax1.axhline(0, color="k", clip_on=False)
    ax4.set_ylabel("Peak load [kW]", size=font_size)
    ax4.tick_params(axis='both', which='major', labelsize=font_size)
    # ax1.text(0.03, 2.5, str(diff_1)+'%', fontsize=14)
    val_max = df_plot['peakload'].max()
    ax4.set_ylim([0, val_max + 50])

    # labels = ['heat', 'electricity', 'all']
    # h, l = ax3.get_legend_handles_labels()
    # ax3.legend(h, labels, loc='upper right', ncol=1, frameon=True, prop={'size': font_size}, edgecolor='black')
    ax4.get_xaxis().set_visible(True)
    ax4.set(xlabel=None)

    # sns.despine(bottom=True)
    plt.tight_layout(h_pad=2)
    plt.subplots_adjust(wspace=0.4)

    plt.show()
    scenario_plot.save_figure(name="commodity imported", res_dir=dir_scenario, calc_type='analysis')


# --------------------------------------------------
# plot of profiles of one building
if case == 2:
    nr_bul = 1
    res_scenarios.remove('analysis')
    res_spec = choice(res_scenarios)
    res_path = os.path.join(dir_scenario, res_spec, 'mainrun/power_grid_timeseries.csv')
    res = pd.read_csv(res_path, index_col=0)
    building_id = eval(res.columns[(nr_bul - 1) * 2])[0]
    # read the building time series from csv
    sim_object = building_id
    res_path = os.path.join(dir_scenario, res_spec, f'mainrun/time_series_{sim_object}.csv')
    res_timeseries = pd.read_csv(res_path, index_col=0)
    temp_zone = res_timeseries['temp_zone']
    soc_elec = res_timeseries['bat_soc']
    soc_th = res_timeseries['tes_soc']
    pow_th_hp = res_timeseries['hp_th']
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1, figsize=(6, 6), sharex=True,
                                             gridspec_kw={'height_ratios': [1, 1, 1, 1]})
    plt.style.use('seaborn-notebook')
    # configure the x-axis with date time
    time_start_unix = 1577833200  # the start of year 2021
    xvalues_date = [time_start_unix + starthour * 3600 + idx * 3600 for idx in range(len(temp_zone))]
    xlims = [min(xvalues_date), max(xvalues_date)]
    ax1.plot(xvalues_date, temp_zone, lw=1)
    ax1.axhline(y=16, color='black', linestyle='--', lw=1)
    ax1.axhline(y=25, color='black', linestyle='--', lw=1)
    ax1.set_ylim([15, 26])
    # ax1.set_xlabel('Zone temeprature', labelpad=10)
    fontsize = 10
    ax1.set_ylabel('Zone \n temeprature [°C]', fontsize=fontsize)
    ax1.set_xlim(xlims)
    ax2.plot(xvalues_date, pow_th_hp, lw=1, color='#b8675f')
    # ax2.set_xlabel('Thermal power of heat pump', labelpad=10)
    ax2.set_ylabel('Thermal power  \n of HP [kW]', fontsize=fontsize)
    ax3.plot(xvalues_date, soc_th*100, lw=1, color='#c23123')
    # ax3.set_xlabel('SOC of thermal energy system', labelpad=10)
    ax3.set_ylabel('SOC of TES [%]', fontsize=fontsize)
    ax4.plot(xvalues_date, soc_elec*100, lw=1, color='#343e7d')
    ax4.set_xlabel('Time')
    ax4.set_ylabel('SOC of BAT [%]', fontsize=fontsize)
    fig.align_ylabels((ax1, ax2, ax3, ax4))
    # fig.subplots_adjust(wspace=0, hspace=-5)
    time_step = 15 * 60  # time step of 60 minutes
    xlims = [min(xvalues_date), max(xvalues_date)]
    x_values = xlims[0] - xlims[0] % time_step
    x_values = list(range(x_values, xlims[1] + time_step, time_step))
    # Get step size depending on duration of simulation
    x_step = [x for x in [1, 2, 4, 8, 16, 24, 96, 2 * 96, 4 * 96, 5 * 96]
              if len(x_values) / x <= 10][0]
    # Get x-values. If they exceed the 5-day threshold (5*96), dates will be displayed only for the first and
    # 15th of each month
    if x_step:
        x_values = x_values[::x_step]
    else:
        x_values = [x for x in x_values if datetime.fromtimestamp(x).strftime("%H") == "00" and
                    (datetime.fromtimestamp(x).strftime("%d") == "01" or
                     datetime.fromtimestamp(x).strftime("%d") == "15")]

    # Change date style based on duration
    if x_step < 4:
        xformat = "%H:%M"
        xformat2 = "%d.%m"
    elif x_step < 96:
        xformat = "%Hh"
        xformat2 = "%d.%m"
    else:
        xformat = "%d"
        xformat2 = "%b"

    # Create labels and improve readability
    x_labels = [datetime.fromtimestamp(x).strftime(xformat) for x in x_values]
    x_labels_adj = x_labels.copy()
    for idx, x in enumerate(x_values[2:]):
        if idx == 0:
            x_labels_adj[idx] = datetime.fromtimestamp(x_values[idx]).strftime(xformat2)
        if x_labels[idx + 2][:2] < x_labels[idx + 1][:2]:
            x_labels_adj[idx + 2] = datetime.fromtimestamp(x).strftime(xformat2)
    plt.xticks(x_values, x_labels_adj)
    plt.tick_params(axis='both', which='major')
    plt.tight_layout()
    plt.show()
    # plt.savefig(f"{fig_dir}/{name}.png")

if case == 3:
    # give the path of results
    res_dir = 'emi_test'
    dir_scenario = os.path.join(os.getcwd(), f'Models/{name_model}/Results/{res_dir}')  # find the results folder
    res_scenarios = os.listdir(dir_scenario)
    num_scenarios = len(res_scenarios)
    list_emi = []
    list_cost = []
    for res in res_scenarios:
        res_path = os.path.join(dir_scenario, res, 'district_timeseries.csv')
        res_timeseries = pd.read_csv(res_path, index_col=0)
        obj_co2 = (res_timeseries['obj_co2']).sum() / 1000   # unit in kg
        obj_cost = (res_timeseries['obj_cost']).sum()   # unit in euro
        list_emi.append(obj_co2)
        list_cost.append(obj_cost)

    plt.figure()
    fig = plt.scatter(list_cost, list_emi)
    plt.xlabel('energy cost [€]', fontsize=15, labelpad=10)
    plt.ylabel('CO2 emission [kg]', fontsize=15, labelpad=10)
    plt.tight_layout()
    plt.show()
    # plt.savefig(dir_scenario+'/')