import os
import csv


def find_logging_errors(root_folder):
    error_messages = []

    for foldername, subfolders, filenames in os.walk(root_folder):
        for filename in filenames:
            if filename == 'logging_error.txt':
                error_file_path = os.path.join(foldername, filename)
                with open(error_file_path, 'r') as error_file:
                    error_message = error_file.read().strip()
                    grandparent_folder = os.path.basename(os.path.dirname(foldername))
                    error_messages.append((grandparent_folder, error_message))

    return error_messages


def save_to_csv(error_messages, csv_filename):
    with open(csv_filename, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Grandparent Folder', 'Error Message'])
        csv_writer.writerows(error_messages)
    print(f'Successfully saved error messages to {csv_filename}')


# Example usage
root_folder = r'E:\Cleanvelope_AN\Cleanvelope\CleanMod\Models\rural\Output' #r'E:\Cleanmod_AN\Cleanvelope\CleanMod\Models\rural\Output'  # Replace with the root folder path you want to search in
csv_filename = root_folder + '\error_messages.csv'  # Replace with the desired output CSV file name

error_messages = find_logging_errors(root_folder)
save_to_csv(error_messages, csv_filename)
