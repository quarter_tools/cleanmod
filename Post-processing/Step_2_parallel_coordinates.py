import os
import pandas as pd
import plotly.graph_objects as go
from ruamel.yaml import YAML


class ScenarioComparison:
    def __init__(self, config_file_path, name_model, check_latest_file=True, folder_name=None):
        """
        This module defines the ScenarioComparison class that can be used to compare different scenarios based on their objectives.

        The class takes a configuration file path, a name for the model, and some optional parameters. It reads in CSV files that are located in a specified directory, and generates a plot based on the data.

        Attributes:
        config_file_path (str): The path to the configuration file.
        name_model (str): The name of the model.
        check_latest_file (bool): Whether or not to check for the latest file in the specified directory.
        folder_name (str): The name of the folder containing the CSV files incase the option check_lastest_file is not selected.

        Methods:
        get_latest_dir(path): Given a path, returns the path to the most recently modified directory.
        read_csv_files(): Reads in the CSV files located in the specified directory and stores the data in a pandas DataFrame.
        plot(): Generates a plot based on the data stored in the pandas DataFrame.
        """
        self.config_file_path = config_file_path
        self.name_model = name_model
        self.check_latest_file = check_latest_file
        self.folder_name = folder_name
        self.df_paths = []
        self.data = pd.DataFrame()
        self.res_dir = None

    def get_latest_dir(self, path):
        """
            Returns the path to the latest modified file in the given directory.

            Args:
            - path (str): path to the directory

            Returns:
            - latest_file_path (str): path to the latest modified file in the directory
            """
        files = os.listdir(path)
        paths = [os.path.join(path, basename) for basename in files]
        return max(paths, key=os.path.getmtime)

    def read_csv_files(self):
        """
        Reads the CSV files from the directory specified in the configuration file and appends the data to a Pandas dataframe.
        """
        if self.check_latest_file:
            mydir = os.path.join(os.getcwd(), 'Models/{0}/Output'.format(self.name_model))
            self.res_dir = self.get_latest_dir(mydir)
        else:
            mydir = os.path.join(os.getcwd(), 'Models/{0}/Output'.format(self.name_model))
            self.res_dir = os.path.join(mydir, self.folder_name)

        for folders, sub_folders, files in os.walk(self.res_dir):
            for name in files:
                if name.endswith('district_sums.csv') and not folders.endswith('precalc'):
                    #SC_Obj = (os.path.basename(os.path.dirname(folders)) + ' - ' + os.path.basename(
                        #os.path.normpath(folders)))
                    SC_Obj = (os.path.basename(os.path.dirname(folders)))
                    a = os.path.join(folders, name)
                    self.df_paths.append((a, SC_Obj))

        for i, val in enumerate(self.df_paths):
            df = pd.read_csv(self.df_paths[i][0], index_col=0, header=None).T
            df['scenario_objective'] = os.path.basename(
                os.path.dirname(os.path.dirname(os.path.dirname(self.df_paths[i][0])))) + '_' + self.df_paths[i][1]
            df['index'] = i
            self.data = self.data.append(df)

        self.data = self.data.set_index(self.data['index'])
        first_column = self.data.pop('scenario_objective')
        self.data.insert(0, 'scenario_objective', first_column)
    def plot(self):
        """
        Creates a parallel coordinate plot using the data in the dataframe.
        """
        dimensions = list([
            dict(range=(self.data['index'].max(), self.data['index'].min()), tickvals=self.data['index'],
                 ticktext=self.data['scenario_objective'], label='scenario_objective', values=self.data['index']),
            dict(range=(self.data['consum_elec'].min(), self.data['consum_elec'].max()), label='consum_elec',
                 values=self.data['consum_elec']),
            dict(range=(self.data['consum_th'].min(), self.data['consum_th'].max()), label='consum_th',
                 values=self.data['consum_th']),
            dict(range=(self.data['gen_pv'].min(), self.data['gen_pv'].max()), label='gen_pv',
                 values=self.data['gen_pv']),
            # dict(range=(self.data['temp_amb'].min(), self.data['temp_amb'].max()), label='temp_amb', values=self.data['temp_amb']),
            dict(range=(self.data['obj_peak'].min(), self.data['obj_peak'].max()), label='obj_peak',
                 values=self.data['obj_peak']),
        ])
        fig = go.Figure(data= go.Parcoords(line = dict(color = self.data['index'], colorscale = 'thermal_r', showscale = False), dimensions = dimensions))
        fig.update_layout(width=1200, height=800,margin=dict(l=150, r=60, t=60, b=40))
        fig.write_image(self.res_dir+'/Scenario_Comparison.png')

if __name__ == '__main__':

    os.chdir('../')
    config_file_path = "Step_0_config.yaml"
    with open(config_file_path) as config_file:
        config = YAML().load(config_file)
    name_model = config['project_settings']['name_model']
    check_latest_file = True
    folder_name = None

    # Create an instance of the ScenarioComparison class
    scenario_comp = ScenarioComparison(config_file_path, name_model, check_latest_file, folder_name)

    # Read the CSV files
    scenario_comp.read_csv_files()

    # Generate the plot
    scenario_comp.plot()
