import os
import pandas as pd
#import matplotlib.pyplot as plt
#from Plots_def_AN import plot_scatter
#from scipy.spatial import KDTree
#import numpy as np
import warnings
warnings.filterwarnings("ignore", category=UserWarning)


name_model = 'Rural'
mydir = os.path.join(os.getcwd(), '../Models/{0}'.format(name_model))
save_dir = os.path.join(mydir, 'Tables')
isExist = os.path.exists(save_dir)
if not isExist:
   os.makedirs(save_dir)


# This part should be identical in script Step_10_Runme_Plots.py
# ----------------------------------------------------------------------------------------------------------------------
# Read data from CSV file
csv_path = os.path.join(mydir, 'Output', 'overview_table.csv')
data = pd.read_csv(csv_path, encoding='latin-1')

# add column scenario_nr
scenario_nr_list = [int(sc.split('S')[1]) for sc in data['scenario'].tolist()]
data['scenario_nr'] = scenario_nr_list
# Sort the data based on the numerical part of scenario names in ascending order
data.sort_values(by=['scenario_nr'], inplace=True)

# Scale the data
data['co2_emission'] /= 1000  # Convert Kg to tons
data['primary_energy_consum'] /= 1000  # Convert KWh to MWh
data['cost_EAC'] /= 1000  # Convert Euros to k€
data['cost_EAC_temporary'] /= 1000  # Convert Euros to k€
data['cost_operating'] /= 1000  # Convert Euros to k€
data['cost_operating_discounted'] /= 1000  # Convert Euros to k€
# Neighbourhood Load Indicators (internal)
data['self_consumption'] = data['self_consumption'] * 100  # Convert Euros to % (the higher the better)
data['self_sufficiency'] = data['self_sufficiency'] * 100  # Convert Euros to % (the higher the better)
# Grid Load Indicators (external)
data['flexible_load_ratio'] = data['flexible_load_ratio'] * 100  # Convert Euros to % (the higher the better)
data['grid_interaction_index'] = data['grid_interaction_index'] * 100  # Convert Euros to % (the lower the better)
data['high_load_rate'] = data['high_load_rate'] * 100  # Convert Euros to % (the lower the better)

# Add inverted data
# Neighbourhood Load Indicators (internal)
data['1 - self_consumption'] = 100 - data['self_consumption']  # in % | Inverted data: the lower the better
data['1 - self_sufficiency'] = 100 - data['self_sufficiency']  # in % | Inverted data: the lower the better
# Grid Load Indicators (external)
data['1 - flexible_load_ratio'] = 100 - data['flexible_load_ratio']  # in % | Inverted data: the lower the better
# ----------------------------------------------------------------------------------------------------------------------
# Adapt scenario names
adaptation_dict = {'S71': 'S0',
                   'S72': 'S1',
                   'S87': 'S12'}
for sc_ori in list(adaptation_dict.keys()):
    try:
        sc_ori_i = data.index[data['scenario'] == sc_ori].tolist()[0]
        data.at[sc_ori_i, 'scenario'] = adaptation_dict[sc_ori]
    except:
        pass

# ----------------------------------------------------------------------------------------------------------------------
# save copy of processed data
data_ori = data.copy()
# ----------------------------------------------------------------------------------------------------------------------
# Overview analysis
analysis_runs_dict = {0: 'Where are we today?',
                      1: 'What could happen in the future if we do nothing?',
                      2: 'What we achieve in the future focusing only on reducing energy consumption?',
                      3: 'What could we achieve in the future scenario if we would additionally consider flexibility?',
                      4: 'What benefit could provide a mix of several building standards for diversifying flexibility?',
                      5: 'What would be the best pathway to achieve the best scenario in the future?',
                      6: 'S0 + Scenarios 2045',
                      7: 'All scenarios',
                      42: 'Like 4 but different weightings - Efficiency',
                      43: 'Like 4 but different weightings - Flexibility',
                      10: 'EV'}

analysis_run_list = list(analysis_runs_dict.keys())

analysis_run_list = [analysis_run_list[4]]  #analysis_run_list[6:8]  #[analysis_run_list[6]]  #  #analysis_run_list[0:2]  #

# ======================================================================================================================
# TABLES SECTION
# ======================================================================================================================
# set additional setting
run_scenarios_vs_s0 = True
run_pathway_analysis_progressive = True

# define focus indicators
indicators = ['co2_emission',
              'primary_energy_consum',
              'cost_operating',
              'cost_EAC',
              'self_consumption',
              'self_sufficiency',
              'flexible_load_ratio',
              'grid_interaction_index',
              'high_load_rate'
              ]


########################################################################################################################
for analysis in analysis_run_list:
    # create subfolder
    save_dir_A = os.path.join(save_dir, 'A' + str(analysis))
    isExist = os.path.exists(save_dir_A)
    if not isExist:
        os.makedirs(save_dir_A)
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 0:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
        # Define weighting factors
        weighting_factors = {'co2_emission': 1,
                             'primary_energy_consum': 1,
                             'cost_operating': 1,
                             'cost_EAC': 1,
                             'self_consumption': 1,
                             'self_sufficiency': 1,
                             'flexible_load_ratio': 1,
                             'grid_interaction_index': 1,
                             'high_load_rate': 1}
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 1:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0, 116]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
        # Define weighting factors
        weighting_factors = {'co2_emission': 1,
                             'primary_energy_consum': 1,
                             'cost_operating': 1,
                             'cost_EAC': 1,
                             'self_consumption': 1,
                             'self_sufficiency': 1,
                             'flexible_load_ratio': 1,
                             'grid_interaction_index': 1,
                             'high_load_rate': 1}
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 2:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0, 116, 36, 22]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_1 = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_2 = data_ori[(data_ori['year']==2045) & (data_ori['calc_type']=='RB')].copy()
            #data = pd.concat([data_1, data_2])
        # Define weighting factors
        weighting_factors = {'co2_emission': 0,
                             'primary_energy_consum': 1,
                             'cost_operating': 0,
                             'cost_EAC': 0,
                             'self_consumption': 0,
                             'self_sufficiency': 0,
                             'flexible_load_ratio': 0,
                             'grid_interaction_index': 0,
                             'high_load_rate': 0}
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 3:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0, 116, 36, 22, 40, 26]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_1 = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_2 = data_ori[(data_ori['year']==2045) & (data_ori['calc_type']=='RB')].copy()
            #data = pd.concat([data_1, data_2])
        # Define weighting factors
        weighting_factors = {'co2_emission': 0,
                             'primary_energy_consum': 1,
                             'cost_operating': 0,
                             'cost_EAC': 0,
                             'self_consumption': 1,
                             'self_sufficiency': 1,
                             'flexible_load_ratio': 1,
                             'grid_interaction_index': 1,
                             'high_load_rate': 1}
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 4:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0, 116, 36, 22, 40, 26, 122, 123, 113]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_1 = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_2 = data_ori[(data_ori['year']==2045) & (data_ori['calc_type']=='RB')].copy()
            #data = pd.concat([data_1, data_2])
        # Define weighting factors
        weighting_factors = {'co2_emission': 1,
                             'primary_energy_consum': 1,
                             'cost_operating': 1,
                             'cost_EAC': 1,
                             'self_consumption': 1,
                             'self_sufficiency': 1,
                             'flexible_load_ratio': 1,
                             'grid_interaction_index': 1,
                             'high_load_rate': 1}
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 5:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0, 116, 36, 22, 40, 26, 122, 123, 113,
                         115, 101, 117, 118, 119, 120, 121]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_1 = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_2 = data_ori[(data_ori['year']==2045) & (data_ori['calc_type']=='RB')].copy()
            #data = pd.concat([data_1, data_2])
        # Define weighting factors
        weighting_factors = {'co2_emission': 1,
                             'primary_energy_consum': 1,
                             'cost_operating': 1,
                             'cost_EAC': 1,
                             'self_consumption': 1,
                             'self_sufficiency': 1,
                             'flexible_load_ratio': 1,
                             'grid_interaction_index': 1,
                             'high_load_rate': 1}
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 6:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0,
                         116,
                         22, 36,
                         26, 40,
                         122, 123, 113]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 7:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0,
                         116,
                         22, 36,
                         26, 40,
                         122, 123, 113,
                         115, 101, 117, 118, 119, 120, 121]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 42:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0, 116, 36, 22, 40, 26, 122, 123, 113]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_1 = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_2 = data_ori[(data_ori['year']==2045) & (data_ori['calc_type']=='RB')].copy()
            #data = pd.concat([data_1, data_2])
        # Define weighting factors
        weighting_factors = {'co2_emission': 1,
                             'primary_energy_consum': 1,
                             'cost_operating': 1,
                             'cost_EAC': 1,
                             'self_consumption': 0,
                             'self_sufficiency': 0,
                             'flexible_load_ratio': 0,
                             'grid_interaction_index': 0,
                             'high_load_rate': 0}
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 43:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0, 116, 36, 22, 40, 26, 122, 123, 113]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_1 = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_2 = data_ori[(data_ori['year']==2045) & (data_ori['calc_type']=='RB')].copy()
            #data = pd.concat([data_1, data_2])
        # Define weighting factors
        weighting_factors = {'co2_emission': 0,
                             'primary_energy_consum': 0,
                             'cost_operating': 0,
                             'cost_EAC': 0,
                             'self_consumption': 1,
                             'self_sufficiency': 1,
                             'flexible_load_ratio': 1,
                             'grid_interaction_index': 1,
                             'high_load_rate': 1}
    # ------------------------------------------------------------------------------------------------------------------
    if analysis == 10:
        # use only selected scenarios
        only_selection = True
        if only_selection:
            selection = [0, 116, 36, 22, 40, 26, 122, 123, 113,
                         115, 101, 117, 118, 119, 120, 121]
            selection_str = ['S' + str(i) for i in selection]
            data = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_1 = data_ori.loc[data_ori['scenario'].isin(selection_str)].copy()
            #data_2 = data_ori[(data_ori['year']==2045) & (data_ori['calc_type']=='RB')].copy()
            #data = pd.concat([data_1, data_2])
        # Define weighting factors
        weighting_factors = {'co2_emission': 1,
                             'primary_energy_consum': 1,
                             'cost_operating': 1,
                             'cost_EAC': 1,
                             'self_consumption': 1,
                             'self_sufficiency': 1,
                             'flexible_load_ratio': 1,
                             'grid_interaction_index': 1,
                             'high_load_rate': 1}
    # ------------------------------------------------------------------------------------------------------------------

    # ======================================================================================================================
    # INDIVIDUAL SCENARIOS COMPARED TO BASIS SCENARIO 'S0'
    # ======================================================================================================================
    if run_scenarios_vs_s0:

        # add variation columns to df
        scenario_list = data['scenario'].tolist()
        for i in indicators:
            abs_vars = []  # variation with respect basis scenario in absolut value
            rel_vars = []  # variation with respect basis scenario in relative % value
            # Reference value from basis scenario
            s0_val = data[data['scenario'] == 'S0'][i].tolist()[0]
            for sc in scenario_list:
                if sc == 'S0':
                    abs_var = rel_var = 0
                else:
                    sc_val = data[data['scenario'] == sc][i].tolist()[0]
                    abs_var = sc_val - s0_val
                    if i in ['self_consumption', 'self_sufficiency', 'flexible_load_ratio',
                             'grid_interaction_index', 'high_load_rate']:
                        rel_var = abs_var
                    else:
                        rel_var = (sc_val / s0_val) - 1

                abs_vars.append(abs_var)
                rel_vars.append(rel_var)
            data[i + '_abs_var'] = abs_vars
            data[i + '_rel_var'] = rel_vars


        def run_rank(val_list):
            rank_list = []
            rank_v = 1
            for i, v in enumerate(val_list):
                if i == 0:
                    pass
                else:
                    if v == val_list[i-1]:
                        pass
                    else:
                        rank_v += 1
                rank_list.append(rank_v)
            return rank_list

        # Rank by indicator
        for i in indicators:
            df_aux = data.copy()
            # omit basis scenario S0
            df_aux = df_aux[df_aux['scenario'] != 'S0']
            # sort
            ascending_order = False if i in ['self_consumption', 'self_sufficiency', 'flexible_load_ratio'] else True
            df_aux.sort_values(by=[i + '_rel_var'], inplace=True, ascending=ascending_order)
            i_vals = df_aux[i + '_rel_var'].tolist()
            # give rank
            i_rank_list = run_rank(i_vals)
            df_aux[i + '_rank'] = i_rank_list
            # recover original order
            df_aux.sort_values(by=['scenario_nr'], inplace=True)
            # save rank in original df
            #data[i + '_rank'] = df_aux[i + '_rank'].tolist()
            data[i + '_rank'] = [''] + df_aux[i + '_rank'].tolist()  # add an space to replace the omitted S0

        # Rank by categories
        categories = ['efficiency', 'flexibility', 'overall']
        for category in categories:
            df_aux = data.copy()
            # omit basis scenario S0
            df_aux = df_aux[df_aux['scenario'] != 'S0']
            if category == 'efficiency':
                weighting_factors = {'co2_emission': 0.25,
                                     'primary_energy_consum': 0.25,
                                     'cost_operating': 0.25,
                                     'cost_EAC': 0.25,
                                     'self_consumption': 0,
                                     'self_sufficiency': 0,
                                     'flexible_load_ratio': 0,
                                     'grid_interaction_index': 0,
                                     'high_load_rate': 0
                                     }
            elif category == 'flexibility':
                weighting_factors = {'co2_emission': 0,
                                     'primary_energy_consum': 0,
                                     'cost_operating': 0,
                                     'cost_EAC': 0,
                                     'self_consumption': 0.2,
                                     'self_sufficiency': 0.2,
                                     'flexible_load_ratio': 0.2,
                                     'grid_interaction_index': 0.2,
                                     'high_load_rate': 0.2
                                     }
            else:
                weighting_factors = {'co2_emission': 0.125,
                                     'primary_energy_consum': 0.125,
                                     'cost_operating': 0.125,
                                     'cost_EAC': 0.125,
                                     'self_consumption': 0.1,
                                     'self_sufficiency': 0.1,
                                     'flexible_load_ratio': 0.1,
                                     'grid_interaction_index': 0.1,
                                     'high_load_rate': 0.1
                                     }
            # # Prepare weighting
            # weighting_factors_sum = 0
            # for key in list(weighting_factors.keys()):
            #     weighting_factors_sum += weighting_factors[key]
            # # Resize the factors
            # for key in list(weighting_factors.keys()):
            #     weighting_factors[key] = weighting_factors[key] / weighting_factors_sum

            # calculate sums
            category_sum_vals = []
            #for sc in scenario_list:
            for sc in df_aux['scenario'].tolist():  # omitting S0
                sc_sum = 0
                for i in indicators:
                    val = df_aux[df_aux['scenario'] == sc][i + '_rank'].tolist()[0]
                    sc_sum += val * weighting_factors[i]
                category_sum_vals.append(sc_sum)
            # add sum col
            df_aux[category + '_sum'] = category_sum_vals
            # sort
            ascending_order = True
            df_aux.sort_values(by=[category + '_sum'], inplace=True, ascending=ascending_order)
            i_vals = df_aux[category + '_sum'].tolist()
            # give rank
            i_rank_list = run_rank(i_vals)
            df_aux[category + '_rank'] = i_rank_list
            # recover original order
            df_aux.sort_values(by=['scenario_nr'], inplace=True)
            # save rank in original df
            #data[category + '_sum'] = df_aux[category + '_sum'].tolist()
            #data[category + '_rank'] = df_aux[category + '_rank'].tolist()
            data[category + '_sum'] = [''] + df_aux[category + '_sum'].tolist()  # adding space to fill the omitted S0
            data[category + '_rank'] = [''] + df_aux[category + '_rank'].tolist() # adding space to fill the omitted S0

        # custom order
        custom_order_dict = {0: 0,
                             115: 1,
                             117: 2,
                             120: 3,
                             101: 4,
                             118: 5,
                             121: 6,
                             119: 7,
                             116: 8,
                             22: 9,
                             36: 10,
                             26: 11,
                             40: 12,
                             122: 13,
                             123: 14,
                             113: 15}
        custom_order_vals = []
        for sc in scenario_list:
            sc_nr = data[data['scenario'] == sc]['scenario_nr'].tolist()[0]
            custom_order_val = custom_order_dict[sc_nr]
            custom_order_vals.append(custom_order_val)
        data['custom_order'] = custom_order_vals

        # df sorted by custom order
        sorted_df = data.copy()
        sorted_df.sort_values(by=['custom_order'], inplace=True)

        # to make easier taking data for word
        sorted_df['CO2'] = [round(i,0) for i in sorted_df['co2_emission']]
        sorted_df['PED'] = [round(i,0) for i in sorted_df['primary_energy_consum']]
        sorted_df['OPC'] = [round(i,0) for i in sorted_df['cost_operating']]
        sorted_df['EAC'] = [round(i,0) for i in sorted_df['cost_EAC']]
        sorted_df['SCR'] = [round(i,2) for i in sorted_df['self_consumption']]
        sorted_df['SSR'] = [round(i,2) for i in sorted_df['self_sufficiency']]
        sorted_df['FLR'] = [round(i,2) for i in sorted_df['flexible_load_ratio']]
        sorted_df['GII'] = [round(i,2) for i in sorted_df['grid_interaction_index']]
        sorted_df['HLR'] = [round(i,2) for i in sorted_df['high_load_rate']]
        #
        sorted_df['CO2_var'] = [round(i*100,1) for i in sorted_df['co2_emission_rel_var']]
        sorted_df['PED_var'] = [round(i*100,1) for i in sorted_df['primary_energy_consum_rel_var']]
        sorted_df['OPC_var'] = [round(i*100,1) for i in sorted_df['cost_operating_rel_var']]
        sorted_df['EAC_var'] = [round(i*100,1) for i in sorted_df['cost_EAC_rel_var']]
        sorted_df['SCR_var'] = [round(i,2) for i in sorted_df['self_consumption_rel_var']]
        sorted_df['SSR_var'] = [round(i,2) for i in sorted_df['self_sufficiency_rel_var']]
        sorted_df['FLR_var'] = [round(i,2) for i in sorted_df['flexible_load_ratio_rel_var']]
        sorted_df['GII_var'] = [round(i,2) for i in sorted_df['grid_interaction_index_rel_var']]
        sorted_df['HLR_var'] = [round(i,2) for i in sorted_df['high_load_rate_rel_var']]

        # Define save paths
        save_path_csv = os.path.join(save_dir_A, 'A' + str(analysis) + '_' + 'overview_table_ranked.csv')
        save_path_xlsx = os.path.join(save_dir_A, 'A' + str(analysis) + '_' + 'overview_table_ranked.xlsx')

        # Saving the results to CSV and Excel files
        sorted_df.to_csv(save_path_csv, index=False)
        sorted_df.to_excel(save_path_xlsx, index=False)

        # --------------------------------------------------------------------------------------------------------------
        # Table for report
        pv_dict = {0: 'Kein',
                   1: 'MIN',
                   2: 'MAX'}

        export_dat1 = {('Szenario', 'Id', '', '', ''): sorted_df['scenario'].tolist(),
                       #('Szenario', 'Nr', '', '', ''): sorted_df['scenario_nr'].tolist(),
                       ('Szenario', 'Parameterkonfiguration', 'Jahr', '', '[-]'): sorted_df['year'].tolist(),
                       ('Szenario', 'Parameterkonfiguration', 'Gebaeudemix', 'Unsaniert', '[%]'): [str(i.split('-')[0]) for i in sorted_df['RenVar'].tolist()],
                       ('Szenario', 'Parameterkonfiguration', 'Gebaeudemix', 'KfW-70', '[%]'): [str(i.split('-')[1]) for i in sorted_df['RenVar'].tolist()],
                       ('Szenario', 'Parameterkonfiguration', 'Gebaeudemix', 'KfW-40', '[%]'): [str(i.split('-')[2]) for i in sorted_df['RenVar'].tolist()],
                       ('Szenario', 'Parameterkonfiguration', 'Waermesystem', '', '[-]'): ['GK' if i in [0, 116, 115, 117, 118]  else 'WP' for i in sorted_df['scenario_nr'].tolist()],
                       ('Szenario', 'Parameterkonfiguration', 'PV-Potential', '', '[-]'): [pv_dict[i]  for i in sorted_df['maxPV'].tolist()],
                       ('Szenario', 'Parameterkonfiguration', 'Steuerung', '', '[-]'): ['Konventionell' if i == 'RB' else 'Intelligent' for i in sorted_df['calc_type'].tolist()],


                       ('Ergebnisswert nach Indikator', 'Effizienz', 'Unweltfaktoren', 'CO2', '[t]'): sorted_df['CO2'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Effizienz', 'Unweltfaktoren', 'PED', '[MWh]'): sorted_df['PED'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Effizienz', 'Wirtschaftlichkeit', 'OPC', '[k€]'): sorted_df['OPC'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Effizienz', 'Wirtschaftlichkeit', 'EAC', '[k€]'): sorted_df['EAC'].tolist(),

                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Quartiersintern', 'SCR', '[%]'): sorted_df['SCR'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Quartiersintern', 'SSR', '[%]'): sorted_df['SSR'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Verteilnetz', 'FLR', '[%]'): sorted_df['FLR'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Verteilnetz', 'GII', '[%]'): sorted_df['GII'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Verteilnetz', 'HLR', '[%]'): sorted_df['HLR'].tolist(),


                       ('Variation gegenüber Basisszenario S0', 'Effizienz', 'Unweltfaktoren', 'CO2', '[%]'): sorted_df['CO2_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Effizienz', 'Unweltfaktoren', 'PED', '[%]'): sorted_df['PED_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Effizienz', 'Wirtschaftlichkeit', 'OPC', '[%]'): sorted_df['OPC_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Effizienz', 'Wirtschaftlichkeit', 'EAC', '[%]'): sorted_df['EAC_var'].tolist(),

                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Quartiersintern', 'SCR', '[%]'): sorted_df['SCR_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Quartiersintern', 'SSR', '[%]'): sorted_df['SSR_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Verteilnetz', 'FLR', '[%]'): sorted_df['FLR_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Verteilnetz', 'GII', '[%]'): sorted_df['GII_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Verteilnetz', 'HLR', '[%]'): sorted_df['HLR_var'].tolist(),

                       ('Ranking nach KPI', 'Effizienz', 'Unweltfaktoren', 'CO2', 'Rang'): sorted_df['co2_emission_rank'].tolist(),
                       ('Ranking nach KPI', 'Effizienz', 'Unweltfaktoren', 'PED', 'Rang'): sorted_df['primary_energy_consum_rank'].tolist(),
                       ('Ranking nach KPI', 'Effizienz', 'Wirtschaftlichkeit', 'OPC', 'Rang'): sorted_df['cost_operating_rank'].tolist(),
                       ('Ranking nach KPI', 'Effizienz', 'Wirtschaftlichkeit', 'EAC', 'Rang'): sorted_df['cost_EAC_rank'].tolist(),

                       ('Ranking nach KPI', 'Flexibilitaet', 'Quartiersintern', 'SCR', 'Rang'): sorted_df['self_consumption_rank'].tolist(),
                       ('Ranking nach KPI', 'Flexibilitaet', 'Quartiersintern', 'SSR', 'Rang'): sorted_df['self_sufficiency_rank'].tolist(),
                       ('Ranking nach KPI', 'Flexibilitaet', 'Verteilnetz', 'FLR', 'Rang'): sorted_df['flexible_load_ratio_rank'].tolist(),
                       ('Ranking nach KPI', 'Flexibilitaet', 'Verteilnetz', 'GII', 'Rang'): sorted_df['grid_interaction_index_rank'].tolist(),
                       ('Ranking nach KPI', 'Flexibilitaet', 'Verteilnetz', 'HLR', 'Rang'): sorted_df['high_load_rate_rank'].tolist(),

                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Effizienz', 'Summe', '', ''): sorted_df['efficiency_sum'].tolist(),
                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Effizienz', 'Rang', '', ''): sorted_df['efficiency_rank'].tolist(),

                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Flexibilitaet', 'Summe', '', ''): sorted_df['flexibility_sum'].tolist(),
                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Flexibilitaet', 'Rang', '', ''): sorted_df['flexibility_rank'].tolist(),

                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Global', 'Summe', '', ''): sorted_df['overall_sum'].tolist(),
                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Global', 'Rang', '', ''): sorted_df['overall_rank'].tolist()
                       }

        # create df
        report_table_df = pd.DataFrame(export_dat1)
        # create a MultiIndex from the column headers
        new_columns = pd.MultiIndex.from_tuples(report_table_df.columns)
        # set the new column headers
        report_table_df.columns = new_columns

        # Define save paths
        save_path_csv = os.path.join(save_dir_A, 'A' + str(analysis) + '_' + 'overview_table_report_TypeA.csv')
        save_path_xlsx = os.path.join(save_dir_A, 'A' + str(analysis) + '_' + 'overview_table_report_TypeA.xlsx')

        # Saving the results to CSV and Excel files
        report_table_df.to_csv(save_path_csv, index=False)
        report_table_df.to_excel(save_path_xlsx)

        # --------------------------------------------------------------------------------------------------------------
        add_line = ['% Variation gegenüber Basisszenario S0']
        export_dat2 = {('Szenario', 'Id', '', ''): [sorted_df['scenario'].tolist()[0]] + add_line + sorted_df['scenario'].tolist()[1:],

                       ('Parameterkonfiguration', 'Jahr', '', '[-]'): [sorted_df['year'].tolist()[0]] + [''] + sorted_df['year'].tolist()[1:],
                       ('Parameterkonfiguration', 'Gebaeudemix', 'Unsaniert', '%'): [[str(i.split('-')[0]) for i in sorted_df['RenVar'].tolist()][0]] + [''] + [str(i.split('-')[0]) for i in sorted_df['RenVar'].tolist()][1:],
                       ('Parameterkonfiguration', 'Gebaeudemix', 'KfW-70', '%'): [[str(i.split('-')[1]) for i in sorted_df['RenVar'].tolist()][0]] + [''] +  [str(i.split('-')[1]) for i in sorted_df['RenVar'].tolist()][1:],
                       ('Parameterkonfiguration', 'Gebaeudemix', 'KfW-40', '%'): [[str(i.split('-')[2]) for i in sorted_df['RenVar'].tolist()][0]] + [''] +  [str(i.split('-')[2]) for i in sorted_df['RenVar'].tolist()][1:],
                       ('Parameterkonfiguration', 'Waermesystem', '', '[-]'): [['GK' if i in [0, 116, 115, 117, 118]  else 'WP' for i in sorted_df['scenario_nr'].tolist()][0]] + [''] +  ['GK' if i in [0, 116, 115, 117, 118]  else 'WP' for i in sorted_df['scenario_nr'].tolist()][1:],
                       ('Parameterkonfiguration', 'PV-Potential', '', '[-]'): [[pv_dict[i]  for i in sorted_df['maxPV'].tolist()][0]] + [''] +  [pv_dict[i]  for i in sorted_df['maxPV'].tolist()][1:],
                       ('Parameterkonfiguration', 'Steuerung', '', '[-]'): [['Konventionell' if i == 'RB' else 'Intelligent' for i in sorted_df['calc_type'].tolist()][0]] + [''] + ['Konventionell' if i == 'RB' else 'Intelligent' for i in sorted_df['calc_type'].tolist()][1:],


                       ('Effizienz', 'Unweltfaktoren', 'CO2', '[t]'): [sorted_df['CO2'].tolist()[0]] + [''] + sorted_df['CO2_var'].tolist()[1:],
                       ('Effizienz', 'Unweltfaktoren', 'PED', '[MWh]'): [sorted_df['PED'].tolist()[0]] + [''] + sorted_df['PED_var'].tolist()[1:],
                       ('Effizienz', 'Wirtschaftlichkeit', 'OPC', '[k€]'): [sorted_df['OPC'].tolist()[0]] + [''] + sorted_df['OPC_var'].tolist()[1:],
                       ('Effizienz', 'Wirtschaftlichkeit', 'EAC', '[k€]'): [sorted_df['EAC'].tolist()[0]] + [''] + sorted_df['EAC_var'].tolist()[1:],

                       ('Flexibilitaet', 'Quartiersintern', 'SCR', '[%]'): [sorted_df['SCR'].tolist()[0]] + [''] + sorted_df['SCR_var'].tolist()[1:],
                       ('Flexibilitaet', 'Quartiersintern', 'SSR', '[%]'): [sorted_df['SSR'].tolist()[0]] + [''] + sorted_df['SSR_var'].tolist()[1:],
                       ('Flexibilitaet', 'Verteilnetz', 'FLR', '[%]'): [sorted_df['FLR'].tolist()[0]] + [''] + sorted_df['FLR_var'].tolist()[1:],
                       ('Flexibilitaet', 'Verteilnetz', 'GII', '[%]'): [sorted_df['GII'].tolist()[0]] + [''] + sorted_df['GII_var'].tolist()[1:],
                       ('Flexibilitaet', 'Verteilnetz', 'HLR', '[%]'): [sorted_df['HLR'].tolist()[0]] + [''] + sorted_df['HLR_var'].tolist()[1:],

                       ('Ranking', 'Effizienz', '', ''): [sorted_df['efficiency_rank'].tolist()[0]] + [''] + sorted_df['efficiency_rank'].tolist()[1:],
                       ('Ranking', 'Flexibilitaet', '', ''): [sorted_df['flexibility_rank'].tolist()[0]] + [''] + sorted_df['flexibility_rank'].tolist()[1:],
                       ('Ranking', 'Global', '', ''): [sorted_df['overall_rank'].tolist()[0]] + [''] + sorted_df['overall_rank'].tolist()[1:]
                       }

        # create df
        report_table_df = pd.DataFrame(export_dat2)
        # create a MultiIndex from the column headers
        new_columns = pd.MultiIndex.from_tuples(report_table_df.columns)
        # set the new column headers
        report_table_df.columns = new_columns

        # Define save paths
        save_path_csv = os.path.join(save_dir_A, 'A' + str(analysis) + '_' + 'overview_table_report_TypeB.csv')
        save_path_xlsx = os.path.join(save_dir_A, 'A' + str(analysis) + '_' + 'overview_table_report_TypeB.xlsx')

        # Saving the results to CSV and Excel files
        report_table_df.to_csv(save_path_csv, index=False)
        report_table_df.to_excel(save_path_xlsx)

    # ------------------------------------------------------------------------------------------------------------------
    # RENOVATION PATHWAYS
    # ------------------------------------------------------------------------------------------------------------------
    from scipy.interpolate import interp1d

    if run_pathway_analysis_progressive:

        # Load the data
        data = data_ori.copy()

        # Define all pathways to be considered
        pathways_dict = {0: [0, 115, 116],  # base pathway
                         1: [0, 118, 122],
                         2: [0, 118, 123],
                         3: [0, 118, 113],
                         4: [0, 118, 40],
                         5: [0, 119, 122],
                         6: [0, 119, 123],
                         7: [0, 119, 113],
                         8: [0, 119, 40],
                         9: [0, 101, 40],
                         10: [0, 117, 40],
                         11: [0, 117, 36],
                         12: [0, 120, 36],
                         13: [0, 121, 122],
                         14: [0, 121, 123],
                         15: [0, 121, 113],
                         16: [0, 121, 40],
                         17: [0, 121, 36],
                         18: [0, 118, 36]}

        # instantiate df
        pw_df = pd.DataFrame()

        #
        pw_nr_list = [nr for nr in list(pathways_dict.keys())]
        pw_id_list = ['SP' + str(nr) for nr in pw_nr_list]

        #
        pw_df['pathway_nr'] = pw_nr_list
        pw_df['pathway_id'] = pw_id_list
        pw_df['description'] = ['S'+str(pathways_dict[nr][0]) + '-' + 'S'+str(pathways_dict[nr][1]) + '-' + 'S'+str(pathways_dict[nr][2]) for nr in pw_nr_list]

        # Calculate adaptation of indicators for pathways
        for i in indicators:
            i_average_vals = []
            for pathway_i in pw_nr_list:
                #
                pathway = pathways_dict[pathway_i]
                #
                X = [2020, 2030, 2045]
                #
                if i != 'cost_EAC':
                    if i == 'cost_operating':
                        ii = 'cost_operating_discounted'
                        Y_path = [data[data['scenario'] == 'S' + str(pathway[0])][ii].tolist()[0],
                                  data[data['scenario'] == 'S' + str(pathway[1])][ii].tolist()[0],
                                  data[data['scenario'] == 'S' + str(pathway[2])][ii].tolist()[0]]
                    else:
                        Y_path = [data[data['scenario'] == 'S' + str(pathway[0])][i].tolist()[0],
                                  data[data['scenario'] == 'S' + str(pathway[1])][i].tolist()[0],
                                  data[data['scenario'] == 'S' + str(pathway[2])][i].tolist()[0]]
                    y_interp_path = interp1d(X, Y_path)
                    # calculation of average annual value in period 2020 to 2045
                    cumulative_path = 0
                    for year in range(2020, 2045 + 1):
                        cumulative_path += y_interp_path(year)
                    average_val = cumulative_path / (2045 - 2020)  # 25 years
                else:
                    Y_path = [data[data['scenario'] == 'S' + str(pathway[0])][i].tolist()[0],  # S0
                              data[data['scenario'] == 'S' + str(pathway[1])]['cost_EAC_temporary'].tolist()[0],  # S1
                              data[data['scenario'] == 'S' + str(pathway[2])][i].tolist()[0]]  # S2
                    new_eac_val = (Y_path[1]*10 + (Y_path[2]*25/1.41)) / 35
                    average_val = new_eac_val

                i_average_vals.append(round(average_val,2))

            # Add column with new vals
            pw_df[i] = i_average_vals


        # add variation columns to df
        pw_base_id = pw_id_list[0]
        for i in indicators:
            abs_vars = []  # variation with respect basis scenario in absolut value
            rel_vars = []  # variation with respect basis scenario in relative % value
            # Reference value from basis scenario
            pw_base_val = pw_df[pw_df['pathway_id'] == pw_base_id][i].tolist()[0]
            for pw in pw_id_list:
                if pw == pw_base_id:
                    abs_var = rel_var = 0
                else:
                    pw_val = pw_df[pw_df['pathway_id'] == pw][i].tolist()[0]
                    abs_var = pw_val - pw_base_val
                    if i in ['self_consumption', 'self_sufficiency', 'flexible_load_ratio',
                             'grid_interaction_index', 'high_load_rate']:
                        rel_var = abs_var
                    else:
                        rel_var = (pw_val / pw_base_val) - 1

                abs_vars.append(abs_var)
                rel_vars.append(rel_var)
            pw_df[i + '_abs_var'] = abs_vars
            pw_df[i + '_rel_var'] = rel_vars


        def run_rank(val_list):
            rank_list = []
            rank_v = 1
            for i, v in enumerate(val_list):
                if i == 0:
                    pass
                else:
                    if v == val_list[i-1]:
                        pass
                    else:
                        rank_v += 1
                rank_list.append(rank_v)
            return rank_list

        data = pw_df

        # Rank by indicator
        for i in indicators:
            df_aux = data.copy()
            # omit basis pathway PW0
            df_aux = df_aux[df_aux['pathway_id'] != pw_base_id]
            # sort
            ascending_order = False if i in ['self_consumption', 'self_sufficiency', 'flexible_load_ratio'] else True
            df_aux.sort_values(by=[i + '_rel_var'], inplace=True, ascending=ascending_order)
            i_vals = df_aux[i + '_rel_var'].tolist()
            # give rank
            i_rank_list = run_rank(i_vals)
            df_aux[i + '_rank'] = i_rank_list
            # recover original order
            df_aux.sort_values(by=['pathway_nr'], inplace=True)
            # save rank in original df
            #data[i + '_rank'] = df_aux[i + '_rank'].tolist()
            data[i + '_rank'] = [''] + df_aux[i + '_rank'].tolist()  # add an space to replace the omitted S0

        # Rank by categories
        categories = ['efficiency', 'flexibility', 'overall']
        for category in categories:
            df_aux = data.copy()
            # omit basis scenario S0
            df_aux = df_aux[df_aux['pathway_id'] != pw_base_id]
            if category == 'efficiency':
                weighting_factors = {'co2_emission': 0.25,
                                     'primary_energy_consum': 0.25,
                                     'cost_operating': 0.25,
                                     'cost_EAC': 0.25,
                                     'self_consumption': 0,
                                     'self_sufficiency': 0,
                                     'flexible_load_ratio': 0,
                                     'grid_interaction_index': 0,
                                     'high_load_rate': 0
                                     }
            elif category == 'flexibility':
                weighting_factors = {'co2_emission': 0,
                                     'primary_energy_consum': 0,
                                     'cost_operating': 0,
                                     'cost_EAC': 0,
                                     'self_consumption': 0.2,
                                     'self_sufficiency': 0.2,
                                     'flexible_load_ratio': 0.2,
                                     'grid_interaction_index': 0.2,
                                     'high_load_rate': 0.2
                                     }
            else:
                weighting_factors = {'co2_emission': 0.125,
                                     'primary_energy_consum': 0.125,
                                     'cost_operating': 0.125,
                                     'cost_EAC': 0.125,
                                     'self_consumption': 0.1,
                                     'self_sufficiency': 0.1,
                                     'flexible_load_ratio': 0.1,
                                     'grid_interaction_index': 0.1,
                                     'high_load_rate': 0.1
                                     }
            # # Prepare weighting
            # weighting_factors_sum = 0
            # for key in list(weighting_factors.keys()):
            #     weighting_factors_sum += weighting_factors[key]
            # # Resize the factors
            # for key in list(weighting_factors.keys()):
            #     weighting_factors[key] = weighting_factors[key] / weighting_factors_sum

            # calculate sums
            category_sum_vals = []
            for pw in df_aux['pathway_id'].tolist():  # omitting PW0
                pw_sum = 0
                for i in indicators:
                    val = df_aux[df_aux['pathway_id'] == pw][i + '_rank'].tolist()[0]
                    pw_sum += val * weighting_factors[i]
                category_sum_vals.append(pw_sum)
            # add sum col
            df_aux[category + '_sum'] = category_sum_vals
            # sort
            ascending_order = True
            df_aux.sort_values(by=[category + '_sum'], inplace=True, ascending=ascending_order)
            i_vals = df_aux[category + '_sum'].tolist()
            # give rank
            i_rank_list = run_rank(i_vals)
            df_aux[category + '_rank'] = i_rank_list
            # recover original order
            df_aux.sort_values(by=['pathway_nr'], inplace=True)
            # save rank in original df
            #data[category + '_sum'] = df_aux[category + '_sum'].tolist()
            #data[category + '_rank'] = df_aux[category + '_rank'].tolist()
            data[category + '_sum'] = [''] + df_aux[category + '_sum'].tolist()  # adding space to fill the omitted S0
            data[category + '_rank'] = [''] + df_aux[category + '_rank'].tolist() # adding space to fill the omitted S0

        # df sorted by pathway nr
        sorted_df = data.copy()
        sorted_df.sort_values(by=['pathway_nr'], inplace=True)

        # to make easier taking data for word
        sorted_df['CO2'] = [round(i,0) for i in sorted_df['co2_emission']]
        sorted_df['PED'] = [round(i,0) for i in sorted_df['primary_energy_consum']]
        sorted_df['OPC'] = [round(i,0) for i in sorted_df['cost_operating']]
        sorted_df['EAC'] = [round(i,0) for i in sorted_df['cost_EAC']]
        sorted_df['SCR'] = [round(i,2) for i in sorted_df['self_consumption']]
        sorted_df['SSR'] = [round(i,2) for i in sorted_df['self_sufficiency']]
        sorted_df['FLR'] = [round(i,2) for i in sorted_df['flexible_load_ratio']]
        sorted_df['GII'] = [round(i,2) for i in sorted_df['grid_interaction_index']]
        sorted_df['HLR'] = [round(i,2) for i in sorted_df['high_load_rate']]
        #
        sorted_df['CO2_var'] = [round(i*100,1) for i in sorted_df['co2_emission_rel_var']]
        sorted_df['PED_var'] = [round(i*100,1) for i in sorted_df['primary_energy_consum_rel_var']]
        sorted_df['OPC_var'] = [round(i*100,1) for i in sorted_df['cost_operating_rel_var']]
        sorted_df['EAC_var'] = [round(i*100,1) for i in sorted_df['cost_EAC_rel_var']]
        sorted_df['SCR_var'] = [round(i,2) for i in sorted_df['self_consumption_rel_var']]
        sorted_df['SSR_var'] = [round(i,2) for i in sorted_df['self_sufficiency_rel_var']]
        sorted_df['FLR_var'] = [round(i,2) for i in sorted_df['flexible_load_ratio_rel_var']]
        sorted_df['GII_var'] = [round(i,2) for i in sorted_df['grid_interaction_index_rel_var']]
        sorted_df['HLR_var'] = [round(i,2) for i in sorted_df['high_load_rate_rel_var']]

        # Define save paths
        save_dir_A = os.path.join(save_dir, 'A_pathways_progressive_NEW')
        isExist = os.path.exists(save_dir_A)
        if not isExist:
            os.makedirs(save_dir_A)

        save_path_csv = os.path.join(save_dir_A, 'PW_overview_table_ranked.csv')
        save_path_xlsx = os.path.join(save_dir_A, 'PW_overview_table_ranked.xlsx')

        # Saving the results to CSV and Excel files
        sorted_df.to_csv(save_path_csv, index=False)
        sorted_df.to_excel(save_path_xlsx, index=False)

        # --------------------------------------------------------------------------------------------------------------
        # Table for report
        pv_dict = {0: 'Kein',
                   1: 'MIN',
                   2: 'MAX'}

        export_dat1 = {('Sanierungspfad', 'Id', '', '', ''): sorted_df['pathway_id'].tolist(),
                       #('Szenario', 'Nr', '', '', ''): sorted_df['pathway_nr'].tolist(),
                       ('Sanierungspfad', 'Szenarien', '2020', '', ''): [str(pw_chain.split('-')[0]) for pw_chain in sorted_df['description'].tolist()],
                       ('Sanierungspfad', 'Szenarien', '2030', '', ''): [str(pw_chain.split('-')[1]) for pw_chain in sorted_df['description'].tolist()],
                       ('Sanierungspfad', 'Szenarien', '2045', '', ''): [str(pw_chain.split('-')[2]) for pw_chain in sorted_df['description'].tolist()],

                       ('Ergebnisswert nach Indikator', 'Effizienz', 'Unweltfaktoren', 'CO2', '[t]'): sorted_df['CO2'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Effizienz', 'Unweltfaktoren', 'PED', '[MWh]'): sorted_df['PED'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Effizienz', 'Wirtschaftlichkeit', 'OPC', '[k€]'): sorted_df['OPC'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Effizienz', 'Wirtschaftlichkeit', 'EAC', '[k€]'): sorted_df['EAC'].tolist(),

                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Quartiersintern', 'SCR', '[%]'): sorted_df['SCR'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Quartiersintern', 'SSR', '[%]'): sorted_df['SSR'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Verteilnetz', 'FLR', '[%]'): sorted_df['FLR'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Verteilnetz', 'GII', '[%]'): sorted_df['GII'].tolist(),
                       ('Ergebnisswert nach Indikator', 'Flexibilitaet', 'Verteilnetz', 'HLR', '[%]'): sorted_df['HLR'].tolist(),


                       ('Variation gegenüber Basisszenario S0', 'Effizienz', 'Unweltfaktoren', 'CO2', '[%]'): sorted_df['CO2_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Effizienz', 'Unweltfaktoren', 'PED', '[%]'): sorted_df['PED_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Effizienz', 'Wirtschaftlichkeit', 'OPC', '[%]'): sorted_df['OPC_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Effizienz', 'Wirtschaftlichkeit', 'EAC', '[%]'): sorted_df['EAC_var'].tolist(),

                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Quartiersintern', 'SCR', '[%]'): sorted_df['SCR_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Quartiersintern', 'SSR', '[%]'): sorted_df['SSR_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Verteilnetz', 'FLR', '[%]'): sorted_df['FLR_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Verteilnetz', 'GII', '[%]'): sorted_df['GII_var'].tolist(),
                       ('Variation gegenüber Basisszenario S0', 'Flexibilitaet', 'Verteilnetz', 'HLR', '[%]'): sorted_df['HLR_var'].tolist(),

                       ('Ranking nach KPI', 'Effizienz', 'Unweltfaktoren', 'CO2', 'Rang'): sorted_df['co2_emission_rank'].tolist(),
                       ('Ranking nach KPI', 'Effizienz', 'Unweltfaktoren', 'PED', 'Rang'): sorted_df['primary_energy_consum_rank'].tolist(),
                       ('Ranking nach KPI', 'Effizienz', 'Wirtschaftlichkeit', 'OPC', 'Rang'): sorted_df['cost_operating_rank'].tolist(),
                       ('Ranking nach KPI', 'Effizienz', 'Wirtschaftlichkeit', 'EAC', 'Rang'): sorted_df['cost_EAC_rank'].tolist(),

                       ('Ranking nach KPI', 'Flexibilitaet', 'Quartiersintern', 'SCR', 'Rang'): sorted_df['self_consumption_rank'].tolist(),
                       ('Ranking nach KPI', 'Flexibilitaet', 'Quartiersintern', 'SSR', 'Rang'): sorted_df['self_sufficiency_rank'].tolist(),
                       ('Ranking nach KPI', 'Flexibilitaet', 'Verteilnetz', 'FLR', 'Rang'): sorted_df['flexible_load_ratio_rank'].tolist(),
                       ('Ranking nach KPI', 'Flexibilitaet', 'Verteilnetz', 'GII', 'Rang'): sorted_df['grid_interaction_index_rank'].tolist(),
                       ('Ranking nach KPI', 'Flexibilitaet', 'Verteilnetz', 'HLR', 'Rang'): sorted_df['high_load_rate_rank'].tolist(),

                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Effizienz', 'Summe', '', ''): sorted_df['efficiency_sum'].tolist(),
                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Effizienz', 'Rang', '', ''): sorted_df['efficiency_rank'].tolist(),

                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Flexibilitaet', 'Summe', '', ''): sorted_df['flexibility_sum'].tolist(),
                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Flexibilitaet', 'Rang', '',''): sorted_df['flexibility_rank'].tolist(),

                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Global', 'Summe', '', ''): sorted_df['overall_sum'].tolist(),
                       ('Ranking nach gewichteter Summe von KPI-Rängen', 'Global', 'Rang', '', ''): sorted_df['overall_rank'].tolist()
                       }

        # create df
        report_table_df = pd.DataFrame(export_dat1)
        # create a MultiIndex from the column headers
        new_columns = pd.MultiIndex.from_tuples(report_table_df.columns)
        # set the new column headers
        report_table_df.columns = new_columns

        # Define save paths
        save_path_csv = os.path.join(save_dir_A, 'PW_overview_table_report_TypeA.csv')
        save_path_xlsx = os.path.join(save_dir_A, 'PW_overview_table_report_TypeA.xlsx')

        # Saving the results to CSV and Excel files
        report_table_df.to_csv(save_path_csv, index=False)
        report_table_df.to_excel(save_path_xlsx)

        # --------------------------------------------------------------------------------------------------------------
        add_line = ['% Variation gegenüber Basissanierungspfad SP0']
        export_dat2 = {('Sanierungspfad', 'Id', '', ''): [sorted_df['pathway_id'].tolist()[0]] + add_line + sorted_df['pathway_id'].tolist()[1:],
                       #('Szenario', 'Nr', '', '', ''): sorted_df['pathway_nr'].tolist(),
                       ('Sanierungspfad', 'Szenarien', '2020', '', ''): [[str(pw_chain.split('-')[0]) for pw_chain in sorted_df['description'].tolist()][0]] + [''] + [str(pw_chain.split('-')[0]) for pw_chain in sorted_df['description'].tolist()][1:],
                       ('Sanierungspfad', 'Szenarien', '2030', '', ''): [[str(pw_chain.split('-')[1]) for pw_chain in sorted_df['description'].tolist()][0]] + [''] + [str(pw_chain.split('-')[1]) for pw_chain in sorted_df['description'].tolist()][1:],
                       ('Sanierungspfad', 'Szenarien', '2045', '', ''): [[str(pw_chain.split('-')[2]) for pw_chain in sorted_df['description'].tolist()][0]] + [''] + [str(pw_chain.split('-')[2]) for pw_chain in sorted_df['description'].tolist()][1:],


                       ('Effizienz', 'Unweltfaktoren', 'CO2', '[t]'): [sorted_df['CO2'].tolist()[0]] + [''] + sorted_df['CO2_var'].tolist()[1:],
                       ('Effizienz', 'Unweltfaktoren', 'PED', '[MWh]'): [sorted_df['PED'].tolist()[0]] + [''] + sorted_df['PED_var'].tolist()[1:],
                       ('Effizienz', 'Wirtschaftlichkeit', 'OPC', '[k€]'): [sorted_df['OPC'].tolist()[0]] + [''] + sorted_df['OPC_var'].tolist()[1:],
                       ('Effizienz', 'Wirtschaftlichkeit', 'EAC', '[k€]'): [sorted_df['EAC'].tolist()[0]] + [''] + sorted_df['EAC_var'].tolist()[1:],

                       ('Flexibilitaet', 'Quartiersintern', 'SCR', '[%]'): [sorted_df['SCR'].tolist()[0]] + [''] + sorted_df['SCR_var'].tolist()[1:],
                       ('Flexibilitaet', 'Quartiersintern', 'SSR', '[%]'): [sorted_df['SSR'].tolist()[0]] + [''] + sorted_df['SSR_var'].tolist()[1:],
                       ('Flexibilitaet', 'Verteilnetz', 'FLR', '[%]'): [sorted_df['FLR'].tolist()[0]] + [''] + sorted_df['FLR_var'].tolist()[1:],
                       ('Flexibilitaet', 'Verteilnetz', 'GII', '[%]'): [sorted_df['GII'].tolist()[0]] + [''] + sorted_df['GII_var'].tolist()[1:],
                       ('Flexibilitaet', 'Verteilnetz', 'HLR', '[%]'): [sorted_df['HLR'].tolist()[0]] + [''] + sorted_df['HLR_var'].tolist()[1:],

                       ('Ranking', 'Effizienz', '', ''): [sorted_df['efficiency_rank'].tolist()[0]] + [''] + sorted_df['efficiency_rank'].tolist()[1:],
                       ('Ranking', 'Flexibilitaet', '', ''): [sorted_df['flexibility_rank'].tolist()[0]] + [''] + sorted_df['flexibility_rank'].tolist()[1:],
                       ('Ranking', 'Global', '', ''): [sorted_df['overall_rank'].tolist()[0]] + [''] + sorted_df['overall_rank'].tolist()[1:]}

        # create df
        report_table_df = pd.DataFrame(export_dat2)
        # create a MultiIndex from the column headers
        new_columns = pd.MultiIndex.from_tuples(report_table_df.columns)
        # set the new column headers
        report_table_df.columns = new_columns

        # Define save paths
        save_path_csv = os.path.join(save_dir_A, 'PW_overview_table_report_TypeB.csv')
        save_path_xlsx = os.path.join(save_dir_A, 'PW_overview_table_report_TypeB.xlsx')

        # Saving the results to CSV and Excel files
        report_table_df.to_csv(save_path_csv, index=False)
        report_table_df.to_excel(save_path_xlsx)

