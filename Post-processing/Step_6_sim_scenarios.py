from ruamel.yaml import YAML
import os
import shutil
import pandas as pd

os.chdir('../')
# this script run cleanmod iteratively with varying parameters
if __name__ == '__main__':
    cleanmod = __import__('1_runme_cleanmod')

    # define the scenario name
    name_scenario = 'emi_test'

    # read the results
    with open(f"0_config_cleanmod.yaml") as config_file:
        config = YAML().load(config_file)
    name_model = config['project_settings']['name_model']
    calc_type = config['simulation_settings']['calc_type']  # 'Temp', 'peak_load', 'elec_bought', 'Qhc'
    mydir = os.path.join(os.getcwd(), 'Models/{0}/Results'.format(name_model))
    res_dir = os.path.join(os.getcwd(), 'Models/{0}/Results'.format(name_model),
                           name_scenario)
    if os.path.isdir(res_dir):
        shutil.rmtree(res_dir)
    os.mkdir(res_dir)

    # loop for different co2 cap
    for cap in range(80, 200, 10):
        # change the parameters
        with open(f"0_config_cleanmod.yaml") as config_file:
            config = YAML().load(config_file)
        config["simulation_settings"]["emission_cap"] = cap
        with open(f"0_config_cleanmod.yaml", 'w+') as file:
            YAML().dump(config, file)
        cleanmod.run_cleanmod(name_scenario)
