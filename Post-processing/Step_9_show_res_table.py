import os
import math
import time
import pandas as pd
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
# import config_cleanmod as config
import Cleanvelope.CleanMod.Scripts.scenario_plotter as scenario_plot
from datetime import datetime
from ruamel.yaml import YAML



def split_val(input_value, option='both'):
    # scale down by 1000 to change unit from W to kW
    value_pos = np.array([val / 1000 if val >= 0 else 0 for val in input_value])
    value_neg = np.array([val / 1000 if val < 0 else 0 for val in input_value])
    if option == 'both':
        _res = (value_pos, value_neg)
    elif option == 'pos':
        _res = value_pos
    else:
        _res = value_neg
    return _res


def join(iterator, seperator):
    it = map(str, iterator)
    seperator = str(seperator)
    string = next(it, '')
    for s in it:
        string += seperator + s
    return string


name_model = 'suburban'
# folder in which all results are saved
folder_name = 'Output'  # please give the folder name if "check_latest_file" is False
# please give season or the start_day and duration, only when season is "na", the start_day is valid
# season: show results of a certain season
# start_day: choose the start_day (0-8760) to show the results
season = 'na'  # [winter, transition, summer, na], choice of season only applicable for 1-year-simulation
start_day = 0
duration = 365  # in days
# community show the electrical and thermal balance of the whole district
# numbers give the heat balance of the 1st or 2nd zone (now only save data of the first two zones)
show_object = 'community'  # 'community or number in integer such as 1, 2, 3 representing the number of buildings

mydir = os.path.join(os.getcwd(), '../Models/{0}'.format(name_model))
res_dir = os.path.join(mydir, folder_name)
res_files = os.listdir(res_dir)
res_array_list = []
scenario_list = []

for res_file in res_files:
    print(f'analyze {res_file}')
    scenario_list.append(res_file)
    res_file_dir = os.path.join(res_dir, f'{res_file}/')
    # search calc_type
    res_type = os.listdir(res_file_dir)
    calc_type = res_type[0]
    # read results
    res_path = os.path.join(res_file_dir, f'{calc_type}/mainrun/district_timeseries.csv')
    res = pd.read_csv(res_path, index_col=0)
    ######################################################
    # electricity imported from or fed into external power grid
    p_import, p_export = split_val(res['import_elec'])
    temp = res["temp_zone_1"]
    p_district_pos = split_val(res['import_th'], 'pos')
    p_pv_pos = split_val(res['gen_pv'], 'pos')
    # show the results of indicators
    obj_cost = (res['obj_cost']).sum()  # the total energy costs, unit in Euro
    obj_co2 = (res['obj_co2']).sum() / 1000  # the total CO2 emission, unit in kg
    # the average value of maximum power flow from external grid of each time step, unit in kW
    obj_peak = np.max(res['obj_peak'] / 1000)  # kW
    temp_avg = temp.mean()
    p_hp_neg = split_val(-res['pow_elec_hp'], 'neg')
    p_sto_pos, p_sto_neg = split_val(res['pow_elec_bat'])  # storage system for electricity/ battery
    p_eh_neg = split_val(-res['pow_elec_eh'], 'neg')  # consumption of electrical heater
    p_chp_elec_pos = split_val(res['pow_elec_chp'], 'pos')
    p_ev_neg = np.array(res['consum_ev_charge'] / 1000)
    if math.isnan(p_ev_neg[0]):
        p_ev_neg = [0] * len(p_chp_elec_pos)
    p_demand_fix_neg = split_val(-res['consum_elec'], 'neg')
    gen_sum = p_chp_elec_pos + p_pv_pos
    load_sum = - p_demand_fix_neg - p_sto_neg - p_ev_neg - p_hp_neg - p_eh_neg
    min_gen_load = np.array([min(gen_sum[i], load_sum[i]) for i, value in enumerate(gen_sum)])

    # calculation of load cover factor / self-sufficiency rate
    f_ss = min_gen_load.sum() / load_sum.sum()
    p_ss = max((f_ss - 0.15)/0.35*100, 0)  # 15%-50%
    # calculate supply cover factor / self-consumption rate
    f_sc = min_gen_load.sum() / gen_sum.sum()  # 50% - 100%
    p_sc = max((f_sc - 0.5) / 0.5 * 100, 0)
    # calculate loss of load probability (LOLP): the fraction of
    # time on-site generation is insufficient and energy must be imported from the grid
    count_import = np.array([1 if value > 0 else 0 for value in p_import]).sum()
    count_export = np.array([1 if value <= 0 else 0 for value in p_export]).sum()
    f_lolp = count_import / (count_export + count_import)  # 30% - 50%
    p_lolp = max((0.5-f_lolp) / 0.2 * 100, 0)
    # calculate grid interaction index:
    # the variability of the energy flow with the grid over the net-zero balancing period, normalized
    # with the maximum net-flow of electricity
    # max_power_grid = max(p_import[50:].max(), -p_export[50:].min())
    import_kw = res['import_elec'] / 1000
    # import_avg = import_kw.mean()
    f_gii = np.std(import_kw / 600)  # 0% - 40%
    p_gii = max((0.4 - f_gii) / 0.4 * 100, 0)
    # print(f_gii)
    # calculate the flexible load ratio
    pow_flex = - p_sto_neg - p_ev_neg - p_hp_neg - p_eh_neg
    f_flr = pow_flex.sum()/load_sum.sum()  # 50% - 80%
    p_flr = max((f_flr-0.5) / 0.3 * 100, 0)
    # print(f_flr)
    # calculate high loading ratio
    threshold = 600 * 0.6  # 60% of maximum load/transformer capacity
    count_high_load = np.array([1 if abs(value) >= threshold else 0 for value in p_import]).sum()
    count_low_load = np.array([1 if abs(value) < threshold else 0 for value in p_import]).sum()
    f_hlr = count_high_load / (count_high_load + count_low_load)
    p_hlr = max((0.1-f_hlr) / 0.1 * 100, 0)  # 0% - 10%
    print(p_hlr)
    # summarize all the indicators -> "Netzdienlichkeit"
    f_ndl = (f_ss + f_sc + f_flr + (1-f_gii) + (1-f_hlr))/5
    p_ndl_local = (p_ss + p_sc)/2
    p_ndl_ext = (p_flr + p_gii + p_hlr)/3
    # print(f_ndl)
    # f_ndl = (f_ss + f_sc) / 2
    p_demand_sh = split_val(res['consum_th'], 'pos').sum()
    p_demand_dhw = split_val(res['consum_dhw'], 'pos').sum()
    elec_consum_grid = p_import.sum()
    elec_consum_grid_ev = -np.array(res['consum_ev_charge'] / 1000).sum()
    elec_consum_grid_building = elec_consum_grid - elec_consum_grid_ev
    elec_consum_grid_total = elec_consum_grid
    elec_consum_pv = (p_pv_pos + p_export).clip(min=0).sum()
    gas_consum = split_val(res['pow_gas_gb'], 'pos').sum()
    primary_energy_consum = 1.8 * elec_consum_grid + 1.1 * gas_consum
    self_sufficiency = f_ss
    self_consumption = f_sc
    loss_of_load_probability = f_lolp
    grid_interaction_index = f_gii
    index_netzdienlichkeit_local = p_ndl_local
    index_netzdienlichkeit_external = p_ndl_ext
    cost_ts = res['obj_cost']
    # cost_share_ev_ts = -np.array(res['consum_ev_charge'] / 1000)
    # cost_share_bulding_ts = (p_import - cost_share_ev_ts).clip(0, 10e9)
    cost_share_ev_ts = sum(-np.array(res['consum_ev_charge'] / 1000))
    cost_share_bulding_ts = sum(p_import) - cost_share_ev_ts
    # for value in cost_ts:
    #     cost_ts_ev =
    #     cost_ts_building =
    # cost_operating_ev = cost_ts * cost_share_ev_ts / (cost_share_ev_ts + cost_share_bulding_ts + 0.1)
    # cost_operating_building = cost_ts * cost_share_bulding_ts / (cost_share_ev_ts + cost_share_bulding_ts + 0.1)
    # cost_operating_ev = cost_operating_ev.sum()
    # cost_operating_building = cost_operating_building.sum()
    cost_operating_per_year = obj_cost  # euro
    cost_operating_ev = obj_cost * cost_share_ev_ts / (cost_share_ev_ts + cost_share_bulding_ts)
    cost_operating_building = obj_cost * cost_share_bulding_ts / (cost_share_ev_ts + cost_share_bulding_ts)
    cost_renovation_construct = res['renovation_cost_construction'][0]
    cost_renovation_hp = res['renovation_cost_hp'][0]
    cost_renovation_pv = res['renovation_cost_pv'][0]
    cost_renovation = res['renovation_cost_total'][0]  # euro
    # calculation of annual factor: construction for 35 years, pv and hp for 20 years, discount rate 3.5%
    annu_factor_construction = (1-(1+0.035)**(-35))/0.035
    annu_factor_devices = (1 - (1 + 0.035) ** (-20)) / 0.035
    cost_invest_per_year = cost_renovation_construct/annu_factor_construction + \
                  (cost_renovation_hp+cost_renovation_pv)/annu_factor_devices

    # equivalent annual cost
    cost_eac = cost_operating_per_year + cost_invest_per_year

    co2_emission = obj_co2  # kg

    res_list = [p_demand_sh, p_demand_dhw, elec_consum_grid_building,
                elec_consum_grid_ev, elec_consum_grid_total,
                elec_consum_pv, gas_consum, primary_energy_consum,
                index_netzdienlichkeit_local, index_netzdienlichkeit_external,
                cost_operating_ev, cost_operating_building, cost_operating_per_year,
                cost_renovation, cost_eac, co2_emission]

    list_index = ['heat_demand_sh',
                  'heat_demand_dhw',
                  'elec_consum_grid_building',
                  'elec_consum_grid_ev',
                  'elec_consum_grid_total',
                  'elec_consum_pv',
                  'gas_consum',
                  'primary_energy_consum',
                  'grid_index_local',
                  'grid_index_external',
                  'cost_operating_ev',
                  'cost_operating_building',
                  'cost_operating',
                  'cost_renovation',
                  'cost_EAC',
                  'co2_emission']

    res_array = pd.Series(res_list, index=list_index)
    res_array_list.append(res_array)

transpose = True  # decide whether scenario names or result types as columns
res_df = pd.DataFrame(res_array_list, index=[scenario_list])
res_df_transposed = res_df.T
res_df_transposed.to_csv('Results/overview_table.csv')
res_df_transposed.to_excel('Results/overview_table.xlsx')

# Define save paths
save_path_csv = os.path.join(res_dir, 'overview_table.csv')
save_path_xlsx = os.path.join(res_dir, 'overview_table.xlsx')

# Saving the results to CSV and Excel files
res_df_transposed.to_csv(save_path_csv)
res_df_transposed.to_excel(save_path_xlsx)