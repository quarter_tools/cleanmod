import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from adjustText import adjust_text

def mark_pathways(ax, data, x_col, y_col, pw):
    """
    Encircle specified scatter plot dots using a larger scatter circle
    """
    x_vals = []
    y_vals = []

    scenarios = pw.split('-')
    for scenario in scenarios:
        if scenario not in data['scenario'].values:
            print(f"Scenario '{scenario}' not present in the data!")
            continue  # Skip the current iteration

        idx = data['scenario'].tolist().index(scenario)
        x_val, y_val = data.iloc[idx][x_col], data.iloc[idx][y_col]
        x_vals.append(x_val)
        y_vals.append(y_val)

    # plot line
    ax.plot(x_vals, y_vals, linestyle='dashed', linewidth=1.5, label=pw, marker='o', markersize=5)

def encircle_scatter_points(ax, data, x_col, y_col, scenarios_to_encircle):
    """
    Encircle specified scatter plot dots using a larger scatter circle
    """
    for scenario in scenarios_to_encircle:
        if scenario not in data['scenario'].values:
            print(f"Scenario '{scenario}' not present in the data!")
            continue  # Skip the current iteration

        idx = data['scenario'].tolist().index(scenario)
        x_val, y_val = data.iloc[idx][x_col], data.iloc[idx][y_col]

        # Draw a larger scatter circle around the point
        ax.scatter(x_val, y_val, s=400, facecolors='none', edgecolors='purple', linewidths=1.5)


def plot_scatter(x, y, x_label, y_label, title, data, original_data, name_model, ax=None, xlim=None, ylim=None,
                 encircle=False, scenarios_to_encircle=[], print_pathway=False, pathways_to_print=[]):
    if ax is None:
        ax = plt.gca()

    xaxis = data[x].tolist()
    yaxis = data[y].tolist()
    scenarios = data['scenario'].tolist()
    maxPV_values = data['maxPV'].tolist()
    calc_types = data['calc_type'].tolist()
    tariffs = data['tariff'].tolist()
    years = [str(i) for i in data['year'].tolist()]
    RenVar = data['RenVar']

    # Define barrier-free colors for each calc_type
    calc_type_colors = {
        'RB': '#377EB8',
        'emi': '#E41A1C',
        'cost': '#4DAF4A',
        'peak_load': '#FF7F00'
    }

    # Define barrier-free colors for each year
    year_type_colors = {
        '2020': '#FF7F00',
        '2030': '#377EB8',
        '2045': '#4DAF4A'
    }

    texts = []
    #marker_shapes = {"001": 'o', "002": 's', "003": '^'}
    marker_shapes = {"100-0-0": 'o',
                     "67-33-0": '<',
                     "67-0-33": '^',
                     "33-67-0": 'v',
                     "33-0-67": '>',
                     "0-100-0": 's',
                     "0-33-67": 'D',
                     "17-0-83": 'd',
                     "0-17-83": 'p',
                     "0-0-100": '*',
                     }

    for i, Var in enumerate(RenVar):
        #if 'RenVar' in Var:
        #    marker = marker_shapes.get(Var[-3:], 'x')
        #else:
        #    marker = '*'
        marker = marker_shapes.get(Var, 'x')

        #color = calc_type_colors.get(calc_types[i], 'black')  # Default to black if no match
        color = year_type_colors.get(years[i], 'black')  # Default to black if no match
        edgecolor = 'black'
        font_weight = 'bold' if maxPV_values[i] != 0 else 'normal'
        alpha_val = 0.6 if maxPV_values[i] != 0 else 0.3

        name = scenarios[i]
        if calc_types[i] != 'RB' and tariffs[i] == 'Constant':
            name += '~'
        elif calc_types[i] != 'RB' and tariffs[i] == 'Dynamic':
            name += '*'

        ax.scatter(xaxis[i], yaxis[i], color=color, marker=marker, s=150, edgecolors=edgecolor, alpha=alpha_val)
        #texts.append(ax.text(xaxis[i], yaxis[i], scenarios[i], ha='center', va='center', fontsize=8, color=color, weight=font_weight, alpha=1))
        texts.append(ax.text(xaxis[i], yaxis[i], name, ha='center', va='center', fontsize=15, color=color,
                             weight=font_weight, alpha=1))
    adjust_text(texts, expand_points=(1.2, 1.2), expand_text=(1.1, 1.1))

    ax.set_xlabel(x_label, fontsize=20)
    ax.set_title(title, fontsize=20)
    if xlim:
        ax.set_xlim(xlim[0], xlim[1])
    if ylim:
        ax.set_ylim(ylim[0], ylim[1])
    ax.xaxis.set_major_formatter(FuncFormatter(lambda x, _: f'{x:.0f}'))
    ax.yaxis.set_major_formatter(FuncFormatter(lambda x, _: f'{x:.2f}'))
    if ax == ax.figure.axes[0]:
        ax.set_ylabel(y_label, fontsize=20)


    # Legend for RenVar categories
    renvar_legend_handles = [plt.Line2D([], [], color='black', marker=marker_shapes[key], linestyle='', markersize=10,
                                        markerfacecolor='white', markeredgecolor='black') for key in marker_shapes]
    renvar_legend_labels = [key for key in marker_shapes]
    renvar_legend = ax.legend(renvar_legend_handles, renvar_legend_labels, title='Building proportions\nnoREN-KfW70-KfW40', loc='center left',
                              bbox_to_anchor=(1.1, 0.75), ncol=1, fontsize=12, borderaxespad=2)
    ax.add_artist(renvar_legend)

    #Additional legend
    add_legend_handles = [plt.Line2D([], [], color='black', marker='o', linestyle='', markersize=1) for i in range(6)]
    add_content = ['no  PV: S0, S115, S116',
                   'min PV: color lighter',
                   'max PV: color darker',
                   'no EMS: name',
                   'EMS + fix €: name ~',
                   'EMS + var €: name *']
    add_legend = ax.legend(add_legend_handles, add_content, title='PV and EMS', loc='center left',
                           bbox_to_anchor=(1.1, 0.50), ncol=1, fontsize=12, borderaxespad=2)
    ax.add_artist(add_legend)

    # Legend for calc_types
    #legend_handles = [plt.Line2D([], [], color=color, marker='s', linestyle='', markersize=10) for color in
    #                  calc_type_colors.values()]
    #ax.legend(legend_handles, list(calc_type_colors.keys()), title='Calc Types', loc='center left',
    #          bbox_to_anchor=(1.05, 0.7), ncol=2, fontsize=10)

    # Legend for years
    legend_handles = [plt.Line2D([], [], color=color, marker='s', linestyle='', markersize=10) for color in
                      year_type_colors.values()]
    ax.legend(legend_handles, list(year_type_colors.keys()), title='Year', loc='center left',
              bbox_to_anchor=(1.1, 0.95), ncol=1, fontsize=12, borderaxespad=2)

    ax.set_title(f"{name_model}\n{title}", fontsize=20)

    # Scenario names with MaxPV are bold, so mention this in the legend
    #ax.text(0.05, -0.2, "Scenario names in bold have MaxPV.", transform=ax.transAxes, fontsize=8)

    #scenarios_to_encircle = ['S0', 'S1', 'S5', 'S8', 'S12', 'S15', 'S19', 'S22', 'S26', 'S29', 'S33', 'S36', 'S40']
    if encircle:
        #scenarios_to_encircle = ['S0', 'S115', 'S116']
        encircle_scatter_points(ax, original_data, x, y, scenarios_to_encircle)

    if print_pathway:
        # pathways_to_print = ['S0-S115-S116',
        #                      'S0-S119-S113',
        #                      'S0-S121-S113',
        #                      'S0-S121-S40']
        for pw in pathways_to_print:
            mark_pathways(ax, data, x, y, pw)

    return ax
