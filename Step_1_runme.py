"""
Use this script to run a simulation, based on the settings defined in the file config_cleanmod.py.
For this, the script calls all necessary functions, the majority of which are located in other .py files.
"""
import sys
import os
import logging
from datetime import datetime
import pandas as pd
from ruamel.yaml import YAML
import numpy as np
from subprocess import call
import Cleanvelope.CleanMod.Scripts.util as util
import Cleanvelope.CleanMod.Scripts.time as time
import Cleanvelope.CleanMod.Scripts.district as district
import Cleanvelope.CleanMod.Scripts.input_preparation as input_preparation
# import Cleanvelope.CleanMod.Scripts.non_residential_load as non_residential
from Cleanvelope.CleanMod.Scripts.non_residential_load_Simplified import NonResidentialLoad
import Cleanvelope.CleanMod.Scripts.scenario_generator as scenario_generator
from Cleanvelope.CleanMod.Scripts.progress_bar import app
import shutil
import pandas as pd
import Cleanvelope.CleanMod.Scripts.weather as weather


def run_cleanmod(scenarios=None, folder_name=None, mydistrict=None):
    """This module executes the core CleanMod scripts based on the user configurations that include district performance
     optimization objectives, indoor comfort targets, and other settings. The module retrieves the user
     configurations, executing the processing of inputs for district simulation, and identifying the simulation
     scenarios. Furthermore, updates the user about the process step by step.
    """
    # TODO - Priority 2 (deadline: July 2023) - Responsible "Ahmad" -  comment by = - MB 06.04.2023: add explanation
    Prallel_Coordinate_Plot = "Post-processing/Step_2_parallel_coordinates.py"

    # import config data
    # ------------------------------------------------------------------------------------------------------------------
    with open(f"Step_0_config.yaml") as config_file:
        config = YAML().load(config_file)
    name_model = config['project_settings']['name_model']
    # scenarios = config['project_settings']['scenarios']   # MB 230616 - adaptation to achieve parallelisation
    complete_building_csv_files = config['project_settings']['complete_building_csv_files']
    exit_sim_after_scenario_processing = config['project_settings']['exit_sim_after_scenario_processing']
    allow_sizing_run = config['project_settings']['allow_sizing_run']
    precalc_days = config['simulation_settings']['precalc_days']
    # calculation_types = config['simulation_settings']['calc_type']   # MB 230616 - adaptation to achieve parallelisation
    time_resolution_h = config['simulation_settings']['time_resolution_h']
    temp_state_names = config['RC_settings']['temp_state_names']
    show_progress = config['progress_bar_setting']['show_progress']
    reduce_to_1_building_with_2_zones = config['project_settings']['reduce_to_1_building_with_2_zones']
    elec_config = config['simulation_settings']['elec_config']
    # set paths of basis data
    # ------------------------------------------------------------------------------------------------------------------
    folder_model = 'Models/{0}'.format(name_model)
    folder_buildings_scenarios = folder_model + '/Input/Buildings_scenarios'
    file_scenarios = folder_model + '/Input/Scenarios.xlsx'
    folder_GH_output = folder_model + '/Input/GH_output'
    file_geometry_zones = folder_model + '/Input/GH_output/Geometry_Zones.csv'
    file_building_variant_compositions = 'Libraries/Building_variant_compositions.csv'

    print('\nRUNME STARTED')
    print('######################################################################')

    # Real time at which the script is run
    now = datetime.now().strftime('Results_%y%m%d_%H%M%S')

    # create log folder and file if doesn't exist
    # ------------------------------------------------------------------------------------------------------------------
    if not os.path.exists('Logs'):
        os.makedirs('Logs')
    # specify logging level and filename of logfile

    log_filename = 'Logs\%s_runme_cleanmod.log' % now
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s.%(msecs)03d %(levelname)s {%(module)s} [%(funcName)s] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    print('\nLog file created: ' + log_filename)
    logging.debug('RUNME STARTED')
    logging.debug('Log file created')

    # create the output folder for the model's processed input data (input_created) and simulation results
    # ------------------------------------------------------------------------------------------------------------------
    if folder_name:
        mydir = os.path.join(os.getcwd(), 'Models/{0}/Output'.format(name_model), folder_name)
    else:
        mydir = os.path.join(os.getcwd(), 'Models/{0}/Output'.format(name_model), now)

    if os.path.isdir(mydir):
        pass
    else:
        try:
            os.makedirs(mydir)
        except:
            pass
    print('\nResults folder created: ' + mydir)

    # preprocess scenarios using the scenario generator
    # ------------------------------------------------------------------------------------------------------------------
    logging.debug('PREPROCESSING OF SCENARIOS STARTED')
    print('\nPREPROCESSING OF SCENARIOS STARTED')
    print('======================================================================')

    # sort the scenarios in the right order to allow cumulative scenarios and open Scenarios.xlsx as df
    scenarios_df, scenario_chain_dict = scenario_generator.scenario_preprocessing(scenarios, file_scenarios)

    # create an object per scenario and export the corresponding buildings_csv files
    scenario_objects = {}
    for scenario_id in scenarios:
        logging.debug('Scenario: ' + scenario_id)
        print('\nScenario: ' + scenario_id)
        print('----------------------------------------------------------------------')
        scenario_chain = scenario_chain_dict[scenario_id]
        if len(scenario_chain) > 1:
            print('\nScenario chain associated with focus scenario ' + scenario_id + ' :', scenario_chain)
            print('\nAll scenarios in the chain will be processed, '
                  'although only a building csv will be exported for the focus scenario ' + scenario_id)
        for chain_scenario_id in scenario_chain:
            if len(scenario_chain) > 1:
                logging.debug('Chain scenario: ' + scenario_id)
                print('\n\tChain scenario: ' + scenario_id)
                print('\t----------------------------------------------------------------------')
            export_csv = True if chain_scenario_id == scenario_id else False
            # Initialize the scenario object for the scenario_id
            scenario_object = scenario_generator.Scenario(chain_scenario_id)
            # Configure the scenario object with the specifications from the file Scenarios.xlsx
            scenario_object.configure_scenario(scenarios_df, folder_buildings_scenarios, folder_GH_output,
                                               file_geometry_zones, file_building_variant_compositions,
                                               scenario_objects, complete_building_csv_files, export_csv)
            # Add the configured object to the dictionary (to be able to process scenarios with parent scenarios)
            scenario_objects[chain_scenario_id] = scenario_object

    if exit_sim_after_scenario_processing:
        print('\nThe simulation process has been exited to allow the user to manually adapt the building_csv files.' +
              '\nAfter making the changes:\n 1) Set exit_sim_after_scenario_processing to False in config_cleanmod' +
              ' and \n 2) Execute runme_cleanmod again')
        sys.exit()

    print('\n======================================================================')
    print('PREPROCESSING OF SCENARIOS FINISHED')
    logging.debug('PREPROCESSING OF SCENARIOS FINISHED')

    # simulation of scenarios
    # ------------------------------------------------------------------------------------------------------------------
    logging.debug('SIMULATION OF SCENARIOS STARTED')
    print('\nSIMULATION OF SCENARIOS STARTED')
    print('======================================================================')

    for scenario_id_i, scenario_id in enumerate(scenarios):

        scenario_object = scenario_objects[scenario_id]

        mydistrict = None  # Clear mydistrict to speed up simulations of more than one scenario
        mydistrict_exists = False  # To by-pass an existing district to speed up simulations with several calc_types

        calculation_types = [scenario_object.calc_type]  # MB 230616 - adaptation to achieve parallelisation
        for calc_type_i, calc_type in enumerate(calculation_types):

            str_scenario_calculation = '[Scenario: ' + scenario_id + ' | ' + 'Calculation type: ' + calc_type + ']'

            logging.debug(str_scenario_calculation)
            print('\n' + str_scenario_calculation)
            print('----------------------------------------------------------------------')

            mytime = time.Time(config=config, precalc=precalc_days)

            # processing of input data (processed data saved in folder Input_created)
            # ----------------------------------------------------------------------------------------------------------
            logging.debug('INPUT PREPARATION STARTED')
            print('\nINPUT PREPARATION STARTED')
            print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')

            # TODO - Priority 2 (deadline: 07.2023) - Responsible "AN" - comment by = - MB 06.04.2023: add a comment explaining this:
            y = (scenario_id_i + calc_type_i + 1) * 100 / (len(scenarios) + len(calculation_types))
            j = y - 15

            # TODO -  Priority 1 (deadline: 19.05) - Responsible "AN" - comment by = AN - Check with and without NonResiLoad
            # NON RESIDENTIAL LOADS
            if 'non_resi_load' in elec_config:
                non_res_load = NonResidentialLoad(scenario_id)
                aggregated_non_resi_load = non_res_load.run_NonResidentialLoad()
                # reducing resolution from 0.25 to 1 h
                aggregated_non_resi_load = [aggregated_non_resi_load[i * 4] for i in range(8760)]
            else:
                aggregated_non_resi_load = 'None'

            # execute preparation_worktrhough
            buildingid_list, zoneid_list = input_preparation.preparation_workthrough(scenario_object, calc_type, j)

            # just for speeding during development
            if reduce_to_1_building_with_2_zones:
                # building_id = buildingid_list[config['simulation_settings']['building_num']]
                building_id = buildingid_list[7]
                buildingid_list = [building_id]
                building_type = building_id.split('.')[2]
                building_series = building_id.split('.')[3]
                building_count = building_id.split('.')[-1]
                zoneid_list = [zone_id for zone_id in zoneid_list
                               if zone_id.split('.')[-1].split('-')[0] == building_count and
                               zone_id.split('.')[2] == building_type and zone_id.split('.')[3] == building_series]
                # zoneid_list = zoneid_list[:2]

            print('\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
            print('INPUT PREPARATION FINISHED')
            logging.debug('INPUT PREPARATION FINISHED')

            # Create district
            # ----------------------------------------------------------------------------------------------------------
            logging.debug('CREATING DISTRICT STARTED')
            print('\nCREATING DISTRICT STARTED')
            print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
            if show_progress:
                app.update_progress(0, 'CREATING DISTRICT STARTED', 'CREATING DISTRICT', False, scenario_id, calc_type,
                                    j + 5)

            # input data files
            input_data_location = input_preparation.folder_input_created + '/' + scenario_id

            # renovation_costs_df
            renovation_costs_df = pd.read_csv(input_preparation.file_renovation_costs, skiprows=[1], delimiter=",")

            # instantiate district object
            if mydistrict_exists:
                print('\nThe district for this scenario already exists from a previous calc_type and it will be used')
                mydistrict.update_district(config)  # update the config parameters in the old district instance
                pass
            else:
                print('\nNo district from a previous calc_type exists for this scenario yet and it will be created')
                mydistrict = district.District(config, buildingid_list, zoneid_list, mytime, input_data_location,
                                               aggregated_non_resi_load,
                                               input_preparation.file_building_pv_potentials,
                                               input_preparation.dir_dhw,
                                               scenario_object, scenario_id, calc_type, j,
                                               input_preparation.file_geometry_surfaces,
                                               renovation_costs_df)
                mydistrict_exists = True  # Changing state after the district of the first calc_type of one scenario

            print('\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
            print('CREATING DISTRICT FINISHED')
            logging.debug('CREATING DISTRICT FINISHED')
            if show_progress:
                app.update_progress(100, 'CREATING DISTRICT FINISHED', 'CREATING DISTRICT', False, scenario_id,
                                    calc_type,
                                    j + 10)

            ############################################################################################################
            # SIMULATION RUNS
            # ----------------------------------------------------------------------------------------------------------
            # initialise directory for Results of scenario
            scenario_dir = os.path.join(mydir, scenario_id)
            if not os.path.exists(scenario_dir):
                os.mkdir(scenario_dir)
            calc_type_dir = os.path.join(scenario_dir, calc_type)
            if not os.path.exists(calc_type_dir):
                os.mkdir(calc_type_dir)
            # ----------------------------------------------------------------------------------------------------------
            # SIZING RUN (sizing run)
            # ----------------------------------------------------------------------------------------------------------
            if scenario_object.heat_supply_system_bv1 == 'Heat_pump' or \
                    scenario_object.heat_supply_system_bv2 == 'Heat_pump' or \
                    scenario_object.heat_supply_system_bv3 == 'Heat_pump':
                hp_in_scenario = True
            else:
                hp_in_scenario = False

            if calc_type != "Temp" and allow_sizing_run is True and hp_in_scenario is True:

                logging.debug('SIZING RUN STARTED')
                print('\nSIZING RUN STARTED')
                print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
                if show_progress:
                    app.update_progress(0, 'SIZING RUN STARTED', 'SIZING RUN', False, scenario_id, calc_type,
                                        j + 10)

                run_mode = 'sizing'
                logging.debug('run_mode ' + str(run_mode))
                logging.debug('calc_type ' + 'Temp')

                # initialise directory for Results of sizing run
                sizing_dir = os.path.join(scenario_dir, calc_type, run_mode)
                if not os.path.exists(sizing_dir):
                    os.mkdir(sizing_dir)
                print('\nSizing run results folder created: ' + sizing_dir)
                logging.debug('Sizing run results folder created: ' + sizing_dir)

                # Find the coldest hour of weather file
                # ------------------------------------------------------------------------------------------------------
                weather_data = weather.Weather(scenario_object.scenario_epw_file,
                                               scenario_object.scenario_direct_rad_file,
                                               scenario_object.scenario_total_rad_file)
                t_out_list = weather_data.temperature_dry_bulb
                t_out_min = min(t_out_list)
                coldest_hour_index = t_out_list.index(t_out_min)
                # Set the start and end time of the sizing simulation period
                # ------------------------------------------------------------------------------------------------------
                sizing_mytime = time.Time(config=config, precalc=0,
                                          sizing_run=True,
                                          sizing_start_hour=max(coldest_hour_index - 72, 0),
                                          sizing_end_hour=coldest_hour_index + 72)

                # Create sizing district
                # ------------------------------------------------------------------------------------------------------
                print('\nCreating sizing_mydistrict started')
                logging.debug('Creating sizing_mydistrict started')
                sizing_calc_type = 'Temp'
                sizing_mydistrict = district.District(config, buildingid_list, zoneid_list, sizing_mytime,
                                                      input_data_location,
                                                      aggregated_non_resi_load,
                                                      input_preparation.file_building_pv_potentials,
                                                      input_preparation.dir_dhw,
                                                      scenario_object, scenario_id, sizing_calc_type, j,
                                                      input_preparation.file_geometry_surfaces,
                                                      renovation_costs_df)
                print('\nCreating sizing_mydistrict finished')
                logging.debug('Creating sizing_mydistrict finished')

                # Simulate the building and energy system in the sizing district
                # ------------------------------------------------------------------------------------------------------
                print('\nDistrict simulation (sizing run) started')
                logging.debug('District simulation (sizing run) started')
                try:
                    sizing_mydistrict.simulate(scenario_id, sizing_calc_type, sizing_mytime, j,
                                               scenario_object.location_year)
                except:
                    raise Exception('Error when trying to simulate mydistrict')
                print('\nDistrict simulation (sizing run) finished')
                logging.debug('District simulation (sizing run) finished')

                # postprocessing
                # ------------------------------------------------------------------------------------------------------
                print('\nPost-processing of district results (sizing run) started')
                logging.debug('Post-processing of district results (sizing run) started')
                # sum values for buildings
                for building in sizing_mydistrict.buildings:
                    setattr(building, 'Qh', building.sum_zone_values(building.zones, 'Qh', sizing_mytime))
                    setattr(building, 'Qc', building.sum_zone_values(building.zones, 'Qc', sizing_mytime))
                # create output files
                util.create_output_files(sizing_mytime, sizing_mydistrict, sizing_dir)
                print('\nPost-processing of district results (sizing run) finished')
                logging.debug('Post-processing of district results (sizing run) finished')

                # update thermal system size values in mydistrict with sizing Results and export values
                # ------------------------------------------------------------------------------------------------------
                mydistrict.sizing_run_completed = True
                sizes_list_kW = []
                sizes_list_Wm2 = []
                for building in mydistrict.buildings:
                    bid = building.building_code
                    sizing_heating_load_kW = max(sizing_mydistrict.time_series_buildings[bid]['hp_th'].tolist())/1000
                    sizing_heating_load_Wm2 = 1000 * sizing_heating_load_kW / building.storey_area
                    building.total_sizing_heating_load = sizing_heating_load_kW
                    sizes_list_kW.append(sizing_heating_load_kW)
                    sizes_list_Wm2.append(sizing_heating_load_Wm2)

                sizes_df = pd.DataFrame()
                sizes_df['building_code'] = [building.building_code for building in sizing_mydistrict.buildings]
                sizes_df['power_th_kW'] = sizes_list_kW
                sizes_df['power_th_Wm2'] = sizes_list_Wm2
                sizes_df.to_csv(os.path.join(sizing_dir, 'HP_sizing.csv'))

            # ----------------------------------------------------------------------------------------------------------
            # TRAINING RUN (precalc run)
            # ----------------------------------------------------------------------------------------------------------
            if precalc_days != 0:
                logging.debug('TRAINING RUN STARTED')
                print('\nTRAINING RUN STARTED')
                print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
                if show_progress:
                    app.update_progress(0, 'TRAINING RUN STARTED', 'TRAINING RUN', False, scenario_id, calc_type,
                                        j + 10)

                run_mode = 'precalc'
                logging.debug('run_mode' + str(run_mode))
                logging.debug('calc_type' + str(calc_type))

                # initialise directory for Results of training run (precalc run)
                precalc_dir = os.path.join(scenario_dir, calc_type, run_mode)
                if not os.path.exists(precalc_dir):
                    os.mkdir(precalc_dir)
                print('\nTraining run results folder created: ' + precalc_dir)
                logging.debug('Training run results folder created: ' + precalc_dir)

                # simulate the building and energy system in the district
                # ------------------------------------------------------------------------------------------------------
                print('\nDistrict simulation (training run) started')
                logging.debug('District simulation (training run) started')
                try:
                    mydistrict.simulate(scenario_id, calc_type, mytime, j, scenario_object.location_year)
                except:
                    raise Exception('Error when trying to simulate mydistrict')
                    pass
                print('\nDistrict simulation (training run) finished')
                logging.debug('District simulation (training run) finished')

                # postprocessing
                # ------------------------------------------------------------------------------------------------------
                print('\nPost-processing of district results (training run) started')
                logging.debug('Post-processing of district results (training run) started')
                # sum values for buildings
                for building in mydistrict.buildings:
                    setattr(building, 'Qh', building.sum_zone_values(building.zones, 'Qh', mytime))
                    setattr(building, 'Qc', building.sum_zone_values(building.zones, 'Qc', mytime))
                # create output files
                util.create_output_files(mytime, mydistrict, precalc_dir)
                print('\nPost-processing of district results (training run) finished')
                logging.debug('Post-processing of district results (training run) finished')

                # update start_dict values with precalc Results for main run
                # ------------------------------------------------------------------------------------------------------
                timestep_start_next = mytime.starthour_run + precalc_days * 24 * time_resolution_h - 1
                for building in mydistrict.buildings:
                    for zone in building.zones:
                        zone.start_dict = {'Qh': zone.Qh.loc[timestep_start_next],
                                           'Qc': zone.Qc.loc[timestep_start_next]}
                        for i in temp_state_names:
                            zone.start_dict[i] = getattr(zone, i).loc[timestep_start_next]

                print('\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
                print('TRAINING RUN FINISHED')
                logging.debug('TRAINING RUN FINISHED')
                if show_progress:
                    app.update_progress(99.9, 'TRAINING RUN FINISHED', 'TRAINING RUN', False, scenario_id, calc_type, y)

            # ----------------------------------------------------------------------------------------------------------
            # MAIN RUN
            # ----------------------------------------------------------------------------------------------------------
            logging.debug('MAIN RUN STARTED')
            print('\nMAIN RUN STARTED')
            print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
            if show_progress:
                app.update_progress(0, 'MAIN RUN STARTED', 'MAIN RUN', False, scenario_id, calc_type, j + 10)

            run_mode = 'mainrun'
            logging.debug('run_mode' + str(run_mode))
            logging.debug('calc_type' + str(calc_type))

            # initialise directory for Results of main run
            mainrun_dir = os.path.join(scenario_dir, calc_type, run_mode)
            if not os.path.exists(mainrun_dir):
                try:
                    os.mkdir(mainrun_dir)
                except:
                    pass

            print('\nMain run results folder created: ' + mainrun_dir)
            logging.debug('Main run results folder created: ' + mainrun_dir)

            # re-create Time object so that time variables match updated precalc_days
            mytime = time.Time(config=config, precalc=0)

            # simulate the building and energy system in the district
            # ----------------------------------------------------------------------------------------------------------
            print('\nDistrict simulation (main run) started')
            logging.debug('District simulation (main run) started')
            try:
                mydistrict.simulate(scenario_id, calc_type, mytime, j, scenario_object.location_year)
                # check system error
                sim_with_error = 'There is no system error'
                error_code = 0
                # check simulation errors
                error_list = [str(error_logging_row) for error_logging_row in mydistrict.error_logging]
                error_logging = np.array(error_list)
                pd.DataFrame(error_logging).to_csv(mainrun_dir + "/simulation_error_logging.csv")
            except:
                sim_with_error = 'There is an system error!!!'
                error_code = 1
                print('error appears')
            print('\nDistrict simulation (main run) finished')
            logging.debug('District simulation (main run) finished')

            # postprocessing
            # ----------------------------------------------------------------------------------------------------------
            print('\nPost-processing of district results (main run) started')
            logging.debug('Post-processing of district results (main run) started')
            # sum values for buildings
            for building in mydistrict.buildings:
                setattr(building, 'Qh', building.sum_zone_values(building.zones, 'Qh', mytime))
                setattr(building, 'Qc', building.sum_zone_values(building.zones, 'Qc', mytime))
            # create output files
            util.create_output_files(mytime, mydistrict, mainrun_dir)
            print('\nPost-processing of district results (main run) finished')
            logging.debug('Post-processing of district results (main run) finished')

            print('\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
            print('MAIN RUN FINISHED')
            logging.debug('MAIN RUN FINISHED')
            if show_progress:
                app.update_progress(99.9, 'MAIN RUN FINISHED', 'MAIN RUN', False, scenario_id, calc_type, y)

    print('\n======================================================================')
    print('SIMULATION OF SCENARIOS FINISHED')
    logging.debug('SIMULATION OF SCENARIOS FINISHED')

    if show_progress:
        app.update_progress(99.9, 'MAIN FINISHED', 'MAIN RUN', False, scenario_id, calc_type, 100)
    # call(["python", Prallel_Coordinate_Plot])
    print("\nComparing Scenarios in Parallel\n")
    print('\n All Scenario simulations completed, have a good day!\n')
    logging.debug("All Scenario simulations completed, have a good day!")
    # start the mainloop of the tkinter window
    if show_progress:
        app.run()
    # close the mainloop of the tkinter window
    if show_progress:
        app.close()
    print('\n######################################################################')
    print('RUNME FINISHED')
    logging.debug("RUNME FINISHED")
    with open(calc_type_dir + '/logging_error.txt', "w") as text_file:
        text_file.write(f"Error state {error_code} of simulation:" + sim_with_error)

    return mydistrict


########################################################################################################################
if __name__ == '__main__':
    # load scenario list
    with open(f"Step_0_config.yaml") as config_file:
        config = YAML().load(config_file)
    scenarios = config['project_settings']['scenarios']
    # execute run_cleanmod()
    run_cleanmod(scenarios)
