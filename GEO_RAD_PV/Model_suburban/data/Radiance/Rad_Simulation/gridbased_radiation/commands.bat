@echo off
SET RAYPATH=.;c:\radiance\lib
PATH=c:\radiance\bin;%PATH%
C:
cd C:\Users\Workstation004\Documents\Michelle\Suburban\Models\Sub_urban\Radiance\Rad_Simulation\gridbased_radiation

echo :: diffuse sky matrix
c:\radiance\bin\gendaymtx -s -O1 -r 0.0 -m 1 -v sky/skymtx_sol_r1_2_108700_48.367_11.8_0.0.wea > sky/skymtx_sol_r1_2_108700_48.367_11.8_0.0.smx
echo :: Done with 0 of 1 ^|----------^| (0.00%%)
echo ::
echo :: start of the calculation for scene, default. State 1 of 1
echo ::
echo :: :: 1. calculating daylight matrices
echo ::
echo :: :: [1/3] scene daylight matrix
echo :: :: rfluxmtx - [sky] [points] [wgroup] [blacked wgroups] [scene] ^> [dc.mtx]
echo ::
c:\radiance\bin\rfluxmtx -y 770 -aa 0.25 -ab 3 -ad 5000 -ar 16 -as 128 -dc 0.25 -dj 0.0 -dp 64 -ds 0.5 -dr 0 -dt 0.5 -I -lr 4 -lw 2e-06 -c 1 -ss 0.0 -st 0.85 - sky\rfluxSky.rad scene\glazing\Rad_Simulation..glz.mat scene\glazing\Rad_Simulation..glz.rad scene\opaque\Rad_Simulation..opq.mat scene\opaque\Rad_Simulation..opq.rad scene\extra\Context.rad < Rad_Simulation.pts > result\matrix\normal_Rad_Simulation..scene..default.dc 
echo :: :: [2/3] black scene daylight matrix
echo :: :: rfluxmtx - [sky] [points] [wgroup] [blacked wgroups] [blacked scene] ^> [black dc.mtx]
echo ::
c:\radiance\bin\rfluxmtx -y 770 -aa 0.25 -ab 1 -ad 5000 -ar 16 -as 128 -dc 0.25 -dj 0.0 -dp 64 -ds 0.5 -dr 0 -dt 0.5 -I -lr 4 -lw 2e-06 -c 1 -ss 0.0 -st 0.85 - sky\rfluxSky.rad scene\glazing\Rad_Simulation..glz.mat scene\glazing\Rad_Simulation..glz.rad scene\opaque\Rad_Simulation..blk.mat scene\opaque\Rad_Simulation..opq.rad scene\extra\black.mat scene\extra\Context_blacked.rad < Rad_Simulation.pts > result\matrix\black_Rad_Simulation..scene..default.dc 
echo :: :: [3/3] black scene analemma daylight matrix
echo :: :: rcontrib - [sun_matrix] [points] [wgroup] [blacked wgroups] [blacked scene] ^> [analemma dc.mtx]
echo ::
c:\radiance\bin\oconv -f scene\glazing\Rad_Simulation..glz.mat scene\glazing\Rad_Simulation..glz.rad scene\opaque\Rad_Simulation..blk.mat scene\opaque\Rad_Simulation..opq.rad scene\extra\black.mat scene\extra\Context_blacked.rad sky\analemma_reversed.rad > analemma.oct
c:\radiance\bin\rcontrib -aa 0.0 -ab 0 -ad 512 -ar 16 -as 128 -dc 1.0 -dj 0.0 -dp 64 -ds 0.5 -dr 0 -dt 0.0 -I -lr 4 -lw 0.05 -M .\sky\analemma.mod -ss 0.0 -st 0.85 analemma.oct < Rad_Simulation.pts > result\matrix\sun_Rad_Simulation..scene..default.dc
echo :: :: 2. matrix multiplication
echo ::
echo :: :: [1/2] calculating daylight mtx * diffuse sky
echo :: :: dctimestep [dc.mtx] [diffuse sky] ^> [diffuse results.rgb]
c:\radiance\bin\dctimestep result\matrix\normal_Rad_Simulation..scene..default.dc sky\skymtx_sol_r1_2_108700_48.367_11.8_0.0.smx > tmp\diffuse..scene..default.rgb
echo :: :: rmtxop -c 47.4 119.9 11.6 [results.rgb] ^> [diffuse results.ill]
echo ::
c:\radiance\bin\rmtxop -c 47.4 119.9 11.6 -fa tmp\diffuse..scene..default.rgb > result\diffuse..scene..default.ill
echo :: :: [2/2] calculating black daylight mtx * analemma
echo :: :: dctimestep [black dc.mtx] [analemma only sky] ^> [sun results.rgb]
c:\radiance\bin\dctimestep result\matrix\sun_Rad_Simulation..scene..default.dc sky\sunmtx.smx > tmp\sun..scene..default.rgb
echo :: :: rmtxop -c 47.4 119.9 11.6 [sun results.rgb] ^> [sun results.ill]
echo ::
c:\radiance\bin\rmtxop -c 47.4 119.9 11.6 -fa tmp\sun..scene..default.rgb > result\sun..scene..default.ill
echo :: :: 3. calculating final results
echo :: :: rmtxop [diff results.ill] + [sun results.ill] ^> [final results.ill]
echo ::
c:\radiance\bin\rmtxop result\diffuse..scene..default.ill + result\sun..scene..default.ill > result\scene..default.ill
echo :: end of calculation for scene, default
echo ::
echo ::