import os
import pandas as pd

# Name of this .py file
py_file_name = os.path.basename(__file__)

# Path of the folder containing this .py file
folder_path = os.getcwd()

# Content of the folder containing this .py file
folder_content = os.listdir()

# Text to be adapted
ori_text = 'Gen_'

# Text to be used as the replacement
new_text = 'Gen.ReEx.001_0'


def process_file(item, item_path, ori_text, new_text):
    export = False
    file_path = item_path
    file_content_df_ori = pd.read_csv(file_path, low_memory=False)
    file_content_df_new = file_content_df_ori.copy()
    for col in file_content_df_ori.columns:
        for i, val in enumerate(file_content_df_ori[col]):
            if type(val) is str:
                if ori_text in val:
                    new_val = new_text.join(val.split(ori_text))
                    file_content_df_new[col].iloc[i] = new_val
                    export = True
    if export:
        file_content_df_new.to_csv(file_path, index=False)
        print('\n', item, ' has been adapted and saved in:', file_path)
    else:
        print('\n', item, ' do not require to be adapted and is saved in:', file_path)


for item in folder_content:
    if item == py_file_name:
        pass
    else:
        print('\n', item, '\n-------------------------------------------------')
        item_path = os.path.join(folder_path, item)
        if os.path.isfile(item_path):
            file_extension = os.path.splitext(item)[1]
            if file_extension == '.csv':
                process_file(item, item_path, ori_text, new_text)
        else:
            sub_folder_path = item_path
            sub_folder_content = os.listdir(sub_folder_path)
            for sub_item in sub_folder_content:
                sub_item_path = os.path.join(sub_folder_path, sub_item)
                if os.path.isfile(sub_item_path):
                    file_extension = os.path.splitext(sub_item)[1]
                    if file_extension == '.csv':
                        process_file(sub_item, sub_item_path, ori_text, new_text)