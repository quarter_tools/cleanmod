1) Search in internet for typical time series of electrical loads of non-residential buildings in W/m2 or kW/m2 for Germany.
If we find them, we will use them instead of the data of Arthur. The timeserie should be at least hourly, better if 15 min resolution.

2) Create an unique csv, in which each column is the time serie of total electrical load per non-residential building use.
The final Non-residentail_loads.csv should be saved in ../Libraries (not in a sub-folder Non-residential_loads)

3) Review the implementation of the use of these time series in cleanmod.
The additional csv file containing the non-resi buildings present in the modelled district should be located in ../Models/[Model]/Input
That file should contain two columns: non-resi_use, total_surface
The code in cleanmod should load the time series in W/m2 of each present non-resi building in the disctrict,
then it should multiply the time serie by the total surface of non-resi building present in the distric,
finally it should aggregate the whole electical load profiles of all non-resi buildings creating an overal non-resi time serie of electricity load.
The final aggregated load should be an attribute of the district.
The class building will be only used for residential buildings.