from ruamel.yaml import YAML
import os
import shutil
import pandas as pd
import time
import numpy as np
import pandas as pd
from Cleanvelope.CleanMod.Step_1_runme import run_cleanmod
import multiprocessing as mp
from multiprocessing import Pool, freeze_support
from functools import partial
from itertools import repeat
from datetime import datetime


# import scenarios from config_cleanmod
with open(f"Step_0_config.yaml") as config_file:
    config = YAML().load(config_file)

scenarios = config['project_settings']['scenarios']
result_folder_name = datetime.now().strftime('Results_%y%m%d_%H%M%S')

list_for_pool = [([scenario], result_folder_name, None) for scenario in scenarios]
num_cores = 3

def main():

    with Pool(num_cores) as pool:
        # mapping
        L = pool.starmap(run_cleanmod, list_for_pool)


if __name__ == "__main__":
    freeze_support()
    main()
    time.sleep(5)

