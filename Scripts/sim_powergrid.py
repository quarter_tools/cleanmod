import powergrid
import os
import math
import time
import pandas as pd
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
from ruamel.yaml import YAML
from datetime import datetime


# read config data
with open(f"../0_config_cleanmod.yaml") as config_file:
    config = YAML().load(config_file)

# load the results from the main_run
# import the results
name_model = config['project_settings']['name_model']  # "Example"
calc_type = config['simulation_settings']['calc_type']  # 'Temp', 'peak_load', 'elec_bought', 'Qhc'

mydir = os.path.join(os.getcwd(), '../Models/{0}/Results'.format(name_model))  # find the results folder
files = os.listdir(mydir)
paths = [os.path.join(mydir, basename) for basename in files]
res_dir = max(paths, key=os.path.getctime)
res_path = os.path.join(res_dir, 'mainrun/power_grid_timeseries.csv')

res = pd.read_csv(res_path, index_col=0)
# time settings
start_h = config['simulation_settings']['starthour']
end_h = config['simulation_settings']['endhour']

# change the time interval
res = res.loc[res.index.repeat(4)]
res = res.reset_index(drop=True)

# prepare the simulation of power grid
power_grid1 = powergrid.POWERGRID("1-LV-urban6--0-sw")
power_grid1.init_grid()
power_grid1.set_timesteps([start_h*4, end_h*4])
power_grid1.apply_time_series(res)
power_grid1.simulate()
power_grid1.plot_res()

