__author__ = "sdlumpp"
__credits__ = []
__license__ = ""
__maintainer__ = "sdlumpp"
__email__ = "sebastian.lumpp@tum.de"

# system packages
import pandas as pd
import numpy as np
import warnings
import json
from random import random

# external/third-party packages
import pyomo.environ as pyo
import feather as ft

# our own modules/applications
import cleanmod.forecasting as fcast


class MyClass:
    """Basic Introduction  ...


        ....

        Public methods:

        __init__ :       Create an instance of the Prosumer class from a configuration folder created using the
                         Simulation class

        Public method 1:      ...
        Public method 2:      ...
    """

    def __init__(self, path, t_override=None):
        """Initialization of MyClass

        Parameters
        ----------
        self : MyClass object

            Object containing information on .... :

                Attribute 1:     ....
                Attribute 2:     ....
                Attribute 2:     ....

        path: path of input data

        Returns
        -------
        Output : dictionary

            The following are included:

                Data 1:        ...
                Data 2:        ...
                Data 3:        ...
        """

        self.dict = np.read_csv(path)

    def solve_pyo_model(_model):

        """Initialization of MyClass

        Parameters
        ----------
        _model : Pyomo model which includes constraint and optimization objective

        Returns
        -------
        Output : results

            Optimization results containing following data:

                Data 1:        ...
                Data 2:        ...
                Data 3:        ...

        """

        results = pyo.solve(_model)

        return results
