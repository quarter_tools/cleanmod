__author__ = "Zyou"
__credits__ = []
__license__ = ""
__maintainer__ = "Zyou"
__email__ = "zhengjie.you@tum.de"

# system packages
import pandas as pd
import numpy as np
import string
from scipy import optimize
import os

# external/third-party packages
from typing import Any, Dict, Union


# our own modules/applications


class HeatPump:
    """
    This module contains a heat pump model capable of calculating the outputs for one of six different generic heat pump types.

    The models correspond to the following types:

    - Outdoor Air / Water: Regulated [1], On-Off [4]
    - Brine / Water: Regulated [2], On-Off [5]
    - Water / Water: Regulated [3], On-Off [6]

    Methods
    -------
    __init__(self, group_id)
        Initializes an instance of the HeatPump class using pre-defined parameter groups for simulation.
        The group_id parameter corresponds to the regulated and on-off versions of the heat pump types.

    simulate(self)
        Executes the simulation of the heat pump model, based on the parameters defined at initialization.
    """

    def __init__(self, parameters: pd.DataFrame):
        self.group_id = float(parameters['Group'].array[0])
        self.p1_p_el_h = float(parameters['p1_P_el_h [1/°C]'].array[0])
        self.p2_p_el_h = float(parameters['p2_P_el_h [1/°C]'].array[0])
        self.p3_p_el_h = float(parameters['p3_P_el_h [-]'].array[0])
        self.p4_p_el_h = float(parameters['p4_P_el_h [1/°C]'].array[0])
        self.p1_cop = float(parameters['p1_COP [-]'].array[0])
        self.p2_cop = float(parameters['p2_COP [-]'].array[0])
        self.p3_cop = float(parameters['p3_COP [-]'].array[0])
        self.p4_cop = float(parameters['p4_COP [-]'].array[0])
        self.p_el_ref = float(parameters['P_el_h_ref [W]'].array[0])
        self.p_th_ref = float(parameters['P_th_h_ref [W]'].array[0])
        try:
            self.p1_eer = parameters['p1_EER [-]'].array[0]
            self.p2_eer = parameters['p2_EER [-]'].array[0]
            self.p3_eer = parameters['p3_EER [-]'].array[0]
            self.p4_eer = parameters['p4_EER [-]'].array[0]
            self.p1_p_el_c = parameters['p1_P_el_c [1/°C]'].array[0]
            self.p2_p_el_c = parameters['p2_P_el_c [1/°C]'].array[0]
            self.p3_p_el_c = parameters['p3_P_el_c [-]'].array[0]
            self.p4_p_el_c = parameters['p4_P_el_c [1/°C]'].array[0]
            self.p_el_col_ref = parameters['P_el_c_ref [W]'].array[0]
        except:
            self.p1_eer = np.nan
            self.p2_eer = np.nan
            self.p3_eer = np.nan
            self.p4_eer = np.nan
            self.p1_p_el_c = np.nan
            self.p2_p_el_c = np.nan
            self.p3_p_el_c = np.nan
            self.p4_p_el_c = np.nan
            self.p_el_col_ref = np.nan

        self.delta_t = 5  # Inlet temperature is supposed to be heated up by 5 K
        self.cp = 4200  # J/(kg*K), specific heat capacity of water

    def simulate(self, t_in_primary: Union[float, np.ndarray], t_in_secondary: Union[float, np.ndarray],
                 t_amb: Union[float, np.ndarray], mode: int = 1, p_th_min: Union[float, np.ndarray] = 0) -> dict:

        """
        Executes the simulation of the heat pump model.

        Parameters
        ----------
        t_in_primary : numeric or iterable (e.g., pd.Series)
            Primary side input temperature (e.g., air, brine, water in °C).

        t_in_secondary : numeric or iterable (e.g., pd.Series)
            Secondary side input temperature, obtained from heating storage or system in °C.

        parameters : pd.DataFrame
            DataFrame containing the heat pump parameters as returned by hplib.getParameters().

        t_amb : numeric or iterable (e.g., pd.Series)
            Ambient temperature (air temperature in °C).

        mode : int
            Operation mode of the heat pump. Use 1 for heating and 2 for cooling.

        P_th_min : numeric
            Minimum thermal power output in Watts. Inverter heat pumps increase electrical power input. At maximum electrical input, an electric heating rod turns on.

        Returns
        -------
        result : dict
            The resulting dictionary has the following keys:
            - T_in: Input temperature at the primary side of the heat pump in °C.
            - T_out: Output temperature at the secondary side of the heat pump in °C.
            - T_amb: Ambient / outdoor temperature in °C.
            - COP: Coefficient of performance of the heat pump.
            - EER: Energy efficiency ratio of the heat pump.
            - P_el: Electrical input power in Watts.
            - P_th: Thermal output power in Watts.
            - m_dot: Mass flow at the secondary side of the heat pump in kg/s.
        """

        if mode == 2 and self.group_id > 1:
            raise ValueError('Cooling is only possible with heat pumps of group id = 1.')

        t_in = t_in_primary  # info value for dataframe
        if mode == 1:
            t_out = t_in_secondary + self.delta_t  # Inlet temperature is supposed to be heated up by 5 K
            eer = 0
        if mode == 2:  # Inlet temperature is supposed to be cooled down by 5 K
            t_out = t_in_secondary - self.delta_t
            cop = 0
        # for subtype = air/water heat pump
        if self.group_id in (1, 4):
            t_amb = t_in
        t_ambient = t_amb
        # for regulated heat pumps
        if self.group_id in (1, 2, 3):
            if mode == 1:
                cop = self.p1_cop * t_in + self.p2_cop * t_out + self.p3_cop + self.p4_cop * t_amb
                p_el = self.p_el_ref * (self.p1_p_el_h * t_in
                                        + self.p2_p_el_h * t_out
                                        + self.p3_p_el_h
                                        + self.p4_p_el_h * t_amb)
                if self.group_id == 1:
                    if isinstance(t_in, np.ndarray):
                        t_in = np.full_like(t_in, -7)
                    else:
                        t_in = -7
                    t_amb = t_in

                elif self.group_id == 2:
                    if isinstance(t_amb, np.ndarray):
                        t_amb = np.full_like(t_amb, -7)
                    else:
                        t_amb = -7
                p_el_25 = 0.25 * self.p_el_ref * (self.p1_p_el_h * t_in
                                                  + self.p2_p_el_h * t_out
                                                  + self.p3_p_el_h
                                                  + self.p4_p_el_h * t_amb)
                if isinstance(p_el, np.ndarray):
                    p_el = np.where(p_el < p_el_25, p_el_25, p_el)
                elif p_el < p_el_25:
                    p_el = p_el_25

                p_th = p_el * cop

                if isinstance(cop, np.ndarray):
                    # turn on heating rod and compressor
                    p_el = np.where((cop > 1) & (p_th < p_th_min) & (self.p_el_ref < p_th_min / cop),
                                    self.p_el_ref + self.p_th_ref, p_el)
                    p_th = np.where((cop > 1) & (p_th < p_th_min) & (self.p_el_ref < p_th_min / cop),
                                    self.p_el_ref * cop + self.p_th_ref, p_th)
                    # increase electrical power for compressor
                    p_el = np.where((cop > 1) & (p_th < p_th_min) & (self.p_el_ref > p_th_min / cop),
                                    p_th_min / cop, p_el)
                    p_th = np.where((cop > 1) & (p_th < p_th_min) & (self.p_el_ref > p_th_min / cop), p_th_min,
                                    p_th)
                    # only turn on heating rod
                    p_el = np.where(cop <= 1, self.p_th_ref, p_el)
                    p_th = np.where(cop <= 1, self.p_th_ref, p_th)
                    cop = p_th / p_el
                else:
                    if cop <= 1:
                        cop = 1
                        p_el = self.p_th_ref
                        p_th = self.p_th_ref
                    elif p_th < p_th_min:
                        if self.p_el_ref > p_th_min / cop:
                            p_el = p_th_min / cop
                            p_th = p_th_min
                        else:
                            p_el = self.p_el_ref + self.p_th_ref
                            p_th = self.p_el_ref * cop + self.p_th_ref
                            cop = p_th / p_el

            if mode == 2:
                eer = (self.p1_eer * t_in + self.p2_eer * t_out + self.p3_eer + self.p4_eer * t_amb)
                if isinstance(t_in, np.ndarray):
                    t_in = np.where(t_in < 25, 25, t_in)
                elif t_in < 25:
                    t_in = 25
                t_amb = t_in
                p_el = (
                               self.p1_p_el_c * t_in + self.p2_p_el_c * t_out + self.p3_p_el_c + self.p4_p_el_c * t_amb) * self.p_el_col_ref
                if isinstance(p_el, np.ndarray):
                    eer = np.where(p_el < 0, 0, eer)
                    p_el = np.where(p_el < 0, 0, p_el)
                elif p_el < 0:
                    eer = 0
                    p_el = 0
                p_th = -(eer * p_el)
                if isinstance(eer, np.ndarray):
                    p_el = np.where(eer <= 1, 0, p_el)
                    p_th = np.where(eer <= 1, 0, p_th)
                    eer = np.where(eer <= 1, 0, eer)
                elif eer < 1:
                    eer = 0
                    p_el = 0
                    p_th = 0

        # for subtype = On-Off
        elif self.group_id in (4, 5, 6):
            p_el = (self.p1_p_el_h * t_in
                    + self.p2_p_el_h * t_out
                    + self.p3_p_el_h
                    + self.p4_p_el_h * t_amb) * self.p_el_ref

            cop = self.p1_cop * t_in + self.p2_cop * t_out + self.p3_cop + self.p4_cop * t_amb

            p_th = p_el * cop

            if isinstance(cop, np.ndarray):
                p_el = np.where((cop > 1) & (p_th < p_th_min), p_el + self.p_th_ref, p_el)
                p_th = np.where((cop > 1) & (p_th < p_th_min), p_th + self.p_th_ref, p_th)
                p_el = np.where(cop <= 1, self.p_th_ref, p_el)
                p_th = np.where(cop <= 1, self.p_th_ref, p_th)
                cop = p_th / p_el

            else:
                if cop <= 1:
                    cop = 1
                    p_el = self.p_th_ref
                    p_th = self.p_th_ref
                elif p_th < p_th_min:
                    p_th = p_th + self.p_th_ref
                    p_el = p_el + self.p_th_ref
                    cop = p_th / p_el

        # massflow
        m_dot = abs(p_th / (self.delta_t * self.cp))

        # round
        result = dict()

        result['T_in'] = t_in_primary
        result['T_out'] = t_out
        result['T_amb'] = t_ambient
        result['COP'] = cop
        result['EER'] = eer
        result['P_el'] = p_el
        result['P_th'] = p_th
        result['m_dot'] = m_dot

        return result


class HeatingSystem:
    def __init__(self, t_outside_min: float = -15.0,
                 t_inside_set: float = 20.0,
                 t_hs_set: list = [35, 28],
                 f_hs_size: float = 1.0,
                 f_hs_exp: float = 1.1):
        """
        Initializes the heating system model with several parameters.

        Parameters
        ----------
        t_outside_min : numeric
            Minimal reference outside temperature for the building.

        t_inside_set : numeric
            Set room temperature for the building.

        t_hf_set : list of numeric
            List with maximum heating flow and return temperatures in °C. Examples include:
            - [35, 28] for floor heating
            - [55, 45] for low temperature radiator
            - [70, 55] for radiator

        f_hs_size : numeric
            Oversizing factor for the heat distribution system.

        f_hs_exp : numeric
            Exponent factor for the heating distribution system (e.g., 1.1 for floor heating, 1.3 for radiator).
        """

        self.t_outside_min = t_outside_min
        self.t_inside_set = t_inside_set
        self.t_hf_max = t_hs_set[0]
        self.t_hf_min = t_inside_set
        self.t_hr_max = t_hs_set[1]
        self.t_hr_min = t_inside_set
        self.f_hs_size = f_hs_size
        self.f_hs_exp = f_hs_exp

    def calc_brine_temp(self, t_avg_d: float):
        """
        Calculates the soil temperature based on the average temperature of the day.

        This method is based on the field measurements of heat pump systems as described in "WP Monitor", Frauenhofer ISE, 2014. To prevent higher temperatures of soil below -10°C, this method adds 9 points at -15°C average day at 3°C soil temperature.

        Parameters
        ----------
        t_avg_d : numeric
            The average temperature of the day.

        Returns
        -------
        t_brine : numeric
            The soil temperature, also known as the brine inflow temperature.
        """

        t_brine = -0.0003 * t_avg_d ** 3 + 0.0086 * t_avg_d ** 2 + 0.3047 * t_avg_d + 5.0647

        return t_brine

    def calc_heating_dist_temp(self, t_avg_d: float):
        """
        Calculates the heat distribution flow and return temperature based on the moving average daily mean outside temperature.

        This method follows the calculations outlined in DIN V 4701-10, Section 5.

        Parameters
        ----------
        self : class instance
            Instance of the class containing the parameters initialized in the __init__ method.

        t_avg_d : numeric
            The average temperature of the day.

        Returns
        -------
        t_dist : list of numeric
            List containing the heating flow and heating return temperatures.
        """

        if t_avg_d > self.t_inside_set:
            t_hf = self.t_hf_min
            t_hr = self.t_hr_min
        else:
            t_hf = self.t_hf_min + ((1 / self.f_hs_size) * (
                    (self.t_inside_set - t_avg_d) / (self.t_inside_set - self.t_outside_min))) ** (
                           1 / self.f_hs_exp) * (self.t_hf_max - self.t_hf_min)
            t_hr = self.t_hr_min + ((1 / self.f_hs_size) * (
                    (self.t_inside_set - t_avg_d) / (self.t_inside_set - self.t_outside_min))) ** (
                           1 / self.f_hs_exp) * (self.t_hr_max - self.t_hr_min)

        t_dist = [t_hf, t_hr]

        return t_dist


############# helper function to acquire parameters needed to create HeatPump instance##############


def load_database() -> pd.DataFrame:
    """
    Loads data from the hplib_database.

    Returns
    -------
    df : pd.DataFrame
        DataFrame containing the contents of the database.
    """


    df = pd.read_csv(cwd() + r'/HP_lib_database.csv')
    return df


def get_parameters(model: str, group_id: int = 0, t_in: int = 0, t_out: int = 0, p_th: int = 0) -> pd.DataFrame:
    """
    Loads the content of the database for a specific heat pump model and returns a DataFrame containing the heat pump parameters.

    Parameters
    ----------
    model : str
        Name of the heat pump model or "Generic".

    group_id : numeric, optional
        Group ID for subtype of heat pump, ranges from 1 to 6. Only applicable for "Generic" model. Default is 0.

    t_in : numeric, optional
        Input temperature (in °C) at the primary side of the heat pump. Only applicable for "Generic" model. Default is 0.

    t_out : numeric, optional
        Output temperature (in °C) at the secondary side of the heat pump. Only applicable for "Generic" model. Default is 0.

    p_th : numeric, optional
        Thermal output power (in Watts) at setpoint t_in, t_out (and for water/water, brine/water heat pumps, t_amb = -7°C).
        Only applicable for "Generic" model. Default is 0.

    Returns
    -------
    parameters : pd.DataFrame
        DataFrame containing the model parameters.
    """

    df = pd.read_csv(os.path.abspath(os.path.join(cwd(), '../Libraries/HP_lib_database.csv')),
                     delimiter=',')
    df = df.loc[df['Model'] == model]
    parameters = pd.DataFrame()
    parameters['Manufacturer'] = (df['Manufacturer'].values.tolist())
    parameters['Model'] = (df['Model'].values.tolist())
    try:
        parameters['MAPE_COP'] = df['MAPE_COP'].values.tolist()
        parameters['MAPE_P_el'] = df['MAPE_P_el'].values.tolist()
        parameters['MAPE_P_th'] = df['MAPE_P_th'].values.tolist()
    except:
        pass
    parameters['P_th_h_ref [W]'] = (df['P_th_h_ref [W]'].values.tolist())
    parameters['P_el_h_ref [W]'] = (df['P_el_h_ref [W]'].values.tolist())
    parameters['COP_ref'] = (df['COP_ref'].values.tolist())
    parameters['Group'] = (df['Group'].values.tolist())
    parameters['p1_P_th [1/°C]'] = (df['p1_P_th [1/°C]'].values.tolist())
    parameters['p2_P_th [1/°C]'] = (df['p2_P_th [1/°C]'].values.tolist())
    parameters['p3_P_th [-]'] = (df['p3_P_th [-]'].values.tolist())
    parameters['p4_P_th [1/°C]'] = (df['p4_P_th [1/°C]'].values.tolist())
    parameters['p1_P_el_h [1/°C]'] = (df['p1_P_el_h [1/°C]'].values.tolist())
    parameters['p2_P_el_h [1/°C]'] = (df['p2_P_el_h [1/°C]'].values.tolist())
    parameters['p3_P_el_h [-]'] = (df['p3_P_el_h [-]'].values.tolist())
    parameters['p4_P_el_h [1/°C]'] = (df['p4_P_el_h [1/°C]'].values.tolist())
    parameters['p1_COP [-]'] = (df['p1_COP [-]'].values.tolist())
    parameters['p2_COP [-]'] = (df['p2_COP [-]'].values.tolist())
    parameters['p3_COP [-]'] = (df['p3_COP [-]'].values.tolist())
    parameters['p4_COP [-]'] = (df['p4_COP [-]'].values.tolist())
    try:
        parameters['P_th_c_ref [W]'] = (df['P_th_c_ref [W]'].values.tolist())
        parameters['P_el_c_ref [W]'] = (df['P_el_c_ref [W]'].values.tolist())
        parameters['p1_Pdc [1/°C]'] = (df['p1_Pdc [1/°C]'].values.tolist())
        parameters['p2_Pdc [1/°C]'] = (df['p2_Pdc [1/°C]'].values.tolist())
        parameters['p3_Pdc [-]'] = (df['p3_Pdc [-]'].values.tolist())
        parameters['p4_Pdc [1/°C]'] = (df['p4_Pdc [1/°C]'].values.tolist())
        parameters['p1_P_el_c [1/°C]'] = (df['p1_P_el_c [1/°C]'].values.tolist())
        parameters['p2_P_el_c [1/°C]'] = (df['p2_P_el_c [1/°C]'].values.tolist())
        parameters['p3_P_el_c [-]'] = (df['p3_P_el_c [-]'].values.tolist())
        parameters['p4_P_el_c [1/°C]'] = (df['p4_P_el_c [1/°C]'].values.tolist())
        parameters['p1_EER [-]'] = (df['p1_EER [-]'].values.tolist())
        parameters['p2_EER [-]'] = (df['p2_EER [-]'].values.tolist())
        parameters['p3_EER [-]'] = (df['p3_EER [-]'].values.tolist())
        parameters['p4_EER [-]'] = (df['p4_EER [-]'].values.tolist())
    except:
        pass

    if model == 'Generic':
        parameters = parameters.iloc[group_id - 1:group_id]

        p_th_ref = fit_p_th_ref(t_in, t_out, group_id, p_th) # may be simplified
        parameters.loc[:, 'P_th_h_ref [W]'] = p_th_ref
        t_in_hp = [-7, 0, 10]  # air/water, brine/water, water/water
        t_out_fix = 52
        t_amb_fix = -7
        p1_cop = parameters['p1_COP [-]'].array[0]
        p2_cop = parameters['p2_COP [-]'].array[0]
        p3_cop = parameters['p3_COP [-]'].array[0]
        p4_cop = parameters['p4_COP [-]'].array[0]
        if (p1_cop * t_in + p2_cop * t_out + p3_cop + p4_cop * t_amb_fix) <= 1.0:
            raise ValueError('COP too low! Increase t_in or decrease t_out.')
        if group_id == 1 or group_id == 4:
            t_in_fix = t_in_hp[0]
        if group_id == 2 or group_id == 5:
            t_in_fix = t_in_hp[1]
        if group_id == 3 or group_id == 6:
            t_in_fix = t_in_hp[2]
        cop_ref = p1_cop * t_in_fix + p2_cop * t_out_fix + p3_cop + p4_cop * t_amb_fix
        p_el_ref = p_th_ref / cop_ref
        parameters.loc[:, 'P_el_h_ref [W]'] = p_el_ref
        parameters.loc[:, 'COP_ref'] = cop_ref
        if group_id == 1:
            try:
                p1_eer = parameters['p1_EER [-]'].array[0]
                p2_eer = parameters['p2_EER [-]'].array[0]
                p3_eer = parameters['p3_EER [-]'].array[0]
                p4_eer = parameters['p4_EER [-]'].array[0]
                eer_ref = p1_eer * 35 + p2_eer * 7 + p3_eer + p4_eer * 35
                parameters.loc[:, 'P_th_c_ref [W]'] = p_el_ref * 0.6852 * eer_ref
                parameters[
                    'P_el_c_ref [W]'] = p_el_ref * 0.6852  # average value from real Heatpumps (P_el35/7 to P_el-7/52)
                parameters.loc[:, 'EER_ref'] = eer_ref
            except:
                pass
    return parameters


def fit_p_th_ref(t_in: int, t_out: int, group_id: int, p_th_set_point: int) -> Any:
    """
    Determines the thermal output power in Watts at reference conditions (T_in = [-7, 0, 10], T_out=52, T_amb=-7)
    for a given set point for a generic heat pump, using a least-square method.

    Parameters
    ----------
    t_in : numeric
        Input temperature (in °C) at the primary side of the heat pump.

    t_out : numeric
        Output temperature (in °C) at the secondary side of the heat pump.

    group_id : numeric
        Group ID for a parameter set, representing an average heat pump of its group.

    p_th_set_point : numeric
        Set point for the thermal output power in Watts.

    Returns
    -------
    p_th : numeric
        Calculated thermal output power in Watts.
    """

    P_0 = [1000]  # starting values
    a = (t_in, t_out, group_id, p_th_set_point)
    p_th, _ = optimize.leastsq(fit_func_p_th_ref, P_0, args=a)
    return p_th


def fit_func_p_th_ref(p_th: int, t_in: int, t_out: int, group_id: int, p_th_set_point: int) -> int:
    """
    Helper function to determine the difference between the given and calculated thermal output power in Watts.

    Parameters
    ----------
    p_th : numeric
        Thermal output power in Watts.

    t_in : numeric
        Input temperature (in °C) at the primary side of the heat pump.

    t_out : numeric
        Output temperature (in °C) at the secondary side of the heat pump.

    group_id : numeric
        Group ID for a parameter set, representing an average heat pump of its group.

    p_th_set_point : numeric
        Set point for the thermal output power in Watts.

    Returns
    -------
    p_th_diff : numeric
        Difference between the given and calculated thermal output power in Watts.
    """

    if group_id == 1 or group_id == 4:
        t_amb = t_in
    else:
        t_amb = -7
    parameters = get_parameters_fit(model='Generic', group_id=group_id, p_th=p_th)
    df = simulate(t_in, t_out - 5, parameters, t_amb)
    p_th_calc = df.P_th.values[0]
    p_th_diff = p_th_calc - p_th_set_point
    return p_th_diff


def get_parameters_fit(model: str, group_id: int = 0, p_th: int = 0) -> pd.DataFrame:
    """
    Helper function for least-square fit of thermal output power at reference set point.

    Parameters
    ----------
    model : str
        Name of the heat pump model.

    group_id : numeric, optional
        Group ID for a parameter set, representing an average heat pump of its group. Default is 0.

    p_th : numeric, optional
        Thermal output power in Watts. Default is 0.

    Returns
    -------
    parameters : pd.DataFrame
        DataFrame containing the model parameters.
    """


    df = pd.read_csv(os.path.abspath(os.path.join(cwd(), '../Libraries/HP_lib_database.csv')),
                     delimiter=',')
    df = df.loc[df['Model'] == model]
    parameters = pd.DataFrame()

    parameters['Model'] = (df['Model'].values.tolist())
    parameters['P_th_h_ref [W]'] = (df['P_th_h_ref [W]'].values.tolist())
    parameters['P_el_h_ref [W]'] = (df['P_el_h_ref [W]'].values.tolist())
    parameters['COP_ref'] = (df['COP_ref'].values.tolist())
    parameters['Group'] = (df['Group'].values.tolist())
    parameters['p1_P_th [1/°C]'] = (df['p1_P_th [1/°C]'].values.tolist())
    parameters['p2_P_th [1/°C]'] = (df['p2_P_th [1/°C]'].values.tolist())
    parameters['p3_P_th [-]'] = (df['p3_P_th [-]'].values.tolist())
    parameters['p4_P_th [1/°C]'] = (df['p4_P_th [1/°C]'].values.tolist())
    parameters['p1_P_el_h [1/°C]'] = (df['p1_P_el_h [1/°C]'].values.tolist())
    parameters['p2_P_el_h [1/°C]'] = (df['p2_P_el_h [1/°C]'].values.tolist())
    parameters['p3_P_el_h [-]'] = (df['p3_P_el_h [-]'].values.tolist())
    parameters['p4_P_el_h [1/°C]'] = (df['p4_P_el_h [1/°C]'].values.tolist())
    parameters['p1_COP [-]'] = (df['p1_COP [-]'].values.tolist())
    parameters['p2_COP [-]'] = (df['p2_COP [-]'].values.tolist())
    parameters['p3_COP [-]'] = (df['p3_COP [-]'].values.tolist())
    parameters['p4_COP [-]'] = (df['p4_COP [-]'].values.tolist())

    if model == 'Generic':
        parameters = parameters.iloc[group_id - 1:group_id]
        parameters.loc[:, 'P_th_h_ref [W]'] = p_th
        t_in_hp = [-7, 0, 10]  # air/water, brine/water, water/water
        t_out_fix = 52
        t_amb_fix = -7
        p1_cop = parameters['p1_COP [-]'].array[0]
        p2_cop = parameters['p2_COP [-]'].array[0]
        p3_cop = parameters['p3_COP [-]'].array[0]
        p4_cop = parameters['p4_COP [-]'].array[0]
        if group_id == 1 or group_id == 4:
            t_in_fix = t_in_hp[0]
        if group_id == 2 or group_id == 5:
            t_in_fix = t_in_hp[1]
        if group_id == 3 or group_id == 6:
            t_in_fix = t_in_hp[2]
        cop_ref = p1_cop * t_in_fix + p2_cop * t_out_fix + p3_cop + p4_cop * t_amb_fix
        p_el_ref = p_th / cop_ref
        parameters.loc[:, 'P_el_h_ref [W]'] = p_el_ref
        parameters.loc[:, 'COP_ref'] = cop_ref
    return parameters


def simulate(t_in_primary: Union[float, np.ndarray], t_in_secondary: Union[float, np.ndarray], parameters,
             t_amb: Union[float, np.ndarray], mode: int = 1, p_th_min: Union[float, np.ndarray] = 0) -> dict:
    """
    Performs the simulation of the heat pump model.

    Parameters
    ----------
    t_in_primary : numeric or iterable (e.g. pd.Series)
        Input temperature (in °C) on the primary side (air, brine, water).

    t_in_secondary : numeric or iterable (e.g. pd.Series)
        Input temperature (in °C) on the secondary side from heating storage or system.

    parameters : pd.DataFrame
        DataFrame containing the heat pump parameters from hplib.getParameters().

    t_amb : numeric or iterable (e.g. pd.Series)
        Ambient temperature (in °C) of the air.

    mode : int
        Set to 1 for heating mode, 2 for cooling mode.

    P_th_min : numeric
        Minimum thermal power output in Watts. Inverter heat pumps increase electrical power input. At maximum electrical input, an electrical heating rod turns on.

    Returns
    -------
    df : pd.DataFrame
        DataFrame with the following columns:
        T_in = Input temperature (in °C) at the primary side of the heat pump.
        T_out = Output temperature (in °C) at the secondary side of the heat pump.
        T_amb = Ambient / Outdoor temperature (in °C).
        COP = Coefficient of Performance.
        EER = Energy Efficiency Ratio.
        P_el = Electrical input power in Watts.
        P_th = Thermal output power in Watts.
        m_dot = Mass flow at the secondary side of the heat pump, in kg/s.
    """

    delta_t = 5  # Inlet temperature is supposed to be heated up by 5 K
    cp = 4200  # J/(kg*K), specific heat capacity of water
    group_id = parameters['Group'].array[0]
    p1_p_el_h = parameters['p1_P_el_h [1/°C]'].array[0]
    p2_p_el_h = parameters['p2_P_el_h [1/°C]'].array[0]
    p3_p_el_h = parameters['p3_P_el_h [-]'].array[0]
    p4_p_el_h = parameters['p4_P_el_h [1/°C]'].array[0]
    p1_cop = parameters['p1_COP [-]'].array[0]
    p2_cop = parameters['p2_COP [-]'].array[0]
    p3_cop = parameters['p3_COP [-]'].array[0]
    p4_cop = parameters['p4_COP [-]'].array[0]
    p_el_ref = parameters['P_el_h_ref [W]'].array[0]
    p_th_ref = parameters['P_th_h_ref [W]'].array[0]
    try:
        p1_eer = parameters['p1_EER [-]'].array[0]
        p2_eer = parameters['p2_EER [-]'].array[0]
        p3_eer = parameters['p3_EER [-]'].array[0]
        p4_eer = parameters['p4_EER [-]'].array[0]
        p1_p_el_c = parameters['p1_P_el_c [1/°C]'].array[0]
        p2_p_el_c = parameters['p2_P_el_c [1/°C]'].array[0]
        p3_p_el_c = parameters['p3_P_el_c [-]'].array[0]
        p4_p_el_c = parameters['p4_P_el_c [1/°C]'].array[0]
        p_el_col_ref = parameters['P_el_c_ref [W]'].array[0]
    except:
        p1_eer = np.nan
        p2_eer = np.nan
        p3_eer = np.nan
        p4_eer = np.nan
        p1_p_el_c = np.nan
        p2_p_el_c = np.nan
        p3_p_el_c = np.nan
        p4_p_el_c = np.nan
        p_el_col_ref = np.nan

    if mode == 2 and group_id > 1:
        raise ValueError('Cooling is only possible with heat pumps of group id = 1.')

    t_in = t_in_primary  # info value for dataframe
    if mode == 1:
        t_out = t_in_secondary + delta_t  # Inlet temperature is supposed to be heated up by 5 K
        eer = 0
    if mode == 2:  # Inlet temperature is supposed to be cooled down by 5 K
        t_out = t_in_secondary - delta_t
        cop = 0
    # for subtype = air/water heat pump
    if group_id in (1, 4):
        t_amb = t_in
    t_ambient = t_amb
    # for regulated heat pumps
    if group_id in (1, 2, 3):
        if mode == 1:
            cop = p1_cop * t_in + p2_cop * t_out + p3_cop + p4_cop * t_amb
            p_el = p_el_ref * (p1_p_el_h * t_in
                               + p2_p_el_h * t_out
                               + p3_p_el_h
                               + p4_p_el_h * t_amb)
            if group_id == 1:
                if isinstance(t_in, np.ndarray):
                    t_in = np.full_like(t_in, -7)
                else:
                    t_in = -7
                t_amb = t_in

            elif group_id == 2:
                if isinstance(t_amb, np.ndarray):
                    t_amb = np.full_like(t_amb, -7)
                else:
                    t_amb = -7
            p_el_25 = 0.25 * p_el_ref * (p1_p_el_h * t_in
                                         + p2_p_el_h * t_out
                                         + p3_p_el_h
                                         + p4_p_el_h * t_amb)
            if isinstance(p_el, np.ndarray):
                p_el = np.where(p_el < p_el_25, p_el_25, p_el)
            elif p_el < p_el_25:
                p_el = p_el_25

            p_th = p_el * cop

            if isinstance(cop, np.ndarray):
                # turn on heating rod and compressor
                p_el = np.where((cop > 1) & (p_th < p_th_min) & (p_el_ref < p_th_min / cop), p_el_ref + p_th_ref, p_el)
                p_th = np.where((cop > 1) & (p_th < p_th_min) & (p_el_ref < p_th_min / cop), p_el_ref * cop + p_th_ref,
                                p_th)
                # increase electrical power for compressor
                p_el = np.where((cop > 1) & (p_th < p_th_min) & (p_el_ref > p_th_min / cop), p_th_min / cop, p_el)
                p_th = np.where((cop > 1) & (p_th < p_th_min) & (p_el_ref > p_th_min / cop), p_th_min, p_th)
                # only turn on heating rod
                p_el = np.where(cop <= 1, p_th_ref, p_el)
                p_th = np.where(cop <= 1, p_th_ref, p_th)
                cop = p_th / p_el
            else:
                if cop <= 1:
                    cop = 1
                    p_el = p_th_ref
                    p_th = p_th_ref
                elif p_th < p_th_min:
                    if p_el_ref > p_th_min / cop:
                        p_el = p_th_min / cop
                        p_th = p_th_min
                    else:
                        p_el = p_el_ref + p_th_ref
                        p_th = p_el_ref * cop + p_th_ref
                        cop = p_th / p_el

        if mode == 2:
            eer = (p1_eer * t_in + p2_eer * t_out + p3_eer + p4_eer * t_amb)
            if isinstance(t_in, np.ndarray):
                t_in = np.where(t_in < 25, 25, t_in)
            elif t_in < 25:
                t_in = 25
            t_amb = t_in
            p_el = (p1_p_el_c * t_in + p2_p_el_c * t_out + p3_p_el_c + p4_p_el_c * t_amb) * p_el_col_ref
            if isinstance(p_el, np.ndarray):
                eer = np.where(p_el < 0, 0, eer)
                p_el = np.where(p_el < 0, 0, p_el)
            elif p_el < 0:
                eer = 0
                p_el = 0
            p_th = -(eer * p_el)
            if isinstance(eer, np.ndarray):
                p_el = np.where(eer <= 1, 0, p_el)
                p_th = np.where(eer <= 1, 0, p_th)
                eer = np.where(eer <= 1, 0, eer)
            elif eer < 1:
                eer = 0
                p_el = 0
                p_th = 0

    # for subtype = On-Off
    elif group_id in (4, 5, 6):
        p_el = (p1_p_el_h * t_in
                + p2_p_el_h * t_out
                + p3_p_el_h
                + p4_p_el_h * t_amb) * p_el_ref

        cop = p1_cop * t_in + p2_cop * t_out + p3_cop + p4_cop * t_amb

        p_th = p_el * cop

        if isinstance(cop, np.ndarray):
            p_el = np.where((cop > 1) & (p_th < p_th_min), p_el + p_th_ref, p_el)
            p_th = np.where((cop > 1) & (p_th < p_th_min), p_th + p_th_ref, p_th)
            p_el = np.where(cop <= 1, p_th_ref, p_el)
            p_th = np.where(cop <= 1, p_th_ref, p_th)
            cop = p_th / p_el

        else:
            if cop <= 1:
                cop = 1
                p_el = p_th_ref
                p_th = p_th_ref
            elif p_th < p_th_min:
                p_th = p_th + p_th_ref
                p_el = p_el + p_th_ref
                cop = p_th / p_el

    # massflow
    m_dot = abs(p_th / (delta_t * cp))

    # round
    result = pd.DataFrame()

    result['T_in'] = [t_in_primary]
    result['T_out'] = [t_out]
    result['T_amb'] = [t_ambient]
    result['COP'] = [cop]
    result['EER'] = [eer]
    result['P_el'] = [p_el]
    result['P_th'] = [p_th]
    result['m_dot'] = [m_dot]
    return result


def same_built_type(modelname: string) -> list:
    """
    Returns all models which have the same parameters but different names.

    Parameters
    ----------
    modelname : str
        The model name as listed in the HP_lib_database.csv

    Returns
    -------
    same_built : list
        List of all models with the same fitting parameters.
    """


    same_built = pd.read_pickle(cwd() + r'/same_built_type.pkl')[modelname]
    return (same_built)


def cwd():
    """
    Returns the parent directory.

    Returns
    -------
    parent_dir : str
        Path to the parent directory.
    """

    real_path = os.path.realpath(__file__)
    dir_path = os.path.dirname(real_path)
    return dir_path
