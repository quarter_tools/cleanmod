import pandas as pd
import os
import glob
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
import logging


def create_output_files(time, district_with_results, directory):
    ### district
    outputs_df = pd.DataFrame(index=time.index_timesteps_run)
    list_renovation_cost = ['renovation_cost_construction',
                            'renovation_cost_hp',
                            'renovation_cost_pv',
                            'renovation_cost_total']
    for output in district_with_results.output_names:
        # fill dataframe with timeseries of single outputs
        outputs_df[output] = getattr(district_with_results, output)
    for output in list_renovation_cost:
        # fill dataframe with timeseries of single outputs
        outputs_df.loc[time.index_timesteps_run[0], output] = getattr(district_with_results, output)
    # fill dataframe with sums of timeseries values
    sums_district_df = outputs_df.sum()
    # add renovation costs
    # sums_district_df['renovation_costs_construction'] = district_with_results.renovation_costs_construction
    # sums_district_df['renovation_costs_hp'] = district_with_results.renovation_costs_hp
    # sums_district_df['renovation_costs_pv'] = district_with_results.renovation_costs_pv
    # sums_district_df['renovation_costs_total'] = district_with_results.renovation_costs_total
    # create csv
    outputs_df.to_csv(os.path.join(directory, 'district_timeseries.csv'))
    sums_district_df.to_csv(os.path.join(directory, 'district_sums.csv'))

    # export the outputs for power grid simulation
    building_list = district_with_results.time_series_power_grid['building_list']
    # outputs_df_grid = pd.DataFrame(index=time.index_timesteps_run)
    # for building in building_list:
    #     for output in ['load', 'sgen']:
    #         elm_name = (building, output)
    #         outputs_df_grid[elm_name] = district_with_results.time_series_power_grid[elm_name]
    #
    # outputs_df_grid.to_csv(os.path.join(directory, 'power_grid_timeseries.csv'))

    # export the outputs for each building
    for building in building_list:
        district_with_results.time_series_buildings[building].\
            to_csv(os.path.join(directory, f'time_series_{building}.csv'))

    # TODO - Priority 1 (deadline: 19.05.2023) - Responsible "MB" - comment by = MB - Check which output is needed and decide whether to activate the code below
    # [MB] the code below has been deactivated only to save space and speed up the simulation during developmente of
    # integration of non resi loads. It can be activated again when needed.

    """
    ### buildings
    for building in district_with_results.buildings:

        outputs_df = pd.DataFrame(index=time.index_timesteps_run)
        for output in ['Qh', 'Qc']:   # #TODO - Priority ? (deadline: ?) - Responsible "ZY" - comment by = ? - update to final output list at building level
            # fill dataframe with timeseries of single outputs
            outputs_df[output] = getattr(building, output)
        # fill dataframe with sums of timeseries values
        sums_df = outputs_df.sum()
        # create csv
        #outputs_df.to_csv(os.path.join(directory, f'bid{building.building_code}_timeseries.csv'))
        sums_df.to_csv(os.path.join(directory, f'bid{building.building_code}_sums.csv'))

        ### zones
        for zone in building.zones:
            outputs_df = pd.DataFrame(index=time.index_timesteps_run)
            for output in district_with_results.output_names_zone:
                # fill dataframe with timeseries of single outputs
                outputs_df[output] = getattr(zone, output)
            # fill dataframe with sums of timeseries values
            sums_df = outputs_df.sum()
            # create csv
            outputs_df.to_csv(os.path.join(directory, f'zid{zone.zid}_timeseries.csv'))
            sums_df.to_csv(os.path.join(directory, f'zid{zone.zid}_sums.csv'))
    """
    pass


def listdir_nohidden(path):
    return glob.glob(os.path.join(path, '*'))


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class InputError(Error):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


def create_interpolated_cdf(x, p):
    """
    Returns the cummulative density function (cdf) as a vector of len(x) values
    given a probability function, p. The cdf interpolated in case the probability
    function has less values than the given x vector.
    """

    # calculate cdf
    cdf = np.cumsum(p)

    # normalize
    cdf_max = max(cdf)
    for iii, val in enumerate(cdf):
        cdf[iii] = val / cdf_max

    # create interpolators so that there is a cdf value for every x-value
    cdf = interpolate.interp1d(cdf, x,
                               fill_value=(x[0], x[-1]), bounds_error=False)

    return cdf


def calc_validation_confidence(Timeseries, Reference, CalcType='Temp', Startday=0, Endday=60):
    x = Timeseries
    # if CalcType == 'Temp':
    # x = x[1:] # check this again! refer to start conditions of optimization

    p = 0  # number of adjustable model parameters; recomm for calibration: p=0
    RefValues = Reference['Tz_TRNSYS'][24 * Startday:24 * Endday]
    CalcValues = x[24 * Startday:24 * Endday]

    n = 24 * (Endday - Startday)
    Mean_TR = sum(RefValues) / n

    # Normalized Mean Bias Error
    NMBE = 1 / Mean_TR * sum(RefValues - CalcValues) / n * 100  # [%]
    print(f'NMBE(Day{Startday} to {Endday}) = {NMBE}')
    logging.debug(f'NMBE(Day{Startday} to {Endday}) = {NMBE}')
    # Coefficient of the Variation of the Root Mean Square Error
    CVRMSE = 1 / Mean_TR * np.sqrt(sum((RefValues - CalcValues) ** 2) / (n - p)) * 100  # [%]
    print(f'CVRMSE(Day{Startday} to {Endday}) = {CVRMSE}')
    logging.debug(f'CVRMSE(Day{Startday} to {Endday}) = {CVRMSE}')


def plot_single_days(list_of_pdseries, title, starthour, days_rel=[1, 2, 10]):
    '''list_of_pdseries has to match 2 because of label_list'''
    plt.figure()
    plt.rcParams['figure.figsize'] = [15, 3]
    fig, axs = plt.subplots(1, 3)
    for num, day in enumerate(days_rel):
        start_rel, end_rel = (day - 1) * 24, day * 24
        start_abs, end_abs = starthour + start_rel, starthour + end_rel
        axs[num].title.set_text(f'Day {day}')
        start = start_abs
        end = end_abs
        label_list = ['calculated', 'reference']
        for i, y in enumerate(list_of_pdseries):
            axs[num].plot(y.loc[start:end], label=label_list[i])

    plt.legend()
    # add title
    plt.show
    logging.debug('Showing Plot - Single days')


def plot_timeperiod(list_of_pdseries, title, starthour, startday_rel=0, endday_rel=31):
    '''list_of_pdseries has to be <=6 because of label_list'''
    fig = plt.figure()
    plt.rcParams['figure.figsize'] = [15, 3]
    start_rel, end_rel = startday_rel * 24, endday_rel * 24
    start_abs, end_abs = starthour + start_rel, starthour + end_rel
    start = start_abs
    end = end_abs
    label_list = ['calculated Tz', 'reference Tz', 'outdoor air temp', 'Taeq', 'TEW', 'TIW']
    for i, y in enumerate(list_of_pdseries):
        plt.plot(y.loc[start:end], label=label_list[i])
    plt.legend()
    fig.suptitle(f'Day {startday_rel} - {endday_rel}')
    plt.show
    logging.debug('Showing Plot - timeperiod')


def plot_results(zoneid_list, outputs_zone_dictdf, outputs_district_df,
                 data_in, baseline, starthour, precalc):
    if precalc:
        # comparison with trnsys values
        for zid in zoneid_list:
            plot_single_days([outputs_zone_dictdf[zid]['Qhc'], data_in[zid]['Qheat_trnsys']],
                             'Qhc_trnsys_comparison', starthour, days_rel=[1, 2, 10])
            plot_single_days([outputs_zone_dictdf[zid]['Tz'], data_in[zid]['Tz_TRNSYS']],
                             'Tz_trnsys_comparison', starthour, days_rel=[1, 2, 10])
            plot_timeperiod([outputs_zone_dictdf[zid]['Tz'], data_in[zid]['Tz_TRNSYS'],
                             data_in[zid]['Tamb'], data_in[zid]['Taeq']], 'Temperatures_trnsys_comparison',
                            starthour, startday_rel=0, endday_rel=31)
    else:
        # comparison with precalc(baseline) values
        # debugging: print(baseline)
        for zid in zoneid_list:
            plot_single_days([outputs_zone_dictdf[zid]['Qhc'], baseline[zid]['Qhc']],
                             'Qhc_baseline_comparison', starthour, days_rel=[1, 2, 10])
            plot_single_days([outputs_zone_dictdf[zid]['Tz'], baseline[zid]['Tz']],
                             'Qhc_baseline_comparison', starthour, days_rel=[1, 2, 10])
            plot_timeperiod(
                [outputs_zone_dictdf[zid]['Tz'], baseline[zid]['Tz'], data_in[zid]['Tamb'], data_in[zid]['Taeq'],
                 outputs_zone_dictdf[zid]['TEW'], outputs_zone_dictdf[zid]['TIW']],
                'Temperatures_baseline_comparison', starthour, startday_rel=0, endday_rel=31)
        plot_single_days([outputs_district_df['gen_district_elec'], outputs_district_df['consumpt_district']],
                         'local_generation_production_comparison', starthour, days_rel=[1, 2, 10])


'''def create_xlsx(casename, calc_part, now, zoneid_list, outputs_district_df,
                outputs_district_totals_df, outputs_zone_dictdf):
    # create csv in Results folder
    mydir = os.path.join(os.getcwd(), 'CleanMod', 'Results', now)
    try:
        os.makedirs(mydir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise  # This was not a "directory exist" error..
    fname = casename+'_'+calc_part
    outputs_district_df.to_excel(os.path.join(mydir, '{}_district.xlsx'.format(fname)),
                                 index = False)
    outputs_district_totals_df.to_excel(os.path.join(mydir, '{}_district_totals.xlsx'.format(fname)), index = False)
    for zid in zoneid_list:
        outputs_zone_dictdf[zid].to_excel(os.path.join(mydir, '{0}_{1}.xlsx'.format(fname,zid)), 
                                                   index = False)
    pass'''
