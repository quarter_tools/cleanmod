import math
import numpy as np
import pandas as pd
import os
import shutil
import csv
from scipy.io import loadmat
from random import randint

from ruamel.yaml import YAML
from . import util as util


def create_synhouseholds(zones_df, uep_region_name, folder):
    """Create file with household data matching the UEP format of synhouseholds.

    Args:
        zones_df (dataframe): zones data.
        uep_region_name (str): name of the simulated study area matching the input name given in the UEP config file.
        folder (str): name of folder where to store the created file.

    Returns:
        Nothing.
    """

    synhouseholds_df = zones_df[['hid', 'bid', 'household_number', 'heated_area', 'number_occupants']]
    csv_name = folder + '/SynHouseholds_' + uep_region_name + '.csv'
    synhouseholds_df.to_csv(csv_name, sep=';', index=False)

    first_line = [uep_region_name]
    with open(csv_name, "r") as infile:
        reader = list(csv.reader(infile))
        reader.insert(0, first_line)

    with open(csv_name, "w") as outfile:
        writer = csv.writer(outfile)
        for line in reader:
            writer.writerow(line)

    pass

def calc_and_create_syncity(buildings_df, buildingcomponent_df_dict, zones_df, uep_region_name,
                            folder, file_dhw):
    """Calc missing building attributes and create file with building data matching the UEP format of syncity.

    Args:
        buildings_df (dataframe): buildings data.
        buildingcomponent_df_dict (dict): buildingcomponents as dataframe of each zone (zoneid as key).
        zones_df (dataframe): zones data.
        uep_region_name (str): name of the simulated study area matching the input name given in the UEP config file.
        folder (str): name of folder where to store the created file.

    Returns:
        Nothing.
    """
    with open(f"Step_0_config.yaml") as config_file:
        config = YAML().load(config_file)
    syncity_df = buildings_df  # index is building_code so far
    # initialise new columns:
    syncity_df['area_corr_factor'] = 1  # TODO - Priority 4 (deadline: until 01.09.2023) - Responsible "MB" -  comment by = MB- update value after separation of heated_area and storey_area
    syncity_df['heated_area'] = 0
    syncity_df['window_area'] = 0  # zones!
    syncity_df['dwelling_size'] = 0
    syncity_df['ref_level_roof'] = 0
    syncity_df['ref_level_wall'] = 0
    syncity_df['ref_level_floor'] = 0
    syncity_df['ref_level_window'] = 0
    syncity_df['U'] = 0
    syncity_df['V'] = 0
    syncity_df['C'] = 0
    syncity_df['Tau'] = 0
    syncity_df['Tset'] = config['building_settings']['Tz_avg']
    syncity_df['act_start'] = 0
    syncity_df['act_end'] = 23
    syncity_df['hw_demand_day'] = 0
    syncity_df['hw_tank_cap'] = 0

    for index, row in syncity_df.iterrows():
        my_heated_area = calc_heated_area(index, zones_df)
        syncity_df.loc[index, 'heated_area'] = my_heated_area
        my_hw_demand_day, my_hw_tank_cap = calc_dhw_parameters(my_heated_area, file_dhw)
        syncity_df.loc[index, 'hw_demand_day'] = my_hw_demand_day
        syncity_df.loc[index, 'hw_tank_cap'] = my_hw_tank_cap
        syncity_df.loc[index, 'dwelling_size'] = calc_average_dwelling_size(row)
        my_window_area, my_ref_levels, my_U, my_V, my_C, my_Tau = calc_params_dependent_on_buildingcomponents(index,
                                                                                            buildingcomponent_df_dict,
                                                                                            zones_df)
        syncity_df.loc[index, 'window_area'] = my_window_area
        syncity_df.loc[index, 'ref_level_roof'] = my_ref_levels['roof']
        syncity_df.loc[index, 'ref_level_wall'] = my_ref_levels['wall']
        syncity_df.loc[index, 'ref_level_floor'] = my_ref_levels['floor']
        syncity_df.loc[index, 'ref_level_window'] = my_ref_levels['window']
        syncity_df.loc[index, 'U'] = my_U
        syncity_df.loc[index, 'V'] = my_V
        syncity_df.loc[index, 'C'] = my_C
        syncity_df.loc[index, 'Tau'] = my_Tau

    csv_name = folder + '/SynCity_' + uep_region_name + '.csv'
    syncity_df.to_csv(csv_name, sep=';', index=False)

    first_line = [uep_region_name]
    with open(csv_name, "r") as infile:
        reader = list(csv.reader(infile))
        reader.insert(0, first_line)

    with open(csv_name, "w") as outfile:
        writer = csv.writer(outfile)
        for line in reader:
            writer.writerow(line)

    pass

def calc_params_dependent_on_buildingcomponents(building_code, building_component_data_dict, zones_data):
    """Calculate parameters for 1R1C model of UEP incl. refurbishment levels.

    Args:
        building_code (str): buildingid from grasshopper script naming convention.
        building_component_data_dict (dict): buildingcomponents as dataframe of each zone (zoneid as key).
        zones_data (dataframe): zones data.

    Returns:
        window_area, ref_levels, U, V, C, Tau.
    """
    window_area = 0
    ref_levels = {'roof': 1, 'wall': 1, 'floor': 1, 'window': 1}
    U = 0   # Transmission losses (U-value), U [W/K]
    V = 0   # Ventilation losses, V [W/K]
    C = 0   # Thermal capacitance, C [J/K]
    # Tau --> see below

    # looping over zones and components in order to sum area, U, V, C values but refreshing ref_levels each \
    # time a component matches the resp. component type (which leads to sticking with the value of just the last \
    # component of this type; okay as long as whole building fulfills one renovation variant
    for zid in list(zones_data.loc[zones_data['building_code'] == building_code].index.values):
        bc_zid = building_component_data_dict[zid]
        for x, row in bc_zid.iterrows():
            if row['AREA'] == row['AREA']:
                area_component = row['AREA']

            bc_type = row['COMPONENT'].split('_')[1]
            x=0
            #if row['COMPONENT'].split('_')[2] == 'W':
            #if row['COMPONENT'].split('.')[1] == 'Window':
            if bc_type == 'W':
                window_area += area_component
                #ref_levels['window'] = ref_level_translator[row['COMPONENT'].split('_')[1]]
                ref_levels['window'] = 1  # Provisional solution for debugging and test model -MB
            else:
                if np.isnan(row['THICKNESS']):
                    pass
                else:
                    C += row['THICKNESS'] * area_component * row['DENSITY'] * row['CAPACITY'] * 1000
            #if row['COMPONENT'].split('_')[2][:2] == 'ER':
            #if row['COMPONENT'].split('.')[1] == 'Roof':
            if bc_type == 'ER':
                #ref_levels['roof'] = ref_level_translator[row['COMPONENT'].split('_')[1]]
                ref_levels['roof'] = 1  # Provisional solution for debugging and test model -MB
            #if row['COMPONENT'].split('_')[2][:2] == 'EW':
            #if row['COMPONENT'].split('.')[1] == 'Wall':
            if bc_type == 'EW' or bc_type == 'IW':
                #ref_levels['wall'] = ref_level_translator[row['COMPONENT'].split('_')[1]]
                ref_levels['wall'] = 1  # Provisional solution for debugging and test model -MB
            #if row['COMPONENT'].split('_')[2][:2] == 'GF':
            #if row['COMPONENT'].split('.')[1] == 'GF':
            if bc_type == 'DF':
                #ref_levels['floor'] = ref_level_translator[row['COMPONENT'].split('_')[1]]
                ref_levels['floor'] = 1  # Provisional solution for debugging and test model -MB
            if int(row['LAYER_NR']) == 1:
                with open(f"Step_0_config.yaml") as config_file:
                    config = YAML().load(config_file)
                U_new = row['UVALUE'] * area_component
                #if row['COMPONENT'].split('_')[2][:2] == 'GF':
                #if row['COMPONENT'].split('.')[1] == 'GF':
                if bc_type == 'GF':
                    U_new *= 0.5
                    x=0
                U += U_new
                #if row['COMPONENT'].split('_')[2][:2] in ['IF', 'GF', 'EF']:
                #if row['COMPONENT'].split('.')[1] in ['IF', 'GF', 'EF']:
                if bc_type == 'IF' or bc_type == 'GF' or bc_type == 'EF':
                    # calculate ventilation losses, V [W/K]
                    # air_specific_heat_capacity * air_density = 0.34 Wh/(m3 K)
                    # simplification: general floor-to-floor height of 3m assumed for ventilation losses
                    # TODO - Priority 4 (deadline: 01.09.2023) - Responsible "MB" -  comment by = ? - include floor-to-floor height in building properties and add it to building csv
                    V += config['building_settings']['acr_global'] * 0.34 * area_component * 3
                    x=0

    # Time constant, Tau [s]
    if U != 0:
        Tau = C / (U + V)
    else:
        Tau = np.nan

    return window_area, ref_levels, U, V, C, Tau

def calc_heated_area(building_code, zones_data):
    """Calculate heated area of building as sum of heated area of zones."""
    heated_area = zones_data.loc[zones_data['building_code'] == building_code, 'heated_area'].sum()
    return heated_area

def calc_average_dwelling_size(row_values):
    """Calculate average dwelling_size by dividing storey_area by number of dwellings."""

    dwelling_size = row_values['storey_area'] / row_values['dwellings']

    return dwelling_size

def calc_dhw_parameters(heated_area, dhw_demand_file):
    """Calculate parameters for energy demand resulting from domestic hot water.

    Procedure copied from UEP and remodelled within one single function.
    Returns the daily hot water demand by getting a random value from the cdf function based on the statistics
    from VDI 3807-3 (specific dhw demand in m3/m2 of living area); Calculates size of hot water tank.
    Size is X times the calculated daily demand.

    Args:
        heated_area (float): heated area of whole building.
        dhw_demand_file (str): name of input csv with dhw probability distribution for residential buildings.

    Returns:
        daily hot water demand and tank capacity of respective building.
    """

    ### SPECIFIC DAILY HOT WATER DEMAND [m3/m2 of living area]
    ### From VDI 3807-3, section 6.2
    dhw_demand_data = np.genfromtxt(dhw_demand_file, dtype='float', delimiter=';', skip_header=2, usecols=range(0, 2))

    # Daily specific dhw consumption [m3/m2 of living area]	as cdf
    x = [i[0] for i in dhw_demand_data]
    p = [i[1] for i in dhw_demand_data]
    dhw_cdf = util.create_interpolated_cdf(x, p)

    # calculate specific dhw demand
    rand_num = np.random.uniform(0, 1, 1)
    specific_dhw = dhw_cdf(rand_num)[0]

    # calculate building_df_row dhw demand
    hw_demand_day = specific_dhw * heated_area

    # set tank capacity as function of daily hot water demand limit
    X = 1.5
    hw_tank_cap = X * hw_demand_day

    return hw_demand_day, hw_tank_cap

