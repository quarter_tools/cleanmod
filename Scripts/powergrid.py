import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os

import pandapower as pp
from pandapower import plotting as plot
from pandapower import networks as nw
import simbench as sb
from matplotlib.pyplot import get_cmap
from matplotlib.colors import Normalize
import pandapower.plotting.plotly as pplotly
from pandapower.plotting.plotly import simple_plotly
from pandapower.plotting.plotly import vlevel_plotly
from pandapower.plotting.plotly import pf_res_plotly
from pandas import Series


class POWERGRID:

    def __init__(self, sb_code):
        # self.input_data = input_data_loc
        self.sb_code = sb_code
        self.net = None
        self.results = None
        self.profiles = None
        self.time_steps = None

    def init_grid(self):
        # choose which grid structure is to be used
        # sb_code1 = "1-LV-rural3--1-no_sw"
        # sb_code1 = "1-LV-semiurb4--0-no_sw"
        sb_code1 = "1-LV-urban6--0-sw"
        # obtain the grid topology
        self.net = sb.get_simbench_net(self.sb_code)

        # preload the profiles for load and generators
        self.profiles = sb.get_absolute_values(self.net, profiles_instead_of_study_cases=True)

        # determine the range of time steps to be simulated

        # build the results dictionary
        self.results = pd.DataFrame([], index=self.time_steps, columns=["PV_power", "line_loading", 'voltage_pu_max',
                                                          'voltage_pu_min', 'Consumer', 'ext_grid_Power', 'line_mean'])
        # results_flex = pd.DataFrame([], index=time_steps, columns=["PV_power", "line_loading", 'voltage_pu_max',
        #                                             'voltage_pu_min', 'Consumer', 'ext_grid_Power', 'line_mean'])

    def set_timesteps(self, timesteps):
        self.time_steps = range(timesteps[0], timesteps[1])

    # read the parameters/load profiles from spreadsheets
    def apply_absolute_values(self, time_step):
        """
        :param net: net instance
        :param absolute_values_dict: profiles saved in spreasheets
        :param case_or_time_step: decide which profile regarding to time step should be read
        :return: none
        """
        for elm_param in self.profiles.keys():
            if self.profiles[elm_param].shape[1]:
                elm = elm_param[0]
                param = elm_param[1]
                if elm == 'sgen':
                    self.net[elm].loc[:, param] = self.profiles[elm_param].loc[time_step]
                    self.net[elm].loc[:, 'q_mvar'] = 0
                    self.net[elm].loc[:, 'sn_mva'] = self.profiles[elm_param].loc[time_step]
                else:
                    self.net[elm].loc[:, param] = self.profiles[elm_param].loc[time_step]

    # use the time series from CleanMod simulation
    def apply_time_series(self, time_series):
        """
        :param net: net instance
        :param absolute_values_dict: profiles saved in spreasheets
        :param case_or_time_step: decide which profile regarding to time step should be read
        :return: none
        """
        # obtain the buidling_id list
        nr_buildings = int(len(time_series.columns)/2)
        id_buildings = np.unique([eval(id_bul)[0] for id_bul in time_series.columns])
        idx_pos_load = []

        # assign each building with a bus location
        for i in range(nr_buildings):
            bus_nr = i + 1
            idx = self.net.load.loc[self.net.load['bus'] == bus_nr].index[0]
            idx_pos_load.append(idx)
            self.net.load.loc[idx, 'name'] = id_buildings[i]

        # expand the sgen list
        if len(self.net.sgen.index) < len(self.net.load.index):
            # modify net attributes
            column_const = self.net.sgen[self.net.sgen.columns[5:]].iloc[0]
            self.net.sgen = pd.DataFrame(columns=self.net.sgen.columns, index=self.net.load.index)
            self.net.sgen['name'] = self.net.load['name']
            self.net.sgen['bus'] = self.net.load['bus']
            self.net.sgen[self.net.sgen.columns[5:]] = column_const

            # modify profiles
            column_const = self.profiles['sgen', 'p_mw'][0]
            self.profiles['sgen', 'p_mw'] = pd.DataFrame(columns=self.profiles['load', 'p_mw'].columns,
                                                         index=self.profiles['load', 'p_mw'].index)
            for column_name in self.profiles['sgen', 'p_mw'].columns:
                self.profiles['sgen', 'p_mw'][column_name] = column_const * 0

        for building_nr in range(nr_buildings):
            for elm_param in self.profiles.keys():
                if elm_param == ('load', 'p_mw'):
                    self.profiles[elm_param].loc[self.time_steps, idx_pos_load[building_nr]] = \
                        time_series[str((id_buildings[building_nr], 'load'))].values * 10e-6
                elif elm_param[0] == 'sgen':
                    self.profiles[elm_param].loc[self.time_steps, idx_pos_load[building_nr]] = \
                        time_series[str((id_buildings[building_nr], 'sgen'))].values * 10e-6

        # apply the flexibility in one scenario
    def apply_flexibility(self, net, load_id, power, start_time, end_time, type, time_step):
        """
        :param net: net instance
        :param load_id: load position IDs which are available the results and excel tables
        :param power: generation or consumption power; unit: [MW]
        :param start_time: starting time step
        :param end_time: ending time step
        :param type: which type it is. It could be "sgen", "gen", "load" ...
        :param time_step: fixed input (always as time_step), which is used to get the current time step from looping
        :return:none
        """
        if start_time <= time_step <= end_time:
            pow_flex = net[type].loc[net[type]['name'] == load_id, 'p_mw']
            net[type].loc[net[type]['name'] == load_id, 'p_mw'] = pow_flex + power

    def simulate(self):
        # perform the simulation of power flow

        for time_step in self.time_steps:
            self.apply_absolute_values(time_step)
            # modify certain profiles for scenarios in the future
            # (e.g. the total pv capacity will increase in next 30 years)
            self.net['sgen'].loc[:, 'p_mw'] = self.net['sgen'].loc[:, 'p_mw'] * 1
            # perform the power flow calculation for reference case
            pp.runpp(self.net)
            # load the results and save them into dictionary
            self.results.loc[time_step, "PV_power"] = self.net.res_sgen.p_mw.sum()
            # results.loc[time_step, "line_loading"] = net.res_line.loading_percent[5]
            self.results.loc[time_step, "line_loading"] = self.net.res_line.loading_percent.max()
            self.results.loc[time_step, "voltage_pu_max"] = self.net.res_bus.vm_pu.max()
            self.results.loc[time_step, "voltage_pu_min"] = self.net.res_bus.vm_pu.min()
            self.results.loc[time_step, "Consumer"] = self.net.res_load.p_mw.sum()
            self.results.loc[time_step, "ext_grid_Power"] = self.net.res_ext_grid['p_mw'][0]
            self.results.loc[time_step, "line_mean"] = self.net.res_line.loading_percent.mean()

            # usage of flexibility to change the load/generation profiles, here e.g. for Load 74 and 75
            # self.apply_flexibility(net, 'MV3.101 Load 74', 1200/1000, 100, 250, 'load', time_ste p)
            # self.apply_flexibility(net, 'MV3.101 Load 75', 1500/1000, 350, 600, 'load', time_step)

            # # plot the results
            # results_flex.loc[time_step, "PV_power"] = net.res_sgen.p_mw.sum()
            # # results.loc[time_step, "line_loading"] = net.res_line.loading_percent[5]
            # results_flex.loc[time_step, "line_loading"] = net.res_line.loading_percent.max()
            # results_flex.loc[time_step, "voltage_pu_max"] = net.res_bus.vm_pu.max()
            # results_flex.loc[time_step, "voltage_pu_min"] = net.res_bus.vm_pu.min()
            # results_flex.loc[time_step, "Consumer"] = net.res_load.p_mw.sum()
            # results_flex.loc[time_step, "ext_grid_Power"] = net.res_ext_grid['p_mw'][0]
            # results_flex.loc[time_step, "line_mean"] = net.res_line.loading_percent.mean()

            # possibility to set the marginal price for power generation
            # costeg = pp.create_poly_cost(net, 0, 'ext_grid', cp1_eur_per_mw=10)
            # costgen1 = pp.create_poly_cost(net, 0, 'gen', cp1_eur_per_mw=10)
            # costgen2 = pp.create_poly_cost(net, 1, 'gen', cp1_eur_per_mw=10)

            # perform optimal power flow
            # pp.runopp(net)

            # perform power flow
            # pp.runpp(net)

            # the simple plot
            # simple_plotly(net)

            # show the results with voltage levels
            # vlevel_plotly(net)

    def plot_res(self):
        # show the plot with all the details
        pf_res_plotly(self.net, filename="simbench/result_pg.html")

        # RES_power: power from renewable energy (PV)

        plt.figure()
        plt.subplot(2, 2, 1)
        plt.plot(self.results.loc[:, "PV_power"])
        # plt.plot(results_flex.loc[:, "PV_power"])
        plt.title('RES_Power')
        plt.xlabel('timesteps [15 min]')
        plt.ylabel('Power [MW]')
        plt.legend(['ref', 'flex'])

        # Line_loading: max. ratio of actual current to rated current in the grid
        plt.subplot(2, 2, 2)
        plt.plot(self.results.loc[:, "line_loading"])
        # plt.plot(results_flex.loc[:, "line_loading"])
        plt.title('Line_loading')
        plt.xlabel('timesteps [15 min ]')
        plt.ylabel('Line_loading in percent [%]')
        plt.legend(['ref', 'flex'])

        # max. voltage in unit [p.u.]
        plt.subplot(2, 2, 3)
        plt.plot(self.results.loc[:, "voltage_pu_max"])
        plt.plot(self.results.loc[:, "voltage_pu_min"])
        # plt.plot(results_flex.loc[:, "voltage_pu_max"])
        # plt.plot(results_flex.loc[:, "voltage_pu_min"])
        plt.title('voltage_range', y=-0.01)
        plt.xlabel('timesteps [15 min ]')
        plt.ylabel('Voltage per unit [pu]')
        plt.legend(['voltage_pu_max', 'voltage_pu_min', 'voltage_pu_max_flex', 'voltage_pu_min_flex'])

        # power consumption of commercial buildings and households
        plt.subplot(2, 2, 4)
        plt.plot(self.results.loc[:, "Consumer"], '--')
        # plt.plot(results_flex.loc[:, "Consumer"], '--')
        plt.title('load commercial/household', y=-0.01)
        plt.xlabel('timesteps [15 min ]')
        plt.ylabel('Power [MW]')
        plt.legend(['ref', 'flex'])
        plt.show()

        # power import/export from/to the external grid
        # power flow through the transformer
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.plot(self.results.loc[:, "ext_grid_Power"])
        # plt.plot(results_flex.loc[:, "ext_grid_Power"])
        plt.title('ext_grid_Power')
        plt.xlabel('timesteps [15 min]')
        plt.ylabel('Power [MW]')
        plt.legend(['ref', 'flex'])

        # mean line loading of the whole grid
        plt.subplot(2, 1, 2)
        plt.plot(self.results.loc[:, "line_mean"])
        # plt.plot(results_flex.loc[:, "line_mean"])
        plt.title('line_loading_mean', y=-0.01)
        plt.xlabel('timesteps [15 min]')
        plt.ylabel('line_loading [%]')
        plt.legend(['ref', 'flex'])
        plt.show()
