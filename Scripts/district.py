import pandas as pd
import numpy as np
import logging
import json
import Cleanvelope.CleanMod.Scripts.building as building
import Cleanvelope.CleanMod.Scripts.heatpump as heatpump
import Cleanvelope.CleanMod.Scripts.electric_vehicle as ev
import pyomo.core as pyo
import pyomo.environ as pyoenv
from Cleanvelope.CleanMod.Scripts.progress_bar import app
from random import shuffle, choice, random, randint, choices
import os


class District:
    """ Class for the representation of a district.

    Attributes:
        buildings (objects): contained in the district.

    """

    def __init__(self, config, buildingids, zoneids, time, input_data_loc, nr_load, file_pv_potentials,
                 dir_dhw, scenario_object, scenario_id, calc_type, j, file_geometry_surfaces, renovation_costs_df):

        self.config = config
        self.time_resolution_h = config['simulation_settings']['time_resolution_h']
        self.predict_horizon_h = config['simulation_settings']['predict_horizon_h']
        self.update_interval_h = config['simulation_settings']['update_interval_h']
        self.elec_config = config['simulation_settings']['elec_config']
        self.show_progress = config['progress_bar_setting']['show_progress']
        # self.tariff = config['simulation_settings']['tariff']  # MB 230616 - adaptation to achieve parallelisation
        self.tariff = scenario_object.tariff  # MB 230616 - adaptation to achieve parallelisation
        self.buildingids = buildingids
        self.zoneids = zoneids
        self.time = time
        self.buildings = []
        self.create_buildings_with_zones(time, input_data_loc, file_pv_potentials, dir_dhw, scenario_object,
                                         scenario_id, calc_type, j, file_geometry_surfaces, renovation_costs_df)

        self.generators = []
        self.gen_in_list = []
        self.gen_ratio_in_dict = {}
        self.gen_ratio_out_dict = {}
        self.gen_cap_dict = {}
        # self.results_precalc = {}
        self.sizing_run_completed = False
        self.create_generator_info()
        self.error_logging = []

        if 'pv_gen' in self.elec_config:
            print('--> Adding pv_gen started')
            self.pv_gen = self.sum_pv_gen(self.buildings, time)
            print('--> Adding pv_gen finished')

        if 'non_resi_load' in self.elec_config:
            print('--> Adding non resi load started')
            self.non_resi_load = self.prepare_non_resi(nr_load, time)
            print('--> Adding non resi load finished')

        self.renovation_costs_construction = self.sum_renovation_costs(self.buildings, 'construction')
        self.renovation_costs_hp = self.sum_renovation_costs(self.buildings, 'hp')
        self.renovation_costs_pv = self.sum_renovation_costs(self.buildings, 'pv')
        self.renovation_costs_total = self.sum_renovation_costs(self.buildings, 'total')

    def prepare_non_resi(self, non_resi_profile, time_input):
        """This def FIRST select the data time serie the same way that is done with PV profiles in zone.py (see
        "def select_hh_load_profile_uep"). This is required for the correct input of the data to the optimiser in
        optimisation.py. SECOND, the data is prepared following the same approach as in "def sum_pv_gen" in this script
        district.py """

        # FIRST
        # Selecting the data according to time settings for simulation
        # ------------------------------------------------------------
        mystarthour = time_input.starthour_run
        myendhour = time_input.endhour_predict
        steps_per_hour = time_input.steps_per_hour

        sr_in = non_resi_profile

        # check whether total of rows matches 1 year in given time resolution
        if float(len(sr_in)) != 8760 * steps_per_hour:
            raise Exception('ERROR: resolution of input non-resi time series do not match simulation time resolution.')

        # set new index and ignore given TIME column with the aim of avoiding format-problems
        index_matching_resolution = pd.Index([i for i in np.arange(0, 8760, self.time_resolution_h)])
        sr_in = pd.DataFrame()
        sr_in[0] = non_resi_profile
        sr_in.index = index_matching_resolution

        # prolong df_in if end_hour + predict_horizon exceeds length of data
        if myendhour >= 8760.0:
            overhang = float(myendhour - 8760)
            index_overhang = pd.Index([i for i in np.arange(8760, myendhour, self.time_resolution_h)])
            data_overhang = sr_in[0:overhang - self.time_resolution_h]
            data_overhang.index = index_overhang
            new = pd.concat([sr_in, data_overhang])
            sr_in = new

        # add data to df_in if start_hour lies prior to start of df_in
        if mystarthour < 0.0:
            overhang = float(-mystarthour)
            index_overhang = pd.Index([i for i in np.arange(mystarthour, 0, self.time_resolution_h)])
            data_overhang = sr_in[8760 - overhang:8760 - self.time_resolution_h]
            data_overhang.index = index_overhang
            new = pd.concat([data_overhang, sr_in])
            sr_in = new

        # reduce total time_series to index_time_steps
        non_resi_profile_selected = sr_in[mystarthour:myendhour]

        # SECOND
        # Preparing the data according to time_input.index_timesteps_run
        nr_prof = pd.Series(np.zeros(len(time_input.index_timesteps_run)))
        nr_prof = pd.concat([nr_prof, non_resi_profile_selected], axis=1)
        non_resi_profile_prepared = nr_prof.sum(numeric_only=True, axis=1)

        return non_resi_profile_prepared

    def select_hh_load_profile_uep(self, elec_load_df, my_start_hour, my_end_hour, steps_per_hour):
        """."""

        df_in = elec_load_df

        # check whether total of rows matches 1 year in given time resolution
        if float(len(df_in)) != 8760 * steps_per_hour:
            raise Exception('ERROR: resolution of input household time series do not match simulation time resolution.')

        # set new index and ignore given TIME column with the aim of avoiding format-problems
        index_matching_resolution = pd.Index([i for i in np.arange(0, 8760, self.time_resolution_h)])
        df_in.set_index(index_matching_resolution, inplace=True)

        # prolong df_in if end_hour + predict_horizon exceeds length of data
        if my_end_hour >= 8760.0:
            overhang = float(my_end_hour - 8760)
            index_overhang = pd.Index([i for i in np.arange(8760, my_end_hour, self.time_resolution_h)])
            data_overhang = df_in.loc[0:overhang - self.time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([df_in, data_overhang])
            df_in = new
        # add data to df_in if star_thour lies prior to start of df_in
        if my_start_hour < 0.0:
            overhang = float(-my_start_hour)
            index_overhang = pd.Index([i for i in np.arange(my_start_hour, 0, self.time_resolution_h)])
            data_overhang = df_in.loc[8760 - overhang:8760 - self.time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([data_overhang, df_in])
            df_in = new

        # reduce total time_series to index_time_steps
        elec_load_zone = df_in.loc[my_start_hour:my_end_hour, self.zid]

        return elec_load_zone

    def create_buildings_with_zones(self, time_input, input_data_location, file_pv_potentials, dir_dhw, scenario_object,
                                    scenario_id, calc_type, j, file_geometry_surfaces, renovation_costs_df):

        print('\n--> Creating buildings with zones started')

        buildings_file = input_data_location + '/Building_data/Buildings_extended.csv'
        buildings_df = pd.read_csv(buildings_file)
        # select focus buildings when reduce_to_1_building_with_2_zones = True
        buildings_df = buildings_df.loc[buildings_df['building_code'].isin(self.buildingids)].copy()
        buildings_df.set_index('building_code', inplace=True)

        zones_file = input_data_location + '/Zone_data/Zones.csv'
        zones_df = pd.read_csv(zones_file)
        # select focus zones when reduce_to_1_building_with_2_zones = True
        zones_df = zones_df.loc[zones_df['Zone-ID'].isin(self.zoneids)].copy()
        zones_df.set_index('Zone-ID', inplace=True)

        i = 1
        district_zones_count = 0
        district_zones_nr = len(zones_df.iloc[:, 0].tolist())
        district_buildings_count = 0
        district_buildings_nr = len(buildings_df.iloc[:, 0].tolist())
        for bid, row in buildings_df.iterrows():
            district_buildings_count += 1
            if bid in self.buildingids:
                i += 0.25
                temp = zones_df.loc[self.zoneids, :]
                zones = temp.loc[temp.building_code == bid]
                building_new = building.Building(self.config, bid, row, zones, time_input, input_data_location,
                                                 file_pv_potentials, dir_dhw, scenario_object, file_geometry_surfaces,
                                                 district_zones_count, district_zones_nr,
                                                 district_buildings_count, district_buildings_nr, renovation_costs_df)
                building_zones_nr = len(zones.iloc[:, 0].tolist())
                district_zones_count += building_zones_nr
                if self.show_progress:
                    app.update_progress(round(i, 2), 'Creating buildings with zones', 'PREPARING DISTRICT', True,
                                        scenario_id, calc_type, j)
                self.buildings.append(building_new)

        print('\n--> Creating buildings with zones finished')
        pass

    def sum_pv_gen(self, buildings, time_input):
        pv_gen_all = pd.Series(np.zeros(len(time_input.index_timesteps_run)))
        for building in buildings:
            pv_gen_all = pd.concat([pv_gen_all, building.pv_gen], axis=1)
        pv_gen_summed = pv_gen_all.sum(axis=1)
        return pv_gen_summed

    def create_generator_info(self):
        # create generator specifications for each building
        for building in self.buildings:
            # get the information from the building
            bid = building.building_code
            energy_standard = building.energy_standard  # not used now
            year_class = building.year_class  # not used now
            heat_supply = building.heat_supply
            # decide the heat generator based on the input parameter
            if heat_supply == 'Gas_boiler':
                heat_generator = 'GB'
            elif heat_supply == 'District_heating':
                heat_generator = 'DH'
            elif heat_supply == 'Heat_pump':
                heat_generator = 'HP'

            # these parameters of generators are for a whole building
            if self.sizing_run_completed and building.total_sizing_heating_load > 0:
                sizing_factor = 1.2
                power_th_sh = building.total_sizing_heating_load * sizing_factor
            else:
                power_th_sh = len(building.zones) * 50  # 40 kW pro zone for space heating
            power_th_dhw = len(building.zones) * 100  # 15 kW electrical power pro zone for domestic hot water
            power_th = power_th_sh  # 80% of heat for domestic hot water comes from heat pump
            generators_lib = {'HP': [1, 'Elec', 1, 'Heat55', 4, power_th, 0, 0, 0],
                              'DH': [1, 'Heat65', 1, 'Heat65', 1, power_th, 0, 0, 0],
                              'GB': [1, 'Gas', 1, 'Heat65', 0.97, power_th, 0, 0, 0],
                              # AC is using 'Bio' as input, emulate air infiltration
                              'AC': [1, 'Bio', 1, 'Cold', 2, 1000, 0, 0, 0],
                              'EH': [2, ['Heat55', 'Elec'], 1, 'Heat65', 1.25, power_th_dhw, 'Heat65', 5, power_th_dhw],
                              'MIX': [1, 'Heat65', 1, 'Heat55', 0.98, 1000, 0, 0, 0],
                              'BHKW': [1, 'Gas', 2, 'Heat55', 0.6, 10, 'Elec', 0.25, 10]}

            generators_dict = {heat_generator: generators_lib[heat_generator],
                               'AC': generators_lib['AC'],
                               'EH': generators_lib['EH'],
                               'MIX': generators_lib['MIX']}

            columns = ['Nr_Inputs', 'Input_Commodity', 'Nr_Outputs', 'Output_Commodity_1', 'Output_Ratio_1',
                       'Output_Capacity_1', 'Output_Commodity_2', 'Output_Ratio_2', 'Output_Capacity_2']
            generators_df = pd.DataFrame.from_dict(generators_dict, orient='index', columns=columns)
            generators_df['GID'] = generators_df.index
            self.generators = list(generators_df.GID.unique())

            # allocate the parameters of generatros
            for index, series in generators_df.iterrows():
                if series['Nr_Inputs'] == 1:
                    self.gen_in_list.append((bid, series['GID'], series['Input_Commodity']))
                    self.gen_ratio_out_dict[(bid, series['GID'], series['Output_Commodity_1'])] = series[
                        'Output_Ratio_1']
                    self.gen_ratio_in_dict[(bid, series['GID'], series['Input_Commodity'])] = 1
                    self.gen_cap_dict[(bid, series['GID'], series['Output_Commodity_1'])] = series['Output_Capacity_1']
                else:
                    self.gen_in_list.append((bid, series['GID'], series['Input_Commodity'][0]))
                    self.gen_in_list.append((bid, series['GID'], series['Input_Commodity'][1]))
                    self.gen_ratio_out_dict[(bid, series['GID'], series['Output_Commodity_1'])] = 1
                    self.gen_ratio_in_dict[(bid, series['GID'], series['Input_Commodity'][0])] = series[
                        'Output_Ratio_1']
                    self.gen_ratio_in_dict[(bid, series['GID'], series['Input_Commodity'][1])] = series[
                        'Output_Ratio_2']
                    self.gen_cap_dict[(bid, series['GID'], series['Output_Commodity_1'])] = series['Output_Capacity_1']
                if series['Nr_Outputs'] == 2:
                    self.gen_ratio_out_dict[(bid, series['GID'], series['Output_Commodity_2'])] = series[
                        'Output_Ratio_2']
                    self.gen_cap_dict[(bid, series['GID'], series['Output_Commodity_2'])] = series['Output_Capacity_2']

    def simulate(self, scenario_id, calc_type, time, j, scenario_location_year):
        """Create optimal schedules for district and hand back Results.

        Args:
            scenario_id (str): id of the scenario being simulated
            calc_type (str): type of objective of optimisation.
            time (obj): object of class Time.

        Returns:
            # TODO - Priority 1 (deadline: until 19.05.2023) - Responsible "AN" -  comment by = - MB 06.04.2023: UPDATE THIS FOR DOCUMENTATION
            start_dict, baseline, outputs_district_df, outputs_district_totals_df, outputs_zone_dictdf, data_in.
        """

        # optimisation
        # set solver instance
        solver = pyoenv.SolverFactory('gurobi')  # check out 'gurobi_persistent' for faster calculation
        solver.options['timeLimit'] = 600  # seconds
        solver.options['mipgap'] = 0.01
        # solver.options['DualReductions'] = 0
        # other options which can be changed
        # opt.options['NumericFocus'] = 3
        # opt.options['ScaleFlag'] = 0
        # opt.set_instance(mymodel)
        print('  > configure MPC start')
        model = self.create_pyomo_model(time, calc_type)
        print('  > configure MPC finished')
        # solve model
        print('  > solving the MPC optimization problem')
        self.solve_model(model, calc_type, solver, time, scenario_id, j,
                         scenario_location_year)  # save the results in district_model

    def create_pyomo_model(self, time, calc_type):
        """ Create a pyomo optimisation model to calculate the optimized schedule.

        Args:
            self(obj): district object with all the information on the district.
            time (obj): object of class Time.
            calc_type (str): objective of optimization ['Temp','Qhc','elec_bought'].

        Returns:
            model (pyomo model): constraints and objectives of the model.
        """
        # the layout and creation procedure of this pyomo model builds upon the code of the open source program 'urbs'
        # basic balances and equations (some overtaken without any alteration of the original incl comments)
        # were extended by equations representing thermal behavior
        # see https://github.com/tum-ens/urbs for more information on 'urbs'

        model = pyo.ConcreteModel()

        # prepare inputs with matching format for initialization of sets and tuples
        # extend buildings by 'hub' building that is in general treated like other buildings within the optimization
        # but features some special configurations in order to be the district's superordinate aggregator/balancer point
        buildings_list = self.buildingids[:]
        buildings_list_no_hub = []
        buildings_list_no_hub[:] = buildings_list
        buildings_list.append('Hub')
        heat_config = self.config['simulation_settings']['heat_config']
        if 'non_resi_load' in self.elec_config or 'non_resi_gen' in self.elec_config:
            buildings_list.append('AllNonResi')

        # fill the zone_list, profiles for fixed generation and consumption, storage_list
        # zone tuples (bid, zid), commodity tuples per type (supim, demand), storage
        # ev is also regarded as a special type of storage
        zones_list = self.zoneids
        zone_tuples_list = []
        com_supim_list = []
        com_demand_fix_list = []
        sto_list = []
        for building in self.buildings:
            for zone in building.zones:
                zone_tuples_list.append((building.building_code, zone.zid))
            if 'pv_gen' in self.elec_config:
                com_supim_list.append((building.building_code, 'Elec'))
            if 'hh_load' in self.elec_config:
                com_demand_fix_list.append((building.building_code, 'Elec'))
            if 'dhw_load' in heat_config:
                # TODO - Priority 3 (deadline: 01.09.2023) - Responsible "ZY" -  comment by = MB - need to change hard coded "65" to building.heat_level_dhw after adapting building_extended file
                # com_demand_fix_list.append((building.building_code, building.heat_level_dhw))
                com_demand_fix_list.append((building.building_code, 'Heat65'))
            else:  # TODO - Priority 1 - Responsible 'ZY' - comment by MB: this is a bug, when no 'dhw_load' in heat_config, the simulation does not work
                com_demand_fix_list.append((building.building_code, 'Heat65'))
            if 'bat' in self.elec_config:
                sto_list.append((building.building_code, 'Elec'))
            if 'tes' in heat_config:
                sto_list.append((building.building_code, building.heat_level_sh))
            else:  # TODO - Priority 1 - Responsible 'ZY' - comment by MB: this is a bug, when no 'tes' in heat_config, the simulation does not work
                sto_list.append((building.building_code, building.heat_level_sh))
            if 'ev' in self.elec_config:
                sto_list.append((building.building_code, 'Elec_ev'))

        if 'non_resi_load' in self.elec_config:
            com_demand_fix_list.append(('AllNonResi', 'Elec'))
        if 'non_resi_gen' in self.elec_config:
            com_supim_list.append(('AllNonResi', 'Elec'))

        # transmission dict now represents a district configuration with exchange possibility of only electricity \
        # between 'Hub' and buildings
        # power transmission losses 0.5%
        transmission_dict = {}
        transmission_cap_dict = {}
        tra_tuples_list = []
        for building in self.buildings:
            # power grid in the neighbourhoods
            transmission_dict[(building.building_code, 'Hub', 'Elec_only', 'Elec')] = 0.995
            transmission_cap_dict[(building.building_code, 'Hub', 'Elec_only', 'Elec')] = 10e9
            transmission_dict[('Hub', building.building_code, 'Elec_only', 'Elec')] = 0.995
            transmission_cap_dict[('Hub', building.building_code, 'Elec_only', 'Elec')] = 10e9
            tra_tuples_list.append((building.building_code, 'Hub', 'Elec_only', 'Elec'))
            tra_tuples_list.append(('Hub', building.building_code, 'Elec_only', 'Elec'))

            # District heating (now only available from hub to buildings) and no losses
            transmission_dict[('Hub', building.building_code, 'Heat_only', 'Heat65')] = 0.95  # efficiency of heat trans
            try:
                transmission_cap_dict[('Hub', building.building_code, 'Heat_only', 'Heat65')] = \
                    self.gen_cap_dict[building.building_code, 'DH', 'Heat65'] * 1000
            except:
                transmission_cap_dict[('Hub', building.building_code, 'Heat_only', 'Heat65')] = 0
            tra_tuples_list.append(('Hub', building.building_code, 'Heat_only', 'Heat65'))

            # define the gas pipeline/network and tranmission effciency
            transmission_dict[('Hub', building.building_code, 'Gas_pipeline', 'Gas')] = 0.99
            transmission_cap_dict[('Hub', building.building_code, 'Gas_pipeline', 'Gas')] = 1e10
            tra_tuples_list.append(('Hub', building.building_code, 'Gas_pipeline', 'Gas'))

            transmission_dict[('Hub', building.building_code, 'Bio_mass_transfer', 'Bio')] = 0.99
            transmission_cap_dict[('Hub', building.building_code, 'Bio_mass_transfer', 'Bio')] = 1e10
            tra_tuples_list.append(('Hub', building.building_code, 'Bio_mass_transfer', 'Bio'))

        if 'non_resi_load' in self.elec_config or 'non_resi_gen' in self.elec_config:
            transmission_dict[('AllNonResi', 'Hub', 'Elec_only', 'Elec')] = 0.995
            transmission_cap_dict[('AllNonResi', 'Hub', 'Elec_only', 'Elec')] = 10e9
            transmission_dict[('Hub', 'AllNonResi', 'Elec_only', 'Elec')] = 0.995
            transmission_cap_dict[('Hub', 'AllNonResi', 'Elec_only', 'Elec')] = 10e9
            tra_tuples_list.append(('AllNonResi', 'Hub', 'Elec_only', 'Elec'))
            tra_tuples_list.append(('Hub', 'AllNonResi', 'Elec_only', 'Elec'))

        model.transmission_dict = transmission_dict

        # process dict for heat pump, boiler and colling
        process_dict = {}
        pro_tuples_list = []
        for building in self.buildings:
            for (bid, generator, output) in self.gen_ratio_out_dict:
                # capacity of a process/generator. e.g. (bid, BHKW, Heat)
                if bid == building.building_code:
                    process_dict[(building.building_code, generator, output)] = \
                        self.gen_cap_dict[(building.building_code, generator, output)] * 1000
            for generator in self.generators:
                pro_tuples_list.append((building.building_code, generator))  # list of process, e.g. (bid, BHKW)

        model.gen_in_list = self.gen_in_list  # key is GID+Input_Commodity
        model.r_out_dict = self.gen_ratio_out_dict  # key is GID+Output_Commodity, value is the efficiency
        model.r_in_dict = self.gen_ratio_in_dict
        # space heating
        # Currently all zones take part of demand management if sh_load is defined in heat_config;
        # in the future, differentiation between buildings taking or not part in management is possible
        zones_therm_list = []
        com_zone_tuples_list = []
        com_zonewise_list = []
        for building in self.buildings:
            if 'sh_load' in heat_config:
                for zone in building.zones:
                    zones_therm_list.append(zone.zid)
                    com_zone_tuples_list.append((building.heat_level_sh, zone.zid))
                    com_zone_tuples_list.append(('Cold', zone.zid))
                com_zonewise_list.append((building.building_code, building.heat_level_sh))
                com_zonewise_list.append((building.building_code, 'Cold'))
                com_zonewise_list.append((building.building_code, 'Gas'))

        # commodity tuples as result from merging per-type-sets
        com_tuples_list = list(set(com_supim_list) | set(com_demand_fix_list) | set(com_zonewise_list))
        com_tuples_list.append(('Hub', 'Elec'))
        com_tuples_list.append(('Hub', 'Heat65'))
        com_tuples_list.append(('Hub', 'Gas'))

        ##################################################
        # Model components describing timesteps
        # N: prediction horizon (Parameter)
        model.N = pyo.Param(initialize=self.predict_horizon_h)

        # dt: time resolution (Parameter)
        # dt = spacing between timesteps.
        model.dt = pyo.Param(
            initialize=int(self.time_resolution_h),
            doc='Time step duration (in hours), default: 1')  # matches our former time resolution

        # t: time steps during prediction horizon considering the defined time resolution (RangeSet)
        # generate ordered time step sets
        model.t = pyo.RangeSet(
            0, model.N, model.dt,
            doc='Set of timesteps')  # matches our format k # [0,1,...,24]

        # tm: time steps during prediction horizon to be simulated, same as t but w/o initial time step "0" (RangeSet)
        # modelled (i.e. excluding init time step) time steps
        model.tm = pyo.RangeSet(
            1, model.N, model.dt,
            doc='Set of modelled timesteps')  # matches our former objk #  # [1,2,...,24]

        # u: time steps of the generated operation plan for the prediction horizon,
        # which will be implemented, the rest will be updated with the next operation plan
        # generate ordered time step sets for update cycles
        model.u = pyo.RangeSet(
            1, self.config['simulation_settings']['update_interval_h'], model.dt,
            doc='Set of timesteps')  # [1,2,...,6]

        ###########################################
        # Sets Syntax: m.{name} = Set({domain}, initialize={values})
        # where name: set name
        #       domain: set domain for tuple sets, a cartesian set product
        #       values: set values, a list or array of element tuples

        model.buildings = pyo.Set(
            initialize=buildings_list,
            doc='Set of buildings')

        model.buildings_no_hub = pyo.Set(
            initialize=buildings_list_no_hub,
            doc='Set of buildings without hub')

        model.zones = pyo.Set(
            initialize=zones_list,
            doc='Set of zones')

        model.zones_therm = pyo.Set(
            initialize=zones_therm_list,
            doc='Set of zones for which thermal behavior is modelled')

        model.com = pyo.Set(
            initialize=eval(self.config['simulation_settings']['commodities']),
            doc='Set of commodities (energy forms)')

        model.sto_com = pyo.Set(
            initialize=['Elec', 'Heat55', 'Elec_ev'],
            doc='Set of commodities for diverse storage types')

        model.pro = pyo.Set(
            initialize=self.generators,
            doc='Set of processes i.e. generator units')

        # transmission (e.g. hvac, hvdc, pipeline...)
        # Elec_only: electrical grid in the district
        # Heat_only: pipes for district heating
        model.tra = pyo.Set(
            initialize=['Elec_only', 'Heat_only', 'Gas_pipeline', 'Bio_mass_transfer'],
            doc='Set of transmission technologies')

        # thermal behavior
        model.temp_states = pyo.Set(
            initialize=self.config['RC_settings']['temp_state_names'],
            doc='Set of temperature states within one zone i.e. TEW, TIW, Tz, T1, T2')

        model.therm_disturb = pyo.Set(
            initialize=self.config['RC_settings']['temp_disturb_names'],
            doc='Set of disturbances within one zone i.e. QEW, QIW, Qcon, Taeq, Tvent')

        model.therm_disturb_ext = pyo.Set(
            initialize=self.config['RC_settings']['temp_disturb_names'] + ['Qsolar', 'QconvInt', 'QradInt',
                                                                           'Qint_pers', 'Qint_lighting', 'Qint_devices',
                                                                           'Q_inf_vent_factor', 'Q_envelope_factor'],
            doc='Add 3 + 4 new parameters to therm_disturb for validation purpose')

        # tuple sets
        model.com_tuples = pyo.Set(
            within=model.buildings * model.com,
            initialize=com_tuples_list,
            doc='Combinations of buildings and commodities, e.g. (bid1,Elec)')
        model.com_therm = pyo.Set(
            within=model.com,
            initialize=self.config['simulation_settings']['commodities_thermal'],
            doc='Set of thermal commodities i.e. Heat70, Heat55, Cold')
        model.zone_tuples = pyo.Set(
            within=model.buildings * model.zones,
            initialize=zone_tuples_list,
            doc='Combinations of buildings and zones, e.g. (bid1,zid1)')
        model.com_zone_tuples = pyo.Set(
            within=model.com * model.zones_therm,
            initialize=com_zone_tuples_list,
            doc='Combinations of commodities and zone for zonewise equations, e.g. (Heat70, zid1)')
        model.pro_tuples = pyo.Set(
            within=model.buildings * model.pro,
            initialize=pro_tuples_list,
            doc='Combinations of possible processes, e.g. (bid1,GSHP)')
        model.com_stock = pyo.Set(
            within=model.buildings * model.com,
            initialize=[('Hub', 'Elec'), ('Hub', 'Heat65'), ('Hub', 'Gas'), ('Hub', 'Bio')],
            doc='Commodities that can be purchased at some site(s); now only for "Hub"-building enabled')

        # commodity type subsets
        model.com_supim = pyo.Set(
            within=model.buildings * model.com,
            initialize=com_supim_list,
            doc='Commodities that have supply via fix renewables timeseries input')
        model.com_sto = pyo.Set(
            within=model.buildings_no_hub * model.sto_com,
            initialize=sto_list,
            doc='Commodities which represent storage system which can be charged or discharged')
        model.com_demand_fix = pyo.Set(
            within=model.buildings * model.com,
            initialize=com_demand_fix_list,
            doc='Commodities that have a demand (implies timeseries)')
        model.com_slack = pyo.Set(
            within=model.com_tuples,
            initialize=[('Hub', 'Elec')],
            doc='Slack-commodity to simplify debugging in cases with otherwise failing optimization')
        model.com_zonewise = pyo.Set(
            within=model.com_tuples,
            initialize=com_zonewise_list,
            doc='Commodities that have a demand which Results from aggregation of zones')

        # process input/output, like heat pumps and cooling
        model.pro_input_tuples = pyo.Set(
            within=model.buildings * model.pro * model.com,
            initialize=[(building, process, commodity) for (building, process, commodity) in tuple(model.gen_in_list)],
            doc='Commodities consumed by process by building, e.g. (bid1,GSHP,Elec)')
        model.pro_output_tuples = pyo.Set(
            within=model.buildings * model.pro * model.com,
            initialize=[(building, process, commodity) for (building, process, commodity) in
                        tuple(model.r_out_dict.keys())],
            doc='Commodities produced by process by building, e.g. (bid1,GSHP,Heat55)')

        model.pro_input_output_tuples = pyo.Set(
            within=model.buildings * model.pro * model.com * model.com,
            initialize=[(building, process, commodity_in, commodity_out)
                        for (building, process, commodity_in) in model.pro_input_tuples
                        for (bid, pro, commodity_out) in tuple(model.r_out_dict.keys())
                        if process == pro and building == bid],
            doc='Commodities produced and consumed by process by building, e.g. (bid1,GSHP, Elec, Heat55)')

        # transmission tuples
        model.tra_tuples = pyo.Set(
            within=model.buildings * model.buildings * model.tra * model.com,
            initialize=tra_tuples_list,
            doc='Combinations of possible transmissions, e.g. (bid1,bid2,Elec_only,Elec)')

        ###############################################
        # Parameters

        # heat periods
        model.heat_period = pyo.Param(
            model.t,
            initialize=0,
            mutable=True,
            doc='decide if this ts belongs to heating period')

        # fix demand and renewables generation
        model.demand_fix = pyo.Param(
            model.t, model.com_demand_fix,
            initialize=0,
            mutable=True,
            doc='fix demand')
        model.supim = pyo.Param(
            model.t, model.com_supim,
            initialize=0,
            mutable=True,
            doc='fix renewable generation')
        # storage initial soc
        model.sto_init_soc = pyo.Param(
            model.com_sto,
            initialize=0,
            mutable=True,
            doc='initial soc of storage systems')
        model.sto_cap = pyo.Param(
            model.com_sto,
            initialize=0,
            mutable=True,
            doc='capacities of storage systems')

        model.ev_power_fcast = pyo.Param(
            model.t, model.buildings_no_hub,
            initialize=0,
            mutable=True,
            doc='forecasting of ev states: potential charging power')

        model.ev_soc_driving = pyo.Param(
            model.t, model.buildings_no_hub,
            initialize=0,
            mutable=True,
            doc='describe the electrical energy in SOC stored in the driving evs')

        model.ev_soc_min_fcast = pyo.Param(
            model.t, model.buildings_no_hub,
            initialize=0,
            mutable=True,
            doc='forecasting of ev states: minimum soc')

        model.ev_consumption_fcast = pyo.Param(
            model.t, model.buildings_no_hub,
            initialize=0,
            mutable=True,
            doc='forecasting of ev states: consumption in kWh')

        # ambient temperature
        model.temp_amb = pyo.Param(
            model.t, model.buildings,
            within=pyo.Reals,
            initialize=0,
            mutable=True,
            doc='time series of ambient temperature of each building')

        # output correction factor, now only applied for heat pumps because the COP changes over the day
        model.pro_out_cor = pyo.Param(
            model.t, model.pro_tuples,
            within=pyo.NonNegativeReals,
            initialize=1,
            mutable=True,
            doc='time series of correction factors for process')

        # process capacity, maximum power of certain process
        model.cap_pro = pyo.Param(
            model.pro_output_tuples,
            initialize=process_dict,
            doc='Total process capacity (W)')

        # transmission capacity
        model.cap_tra = pyo.Param(
            model.tra_tuples,
            initialize=transmission_cap_dict,
            # possible extension: separate cap for each transmission line via transmission_dict
            doc='total transmission capacity (W)')

        # thermal behavior of zones
        temp_state_names = self.config['RC_settings']['temp_state_names']
        temp_disturb_names = self.config['RC_settings']['temp_disturb_names']
        model.param_A = pyo.Param(
            model.temp_states, model.temp_states, model.zones_therm,
            initialize=self.create_dict_with_indexed_param('A_dis', self, model.zones_therm, temp_state_names,
                                                           temp_state_names),
            mutable=True,
            doc='Parameter describing dependency of one temperature state on another')
        model.param_B1 = pyo.Param(
            model.temp_states, model.therm_disturb, model.zones_therm,
            initialize=self.create_dict_with_indexed_param('B1_dis', self, model.zones_therm, temp_state_names,
                                                           temp_disturb_names),
            mutable=True,
            doc='Parameter describing dependency of temperature state on disturbance')
        model.param_B2 = pyo.Param(
            model.temp_states, model.com_zone_tuples,
            initialize=self.create_dict_with_indexed_param('B2_dis', self, model.zones_therm, temp_state_names,
                                                           'heat-cold'),
            mutable=True,
            doc='Parameter describing dependency of temperature state on heating/cooling')
        model.bin_heat = pyo.Param(
            model.com_therm,
            initialize=self.config['simulation_settings']['binary_heat_dict'],
            mutable=True,
            doc='Sign of effect of heating/cooling on temperature state: +1 or -1')
        model.data_therm = pyo.Param(
            model.t, model.therm_disturb_ext, model.zones_therm,
            initialize=0,
            mutable=True,
            doc='Disturbances as inputs to thermal zone')
        model.temp_set = pyo.Param(
            model.zones_therm,
            initialize=self.config['building_settings']['Tz_avg'],
            mutable=True,
            doc='Set temperature for heating system')
        model.delta_temp_max = pyo.Param(
            model.zones_therm,
            initialize=self.config['building_settings']['Tdelta_max'],
            mutable=True,
            doc='Maximum temperature increase/decrease per timestep')  # TODO - Priority 2 (deadline: July 2023) - Responsible "ZY" -  comment by = ZY- adapt value for 15min timesteps: currently max +-2K/h(set by the norm), w 15min: +-0.5K/0.25h?
        model.update_avg_temp = pyo.Param(
            initialize=self.config['simulation_settings']['update_interval_h'],
            mutable=True,
            doc='update interval for average temperature rule')
        # Past timesteps = horizon - update i.e. 24 - 6 = 18 (or 3x6:updates X udpateclycle)
        # so an avg value of the avg values of the three last update cycles
        model.avg_temp_past = pyo.Param(
            model.zones_therm,
            mutable=True,
            doc='average temperature of past timesteps fitting the avg_temp_rule')

        # energy price
        model.elec_price = pyo.Param(model.t, initialize=0, mutable=True)
        model.dh_price = pyo.Param(model.t, initialize=0, mutable=True)
        model.gas_price = pyo.Param(initialize=0, mutable=True)
        model.bio_price = pyo.Param(initialize=0, mutable=True)

        # CO2 emission
        model.emission_elec = pyo.Param(model.t, initialize=0, mutable=True)  # specific emission of electricity
        model.emission_th = pyo.Param(model.t, initialize=0, mutable=True)  # specific emission of district heating
        model.emission_gas = pyo.Param(initialize=0, mutable=True)

        # penalty factor for rule-based control (HP and EV)
        model.penalty_hp = pyo.Param(model.t, initialize=0, mutable=True)
        model.penalty_ev = pyo.Param(model.t, initialize=0, mutable=True)

        ##################################################################
        # Variables

        # commodity
        model.e_co_stock_plus = pyo.Var(
            model.t, model.com_stock,
            within=pyo.NonNegativeReals,
            initialize=0,
            doc='electrical or thermal power imported (W) per timestep')
        model.e_co_stock_minus = pyo.Var(
            model.t, model.com_stock,
            within=pyo.NonNegativeReals,
            initialize=0,
            doc='electrical or thermal power exported (W) per timestep')

        model.e_co_stock_cap = pyo.Var(
            ['Elec', 'Heat65', 'Gas', 'Bio'],
            within=pyo.NonNegativeReals,
            initialize=1e15,
            doc='max import and export power (W)')

        model.e_slack = pyo.Var(
            model.t, model.com_slack,
            within=pyo.NonPositiveReals,
            initialize=0,
            doc='Use of slack commodity source (W) per timestep')

        model.e_zone_in = pyo.Var(
            model.t, model.com_zone_tuples,
            within=pyo.NonNegativeReals,
            initialize=0,
            doc='controllable energy flow (W) into zone per timestep, i.e. heat and cold')

        # help variables for zone heat balance, should be deleted after the model ist finished
        model.zone_dtemp_temp = pyo.Var(
            model.t, model.zones_therm,
            within=pyo.Reals,
            initialize=0,
            doc='temperature of last time step + temperature change caused by temperature differences')
        model.zone_dtemp_dis = pyo.Var(
            model.t, model.zones_therm,
            within=pyo.Reals,
            initialize=0,
            doc='temperature change caused by disturbance')
        model.zone_dtemp_heat = pyo.Var(
            model.t, model.zones_therm,
            within=pyo.Reals,
            initialize=0,
            doc='temperature change caused by heating/cooling')

        # process
        model.e_pro_in = pyo.Var(
            model.t, model.pro_input_tuples,
            within=pyo.NonNegativeReals,
            initialize=0,
            doc='Power flow of commodity into process (W) per timestep')  # Energy consumption from generator
        model.e_pro_out = pyo.Var(
            model.t, model.pro_output_tuples,
            within=pyo.NonNegativeReals,
            initialize=0,
            doc='Power flow out of process (W) per timestep')  # Energy production from generator

        # storage system, e.g. battery and thermal energy storage
        model.e_sto_soc = pyo.Var(
            model.t, model.com_sto,
            within=pyo.NonNegativeReals,
            bounds=(0, 1),
            initialize=0.5,
            doc='state of charge of energy storage system (%)')  # Energy consumption from generator

        model.e_sto_in = pyo.Var(
            model.t, model.com_sto,
            within=pyo.NonNegativeReals,
            initialize=10000,
            doc='Power flow into the storage (W) per timestep')  # charging of storage

        model.e_sto_out = pyo.Var(
            model.t, model.com_sto,
            within=pyo.NonNegativeReals,
            initialize=10000,
            doc='Power flow out of storage (W) per timestep')  # discharging of storage

        # transmission
        # transmission line: line between hub and each building
        model.e_tra_in = pyo.Var(
            model.t, model.tra_tuples,
            within=pyo.NonNegativeReals,
            initialize=0,
            doc='Power flow into transmission line (W) per timestep')
        model.e_tra_out = pyo.Var(
            model.t, model.tra_tuples,
            within=pyo.NonNegativeReals,
            initialize=0,
            doc='Power flow out of transmission line (W) per timestep')  # The same as above but in the other direction.
        model.e_tra_bin = pyo.Var(
            model.t, model.tra_tuples,
            within=pyo.Binary,
            initialize=0,
            doc='Binary variable to avoid input and output at the same time')

        # thermal behavior
        # lower bound for room temperature, other temperature is unlimited
        comfort_bands = self.config['building_settings']['comfort_bands']
        comfort_category = self.config['building_settings']['comfort_category']
        lb = {'Tz': -99, 'TEW': -99, 'TIW': -99, 'T1': -99, 'T2': -99}
        # upper bound for room temperature, other temperature is unlimited
        ub = {'Tz': 99, 'TEW': 99, 'TIW': 99, 'T1': 99, 'T2': 99}

        def Tz_comfortband_rule(model, t, i, zid):
            return (lb[i], ub[i])

        model.temperature = pyo.Var(
            model.t, model.temp_states, model.zones_therm,
            within=pyo.NonNegativeReals,
            bounds=Tz_comfortband_rule,
            initialize=18,
            doc='Temperature (degC) per timestep')

        model.temp_slack = pyo.Var(
            model.zones_therm,
            within=pyo.NonNegativeReals,
            initialize=0,
            doc='Penalty for temperature deviation')

        model.bin_on = pyo.Var(
            model.t, model.com_zone_tuples,
            within=pyo.Binary,
            initialize=0,
            doc='Binary variable ensuring that only one of {heating, cooling} is switched on at a given timestep')

        ###############################################################
        # Constraints

        # process

        # process output power = process input * output ratio
        def def_process_output_rule(m, t, sit, pro, com_in, com_out):
            return (m.e_pro_out[t, sit, pro, com_out] ==
                    m.e_pro_in[t, sit, pro, com_in] * m.r_in_dict[(sit, pro, com_in)] *
                    m.r_out_dict[(sit, pro, com_out)] * m.pro_out_cor[t, sit, pro])

        # process output <= process capacity
        def res_process_output_by_capacity_rule(m, t, sit, pro, com):
            return m.e_pro_out[t, sit, pro, com] <= m.cap_pro[sit, pro, com] * m.pro_out_cor[t, sit, pro]

        # constarint for purchase in stock
        def def_stock_plus_cap_rule(m, t, sit, com):
            return m.e_co_stock_plus[t, sit, com] <= m.e_co_stock_cap[com]

        def def_stock_minus_cap_rule(m, t, sit, com):
            return m.e_co_stock_minus[t, sit, com] <= m.e_co_stock_cap[com]

        # storage system, battery and thermal energy system
        # storage system for heat and electricity, efficiency now hard coded as 0.98
        def def_sto_rule(m, t, sit, com):
            if t == 0:
                return m.e_sto_soc[t, sit, com] == m.sto_init_soc[sit, com] + \
                    (m.e_sto_in[t, sit, com] * 0.98 - m.e_sto_out[t, sit, com] / 0.98) * m.dt / m.sto_cap[sit, com]
            else:
                return m.e_sto_soc[t, sit, com] == m.e_sto_soc[t - 1, sit, com] + \
                    (m.e_sto_in[t, sit, com] * 0.98 - m.e_sto_out[t, sit, com] / 0.98) * m.dt / m.sto_cap[sit, com]

        def def_sto_min_rule(m, t, sit, com):
            if com == 'Elec_ev':
                return m.e_sto_soc[t, sit, com] >= model.ev_soc_min_fcast[t, sit]
                # return m.e_sto_soc[t, sit, com] >= 0.5
            elif t == self.predict_horizon_h:
                return m.e_sto_soc[t, sit, com] >= 0.5  # the SOC of last time steps should be greater than 50%
            else:
                return pyo.Constraint.Skip

        # constrains for limiting charging and discharging power
        # TODO - Priority 2 (deadline: 07.2023) - Responsible "ZY" -  comment by =: can be changed to be a function of the storage capacity
        def def_sto_in_max_rule(m, t, sit, com):
            if com == 'Elec':
                return m.e_sto_in[t, sit, com] <= len(building.zones) * 2000  # 2 kW pro household for electricl power
            elif com == 'Elec_ev':
                # return pyo.Constraint.Skip
                return m.e_sto_in[t, sit, com] <= model.ev_power_fcast[t, sit]
                # return m.e_sto_in[t, sit, com] <= len(building.zones) * 10000
            elif com == 'Heat55':
                return m.e_sto_in[t, sit, com] <= len(building.zones) * 10000  # 10 kW pro household for thermal power

        def def_sto_out_max_rule(m, t, sit, com):
            if com == 'Elec':
                return m.e_sto_out[t, sit, com] <= len(building.zones) * 2000  # 2 kW pro household for electrical power
            elif com == 'Elec_ev':
                # return m.e_sto_out[t, sit, com] == model.ev_consumption_fcast[t, sit] / config.time_resolution_h
                # return pyo.Constraint.Skip
                return m.e_sto_out[t, sit, com] == model.ev_consumption_fcast[t, sit]
            elif com == 'Heat55':
                return m.e_sto_out[t, sit, com] <= len(building.zones) * 10000  # 10 kW pro household for thermal power

        # transmission
        # transmission output == transmission input * efficiency
        def def_transmission_output_rule(m, t, sin, sout, tra, com):
            return (m.e_tra_out[t, sin, sout, tra, com] == m.e_tra_in[t, sin, sout, tra, com] *
                    m.transmission_dict[(sin, sout, tra, com)])

        # transmission input <= transmission capacity
        def res_transmission_input_by_capacity_rule(m, t, sin, sout, tra, com):
            return (m.e_tra_in[t, sin, sout, tra, com] <= m.cap_tra[sin, sout, tra, com])

        # binary constraint to avoid bi-direcitonal transmission
        def transmission_bin_in_rule(m, t, sin, sout, tra, com):
            return m.e_tra_in[t, sin, sout, tra, com] <= m.e_tra_bin[t, sin, sout, tra, com] * 1e10

        def transmission_bin_out_rule(m, t, sin, sout, tra, com):
            if com == 'Elec':
                return m.e_tra_in[t, sout, sin, tra, com] <= (1 - m.e_tra_bin[t, sin, sout, tra, com]) * 1e10
            else:
                return pyo.Constraint.Skip

        # transmission balance
        # Sum of com in and out from building to hub and from hub to building pro building
        def transmission_balance(m, t, sit, com):
            """called in commodity balance
            For a given commodity co and timestep tm, calculate the balance of
            import and export through transmission lines"""

            return (sum(m.e_tra_in[(t, site_in, site_out,
                                    transmission, com)]
                        # exports increase balance
                        for site_in, site_out, transmission, commodity
                        in m.tra_tuples
                        if (site_in == sit and
                            commodity == com)) -
                    sum(m.e_tra_out[(t, site_in, site_out,
                                     transmission, com)]
                        # imports decrease balance
                        for site_in, site_out, transmission, commodity
                        in m.tra_tuples
                        if (site_out == sit and
                            commodity == com)))

        # aggregation zones to buildings
        # Sum of com demand of all zones per building (in this configuration Heat55 & Cold
        # (so, flexible energy controlable by optimiser)
        # but not elec because it is fixed (HH_load) and calculated in another place [see #])
        def aggregation_zones(m, t, sit, com):

            return (sum(m.e_zone_in[(t, com, zone)]
                        for commodity, zone in m.com_zone_tuples
                        if commodity == com and
                        (sit, zone) in m.zone_tuples))

        # binar constraint for heating and cooling power
        # TODO - Priority 1 (deadline: until 19.05.2023) - Responsible "ZY" -  comment by = ? - need to check if this constraint is necessary
        def res_process_output_by_binary_rule(m, t, com, zid):
            return m.e_zone_in[t, com, zid] <= m.bin_on[t, com, zid] * 1e15

        # Kren von RC-Model !!!
        # Constraint for the operation plan of the optimiser for the horizon periode: the resulting temp for each timestep
        # should respect the relationship set with the contraint i.e. Temp2 comes from Temp1 and the Disturbancies in Tstep-Temp2, etc.
        def temperature_update_rule(m, t, i, zid):
            # t: time step, i:temp states, "TEW" "Tz", zid: zone id
            if t == self.predict_horizon_h:
                return pyo.Constraint.Skip
            # TODO - Priority 1 (deadline: until 19.05.2023) - Responsible "ZY" -  comment by = ? - check the time inverval ZY
            else:
                return (m.temperature[t + m.dt, i, zid] == sum(
                    m.param_A[i, n, zid] * m.temperature[t, n, zid] for n in m.temp_states)
                        + sum(m.param_B1[i, d, zid] * m.data_therm[t, d, zid] for d in m.therm_disturb)
                        + sum(m.param_B2[i, com, zone] * m.e_zone_in[(t, com, zone)] * m.bin_heat[com]
                              for (com, zone) in m.com_zone_tuples
                              if zone == zid))
        #
        def max_temp_up_rule(m, t, zid):
            if t == self.predict_horizon_h:
                return pyo.Constraint.Skip
            else:
                return m.temperature[t + m.dt, 'Tz', zid] - m.temperature[t, 'Tz', zid] <= m.delta_temp_max[zid] \
                    + m.temp_slack[zid]

        #
        def max_temp_down_rule(m, t, zid):
            if t == self.predict_horizon_h:
                return pyo.Constraint.Skip
            else:
                return m.temperature[t + m.dt, 'Tz', zid] - m.temperature[t, 'Tz', zid] >= -m.delta_temp_max[zid] \
                    - m.temp_slack[zid]

        #
        def avg_temp_future_rule_min(m, t, zid):
            if calc_type == 'Temp':
                return pyo.Constraint.Skip
            else:
                return (sum(m.temperature[tm, 'Tz', zid] for tm in m.tm) / (m.N / m.dt) >= m.temp_set[zid])

        def avg_temp_future_rule_max(m, t, zid):
            if calc_type == 'Temp':
                return pyo.Constraint.Skip
            else:
                return m.temperature[t, 'Tz', zid] <= m.temp_set[zid] + 4 + m.temp_slack[zid]

        # It takes into account the avg of the temps of the current update timesteps + the temps of the timesteps HorizonPeriode-Update of the last periode
        # see above, parameters
        # i.e. : 3*6 + 6 timesteps

        def avg_temp_past_rule_min(m, t, zid):
            weight_new = m.update_avg_temp / (m.N / m.dt)
            weight_old = 1 - weight_new
            Tz_avg_new = sum([m.temperature[u, 'Tz', zid] for u in m.u]) / m.update_avg_temp  # start at u=1
            # because Tz[0] already determined by prior iteration
            Tz_avg_total = m.avg_temp_past[zid] * weight_old + Tz_avg_new * weight_new
            return Tz_avg_total >= m.temp_set[zid]

        def avg_temp_past_rule_max(m, t, zid):
            weight_new = m.update_avg_temp / (m.N / m.dt)
            weight_old = 1 - weight_new
            Tz_avg_new = sum([m.temperature[u, 'Tz', zid] for u in m.u]) / m.update_avg_temp  # start at u=1
            # because Tz[0] already determined by prior iteration
            Tz_avg_total = m.avg_temp_past[zid] * weight_old + Tz_avg_new * weight_new
            return Tz_avg_total <= (m.temp_set[zid] + m.temp_slack[zid]) * (1 + m.heat_period[t])

        # max temperature from comfort bands
        def temp_rule_max(m, t, zid):
            if calc_type == 'Temp':
                return m.temperature[t, 'Tz', zid] <= m.temp_set[zid] + m.temp_slack[zid]
            else:
                return m.temperature[t, 'Tz', zid] <= comfort_bands[comfort_category][1] + m.temp_slack[zid]

        # min temperature from comfort bands
        def temp_rule_min(m, t, zid):
            if calc_type == 'Temp':
                return m.temperature[t, 'Tz', zid] >= m.temp_set[zid] - m.temp_slack[zid]
            else:
                return m.temperature[t, 'Tz', zid] >= comfort_bands[comfort_category][0] - m.temp_slack[zid]

        # commodity
        # sum of produced and consumed energy pro com for each generator and pro building
        # So: for each building: welche com?, is there a generator that use/produce that com?, if yes and yes, calculate balance in+out
        def commodity_balance(m, t, sit, com):
            """Calculate commodity balance at given timestep.
            For a given commodity co and timestep tm, calculate the balance of
            consumed (to process/storage/transmission, counts positive) and provided
            (from process/storage/transmission, counts negative) commodity flow. Used
            as helper function in create_model for constraints on demand and stock
            commodities.
            Args:
                m: the model object
                t: the timestep
                site: the site
                com: the commodity
            Returns
                balance: net value of consumed (positive) or provided (negative) power
            """
            balance = (sum(m.e_pro_in[(t, site, process, com)]
                           # usage as input for process increases balance
                           # the more consumption, the higher the result
                           for site, process in m.pro_tuples
                           if site == sit and
                           (sit, process, com) in m.gen_in_list) -
                       sum(m.e_pro_out[(t, site, process, com)]
                           # output from processes decreases balance
                           # the more production, the smaller the result
                           for site, process in m.pro_tuples
                           if site == sit and
                           (sit, process, com) in m.r_out_dict)) + \
                      sum(m.e_sto_in[(t, site, com)] - m.e_sto_out[(t, site, com)]
                          for site, commodity in m.com_sto if site == sit and commodity == com) + \
                      transmission_balance(m, t, sit, com)

            if com == 'Elec' and 'ev' in self.elec_config:
                balance += sum(m.e_sto_in[(t, site, 'Elec_ev')]
                               for site, commodity in m.com_sto if site == sit and commodity == com)

            # Example Heat from HP:
            # sum(m.e_pro_in... : this will be zero, because input com HP is electricity,
            # sum(m.e_pro_out... : this will be something, because HP offers heat as the output,
            # and transmission_balance: this will be also zero because in our case there are no heat pipe lines

            return balance

        # Main balance in each building (and the Hub) !!!
        # vertex equation: calculate balance for given commodity pro building and the hub (it must be always zero);
        # contains implicit constraints for process activity, import/export
        # (calculated by function commodity_balance);
        # contains implicit constraint for stock commodity source term
        def res_vertex_rule(m, t, sit, com):

            # helper function commodity_balance calculates balance from input to
            # and output from processes and transmission.
            # if power_surplus > 0: production/storage/imports create net positive
            #                       amount of commodity com
            # if power_surplus < 0: production/storage/exports consume a net
            #                       amount of the commodity com
            power_surplus = - commodity_balance(m, t, sit, com)

            # if com is a stock commodity, the commodity source term e_co_stock
            # can supply a possibly negative power_surplus
            # if com is a stock commodity (i.e. total electr from quarter), and its demand cannot be covered internally in the
            # quarter, the commodity should be supplied from the exterior i.e. a grid, which is e_co_stock, and in our case
            # is only possible by "Hub"
            if (sit, com) in m.com_stock:
                if com == 'Elec':
                    power_surplus += m.e_co_stock_plus[t, sit, com] - m.e_co_stock_minus[t, sit, com]
                else:
                    power_surplus += m.e_co_stock_plus[t, sit, com]  # for heat only import possible

            # if site (building) has the slack commodity, the commodity source term e_slack
            # can balance a possibly positive power_surplus
            # Note: slack commodity is not a real supply, it is just an internal tool/indicator to make sure that the result of the
            # simulation is correct, so, a debug tool. A correct simulation should have a final slack value of zero!!!
            # if (sit, com) in m.com_slack:
            #     power_surplus += m.e_slack[t, sit, com]

            # if com is a renewable supply commodity, the power_surplus is increased by the
            # supim value; no scaling by m.dt is needed here, as this
            # constraint is about power (W), not energy (Wh)
            if (sit, com) in m.com_supim:
                power_surplus += m.supim[(t, sit, com)]

            # if com is a demand commodity, the power_surplus is reduced by the
            # demand value; no scaling by m.dt is needed here, as this
            # constraint is about power (W), not energy (MWh)
            if (sit, com) in m.com_zonewise:
                power_surplus -= aggregation_zones(m, t, sit, com)
            if (sit, com) in m.com_demand_fix:
                power_surplus -= m.demand_fix[(t, sit, com)]
            return power_surplus == 0

        # ------------------------------------------------------------------------------------------------------------------
        # Equation declarations
        # equation bodies are defined in separate functions above, referred to here by
        # their name in the "rule" keyword.
        # commodity
        model.res_vertex = pyo.Constraint(
            model.t, model.com_tuples,
            rule=res_vertex_rule,
            doc='transmission + process + stock == demand - renewable supply')

        # process
        model.def_process_output = pyo.Constraint(
            model.t, model.pro_input_output_tuples,
            rule=def_process_output_rule,
            doc='process output = process input * output ratio')
        model.res_process_output_by_capacity = pyo.Constraint(
            model.t, model.pro_output_tuples,
            rule=res_process_output_by_capacity_rule,
            doc='process output <= total process capacity')
        # stock capacity
        model.def_stock_cap = pyo.Constraint(model.t, model.com_stock,
                                             rule=def_stock_plus_cap_rule,
                                             doc='stock purchase <= total process capacity')

        model.def_stock_minus_cap = pyo.Constraint(model.t, model.com_stock,
                                                   rule=def_stock_minus_cap_rule,
                                                   doc='stock purchase <= total process capacity')

        # storage system
        model.def_sto = pyo.Constraint(
            model.t, model.com_sto,
            rule=def_sto_rule,
            doc='storage soc = sto_in * eff - sto_out / eff')

        model.def_sto_min = pyo.Constraint(
            model.t, model.com_sto,
            rule=def_sto_min_rule,
            doc='storage end soc >= 50%')

        model.def_sto_in_max = pyo.Constraint(
            model.t, model.com_sto,
            rule=def_sto_in_max_rule,
            doc='storage charge <= max')

        model.def_sto_out_max = pyo.Constraint(
            model.t, model.com_sto,
            rule=def_sto_out_max_rule,
            doc='storage discharge <= max')

        # transmission
        model.def_transmission_output = pyo.Constraint(
            model.t, model.tra_tuples,
            rule=def_transmission_output_rule,
            doc='transmission output = transmission input * efficiency')
        model.res_transmission_input_by_capacity = pyo.Constraint(
            model.t, model.tra_tuples,
            rule=res_transmission_input_by_capacity_rule,
            doc='transmission input <= total transmission capacity')
        model.transmission_bin_in = pyo.Constraint(
            model.t, model.tra_tuples,
            rule=transmission_bin_in_rule,
            doc='transmission input or output in the same line must be zero')
        model.transmission_bin_out = pyo.Constraint(
            model.t, model.tra_tuples,
            rule=transmission_bin_out_rule,
            doc='transmission input or output in the same line must be zero')

        model.def_temperature_update = pyo.Constraint(
            model.t, model.temp_states, model.zones_therm,
            rule=temperature_update_rule,
            doc='temp_next == A * temp + B1 * disturbances + B2 * heating/cooling')

        model.def_max_temp_up = pyo.Constraint(
            model.t, model.zones_therm,
            rule=max_temp_up_rule,
            doc='Tz(t+1) - Tz(t) <= dT_max')
        model.def_max_temp_down = pyo.Constraint(
            model.t, model.zones_therm,
            rule=max_temp_down_rule,
            doc='Tz(t+1) - Tz(t) >= - dT_max')
        model.def_avg_temp_future_min = pyo.Constraint(
            model.t, model.zones_therm,
            rule=avg_temp_future_rule_min,
            doc='mean(Tz[tm] for tm in [dt:predict_horizon]) >= Tset')
        model.def_avg_temp_past_min = pyo.Constraint(
            model.t, model.zones_therm,
            rule=avg_temp_past_rule_min,
            doc='weighted_mean(Tz[all tm within update_horizon] and Tz[-(predict_horizon-update_horizon):0]) >= Tset')
        model.def_temp_rule_max = pyo.Constraint(
            model.t, model.zones_therm,
            rule=temp_rule_max,
            doc='restrict the max zone temperature')
        model.def_temp_rule_min = pyo.Constraint(
            model.t, model.zones_therm,
            rule=temp_rule_min,
            doc='restrict the min zone temperature')

        # ------------------------------------------------------------------------------------------------------------------
        # objectives
        # It is always about to maximise or minimise something (i.e. maximise self-consumption from PV)
        # Default is to minimise
        # Important: only one objective can be set per simulation !!!

        # Zone temperature should be as much close to setpoint temp as possible (so: minimise deviation)
        # **2 is the way to penalise deviation (in both directions: lower or higher)
        # model.temp_set = Tz_avg in config file
        # def obj_rule_temperature(m):
        #     return sum(sum((m.temperature[tm, 'Tz', zid] - m.temp_set[zid]) ** 2 for tm in m.tm)
        #                for zid in m.zones_therm)
        def obj_rule_temperature(m):

            return sum(m.temp_slack[zid] for zid in m.zones_therm)

        # Minimise the total cost
        def obj_rule_cost(m):
            return sum((m.e_co_stock_plus[t, ('Hub', 'Elec')]) / 1e6 * (m.elec_price[t] + 180) -
                       (m.e_co_stock_minus[t, ('Hub', 'Elec')]) / 1e6 * m.elec_price[t] +
                       m.e_co_stock_plus[t, 'Hub', 'Heat65'] / 1e6 * m.dh_price[t] +
                       m.e_co_stock_plus[t, 'Hub', 'Gas'] / 1e6 * m.gas_price for t in m.t) + \
                sum(m.temp_slack[zid] for zid in m.zones_therm) * 1e7

        def obj_rule_emi(m):
            return sum(m.e_co_stock_plus[t, ('Hub', 'Elec')] / 1000 * m.emission_elec[t] -
                       m.e_co_stock_minus[t, ('Hub', 'Elec')] / 1000 * m.emission_elec[t] +
                       m.e_co_stock_plus[t, 'Hub', 'Heat65'] / 1000 * m.emission_th[t] +
                       m.e_co_stock_plus[t, 'Hub', 'Gas'] / 1000 * m.emission_gas for t in m.t) + \
                sum(m.temp_slack[zid] for zid in m.zones_therm) * 1e7

        def obj_rule_peak_load(m):
            return m.e_co_stock_cap['Elec'] + m.e_co_stock_cap['Heat65'] * 10e-6 + \
                sum(m.temp_slack[zid] for zid in m.zones_therm) * 1e7

        def obj_rule_RB(m):
            if 'ev' in self.elec_config:
                return -sum(sum(m.e_sto_soc[t, sit, 'Heat55'] * m.penalty_hp[t] for sit in
                                m.buildings_no_hub) for t in m.t) - \
                    sum(sum(m.e_sto_soc[t, sit, 'Elec_ev'] for sit in m.buildings_no_hub) for t in m.t) + \
                    sum(m.temp_slack[zid] for zid in m.zones_therm) * 1e5 + \
                    sum(sum(m.e_zone_in[(t, 'Cold', zid)] for zid in m.zones_therm) for t in m.t)
            else:
                return -sum(sum(m.e_sto_soc[t, sit, 'Heat55'] * m.penalty_hp[t] for sit in
                                m.buildings_no_hub) for t in m.t) + \
                    sum(m.temp_slack[zid] for zid in m.zones_therm) * 1e5 + \
                    sum(sum(m.e_zone_in[(t, 'Cold', zid)] for zid in m.zones_therm) for t in m.t)

        def obj_rule_RB(m):
            return sum(sum(m.temperature[tm, 'Tz', zid] + m.e_zone_in[tm, 'Cold', zid] * 0.1
                           for tm in m.tm) + m.temp_slack[zid] * 1e7 for zid in m.zones_therm) + \
                sum((m.e_co_stock_plus[t, ('Hub', 'Elec')]) / 1e10 -
                       (m.e_co_stock_minus[t, ('Hub', 'Elec')]) / 1e10 for t in m.t)

        def obj_rule_mul_obj(m):
            return sum((m.e_co_stock_plus[t, ('Hub', 'Elec')]) / 1000 * m.emission_elec[t] +
                       m.e_co_stock_plus[t, 'Hub', 'Heat65'] / 1000 * m.emission_th[t] for t in m.t) * 10e-8 + \
                sum((m.e_co_stock_plus[t, ('Hub', 'Elec')]) / 1000 * m.elec_price[t] - \
                    (m.e_co_stock_minus[t, ('Hub', 'Elec')]) / 1000 * m.elec_price[t] * 0.1 +
                    m.e_co_stock_plus[t, 'Hub', 'Heat65'] / 1000 * 0.08 for t in m.t) * 10e-2 + \
                (m.e_co_stock_cap['Elec'] + m.e_co_stock_cap['Heat55'] * 10e-6) * 10e-4 + \
                sum(m.temp_slack[zid] for zid in m.zones_therm) * 1e20

        if calc_type == 'Temp':
            model.obj = pyo.Objective(rule=obj_rule_temperature)
        elif calc_type == 'RB':
            model.obj = pyo.Objective(rule=obj_rule_RB)
        elif calc_type == 'peak_load':
            model.obj = pyo.Objective(rule=obj_rule_peak_load)
        elif calc_type == 'cost':
            model.obj = pyo.Objective(rule=obj_rule_cost)
        elif calc_type == 'emi':
            model.obj = pyo.Objective(rule=obj_rule_emi)
        elif calc_type == 'mul_obj':
            model.obj = pyo.Objective(rule=obj_rule_mul_obj)
        return model

    # UNTIL HERE it was about building the whole optimisation model structure
    # -----------------------------------------------------------------------
    def solve_model(self, model, calc_type, solver, time, scenario_id, j, scenario_location_year):
        """Solve optimisation multiple times according to MPC approach, save outputs and update inputs iteratively.

        Args:
            model (obj): pyomo model.
            self (obj): district containing all the information to be read in.
            solver (str): gurobi, GLPK, CPLEX, etc.
            time (obj): time object with information on time resolution, update interval etc.

        Returns:
            district as copy of original input district enriched by output information."""

        # outputs
        # initialise outputs as attributes of district duplicate
        # Here our object "District" is duplicated and then the results are added, so ...
        # ... one can call them through district_with_results. something
        stepcount = 10
        if self.show_progress:
            app.update_progress(stepcount, 'Solver is solving', 'Solving Model', False, scenario_id, calc_type, j)
        # initialise zone-specific output for analysis
        # -> All temperature vales from the zone
        output_names_zone = [i for i in model.temp_states]
        # -> Demand data
        output_names_zone.extend(['Qh',
                                  'Qc',
                                  'Qhc',
                                  'T_op'])

        # -> then create the outputs above as dummy attributes of the duplicated district object
        self.output_names_zone = output_names_zone
        for building in self.buildings:
            for zone in building.zones:
                for output in self.output_names_zone:
                    setattr(zone, output, pd.Series(index=time.index_timesteps_run, dtype='float64'))

        # (the same initialisation above but for result data added at the district level - not per zone)
        # initialise district-wide output for analysis
        self.output_names = ['import_elec',
                             'import_th',
                             'consum_elec',
                             'consum_th',
                             'consum_cold',
                             'pow_elec_hp',
                             'pow_th_hp',
                             'pow_th_gb',
                             'pow_th_chp',
                             'pow_gas_gb',
                             'pow_elec_eh',
                             'pow_elec_ac',
                             'pow_elec_chp',
                             'pow_th_eh_in',
                             'pow_th_eh_out',
                             'consum_dhw',
                             'gen_pv',
                             'trans_to_hub',
                             'trans_to_bul',
                             'dh_to_bul_1',
                             'dh_to_bul_2',
                             'temp_zone_1',
                             'temp_zone_2',
                             'temp_amb',
                             'pow_elec_bat',
                             'pow_th_tes',
                             'soc_bat_avg',
                             'soc_tes_avg',
                             'soc_ev_1',
                             'consum_ev_drive_1',
                             'consum_ev_charge_1',
                             'consum_ev_drive',
                             'consum_ev_charge',
                             'soc_ev_1_driving',
                             'obj_cost',
                             'obj_co2',
                             'obj_peak',
                             'emi_factor_elec',
                             'emi_factor_dh',
                             'price_elec',
                             'price_dh',
                             'slack_ext',
                             'restload_avg']

        for output in self.output_names:
            setattr(self, output, pd.Series(index=time.index_timesteps_run, dtype='float64'))

        list_renovation_cost = ['renovation_cost_construction',
                                'renovation_cost_hp',
                                'renovation_cost_pv',
                                'renovation_cost_total']
        for output in list_renovation_cost:
            setattr(self, output, 0)

        # create attribute to hold time series for simulation of power grid
        setattr(self, 'time_series_power_grid', {})
        self.time_series_power_grid['building_list'] = [building.building_code for building in self.buildings]
        output_names = {'load', 'sgen'}
        for building in self.buildings:
            for output in output_names:
                self.time_series_power_grid[(building.building_code, output)] = \
                    pd.Series(index=time.index_timesteps_run, dtype='float64')

        # create attribute to house time series for each building
        setattr(self, 'time_series_buildings', {})
        # NOTE: _1 and _2 refer to the first and second zone in the building. Code is so defined for validation check
        columns = ['load_elec', 'grid', 'hp_elec', 'ev', 'pv', 'bat_pow',
                   'cool', 'load_th_sh', 'load_th_dhw', 'tes_pow', 'dh', 'hp_th',
                   'bat_soc', 'tes_soc', 'ev_soc', 'eh_th_in',
                   'eh_elec_in', 'eh_th_out',
                   'temp_slack', 'temp_zone_1', 'temp_zone_2', 'temp_update',
                   'Qsolar_1', 'QconvInt_1', 'QradInt_1',
                   'Qvent_1', 'Qconduc_1', 'Qheating_1',
                   'Qsolar_2', 'QconvInt_2', 'QradInt_2',
                   'Qvent_2', 'Qconduc_2', 'Qheating_2',
                   'temp_amb', 'temp_aggregate_1', 'temp_aggregate_2',
                   'temp_wall_1', 'temp_wall_2',
                   '_Tamb',
                   '_Tz_1', '_TEW_1', '_TIW_1', '_T1_1', '_T2_1', '_Taeq_1', '_Tvent_1',
                   '_Qheating_1', '_Qinfvent_1', '_Qenvelope_1',
                   '_Qsolar_1', '_Qint_people_1', '_Qint_lighting_1', '_Qint_appliances_1',
                   '_Tz_2', '_TEW_2', '_TIW_2', '_T1_2', '_T2_2', '_Taeq_2', '_Tvent_2',
                   '_Qheating_2', '_Qinfvent_2', '_Qenvelope_2',
                   '_Qsolar_2', '_Qint_people_2', '_Qint_lighting_2', '_Qint_appliances_2'
                   ]

        for building in self.buildings:
            self.time_series_buildings[building.building_code] = \
                pd.DataFrame(0, index=time.index_timesteps_run, columns=columns, dtype='float64')

        # --------------------------------------------------------------------------------------------------------------
        # > Create dictionary before Parameter definition block (under space heating), using code below
        # > when the parameter "avg_temp_past" is created, then use the dictionary as initialization (initialise= dict)
        # ---------------------------------------------------------------------
        for building in self.buildings:
            for zone in building.zones:
                if zone.zid in model.zones_therm:
                    model.avg_temp_past[zone.zid] = sum(zone.Tz_avg_past) / len(zone.Tz_avg_past)
        # --------------------------------------------------------------------
        # decide the simulation year based on the scenario
        scenario_year = scenario_location_year.split('_')[1]
        if scenario_year == 'Present' or scenario_year == '2020':
            sim_year = 2020
        elif scenario_year == '2030':
            sim_year = 2030
        elif scenario_year == '2045' or scenario_year == '2050':
            sim_year = 2045
            if scenario_year == '2050':
                print('WARNING: energy cost data missing for scenario year:', scenario_year)
        else:
            print('WARNING: energy cost data missing for scenario year:', scenario_year)
            sim_year = 2020
        print('  > Applied year for Energy costs:', sim_year)

        # preparation of time of use tariffs for electricity #
        # Prior to including sizing run - MB 18.06.2023
        # --------------------------------------------------------------------------------------------------------------
        # starthour = self.config['simulation_settings']['starthour']
        # endhour = self.config['simulation_settings']['endhour']
        # precalc_days = self.config['simulation_settings']['precalc_days']
        # start_fcast = starthour - precalc_days * 24
        # end_fcast = endhour + self.predict_horizon_h
        # n_hours = endhour + precalc_days * 24 + self.predict_horizon_h
        # --------------------------------------------------------------------------------------------------------------
        # New for including sizing run - MB 18.06.2023
        precalc_days = time.precalc_days
        starthour = time.starthour_run + precalc_days * 24
        endhour = time.endhour_predict - self.predict_horizon_h
        start_fcast = time.starthour_run
        end_fcast = time.endhour_predict
        n_hours = endhour + precalc_days * 24 + self.predict_horizon_h
        days = n_hours // 24

        # preparation of energy prices for electricity and district heating, unit in euro per MWh
        price_dh_file = open(f'Libraries/Energy_cost/cost_dh_{sim_year}.json')  # read the input data
        res_dh_price = json.load(price_dh_file)
        res_dh_price_extend = res_dh_price[:24] * precalc_days + res_dh_price + res_dh_price[:24]
        if self.tariff == 'dynamic':
            price_elec_file = open(f'Libraries/Energy_cost/cost_elec_{sim_year}.json')  # read the input data
            res_elec_price = json.load(price_elec_file)
        elif self.tariff == 'ToU':
            # general tariffs
            elec_price_avg = 240
            # preparation of price tariff for electricity
            high_tariff = [[7, 14], [18, 22]]  # time periods for high tariffs
            tariff_rate = [160] * 24  # default price for low tariff
            # set the electricity price within high tariff periods to doppel
            for i in range(len(high_tariff)):
                tariff_rate[high_tariff[i][0]:high_tariff[i][1]] = \
                    [320] * (high_tariff[i][1] - high_tariff[i][0])
            res_elec_price = tariff_rate * days
        elif self.tariff == 'const':
            price_elec_dict = {2020: 225, 2030: 246, 2045: 255}  # total electricity price
            elec_price_const = price_elec_dict[sim_year] - 180  # exclude levy
            res_elec_price = [elec_price_const] * 24 * days
        else:
            raise Exception('no tariff selected')
        res_elec_price_extend = res_elec_price[:24] * precalc_days + res_elec_price + res_elec_price[:24]
        # generate arrays for price time series, 52 and 180 Euro/MWh additional feeds for DH and electricity
        price_dh = pd.DataFrame(data=res_dh_price_extend[start_fcast + precalc_days * 24:end_fcast + precalc_days * 24],
                                index=[np.arange(start_fcast, end_fcast, 1)]) + 52  # unit in g/kWh
        price_elec = pd.DataFrame(
            data=res_elec_price_extend[start_fcast + precalc_days * 24:end_fcast + precalc_days * 24],
            index=[np.arange(start_fcast, end_fcast, 1)])

        # preparation of CO2_emission for electricity and district heating, unit in g/kWh
        emission_dh_file = open(f'Libraries/Energy_emission/emission_dh_{sim_year}.json')  # read the input data
        res_dh_emission = json.load(emission_dh_file)
        res_dh_emission_extend = res_dh_emission[:24] * precalc_days + res_dh_emission + res_dh_emission[:24]
        emission_elec_file = open(f'Libraries/Energy_emission/emission_elec_{sim_year}.json')  # read the input data
        res_elec_emission = json.load(emission_elec_file)
        res_elec_emission_extend = res_elec_emission[:24] * precalc_days + res_elec_emission + res_elec_emission[:24]

        # generate arrays for emission time series
        emission_dh = pd.DataFrame(
            data=res_dh_emission_extend[start_fcast + precalc_days * 24:end_fcast + precalc_days * 24],
            index=[np.arange(start_fcast, end_fcast, 1)]) * 1000
        emission_elec = pd.DataFrame(
            data=res_elec_emission_extend[start_fcast + precalc_days * 24:end_fcast + precalc_days * 24],
            index=[np.arange(start_fcast, end_fcast, 1)]) * 1000

        # preparation of gas price and emission factor, constant for each year
        emission_gas_dict = {2020: 199, 2030: 199, 2045: 199}
        price_gas_dict = {2020: 59, 2030: 63, 2045: 65}
        emission_gas = emission_gas_dict[sim_year]
        price_gas = price_gas_dict[sim_year]

        # pirce for biomass: 300 Euro/t, 4.9 kWh/kg  -> 61 Euro / MWh
        price_bio = 61

        # preparation of penalty factors for HP operation
        time_operation = [[0, 6], [10, 16]]  # time periods in which the heat pump should be turned on if possible
        penalty_day = [0] * 24  # default penalty factor set to 1
        # set the penalty factros within the pre-defined periods to -1
        for i in range(len(time_operation)):
            penalty_day[time_operation[i][0]:time_operation[i][1]] = \
                [1] * (time_operation[i][1] - time_operation[i][0])
        penalty_list_hp = penalty_day * days

        # finish the prediction of the EV
        list_ev_profiles = {}
        list_ev_cap_sum = {}

        logging.debug('simulate electric vehicle start')
        print('  > simulate electric vehicle start')
        nr_seed = 0
        for building in self.buildings:
            ev_profile_file = "Libraries/EV_profiles"  # read the ev data
            # ev_pool_model = ev.EV(self.config, ev_profile_file, starthour, endhour)
            ev_pool_model = ev.EV(self.config, ev_profile_file, starthour=start_fcast, endhour=end_fcast)
            ev_sim_res, ev_cap_sum = ev_pool_model.simulate(n_ev=building.n_ev, nr_seed=nr_seed)
            list_ev_profiles[building.building_code] = ev_sim_res
            list_ev_cap_sum[building.building_code] = ev_cap_sum
            nr_seed = nr_seed + 1
        logging.debug('simulate electric vehicle finished')
        print('  > simulate electric vehicle finished')

        # create the penalty list for rule-based ev control
        penalty_list_ev = np.linspace(0, 100000, num=days * 24)

        # finish the prediction of heat pump COPs
        logging.debug('simulate heat pump start')
        print('  > simulate heat pump start')
        for building in self.buildings:
            for (bid, gid, com) in self.gen_ratio_out_dict:
                if bid == building.building_code:
                    if 'HP' in gid:
                        cap_ref = model.cap_pro[bid, gid, com]
                        cop_ref = model.r_out_dict[bid, gid, com]
                        t_in_ref = -7
                        t_out_ref = 52  # int(heatlevel[-2:])
                        parameters = heatpump.get_parameters(model='Generic', group_id=4,
                                                             t_in=t_in_ref, t_out=t_out_ref, p_th=cap_ref)
                        heatpump_obj = heatpump.HeatPump(parameters)
                        t_amb = building.zones[0].weather['Tamb'][time.starthour_run:time.endhour_predict]
                        # indexes of cos_ts starts always from 0, instead of the number of start_hour
                        cop_ts = heatpump_obj.simulate(t_in_primary=np.array(t_amb), t_in_secondary=t_out_ref - 5,
                                                       t_amb=np.array(t_amb), mode=1)['COP']
        logging.debug('simulate heat pump finished')
        print('  > simulate heat pump finished')

        # update_timesteps_run is based on the simulation duration and update_interval, starting from 0
        for timestep in time.update_timesteps_run:  # e.g. [0.0, 6.0, 12.0, 18.0, ...]
            index_abs = []
            # initialize the parameters of which the values vary with timestep (timestep as part of the index) (see below "-->")
            for k in np.arange(0, self.predict_horizon_h, self.time_resolution_h):
                timestep_abs = time.starthour_run + timestep + k
                timestep_rel = timestep + k
                index_abs.append(timestep_abs)
                # print(timestep_rel)
                # update ev forecasting
                if 'ev' in self.elec_config:
                    for building in self.buildings:
                        sit = building.building_code
                        model.ev_power_fcast[k, sit] = list_ev_profiles[sit].loc[timestep_rel, 'pow_char']
                        # model.ev_avail[k, sit] = list_ev_profiles[sit].loc[timestep_rel, 'availability_cap']
                        model.ev_soc_min_fcast[k, sit] = list_ev_profiles[sit].loc[timestep_rel, 'soc_min']
                        model.ev_soc_driving[k, sit] = list_ev_profiles[sit].loc[timestep_rel, 'soc_driving']
                        if k == self.predict_horizon_h:
                            model.ev_soc_min_fcast[k, sit] = max(list_ev_profiles[sit].loc[timestep_rel, 'soc_min'],
                                                                 list_ev_profiles[sit].loc[
                                                                     timestep_rel, 'soc_min_last'])
                for building in self.buildings:
                    sit = building.building_code
                    model.ev_consumption_fcast[k, sit] = list_ev_profiles[sit].loc[
                        timestep_rel, 'consumption']  # Wh

                # update hp forecasting
                for building in self.buildings:
                    for (bid, gid, com) in self.gen_ratio_out_dict:
                        if bid == building.building_code:
                            if 'HP' in gid:
                                # as the index of cop_ts starts from 0, the relative timestep should be used here
                                model.pro_out_cor[k, building.building_code, gid] = cop_ts[int(timestep_rel)] / cop_ref

                # update energy prices
                model.elec_price[k] = float((price_elec.loc[int(timestep_abs), 0]))
                model.dh_price[k] = float(price_dh.loc[int(timestep_abs), 0])
                model.gas_price = price_gas

                # update biomass price
                model.bio_price = price_bio

                # update CO2 emission
                model.emission_elec[k] = float(emission_elec.loc[int(timestep_abs), 0])
                model.emission_th[k] = float(emission_dh.loc[int(timestep_abs), 0])
                model.emission_gas = emission_gas

                # adjust the penalty factors based on the start hour
                model.penalty_hp[k] = penalty_list_hp[int(timestep_rel)]
                # model.penalty_ev[k] = penalty_list[int(timestep_abs)]

                # set the values for heating period
                if -300 < timestep_abs < 2880 or 6481 < timestep_abs <= 8760:
                    model.heat_period[k] = True
                else:
                    model.heat_period[k] = False
                for building in self.buildings:
                    bid = building.building_code
                    model.temp_amb[k, bid] = building.zones[0].weather['Tamb'][timestep_abs]
                    for zone in building.zones:
                        if zone.zid in model.zones_therm:
                            for n in model.therm_disturb:
                                model.data_therm[k, n, zone.zid] = zone.actives[n][
                                    timestep_abs]  # --> disturbances with index k
                            # for plausibility check
                            model.data_therm[k, 'Qsolar', zone.zid] = zone.actives['Qsolar'][timestep_abs]
                            model.data_therm[k, 'QconvInt', zone.zid] = zone.actives['QconvInt'][timestep_abs]
                            model.data_therm[k, 'QradInt', zone.zid] = zone.actives['QradInt'][timestep_abs]
                            model.data_therm[k, 'Qint_pers', zone.zid] = zone.actives['Qint_pers'][timestep_abs]
                            model.data_therm[k, 'Qint_lighting', zone.zid] = zone.actives['Qint_lighting'][timestep_abs]
                            model.data_therm[k, 'Qint_devices', zone.zid] = zone.actives['Qint_devices'][timestep_abs]
                            model.data_therm[k, 'Q_inf_vent_factor', zone.zid] = zone.actives['Q_inf_vent_factor']
                            model.data_therm[k, 'Q_envelope_factor', zone.zid] = zone.actives['Q_envelope_factor']
                    for bid, com in model.com_demand_fix:
                        if bid == building.building_code:
                            # DHW load with Heat65
                            model.demand_fix[k, bid, 'Heat65'] = building.dhw_load[timestep_abs]  # --> dhw_load
                            # model.demand_fix[k, bid, 'Heat65'] = -building.dhw_load[int(timestep_abs)]  # --> DHCcalc
                            # electrical load
                            model.demand_fix[k, bid, 'Elec'] = building.elec_load_total[timestep_abs]  # --> hh_load
                    for bid, com in model.com_supim:
                        if bid == building.building_code:
                            model.supim[k, bid, com] = building.pv_gen[timestep_abs]  # --> pv_gen

                # Non_resi
                if 'non_resi_load' in self.elec_config:
                    model.demand_fix[k, 'AllNonResi', 'Elec'] = self.non_resi_load[
                        timestep_abs]  # --> non_resi_load
                if 'non_resi_gen' in self.elec_config:
                    model.supim[k, 'AllNonResi', 'Elec'] = self.non_resi_gen[
                        timestep_abs]  # --> non_resi_gen

            # initialize the variables for storage system
            for building in self.buildings:
                # 3 kWh capacity of battery for each zone
                model.sto_cap[building.building_code, 'Elec'] = len(building.zones) * 3000
                # 30 kWh capacity for thermal energy storage
                model.sto_cap[building.building_code, 'Heat55'] = len(building.zones) * 30000
                if 'ev' in self.elec_config:
                    model.sto_cap[building.building_code, 'Elec_ev'] = list_ev_cap_sum[building.building_code]

                if 'bat' in self.elec_config:
                    if timestep == 0.0:
                        model.sto_init_soc[building.building_code, 'Elec'] = 0.5
                    else:
                        model.sto_init_soc[building.building_code, 'Elec'] = \
                            model.e_sto_soc[self.update_interval_h-1, building.building_code, 'Elec']()
                if 'tes' in self.config['simulation_settings']['heat_config']:
                    if timestep == 0.0:
                        model.sto_init_soc[building.building_code, 'Heat55'] = 0.5
                    else:
                        model.sto_init_soc[building.building_code, 'Heat55'] = \
                            model.e_sto_soc[self.update_interval_h-1, building.building_code, 'Heat55']()
                else:  # TODO - Priority 1 - Responsible 'ZY' - comment by MB: this is a bug, when no 'tes' in heat_config, the simulation does not work
                    model.sto_init_soc[building.building_code, 'Heat55'] = 0

                if 'ev' in self.elec_config:
                    if timestep == 0.0:
                        model.sto_init_soc[building.building_code, 'Elec_ev'] = 0.8
                    else:
                        model.sto_init_soc[building.building_code, 'Elec_ev'] = \
                            model.e_sto_soc[self.update_interval_h-1, building.building_code, 'Elec_ev']()

            # initialize variables regarding thermal behavior
            for building in self.buildings:
                for zone in building.zones:
                    if zone.zid in model.zones_therm:

                        # first update timestep initialized with start_dict values
                        if timestep == 0.0:
                            model.e_zone_in[0.0, 'Heat55', zone.zid].value = zone.start_dict['Qh']
                            model.e_zone_in[0.0, 'Cold', zone.zid].value = zone.start_dict['Qc']
                            for i in model.temp_states:
                                # model.temperature[0.0, i, zone.zid].fix(zone.start_dict[i])
                                model.temperature[0.0, i, zone.zid].fix(zone.start_dict[i])

                        # following update timesteps initialized with corresponding values of prior update timestep
                        else:
                            model.e_zone_in[0.0, building.heat_level_sh, zone.zid].fix(
                                model.e_zone_in[self.update_interval_h, building.heat_level_sh, zone.zid]())
                            # model.e_zone_in[0.0, 'Cold', zone.zid].fix(
                            #     model.e_zone_in[self.update_interval_h, 'Cold', zone.zid]())
                            for i in model.temp_states:
                                model.temperature[0.0, i, zone.zid].fix(
                                    model.temperature[self.update_interval_h, i, zone.zid]())
                            # recalculate Tz_avg_past
                            Tz_list = [model.temperature[k, 'Tz', zone.zid]() for k in
                                       np.arange(model.update_avg_temp())]
                            Tz_avg_timestep = sum(Tz_list) / len(Tz_list)
                            Tz_avg_past_new = zone.Tz_avg_past[1:]
                            Tz_avg_past_new.append(Tz_avg_timestep)
                            zone.Tz_avg_past = Tz_avg_past_new
                            model.avg_temp_past[zone.zid] = sum(zone.Tz_avg_past) / len(zone.Tz_avg_past)

            # model.pprint()  # TODO - Priority 4 (deadline: 01.09.2023) - Responsible "AN" -  comment by = ? - make sure pprint also works when "time_resolution_h" in config is a float (currently only with integrer is working)

            # solving
            # model.setParam("ScaleFlag", 2)
            res_sim = solver.solve(model, tee=False)
            self.error_logging.append(res_sim.solver.status)
            # log_infeasible_constraints(model, log_expression=True, log_variables=True)
            # logging.basicConfig(filename=f'example_{timestep_abs}.log', encoding='utf-8', level=logging.INFO)
            print('      Solution of timestep:', timestep, '-', timestep + self.update_interval_h)
            logging.debug('Solution of timestep: %s', timestep)
            stepcount += 10
            if self.show_progress:
                app.update_progress(stepcount, 'Solver is solving', 'Solving Model', True, scenario_id, calc_type, j)
            # --> also for debugging:
            # log_infeasible_constraints(model)

            # outputs # TODO - Priority 4 (deadline: 01.09.2023) - Responsible "ZY" -  comment by = ? - make output procedure more general and transferable to other outputs / configurations (too specific now)
            # for k in np.arange(0.0, update_interval_h, time_resolution_h):
            for k in np.arange(0.0, self.update_interval_h, self.time_resolution_h):

                timestep_abs = time.starthour_run + timestep + k

                # district-wide outputs
                # electrical power imported
                self.import_elec.loc[timestep_abs] = model.e_co_stock_plus[k, 'Hub', 'Elec']() - model.e_co_stock_minus[
                    k, 'Hub', 'Elec']()
                # thermal power imported
                self.import_th.loc[timestep_abs] = model.e_co_stock_plus[k, 'Hub', 'Heat65']()
                # sum of fixed electrical consumption of the district
                self.consum_elec.loc[timestep_abs] = sum(
                    [model.demand_fix[k, bid, 'Elec']() for bid in model.buildings if
                     (bid, 'Elec') in model.com_demand_fix])
                # sum of heating power for zones
                self.consum_th.loc[timestep_abs] = sum(
                    [model.e_zone_in[k, 'Heat55', zid]() for zid in model.zones_therm])
                # sum of cooling power for zones
                self.consum_cold.loc[timestep_abs] = sum(
                    [model.e_zone_in[k, 'Cold', zid]() for zid in model.zones_therm])
                # sum of electrical consumption of heat pumps
                self.pow_elec_hp.loc[timestep_abs] = sum([model.e_pro_in[(k, bid, pro, 'Elec')]()
                                                          for bid, pro in model.pro_tuples if 'HP' in pro])
                # sum of thermal output of heat pumps
                self.pow_th_hp.loc[timestep_abs] = sum([model.e_pro_out[(k, bid, pro, 'Heat55')]()
                                                        for bid, pro in model.pro_tuples if 'HP' in pro])
                # sum of gas consumption and thermal output of gas boilder
                self.pow_gas_gb.loc[timestep_abs] = model.e_co_stock_plus[k, 'Hub', 'Gas']()
                self.pow_th_gb.loc[timestep_abs] = sum([model.e_pro_out[(k, bid, pro, 'Heat65')]()
                                                        for bid, pro in model.pro_tuples if 'GB' in pro])
                # sum of electrical consumption of cooling devices
                self.pow_elec_ac.loc[timestep_abs] = sum(model.e_pro_in[(k, bid, pro, 'Bio')]()
                                                         for bid, pro in model.pro_tuples if 'AC' in pro)
                # sum of electrical heater for DHW
                self.pow_elec_eh.loc[timestep_abs] = sum(model.e_pro_in[(k, bid, pro, 'Elec')]()
                                                         for bid, pro in model.pro_tuples if 'EH' in pro)
                self.pow_th_eh_in.loc[timestep_abs] = sum(model.e_pro_in[(k, bid, pro, 'Heat55')]()
                                                          for bid, pro in model.pro_tuples if 'EH' in pro)
                self.pow_th_eh_out.loc[timestep_abs] = sum(model.e_pro_out[(k, bid, pro, 'Heat65')]()
                                                           for bid, pro in model.pro_tuples if 'EH' in pro)
                # sum of combined heat and power
                self.pow_th_chp.loc[timestep_abs] = sum(model.e_pro_out[(k, bid, pro, 'Heat55')]()
                                                        for bid, pro in model.pro_tuples if 'BHKW' in pro)

                self.pow_elec_chp.loc[timestep_abs] = sum(model.e_pro_out[(k, bid, pro, 'Elec')]()
                                                          for bid, pro in model.pro_tuples if 'BHKW' in pro)

                # sum of domestic hot water consumption of the district
                self.consum_dhw.loc[timestep_abs] = sum([model.demand_fix[k, bid, 'Heat55']() for bid in model.buildings
                                                         if (bid, 'Heat55') in model.com_demand_fix]) + \
                                                    sum([model.demand_fix[k, bid, 'Heat65']() for bid in model.buildings
                                                         if (bid, 'Heat65') in model.com_demand_fix])
                # sum of electrical generation of the district
                self.gen_pv.loc[timestep_abs] = sum(
                    [model.supim[k, bid, 'Elec']() for bid in model.buildings if (bid, 'Elec') in model.com_supim])
                # sum of transmission power flow between hub and buildings
                self.trans_to_hub.loc[timestep_abs] = sum(
                    [model.e_tra_in[k, building.building_code, 'Hub', 'Elec_only', 'Elec']() for building in
                     self.buildings])
                self.trans_to_bul.loc[timestep_abs] = sum(
                    [model.e_tra_in[k, 'Hub', building.building_code, 'Elec_only', 'Elec']() for building in
                     self.buildings])
                self.dh_to_bul_1.loc[timestep_abs] = \
                    model.e_tra_in[k, 'Hub', self.buildings[0].building_code, 'Heat_only', 'Heat65']()
                # zone temperature
                # zone temperature of building 1 zone 1
                self.temp_zone_1.loc[timestep_abs] = model.temperature[
                    k, 'Tz', self.buildings[0].zones[-1].zid]()

                # ambient temperature and thermal power of heat pump of the building 1
                self.temp_amb.loc[timestep_abs] = model.temp_amb[k, self.buildings[0].building_code]()

                # balance of all battery systems
                self.pow_elec_bat[timestep_abs] = sum([model.e_sto_out[k, bid, 'Elec']()
                                                       - model.e_sto_in[k, bid, 'Elec']()
                                                       for bid in model.buildings if (bid, 'Elec') in model.com_sto])
                self.soc_bat_avg[timestep_abs] = np.mean([model.e_sto_soc[k, bid, 'Elec']()
                                                          for bid in model.buildings if (bid, 'Elec') in model.com_sto])
                # balance of all thermal energy systems
                self.pow_th_tes[timestep_abs] = sum([model.e_sto_out[k, bid, 'Heat55']()
                                                     - model.e_sto_in[k, bid, 'Heat55']()
                                                     for bid in model.buildings if (bid, 'Heat55') in model.com_sto])
                self.soc_tes_avg[timestep_abs] = np.mean([model.e_sto_soc[k, bid, 'Heat55']()
                                                          for bid in model.buildings if
                                                          (bid, 'Heat55') in model.com_sto])
                if 'ev' in self.elec_config:
                    self.soc_ev_1[timestep_abs] = model.e_sto_soc[k, self.buildings[0].building_code, 'Elec_ev']()
                    # self.soc_ev_2[timestep_abs] = model.e_sto_soc[k, self.buildings[1].building_code, 'Elec_ev']()
                    # balance of ev
                    self.consum_ev_drive[timestep_abs] = sum([model.e_sto_out[k, bid, 'Elec_ev']()
                                                              for bid in model.buildings if (bid, 'Elec_ev')
                                                              in model.com_sto])
                    self.consum_ev_drive_1[timestep_abs] = model.e_sto_out[k, self.buildings[0].building_code, 'Elec_ev']()
                    self.consum_ev_charge[timestep_abs] = sum([-model.e_sto_in[k, bid, 'Elec_ev']()
                                                               for bid in model.buildings if (bid, 'Elec_ev')
                                                               in model.com_sto])
                    self.consum_ev_charge_1[timestep_abs] = -model.e_sto_in[k, self.buildings[0].building_code, 'Elec_ev']()
                    self.soc_ev_1_driving[timestep_abs] = model.ev_soc_driving[k, self.buildings[0].building_code]()
                    # self.soc_ev_2_driving[timestep_abs] = model.ev_soc_driving[k, self.buildings[1].building_code]()

                # emission factor and prices for electricity and district heating
                self.emi_factor_elec[timestep_abs] = model.emission_elec[k]()
                self.emi_factor_dh[timestep_abs] = model.emission_th[k]()
                self.price_elec[timestep_abs] = model.elec_price[k]()
                self.price_dh[timestep_abs] = model.dh_price[k]()

                # calculation of renovation costs
                self.renovation_cost_construction = self.renovation_costs_construction
                self.renovation_cost_hp = self.renovation_costs_hp
                self.renovation_cost_pv = self.renovation_costs_pv
                self.renovation_cost_total = self.renovation_costs_total

                # summary of objectives
                pow_balance_sum = (model.e_co_stock_plus[k, ('Hub', 'Elec')]() -
                                   model.e_co_stock_minus[k, ('Hub', 'Elec')]()) / 1e6  # in MW
                self.obj_cost[timestep_abs] = (pow_balance_sum * (model.elec_price[k]() + 180)
                                               if pow_balance_sum >= 0 else pow_balance_sum * model.elec_price[k]()) + \
                                              model.e_co_stock_plus[k, 'Hub', 'Heat65']() / 1e6 * model.dh_price[k]() + \
                                              model.e_co_stock_plus[k, 'Hub', 'Gas']() / 1e6 * model.gas_price()
                # self.obj_cost[timestep_abs] = model.e_co_stock_plus[k, ('Hub', 'Elec')]() / 1e6 * \
                #                               (model.elec_price[k]() + 180) - \
                #                               model.e_co_stock_minus[k, ('Hub', 'Elec')]() / 1e6 * \
                #                               model.elec_price[k]() + \
                #                               model.e_co_stock_plus[k, 'Hub', 'Heat65']() / 1e6 * model.dh_price[k]() + \
                #                               model.e_co_stock_plus[k, 'Hub', 'Gas']() / 1e6 * model.gas_price()
                self.obj_co2[timestep_abs] = (model.e_co_stock_plus[k, ('Hub', 'Elec')]() - \
                                              model.e_co_stock_minus[k, ('Hub', 'Elec')]()) / 1000 * \
                                             model.emission_elec[k]() + \
                                             model.e_co_stock_plus[k, 'Hub', 'Heat65']() / 1000 * model.emission_th[
                                                 k]() + \
                                             model.e_co_stock_plus[k, 'Hub', 'Gas']() / 1000 * model.emission_gas()
                self.obj_peak[timestep_abs] = model.e_co_stock_plus[k, 'Hub', 'Elec']() - \
                                              model.e_co_stock_minus[k, 'Hub', 'Elec']()
                # self.ev_slack[timestep_abs] = str([model.ev_slack[sit]() for sit in model.buildings_no_hub])

                # prepare the generation and consumption profile of each building for power grid simulation
                for building in self.buildings:
                    self.time_series_power_grid[(building.building_code, 'load')].loc[timestep_abs] = \
                        model.demand_fix[k, building.building_code, 'Elec']()
                    self.time_series_power_grid[(building.building_code, 'sgen')].loc[timestep_abs] = \
                        model.supim[k, building.building_code, 'Elec']() if 'pv_gen' in self.elec_config else 0

                # outputs_zone
                for building in self.buildings:
                    for zone in building.zones:
                        zone.Qh.loc[timestep_abs] = model.e_zone_in[k, building.heat_level_sh, zone.zid]()
                        zone.Qc.loc[timestep_abs] = model.e_zone_in[k, 'Cold', zone.zid]()
                        for i in model.temp_states:
                            # zone.Temperatures[i].loc[timestep_abs] = model.x[i, k, zone.zid]()
                            getattr(zone, i).loc[timestep_abs] = model.temperature[k, i, zone.zid]()
                        zone.T_op.loc[timestep_abs] = ((model.temperature[k, 'T2', zone.zid]() * zone.area_int / (
                                zone.area_sum - zone.area_windows)
                                                        + model.temperature[k, 'T1', zone.zid]() * zone.area_ext / (
                                                                zone.area_sum - zone.area_windows))
                                                       + model.temperature[k, 'Tz', zone.zid]()) / 2

                # create outputs for each building
                for building in self.buildings:
                    bid = building.building_code
                    df_building = self.time_series_buildings[bid]
                    df_building.loc[timestep_abs, 'load_elec'] = model.demand_fix[k, bid, 'Elec']() \
                        if (bid, 'Elec') in model.com_demand_fix else 0
                    df_building.loc[timestep_abs, 'grid'] = \
                        model.e_tra_in[k, 'Hub', building.building_code, 'Elec_only', 'Elec']() - \
                        model.e_tra_in[k, building.building_code, 'Hub', 'Elec_only', 'Elec']()
                    df_building.loc[timestep_abs, 'hp_elec'] = sum(model.e_pro_in[(k, bid, pro, 'Elec')]()
                                                                   for bul, pro in model.pro_tuples
                                                                   if 'HP' in pro and bul == bid)
                    df_building.loc[timestep_abs, 'hp_th'] = sum(model.e_pro_out[(k, bid, pro, 'Heat55')]()
                                                                 for bul, pro in model.pro_tuples
                                                                 if 'HP' in pro and bul == bid)
                    if 'ev' in self.elec_config:
                        df_building.loc[timestep_abs, 'ev_soc'] = model.e_sto_soc[k, bid, 'Elec_ev']()
                        df_building.loc[timestep_abs, 'ev'] = -model.e_sto_in[k, bid, 'Elec_ev']() \
                            if (bid, 'Elec_ev') in model.com_sto else 0
                    df_building.loc[timestep_abs, 'pv'] = model.supim[k, bid, 'Elec']() \
                        if (bid, 'Elec') in model.com_supim else 0
                    df_building.loc[timestep_abs, 'bat_pow'] = model.e_sto_out[k, bid, 'Elec']() - \
                                                               model.e_sto_in[k, bid, 'Elec']() \
                        if (bid, 'Elec') in model.com_sto else 0
                    df_building.loc[timestep_abs, 'eh_th_in'] = sum(model.e_pro_in[(k, bid, pro, 'Heat55')]()
                                                                    for bul, pro in model.pro_tuples
                                                                    if 'EH' in pro and bul == bid)
                    df_building.loc[timestep_abs, 'eh_elec_in'] = sum(model.e_pro_in[(k, bid, pro, 'Elec')]()
                                                                      for bul, pro in model.pro_tuples
                                                                      if 'EH' in pro and bul == bid)
                    df_building.loc[timestep_abs, 'eh_th_out'] = sum(model.e_pro_out[(k, bid, pro, 'Heat65')]()
                                                                     for bul, pro in model.pro_tuples
                                                                     if 'EH' in pro and bul == bid)
                    df_building.loc[timestep_abs, 'bat_soc'] = model.e_sto_soc[k, bid, 'Elec']()
                    df_building.loc[timestep_abs, 'tes_soc'] = model.e_sto_soc[k, bid, 'Heat55']()
                    df_building.loc[timestep_abs, 'cool'] = sum(model.e_pro_in[(k, bid, pro, 'Elec')]()
                                                                for bul, pro in model.pro_tuples
                                                                if 'Cool' in pro and bul == bid)
                    df_building.loc[timestep_abs, 'load_th_sh'] = sum(model.e_zone_in[k, 'Heat55', zone.zid]()
                                                                      for zone in building.zones)
                    df_building.loc[timestep_abs, 'load_th_dhw'] = model.demand_fix[k, bid, 'Heat65']()

                    df_building.loc[timestep_abs, 'tes_pow'] = (model.e_sto_out[k, bid, 'Heat55']() -
                                                                model.e_sto_in[k, bid, 'Heat55']()) \
                        if (bid, 'Heat55') in model.com_sto else 0
                    df_building.loc[timestep_abs, 'dh'] = \
                        model.e_tra_in[k, 'Hub', building.building_code, 'Heat_only', 'Heat65']()

                    df_building.loc[timestep_abs, 'temp_slack'] = str(
                        [model.temp_slack[zone.zid]() for zone in building.zones])
                    # calculate the temperature changes by different sources
                    for idx in [1]:
                        zid = building.zones[idx - 1].zid
                        param_to_power_factor = model.param_B2['Tz', 'Heat55', zid]()
                        # MB 25.05.2023:
                        # Temperatures
                        df_building.loc[timestep_abs, f'_Tamb'] = round(model.temp_amb[k, building.building_code](), 2)
                        df_building.loc[timestep_abs, f'_TEW_{idx}'] = round(model.temperature[k, 'TEW', zid](), 2)
                        df_building.loc[timestep_abs, f'_TIW_{idx}'] = round(model.temperature[k, 'TIW', zid](), 2)
                        df_building.loc[timestep_abs, f'_Tz_{idx}'] = round(model.temperature[k, 'Tz', zid](), 2)
                        df_building.loc[timestep_abs, f'_T1_{idx}'] = round(model.temperature[k, 'T1', zid](), 2)
                        df_building.loc[timestep_abs, f'_T2_{idx}'] = round(model.temperature[k, 'T2', zid](), 2)
                        df_building.loc[timestep_abs, f'_Taeq_{idx}'] = round(model.data_therm[k, 'Taeq', zid](), 2)
                        df_building.loc[timestep_abs, f'_Tvent_{idx}'] = round(model.data_therm[k, 'Tvent', zid](), 2)
                        # Thermal loads
                        _Q_heating = round(model.e_zone_in[k, 'Heat55', zone.zid](), 2)
                        _Q_solar = round(model.data_therm[k, 'Qsolar', zid](), 2)
                        _Q_int_people = round(model.data_therm[k, 'Qint_pers', zid](), 2)
                        _Q_int_lighting = round(model.data_therm[k, 'Qint_lighting', zid](), 2)
                        _Q_int_appliances = round(model.data_therm[k, 'Qint_devices', zid](), 2)
                        _Q_inf_vent = round(model.data_therm[k, 'Q_inf_vent_factor', zid]() *
                                            (model.data_therm[k, 'Tvent', zid]() -
                                             model.temperature[k, 'Tz', zid]()), 2)

                        _Q_envelope = round(model.data_therm[k, 'Q_envelope_factor', zid]() *
                                            (model.temp_amb[k, building.building_code]() -
                                             model.temperature[k, 'Tz', zid]()), 2)

                        df_building.loc[timestep_abs, f'_Qheating_{idx}'] = _Q_heating
                        df_building.loc[timestep_abs, f'_Qsolar_{idx}'] = _Q_solar
                        df_building.loc[timestep_abs, f'_Qint_people_{idx}'] = _Q_int_people
                        df_building.loc[timestep_abs, f'_Qint_lighting_{idx}'] = _Q_int_lighting
                        df_building.loc[timestep_abs, f'_Qint_appliances_{idx}'] = _Q_int_appliances
                        df_building.loc[timestep_abs, f'_Qinfvent_{idx}'] = _Q_inf_vent
                        df_building.loc[timestep_abs, f'_Qenvelope_{idx}'] = _Q_envelope

                        # temp aggregated
                        df_building.loc[timestep_abs, f'temp_aggregate_{idx}'] = sum(model.param_A['Tz', n, zid]() *
                                                                                     model.temperature[k, n, zid]()
                                                                                     for n in model.temp_states) + \
                                                                                 (model.param_B1['Tz', 'Taeq', zid]() +
                                                                                  model.param_B1[
                                                                                      'Tz', 'Tvent', zid]()) * \
                                                                                 model.temperature[k, 'Tz', zid]()
                        # wall temperature
                        df_building.loc[timestep_abs, f'temp_wall_{idx}'] = (model.temperature[k, 'TEW', zid]() + \
                                                                             model.temperature[k, 'TIW', zid]()) / 2

                        # conduction losses
                        df_building.loc[timestep_abs, f'Qconduc_{idx}'] = (sum(model.param_A['Tz', n, zid]() *
                                                                               model.temperature[k, n, zid]() for n in
                                                                               model.temp_states) +
                                                                           model.param_B1['Tz', 'Taeq', zid]() *
                                                                           model.data_therm[k, 'Taeq', zid]() +
                                                                           model.param_B1['Tz', 'Tvent', zid]() *
                                                                           model.temperature[k, 'Tz', zid]() -
                                                                           model.temperature[
                                                                               k, 'Tz', zid]()) / param_to_power_factor
                        # ventilation losses
                        df_building.loc[timestep_abs, f'Qvent_{idx}'] = model.param_B1['Tz', 'Tvent', zid]() * \
                                                                        (model.data_therm[k, 'Tvent', zid]() -
                                                                         model.temperature[k, 'Tz', zid]()) \
                                                                        / param_to_power_factor
                        # else:
                        #     df_building.loc[timestep_abs, f'hb_conduc_{idx}'] = 0
                        # solar gains
                        df_building.loc[timestep_abs, f'Qsolar_{idx}'] = model.param_B1['Tz', 'QEW', zid]() * \
                                                                         model.data_therm[k, 'Qsolar', zid]() / \
                                                                         param_to_power_factor
                        # internal convective gains
                        df_building.loc[timestep_abs, f'QconvInt_{idx}'] = model.param_B1['Tz', 'Qcon', zid]() * \
                                                                           model.data_therm[k, 'QconvInt', zid]() / \
                                                                           param_to_power_factor
                        # internal radiative gains
                        df_building.loc[timestep_abs, f'QradInt_{idx}'] = model.param_B1['Tz', 'QEW', zid]() * \
                                                                          model.data_therm[k, 'QradInt', zid]() / \
                                                                          param_to_power_factor
                        # heating device
                        df_building.loc[timestep_abs, f'Qheating_{idx}'] = model.e_zone_in[k, 'Heat55', zid]() - \
                                                                           model.e_zone_in[k, 'Cold', zid]() * \
                                                                           model.param_B2['Tz', 'Cold', zid]() / \
                                                                           param_to_power_factor
                        df_building.loc[timestep_abs, f'temp_zone_{idx}'] = round(model.temperature[k, 'Tz', zid](), 2)
                        df_building.loc[timestep_abs, 'temp_amb'] = model.temp_amb[k, building.building_code]()

        # (above, outputs for district and zone, ... it is also possible to generate here outputs at the building level)

        # for short plausibility check
        '''for x in np.arange(1.0, update_interval_h, time_resolution_h):
            print('MFH1-Zone_1 -- Heat', model.e_zone_in[x, ('Heat70', 'MFH1-Zone_1')]())
            print('MFH1-Zone_1 -- Cold', model.e_zone_in[x, ('Cold', 'MFH1-Zone_1')]())
            print('MFH1-Zone_1 -- Tz', model.temperature[x, 'Tz', 'MFH1-Zone_1']())
            print('MFH1 -- tau_pro', model.tau_pro[x, 'MFH1', 'GSHP']())
            print('MFH1 -- tau_pro', model.tau_pro[x, 'MFH1', 'Cooling']())'''

    def update_district(self, config):
        self.config = config
        self.time_resolution_h = config['simulation_settings']['time_resolution_h']
        self.predict_horizon_h = config['simulation_settings']['predict_horizon_h']
        self.update_interval_h = config['simulation_settings']['update_interval_h']
        self.elec_config = config['simulation_settings']['elec_config']
        self.show_progress = config['progress_bar_setting']['show_progress']
        # self.tariff = config['simulation_settings']['tariff']

    @staticmethod
    def create_dict_with_indexed_param(matrix_name, district, subset_zones_therm, Ndim1, Ndim2):
        """Create a dictionary with multiple keys from raw numpy array.

        Args:
            matrix_name (str): name of the matrix {A_dis, B1_dis, B2_dis}.
            district (obj): district object which contains the information on zones and their matrices as numpy array.
            subset_zones_therm (list): list of zoneIDs for which the thermal behavior is modelled explicitly.
            Ndim1 (list): list of labels referring to dimension1 of the corresponding matrix.
            Ndim2 (list): list of labels referring to dimension2 of the corresponding matrix.

        The length of the lists corresponds with the rows and columns of the matrix, and the order needs to be correct.

        Returns:
            dictionary with the keys given by the labels and values from the matrix.
        """

        param_indexed = {}

        for building in district.buildings:
            for zone in building.zones:
                if zone.zid in subset_zones_therm:
                    for i, N1 in enumerate(Ndim1):
                        if Ndim2 == 'heat-cold':
                            Ndim2_updated = [building.heat_level_sh, 'Cold']
                        else:
                            Ndim2_updated = Ndim2
                        for j, N2 in enumerate(Ndim2_updated):
                            if matrix_name == 'A_dis':
                                param_indexed[N1, N2, zone.zid] = float(zone.A_dis[i, j])
                            elif matrix_name == 'B1_dis':
                                param_indexed[N1, N2, zone.zid] = float(zone.B1_dis[i, j])
                            elif matrix_name == 'B2_dis':
                                param_indexed[N1, N2, zone.zid] = float(zone.B2_dis[i, j])

        return param_indexed

    @staticmethod
    def sum_renovation_costs(buildings, key):
        costs = 0
        for building in buildings:
            base_dict = {'construction': building.renovation_costs_construction,
                         'hp': building.renovation_costs_hp,
                         'pv': building.renovation_costs_pv,
                         'total': building.renovation_costs_total}
            costs += base_dict[key]
        return costs
