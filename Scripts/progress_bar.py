import tkinter as tk
from tkinter import *
from tkinter import ttk
import threading
import time

class ProgressBarApp:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title("CleanMod Simulation")
        self.root.geometry("400x90")

        self.style = ttk.Style(self.root)
        self.style.layout('text.Horizontal.TProgressbar',
                     [('Horizontal.Progressbar.trough',
                       {'children': [('Horizontal.Progressbar.pbar',
                                      {'side': 'left', 'sticky': 'ns'})],
                        'sticky': 'nswe'}),
                      ('Horizontal.Progressbar.label', {'sticky': ''})])
        self.style.configure('text.Horizontal.TProgressbar', text='0 %')

        self.style2 = ttk.Style(self.root)
        self.style2.layout('text.Horizontal.TProgressbar2',
                     [('Horizontal.Progressbar.trough',
                       {'children': [('Horizontal.Progressbar.pbar',
                                      {'side': 'left', 'sticky': 'ns'})],
                        'sticky': 'nswe'}),
                      ('Horizontal.Progressbar.label', {'sticky': ''})])
        self.style2.configure('text.Horizontal.TProgressbar2', text='0 %')

        self.style3 = ttk.Style(self.root)
        self.style3.layout('text.Horizontal.TProgressbar3',
                     [('Horizontal.Progressbar.trough',
                       {'children': [('Horizontal.Progressbar.pbar',
                                      {'side': 'left', 'sticky': 'ns'})],
                        'sticky': 'nswe'}),
                      ('Horizontal.Progressbar.label', {'sticky': ''})])
        self.style3.configure('text.Horizontal.TProgressbar3', text='Background tasks')

        self.my_progress = ttk.Progressbar(self.root, style='text.Horizontal.TProgressbar', length=350, mode='determinate')
        self.my_progress2 = ttk.Progressbar(self.root, style='text.Horizontal.TProgressbar2', length=350, mode='determinate')
        self.my_progress3 = ttk.Progressbar(self.root, style='text.Horizontal.TProgressbar3', length=350, mode='indeterminate')

        self.my_progress.pack()
        self.my_progress2.pack()

        self.style3.configure('text.Horizontal.TProgressbar3', text='Background tasks' )
        self.my_progress3.pack()

        self.my_Message = Message(self.root, text='', width=200, anchor='w')
        self.my_Message.pack()


    def Refresh(self):
        self.update_progress(50, "Refreshing ... ", "...", True, "...", "...", 50)

    def update_progress(self, percent, message, stage, progress, scenario, c_type, j):
        if progress == True:
            self.my_progress['value'] += percent
        else:
            self.my_progress['value'] = percent

        self.my_progress2['value'] = j
        self.style.configure('text.Horizontal.TProgressbar', text=stage + f": Progress: {self.my_progress['value']}%")
        self.style2.configure('text.Horizontal.TProgressbar2', text=f"Scenario: " + scenario + " - calc_type: " + c_type)
        self.my_Message.config(text=str(message))
        self.my_Message.pack()
        self.root.update_idletasks()

    def UEP_Progress(self, Switch):
        if Switch:
            self.style3.configure('text.Horizontal.TProgressbar3', text=f"UEP is Running ")
            self.my_progress3.start()
            for i in range(0, 101, 10):
                self.my_progress3['value'] = i
                time.sleep(1)
                self.root.update_idletasks()
            self.my_progress3['value'] = 50
            self.root.update_idletasks()
        else:
            self.style3.configure('text.Horizontal.TProgressbar3', text=f"UEP files are ready")
            self.my_progress3.stop()
            self.root.update_idletasks()



    def run(self):
        self.root.mainloop()

    def close(self):
        self.root.destroy()


# create an instance of the ProgressBarApp class
app = ProgressBarApp()



