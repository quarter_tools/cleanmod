import pandas as pd
from random import choice, choices
import random
import numpy as np
import os
import time
import matplotlib.pyplot as plt
from ruamel.yaml import YAML


class EV:

    def __init__(self, config, input_file_path, starthour, endhour):
        self.config = config
        self.file_path = input_file_path
        self.starthour = starthour
        self.endhour = endhour

    def simulate(self, n_ev=80, nr_seed=0):
        # Forecasting of distance drived and availability of EVs
        files = os.listdir(self.file_path)
        ev_profile_files = [os.path.join(self.file_path, basename) for basename in files]
        # df_in.set_index("timestamp", inplace=True)

        # TODO - Priority 2 (deadline: until 07.2023) - Responsible "ZY" -  comment by = MB 28.04.2023: fix bug. Code does not work when n_ev = 0
        if n_ev == 0:
            n_ev = 1
            print('\nWARNING !!! :  This is a bug which needs to be fixed. Code does not run if n_ve = 0.' +
                  ' n_ve = 1 has been provisionally applied to allow simulating. Check electric_vehicle.py')

        list_ev = [None] * n_ev
        # len_total = len(list(df_ev.index.values))

        # configure the time settings
        predict_horizon_h = self.config['simulation_settings']['predict_horizon_h']
        update_interval_h = self.config['simulation_settings']['update_interval_h']
        time_resolution_h = self.config['simulation_settings']['time_resolution_h']
        start_date = pd.Timestamp('2021-01-01') + pd.to_timedelta(self.starthour, unit='H')
        end_date = pd.Timestamp('2021-01-01') + pd.to_timedelta(self.endhour - update_interval_h
                                                                + predict_horizon_h, unit='H')
        time_start_unix = time.mktime(start_date.timetuple())
        time_end_unix = time.mktime(end_date.timetuple())
        random.seed(nr_seed)

        for i in range(n_ev):
            # the distribution of the dataset is: 20% part-time, 10% free-time, 70% full-time
            path_single = choice(ev_profile_files)
            df_ev_input = pd.read_csv(path_single, index_col='timestamp')
            df_in = df_ev_input.loc[time_start_unix:time_end_unix - 900]
            df_in = df_in.reset_index()
            df_in = df_in.drop(columns=['timestamp'])
            # df_in.set_index([pd.Index(range(predict_horizon_h))], inplace=True)
            df_in = df_in.groupby(df_in.index // 4).sum()
            df_in.loc[(df_in.availability <= 2) & (df_in.distance_driven < 0.01), 'availability'] = 0
            df_in = df_in.clip(upper=pd.Series({'availability': 1, 'distance_driven': 2000}), axis=1)
            # df_in.loc[(df_in.availability <= 2) & (df_in.distance_driven < 0.01), 'availability'] = 0
            list_ev[i] = df_in

        ev_fcast_sum = pd.DataFrame(index=df_in.index.values, columns=['pow_char', 'soc_min', 'soc_min_last',
                                                                       'soc_driving', 'soc_min_leave',
                                                                       'consumption', 'availability'])
        ev_fcast_sum[:] = 0
        # ev_pow_char_list = [22.2, 22.2, 22.2, 22.2, 22.2, 11.1, 11.1, 11.1, 3.7, 3.7]
        # list_ev_pow_char = [choice(ev_pow_char_list) for i in range(n_ev)]
        ev_cap_list = [15, 20, 25, 35, 40, 45, 50, 55, 60, 65, 80, 90, 95]
        weights = [4, 3, 2, 4, 11, 7, 2, 2, 1, 4, 2, 1, 4]
        list_ev_cap = choices(ev_cap_list, weights, k=n_ev)
        list_ev_pow_char = [np.clip(list_ev_cap[i]*0.5, 10, 20) for i in range(n_ev)]
        # list_ev_pow_char = [max(i, 100) for i in range(n_ev)]
        # list_ev_pow_char = [list_ev_cap[i] * 2 / 15 + 10 for i in range(n_ev)]
        ev_cap_sum = sum(list_ev_cap) * 1000
        ev_consum = 15000  # Wh per 100 km
        min_soc = 0.5

        # print('success1')
        for i in range(n_ev):  # iterate over the ev
            # print('ev number' + str(i))
            df_ev = list_ev[i]
            ev_cap = list_ev_cap[i]
            diff_res = np.diff(df_ev['availability'], axis=0)  # "-1" means departure and "1" means arrival
            ev_fcast_sum['pow_char'] = ev_fcast_sum['pow_char'] + df_ev['availability'] * list_ev_pow_char[i] * 1000
            ev_fcast_sum['availability'] = ev_fcast_sum['availability'] + df_ev['availability']
            ev_single_consum = np.clip(df_ev['distance_driven'] / 100 * ev_consum, 0,
                                       list_ev_pow_char[i]*1000*1.5*0.9)
            ev_fcast_sum['consumption'] = ev_fcast_sum['consumption'] + ev_single_consum
            ev_fcast_sum['soc_driving'] = ev_fcast_sum['soc_driving'] + \
                                          list_ev_cap[i] * 1000 * min_soc / ev_cap_sum * (1 - df_ev['availability'])

            # for ix, row in df_ev.iterrows():  # iterate over the time
            #     ev_fcast_sum.loc[ix, 'pow_char'] += row['availability'] * list_ev_pow_char[i] * 1000
            #     ev_fcast_sum.loc[ix, 'availability'] += row['availability']
            #     # ev_fcast_sum.loc[ix, 'availability_cap'] += row['availability'] * list_ev_cap[i] * 1000 / ev_cap_sum
            #     if ix == 0 and row['distance_driven']:
            #         ev_fcast_sum.loc[ix, 'consumption'] += row['distance_driven'] / 100 * ev_consum
            #     elif diff_res[ix-1] == 1:
            #         ev_fcast_sum.loc[ix, 'consumption'] += row['distance_driven'] / 100 * ev_consum

            # calculate the soc requirement when the ev has left
            # if row['availability'] == 0:
            #     ev_fcast_sum.loc[ix, 'soc_driving'] += list_ev_cap[i] * 1000 * min_soc / ev_cap_sum

            for ix, row in df_ev.iterrows():  # iterate over the time
                # calculate the soc requirement for every time step if the ev will leave in the next time step
                if ix < (self.endhour - update_interval_h + predict_horizon_h - self.starthour - 2):
                    if row['availability'] == 1 and df_ev.loc[ix + 1, 'availability'] == 0:
                        ev_fcast_sum.loc[ix, 'soc_min_leave'] += list_ev_cap[i] * 1000 * min_soc / ev_cap_sum
                        # calculate the soc backwards
                        for ii in range(ix - 1, max(1, ix - 4), -1):  # calculate 6 time steps back
                            if df_ev.loc[ii, 'availability'] > 0:
                                ev_fcast_sum.loc[ii, 'soc_min_leave'] += max(list_ev_cap[i] * 1000 * min_soc / ev_cap_sum -
                                                                         list_ev_pow_char[i] * 1000 * (ix - ii) *
                                                                         time_resolution_h / ev_cap_sum, 0)
                # calculate the min soc requirement for the last time step of the prediction horizon
                # soc_min_last indicate the soc constraints which should be fulfilled in the last steps
                # of a prediction horizon
                ################################# for soc min of last time step of prediction horizon ###########
                # for nr_predict in range((self.endhour-update_interval_h-self.starthour) // update_interval_h + 1):
                #     end_predict = predict_horizon_h + nr_predict * \
                #                   self.config['simulation_settings']['update_interval_h']
                #     if ix == end_predict - 1 and row['availability'] == 1:
                #         ev_fcast_sum.loc[ix, 'soc_min_last'] += list_ev_cap[i] * 1000 * min_soc * 0.5 / ev_cap_sum
                ###############################################

                    # calculate the minimun soc backwards
                    # comment this part out to accelerate the calculation
                    # for j in range(end_predict - 1, end_predict - 4, -1):
                    #     if ev_fcast_sum.loc[j, 'soc_min_last'] > 0.01:
                    #         ev_fcast_sum.loc[j - 1, 'soc_min_last'] = max(ev_fcast_sum.loc[j, 'soc_min_last'] -
                    #                                                       ev_fcast_sum.loc[j, 'pow_char'] *
                    #                                                       time_resolution_h / ev_cap_sum, 0)

        # ev_fcast_sum['pow_char'] = [max(_pow, 4000) for _pow in ev_fcast_sum['pow_char']]
        ev_fcast_sum['soc_min'] = ev_fcast_sum['soc_min_leave'] + ev_fcast_sum['soc_driving']
        ev_fcast_sum['soc_min_last'] = ev_fcast_sum['soc_min_last'] + ev_fcast_sum['soc_driving']

        def distribute_array(arr):
            arr_output = np.zeros(len(arr))
            for i in range(len(arr)):
                num_split = 4
                if arr[i] != 0:
                    value = arr[i] / num_split
                    for j in range(i + 1, min(i + 1 + num_split, len(arr))):
                        arr_output[j] += value
            return arr_output
        # ev_fcast_sum['consumption'] = distribute_array(ev_fcast_sum['consumption'])  # used only for no smart charging

        # print('success2')
        return ev_fcast_sum, ev_cap_sum


if __name__ == "__main__":
    calc_res = True
    with open(f"../Step_0_config.yaml") as config_file:
        config = YAML().load(config_file)
    if calc_res:
        ev = EV(config, '../Libraries/EV_profiles', 0, 5000)
        n_ev = 5
        ev_fcast_sum, ev_cap_sum = ev.simulate(n_ev=n_ev, nr_seed=5)
        # distance_driven_avg should be nearly 30 kWh/d
        distance_driven_avg = ev_fcast_sum['consumption'].sum() / 1000 / 15 * 100 / n_ev / ((2096-2000)/24)
        plt.plot(ev_fcast_sum['soc_min'])
        # plt.plot(ev_fcast_sum['soc_min_last'], color='red')
        # plt.plot(ev_fcast_sum['consumption']/10000, color='yellow')
        plt.show()
        for i in range(ev_fcast_sum.shape[0]-1):
            pow_char = ev_fcast_sum.loc[i, 'pow_char']
            consumption = ev_fcast_sum.loc[i, 'consumption']
            availability_current = ev_fcast_sum.loc[i, 'availability']
            availability_next = ev_fcast_sum.loc[i+1, 'availability']
            # check charging power after arriving home
            if 0.5*ev_cap_sum+pow_char-consumption < 0:
                print(f'error at returning home for {i} time step')
            if (availability_next == 0) and (availability_current == 1):
                if ((ev_fcast_sum.loc[max(i-1, 0), 'availability'] == 0) or
                        (ev_fcast_sum.loc[max(i-2, 0), 'availability'] == 0)) and consumption > 10000:
                    print(f'error at charging home for {i} time step')
    else:
        file_path = '../Libraries/EV_profiles'
        files = os.listdir(file_path)
        ev_profile_files = [os.path.join(file_path, basename) for basename in files]
        distance_daily_freetime = []
        frequenz_freetime = []
        distance_daily_fulltime = []
        frequenz_fulltime = []
        distance_daily_parttime = []
        frequenz_parttime = []
        for ev_file in ev_profile_files:
            df_ev_input = pd.read_csv(ev_file, index_col='timestamp')
            distance_driven = df_ev_input['distance_driven']
            distance_driven = distance_driven.reset_index()
            idx_time = distance_driven.index[distance_driven['distance_driven'] > 0]
            frequenz = [val % 96 for val in idx_time]
            if 'freetime' in ev_file:
                distance_daily_freetime.append(distance_driven['distance_driven'].mean() * 4 * 24)
                frequenz_freetime.extend(frequenz)
            elif 'fulltime' in ev_file:
                distance_daily_fulltime.append(distance_driven['distance_driven'].mean() * 4 * 24)
                frequenz_fulltime.extend(frequenz)
            elif 'parttime' in ev_file:
                distance_daily_parttime.append(distance_driven['distance_driven'].mean() * 4 * 24)
                frequenz_parttime.extend(frequenz)
        print('freetime: ' + str(np.mean(distance_daily_freetime)))
        print('fulltime: ' + str(np.mean(distance_daily_fulltime)))
        print('parttime: ' + str(np.mean(distance_daily_parttime)))
        fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
        ax1.hist(frequenz_freetime, color="skyblue", lw=0)
        ax2.hist(frequenz_fulltime, color="skyblue", lw=0)
        ax3.hist(frequenz_parttime, color="skyblue", lw=0)
        plt.show()
        # for file in files:
        #     os.rename(os.path.join(file_path, file), os.path.join(file_path, ''.join(file.replace('Kopie', ''))))




