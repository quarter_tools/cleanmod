import pandas as pd
import datetime
from ruamel.yaml import YAML
import logging
import os


class NonResidentialLoad:

    def __init__(self, scenario_id):
        """
          A class for calculating energy usage based on floor areas and energy usage data.

          Attributes:
              config (dict): Dictionary containing the configuration data from the YAML file.
              name_model (str): The name of the model.
              elec_config (str): The configuration of the electricity data.
              time_resolution_h (int): The time resolution in hours.
              area_filepath (str): Filepath of the csv file containing non-residential building's floor areas.
              energy_filepath (str): Filepath of the csv file containing energy usage data.
              output_filepath (str): Filepath of the csv file to save the result to.
              dt (datetime): The start datetime.
              end (datetime): The end datetime.
              step (datetime.timedelta): The time delta between each timestep.
              L (list): List to hold the calculated energy usage data.
          """
        with open(f"Step_0_config.yaml") as config_file:
            self.config = YAML().load(config_file)
        self.name_model = self.config['project_settings']['name_model']
        self.elec_config = self.config['simulation_settings']['elec_config']
        self.time_resolution_h = self.config['simulation_settings']['time_resolution_h']
        self.area_filepath = f'Models/{self.name_model}/Input/NonResidentialBuildingsFloorArea.csv'
        self.energy_filepath = 'Libraries/Non-residential_loads_Total.csv'
        self.scenario_name = scenario_id
        self.output_filepath = f"Models/{self.name_model}/Output/Input_created/{self.scenario_name}/Non_residential/timeseries_aggregate.csv"
        self.directory = f"Models/{self.name_model}/Output/Input_created/{self.scenario_name}/Non_residential"
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        self.dt = datetime.datetime(2022, 1, 1)
        self.end = datetime.datetime(2022, 12, 31, 23, 59, 59)
        self.step = datetime.timedelta(minutes=15)
        self.L = []

        self.non_resi_aggreagated_timeseries = []

    def calculate_timesteps(self):
        """
        Creates a timestep based on datetime.

        Returns:
        pandas.DataFrame: A dataframe with the 15-minute timesteps as the index
        """
        timesteps = []
        dt = self.dt
        end = self.end
        step = self.step
        while dt <= end:
            timesteps.append(dt.strftime('%Y-%m-%d %H:%M:%S'))
            dt += step
        return pd.DataFrame(index=pd.to_datetime(timesteps))

    def calculate_nr_usage(self):
        """
    Calculates the energy usage based on floor areas and energy usage data.

    Returns:
    pandas.DataFrame: A dataframe containing the calculated energy usage data.

    Reads in two csv files containing the floor areas and energy usage data and multiplies the floor area of each
    non-residential use with the respective energy usage. Calculates the aggregate energy usage of all consumers and
    saves the resulting dataframe to a new csv file.
    """

        # read the dataframes from csv files
        df1 = pd.read_csv(self.area_filepath)
        df2 = pd.read_csv(self.energy_filepath, index_col=0, parse_dates=True)
        nr_use_areas = self.calculate_timesteps()
        # create a dictionary of nr_use areas from df1
        nr_use_areas_df = pd.DataFrame({
            nr_use: df1.loc[df1['non_residential_use'] == nr_use, 'floor_area_m2'].values[0] for nr_use in
            df1['non_residential_use']
        }, index=nr_use_areas.index)
        # multiply the area of each nr_use with the respective energy usage
        for nr_use in nr_use_areas_df.columns:
            df2[nr_use + '_Wh'] = df2[nr_use + '_Wh/m2'] * nr_use_areas_df[nr_use].values

        # remove the original energy usage columns
        df2.drop(columns=[nr_use + '_Wh/m2' for nr_use in nr_use_areas_df.columns], inplace=True)

        # calculate aggregate of all consumers
        df2['Total_Wh'] = df2.sum(axis=1)
        self.non_resi_aggreagated_timeseries = df2['Total_Wh'].tolist()  # in Wh

        # save the result to a csv file
        df2.to_csv(self.output_filepath)
        #return df2

    def calculate_total_nr_consumption(self):

        """
        Calculates the total consumption of energy in watts by adding all the timeseries data.

        Returns:
        float: Total energy consumption (1 year) in watts
        """

        df3 = pd.read_csv(self.output_filepath)
        df3 = df3.rename(columns={col: col[:-3] + '_W' for col in df3.columns if col.endswith('_Wh')})
        nr_loads = df3.drop('timestep', axis=1).sum()
        return nr_loads['Total_W']

    def run_NonResidentialLoad(self):
        self.calculate_nr_usage()
        self.calculate_total_nr_consumption()
        print('NON-RESI = ' + str(self.calculate_total_nr_consumption()))
        logging.debug('NON RESIDENTIAL COMPONENTS calculated successfully.')
        print('\n--> NON RESIDENTIAL COMPONENTS calculated successfully.')
        #return self.calculate_total_nr_consumption()
        return self.non_resi_aggreagated_timeseries

#non_res_load = NonResidentialLoad()



#if __name__ == '__main__':
#    # create an NonResidentialLoad object
#    calc = NonResidentialLoad()
#
#    # calculate the energy usage per interval and save the result to a csv file
#    calc.calculate_nr_usage()

#    # calculate total consumption
#    calc.calculate_total_nr_consumption()

#    logging.debug('NON RESIDENTIAL COMPONENTS calculated successfully.')
#    print('\n--> NON RESIDENTIAL COMPONENTS calculated successfully.')