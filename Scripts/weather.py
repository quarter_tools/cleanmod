"""
MB notes for development of the code:
What we need for simulation:
- content of Weather.csv:
TIME	Tamb	Cloudiness	Tsky	AZEN	AAZM
HOURS	°C	    [0-1]	    °C	    DEG	    DEG
--> Tamb can be extracted from EPW
--> Cloudinees can be calculated using Total Sky Cover from EPW data
--> Tsky can be calculated using EPW data and code from GH which is based in EnergyPlus
--> New code is required for calculation of AZEN and AAZM, can be based on LB:
sunZenithD, sunAzimuthD, sunAltitudeD = lb_photovoltaics.NRELsunPosition(latitude, longitude, timeZone, years[i], months[i], days[i], hours[i]-1) See method below

- Solar radiation data:
    - detail from GH or
    - simplified using code from LB and EPW data, see below. Requires albedo data, check and decide:
        - from EPW data
        - from method calculation based on LB photovoltaic
"""


from ruamel.yaml import YAML
import os
import math
import pandas as pd


class Weather:

    def __init__(self, file_epw, file_direct_rad, file_total_rad):
        """
        Initialize Weather object and load data

        """

        # Initialize attributes
        # --------------------------------------------------------------------------------------------------------------
        self.file_epw = None
        self.file_direct_rad = None
        self.file_total_rad = None
        #
        self.location_name = None
        self.location_latitude = None
        self.location_longitude = None
        #
        self.HOY = None
        #
        self.temperature_dry_bulb = None
        self.temperature_sky = None
        #
        self.solar_zenith = None
        self.solar_azimuth = None
        self.solar_altitude = None
        #
        self.solar_radiation_direct = None
        self.solar_radiation_total = None

        # Load data to initialized attributes
        # --------------------------------------------------------------------------------------------------------------
        self.load_file_data(file_epw, file_direct_rad, file_total_rad)
        self.load_location_data()
        self.load_HOY_timeseries()
        self.load_temperature_data()
        self.load_solar_angels()
        self.load_solar_radiation_data()

    def load_file_data(self, file_epw, file_direct_rad, file_total_rad):
        """
        Save the paths of the epw file and the csv files with solar radiation data obtained from the solar radiation
        analysis in rhino-grasshopper using ladybug tools

        """
        self.file_epw = file_epw
        self.file_direct_rad = file_direct_rad
        self.file_total_rad = file_total_rad

    def load_location_data(self):
        """
        Extract name, latitude and altitude of location form the epw file

        """
        with open(self.file_epw, "r") as f:
            first_line = f.readline()
        self.location_name = first_line.split(",")[1]
        self.location_latitude = float(first_line.split(",")[6])
        self.location_longitude = float(first_line.split(",")[7])

    def load_HOY_timeseries(self):
        """
        Create a time series of hours of the years in hourly resolution

        """
        self.HOY = [i+1 for i in range(8760)]

    def load_temperature_data(self):
        """Extract data of dry bulb temperature form the epw file and calculate sky temperature in degree Celsius
        using data on Horizontal Infrared Radiation Intensity from epw according to approach in:
        https://bigladdersoftware.com/epx/docs/8-9/engineering-reference/climate-calculations.html#energyplus-sky-temperature-calculation"""
        with open(self.file_epw, "r") as f:
            data_time_series = f.readlines()[8:]
        # Extract and load data on dry bulb temperature
        self.temperature_dry_bulb = [float(i.split(",")[6]) for i in data_time_series]  # [°C]
        # Extract data on horizontal infrared radiation intensity
        horizontal_IR = [float(i.split(",")[12]) for i in data_time_series]  # [Wh/m2]
        # Set constants
        sigma = 5.6697e-8  # stefan-boltzmann constant
        temp_K = 273.15  # Temperature conversion from Kelvin to C
        # Calculate and load sky temperature values
        self.temperature_sky = [((h_IRi / sigma) ** 0.25) - temp_K for h_IRi in horizontal_IR]  # [°C]

    def calculate_solar_angles(self, latitude, longitude, timeZone, year, month, day, hour):
        """
        Calculate solar angles (zenith, azimuth and altitude) in DEG based on the latitude and longitude of the
        location in the ewp file. This code corresponds to the one in the function NRELsunPosition from the
        photovoltaics components from ladybug tools

        """

        # sunZenith, sunAzimuth, sunAltitude angles
        # based on Michalsky (1988), modified to calculate sun azimuth angles for locations south of the equator using the approach described in (Iqbal, 1983)
        min = 30

        # leap year
        if year % 4 == 0:
            k = 1
        else:
            k = 0

        numOfDays = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]
        jdoy = int(numOfDays[int(month) - 1] + int(day))

        # julian day of year
        if month > 2:
            jdoy = jdoy + k

        # current decimal time of day in UTC
        tutc = hour + min / 60.0 - timeZone

        if tutc < 0:
            tutc = tutc + 24
            jdoy = jdoy - 1
        elif tutc > 24:
            tutc = tutc - 24
            jdoy = jdoy + 1

        julian = 32916.5 + 365 * (year - 1949) + int((year - 1949) / 4) + jdoy + (tutc / 24) - 51545

        mnlong = 280.46 + 0.9856474 * julian  # in degrees
        mnlong = mnlong - 360 * int(mnlong / 360)

        if (mnlong < 0):
            mnlong = (mnlong + 360)

        mnanom = (357.528 + 0.9856003 * julian)
        mnanom = mnanom - 360 * int(mnanom / 360)

        if (mnanom < 0):
            mnanom = (mnanom + 360)
        mnanom = mnanom * (math.pi / 180)  # in radians

        eclong = (mnlong + 1.915 * math.sin(mnanom) + 0.02 * math.sin(2 * mnanom))
        eclong = eclong - 360 * int(eclong / 360)

        if (eclong < 0):
            eclong = (eclong + 360)
        eclong = eclong * (math.pi / 180)

        obleq = (math.pi / 180) * (23.439 - 0.0000004 * julian)

        if (math.cos(eclong) < 0):
            ra = math.atan(((math.cos(obleq) * math.sin(eclong)) / math.cos(eclong))) + math.pi
        elif (math.cos(obleq) * math.sin(eclong) < 0):
            ra = math.atan(((math.cos(obleq) * math.sin(eclong)) / math.cos(eclong))) + 2 * math.pi
        else:
            ra = math.atan(((math.cos(obleq) * math.sin(eclong)) / math.cos(eclong)))

        beta = math.asin(math.sin(obleq) * math.sin(eclong))  # in radians

        # perform check and adjustment for sunrise or sunset
        sunrise_a = -math.tan((math.pi / 180) * latitude) * math.tan(beta)
        if sunrise_a >= 1:
            sunrise_HAR = 0
        elif sunrise_a <= -1:
            sunrise_HAR = math.pi
        else:
            sunrise_HAR = math.acos(sunrise_a)

        sunrise_a = (1 / 15.0) * (mnlong - (180 / math.pi) * ra)
        if sunrise_a < -0.33:
            sunrise_EOT = sunrise_a + 24
        elif sunrise_a > 0.33:
            sunrise_EOT = sunrise_a - 24
        else:
            sunrise_EOT = sunrise_a

        t_sunrise = 12 - (1 / 15.0) * (180 / math.pi) * sunrise_HAR - (longitude / 15 - timeZone) - sunrise_EOT
        t_sunset = 12 + (1 / 15.0) * (180 / math.pi) * sunrise_HAR - (longitude / 15 - timeZone) - sunrise_EOT

        if int(t_sunrise) == hour:
            min = (((t_sunrise - int(t_sunrise)) * 60) + 60) / 2
            tutc = hour + min / 60.0 - timeZone
        elif int(t_sunset) == hour:
            min = ((t_sunset - int(t_sunset)) * 60) / 2
            tutc = hour + min / 60.0 - timeZone

        gmst = 6.697375 + 0.0657098242 * julian + tutc
        gmst = gmst - 24 * int(gmst / 24)

        if (gmst < 0):
            gmst = gmst + 24

        lmst = gmst + longitude / 15
        lmst = lmst - 24 * int(lmst / 24)

        if (lmst < 0):
            lmst = lmst + 24

        b = 15 * (math.pi / 180) * lmst - ra

        if (b < -math.pi):
            HA = b + 2 * math.pi  # in radians
        elif (b > math.pi):
            HA = b - 2 * math.pi  # in radians
        else:
            HA = b

        # sun altitude, not corrected for radiation (in radians):
        a = math.sin(beta) * math.sin((math.pi / 180) * latitude) + math.cos(beta) * math.cos(
            (math.pi / 180) * latitude) * math.cos(HA)

        if (a >= -1) and (a <= 1):
            alpha0 = math.asin(a)
        elif (a > 1):
            alpha0 = math.pi / 2
        elif (a < -1):
            alpha0 = -math.pi / 2

        # sun altitude, corrected for refraction (in radians):
        alpha0d = 180 / math.pi * alpha0

        if (alpha0d > -0.56):
            r = 3.51561 * ((0.1594 + 0.0196 * alpha0d + 0.00002 * (alpha0d ** 2)) / (
                        1 + 0.505 * alpha0d + 0.0845 * (alpha0d ** 2)))
        elif (alpha0d <= -0.56):
            r = 0.56

        if (alpha0d + r > 90):
            sunAltitudeR = math.pi / 2
        elif (alpha0d + r <= 90):
            sunAltitudeR = (math.pi / 180) * (alpha0d + r)

        # sun azimuth angle (in radians):
        a = (math.sin(alpha0) * math.sin(math.pi / 180 * latitude) - math.sin(beta)) / (
                    math.cos(alpha0) * math.cos(math.pi / 180 * latitude))

        if (a >= -1) and (a <= 1):
            b = math.acos(a)
        elif (math.cos(alpha0) == 0) or (a < -1):
            b = math.pi
        elif (a > 1):
            b = 0

        if (HA < -math.pi):
            sunAzimuthR = b
        elif ((HA >= -math.pi) and (HA <= 0)) or (HA >= math.pi):
            sunAzimuthR = math.pi - b
        elif (HA > 0) and (HA < math.pi):
            sunAzimuthR = math.pi + b

        # sun zenith angle (in radians)
        sunZenithR = (math.pi / 2) - sunAltitudeR

        sunZenithD = math.degrees(sunZenithR)
        sunAzimuthD = math.degrees(sunAzimuthR)
        sunAltitudeD = math.degrees(sunAltitudeR)

        return sunZenithD, sunAzimuthD, sunAltitudeD

    def load_solar_angels(self):
        """
        Load time series of solar zenith and azimuth angles calculated based on the latitude and longitude of the
        location in the ewp file.

        """
        # Extract relevant data from ewp file for the calculation
        latitude = self.location_latitude
        longitude = self.location_longitude
        with open(self.file_epw, "r") as f:
            lines = f.readlines()
            first_line = lines[0]
            data_time_series = lines[8:]
        timeZone = int(first_line.split(",")[8])
        year = [int(i.split(",")[0]) for i in data_time_series]
        months = [int(i.split(",")[1]) for i in data_time_series]
        days = [int(i.split(",")[2]) for i in data_time_series]
        hours = [int(i.split(",")[3]) for i in data_time_series]
        # Calculate solar angles
        solar_zenith = []
        solar_azimuth = []
        solar_altitude = []
        for i,hour in enumerate(hours):
            solar_zenith_i, solar_azimuth_i, solar_altitude_i = self.calculate_solar_angles(latitude, longitude,
                                                                            timeZone, year[i], months[i], days[i], hour)
            solar_zenith.append(solar_zenith_i)
            solar_azimuth.append(solar_azimuth_i)
            solar_altitude.append(solar_altitude_i)
        # Load solar angles
        self.solar_zenith = [round(i,2) for i in solar_zenith]
        self.solar_azimuth = [round(i,2) for i in solar_azimuth]
        self.solar_altitude = [round(i,2) for i in solar_altitude]

    def load_solar_radiation_data(self):
        """
        Read the radiation csv files as load them as dataframes

        """
        self.solar_radiation_direct = pd.read_csv(self.file_direct_rad, skiprows=[1], low_memory=False)
        self.solar_radiation_total = pd.read_csv(self.file_total_rad, skiprows=[1], low_memory=False)