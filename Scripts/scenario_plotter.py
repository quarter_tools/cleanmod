import os
import math
import time
import pandas as pd
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
from datetime import datetime


class ScenarioPlotter():
    """
    A class used to create a uniform look for the plots

    Attributes
    ----------
    fig : figure
        figure that contains the plots
    ax : axis
        primary axis
    ax2 : axis
        secondary axis (optional)

    Public Methods
    -------
    __init__() -> None
        initializer, requires no input
    figure_setup(title: str = "", xlabel: str = "", ylabel: str = "", ylabel_right: str = None,
                 legend_labels: tuple = (), xlims: list = None, xticks_style: str = None) -> None
        sets up the default figure configurations to give the plots a uniform look

    """

    def __init__(self):
        # Style settings
        plt.style.use("../lemlab_plots.mplstyle")
        # Plots
        self.fig, self.ax = plt.subplots(figsize=(8, 6))
        self.ax2 = None

    def figure_setup(self, title: str = "", xlabel: str = "", ylabel: str = "", ylabel_right: str = None,
                     legend_labels: tuple = (), xlims: list = None, handles: tuple = (),
                     xticks_style: str = None) -> None:
        """
        sets up the default figure configurations to give the plots a uniform look

        Args:
            title (str): string that contains the title of the figure
            xlabel (str): string that contains the x-label of the figure
            ylabel (str): string that contains the left y-label of the figure
            ylabel_right (str): string that contains the right y-label of the figure if figures has two y-axes
            legend_labels (tuple): tuple that contains the legend labels of the figure
            xlims (list): list that contains the minimum and maximum value of the x-axis
            xticks_style (str): string that contains the style of the x-ticks. Currently three styles are available:
                                1. None:    No x-ticks are displayed
                                2. numeric: X-axis is displayed with numeric values
                                3. date:    X-axis is displayed with dates

        Returns:
            None

        """


        # Title settings
        plt.title(title)

        # Axes settings
        self.ax.set_ylabel(ylabel=ylabel)
        self.ax.set_xlabel(xlabel=xlabel)
        # checks if x-axis needs to be adjusted in case it differs from the standard output
        self.__set_xticks(xlims, xticks_style)
        self.ax.tick_params(axis='both', which='major')

        # Legend
        if handles is not None:
            if 0 < len(legend_labels) <= 4:  # change labels of legend, if desired
                axbox = self.ax.get_position()
                self.ax.legend(handles, legend_labels,
                               bbox_to_anchor=(0.5, 1.45*axbox.height-(0.1*math.ceil(len(legend_labels)/4))), ncol=4)
            elif len(legend_labels) <= 8:
                # legend_labels = legend_labels
                axbox = self.ax.get_position()
                self.ax.legend(handles, legend_labels,
                               bbox_to_anchor=(0.5, 1.58*axbox.height-(0.1*math.ceil(len(legend_labels)/4))),
                               ncol=min(4, math.floor((len(legend_labels)+1)/2)))
            elif len(legend_labels) > 8:
                # legend_labels = legend_labels
                axbox = self.ax.get_position()
                self.ax.legend(handles, legend_labels,
                               bbox_to_anchor=(0.5, 1.71 * axbox.height - (0.1 * math.ceil(len(legend_labels) / 4))),
                               ncol=min(4, math.floor((len(legend_labels) + 1) / 2)))
        else:
            if 0 < len(legend_labels) <= 4:  # change labels of legend, if desired
                axbox = self.ax.get_position()
                self.ax.legend(legend_labels,
                               bbox_to_anchor=(0.5, 1.45*axbox.height-(0.1*math.ceil(len(legend_labels)/4))), ncol=4)
            elif len(legend_labels) <= 8:
                # legend_labels = legend_labels
                axbox = self.ax.get_position()
                self.ax.legend(legend_labels,
                               bbox_to_anchor=(0.5, 1.58*axbox.height-(0.1*math.ceil(len(legend_labels)/4))),
                               ncol=min(4, math.floor((len(legend_labels)+1)/2)))
            elif len(legend_labels) > 8:
                # legend_labels = legend_labels
                axbox = self.ax.get_position()
                self.ax.legend(legend_labels,
                               bbox_to_anchor=(0.5, 1.71 * axbox.height - (0.1 * math.ceil(len(legend_labels) / 4))),
                               ncol=min(4, math.floor((len(legend_labels) + 1) / 2)))
        # Scale x-axis tightly
        self.ax.autoscale(enable=True, axis='x', tight=True)
        self.ax.grid(color='grey', linestyle='--', linewidth=1, alpha=0.5)

        # Adjust second y-axis if it exists
        if ylabel_right:
            self.ax2.set_ylabel(ylabel=ylabel_right)
            self.ax2.set_xlabel(xlabel=xlabel)
            self.ax2.tick_params(axis='both', which='major')
            self.ax2.autoscale(enable=True, axis='x', tight=True)
            self.ax.grid(b=False)

        plt.tight_layout()

    @staticmethod
    def __set_xticks(xlims: list, xticks_style: str = None) -> None:
        """
        sets the x-ticks labels according to the chosen style

        Args:
            xlims (list): list that contains the minimum and maximum value of the x-axis
            xticks_style (str): string that contains the style of the x-ticks. Currently three styles are available:
                                1. None:    No x-ticks are displayed
                                2. numeric: X-axis is displayed with numeric values
                                3. date:    X-axis is displayed with dates

        Returns:
            None

        """

        # maximum number of x-ticks
        n_max = 10

        if not xticks_style:
            # turn x-ticks off
            plt.xticks([])
        elif xticks_style.lower() == "numeric":
            # create numeric values for entire range
            x_values = list(range(int(xlims[0]), int(xlims[1] + 1)))
            # reduce list of x-values by half until maximum number of x-ticks is reached
            while len(x_values) > n_max:
                x_values = x_values[::2]
            plt.xticks(x_values, x_values)
        elif xticks_style.lower() == "date":
            time_step = 1 * 15 * 60  # time step of 15 minutes
            x_values = xlims[0] - xlims[0] % time_step  # ensure that first value is a multiple of time_step
            x_values = list(range(x_values, xlims[1] + time_step, time_step))

            # Get step size depending on duration of simulation
            x_step = [x for x in [1, 2, 4, 8, 16, 24, 96, 2*96, 4*96, 5*96, 7*96, 30*24, 90*24, 365*24]
                      if len(x_values) / x <= n_max][0]

            # Get x-values. If they exceed the 5-day threshold (5*96), dates will be displayed only for the first and
            # 15th of each month
            if x_step:
                x_values = x_values[::x_step]
            else:
                x_values = [x for x in x_values if datetime.fromtimestamp(x).strftime("%H") == "00" and
                            (datetime.fromtimestamp(x).strftime("%d") == "01" or
                             datetime.fromtimestamp(x).strftime("%d") == "15")]

            # Change date style based on duration
            if x_step < 4:
                xformat = "%H:%M"
                xformat2 = "%d.%m"
            elif x_step < 96:
                xformat = "%Hh"
                xformat2 = "%d.%m"
            else:
                xformat = "%d"
                xformat2 = "%b"

            # Create labels and improve readability
            x_labels = [datetime.fromtimestamp(x).strftime(xformat) for x in x_values]
            x_labels_adj = x_labels.copy()
            for idx, x in enumerate(x_values[2:]):
                if idx == 0:
                    x_labels_adj[idx] = datetime.fromtimestamp(x_values[idx]).strftime(xformat2)
                if x_labels[idx+2][:2] < x_labels[idx+1][:2]:
                    x_labels_adj[idx+2] = datetime.fromtimestamp(x).strftime(xformat2)
            plt.xticks(x_values, x_labels_adj)


def save_figure(name, res_dir, calc_type) -> None:
    """
    saves the current plot under the provided name as png

    Args:
        name (str): string that contains the name of the plot

    Returns:
        None

    """
    if calc_type == 'analysis':
        fig_dir = os.path.join(res_dir, f'{calc_type}/')
        if not os.path.exists(fig_dir):
            os.mkdir(fig_dir)
    else:
        fig_dir = os.path.join(res_dir, f'mainrun/{calc_type}/')
        if not os.path.exists(fig_dir):
            os.mkdir(fig_dir)

    # Save file to analyzer directory. If file with the name already exists, replace it with the new file
    if os.path.isfile(f"{fig_dir}/{name}.png"):
        os.remove(f"{fig_dir}/{name}.png")
    plt.savefig(f"{fig_dir}/{name}.png")