import numpy as np

class Time:

    def __init__(self, config, precalc, sizing_run=False, sizing_start_hour=0, sizing_end_hour=0):
        """
        Creates time indices as attributes, including extended time periods by precalc and prediction.

        This method generates four versions of time indices:
        1. Index1: Focus period only
        2. Index2: Focus period plus prediction horizon
        3. Index3: Precalculation period plus focus period
        4. Index4: Precalculation period, focus period, and prediction horizon

        For versions 1 and 2, they are made available in simulations with precalculation as well to enable
        postprocessing with the reduced time period.

        Args:
            precalc (int): Precalculation period before start hour in days.

        Returns:
            None. This method updates object attributes and does not return a value.

        """


        # read settings from config
        if sizing_run:
            starthour = sizing_start_hour
            endhour = sizing_end_hour
        else:
            starthour = config['simulation_settings']['starthour']
            endhour = config['simulation_settings']['endhour']
        predict_horizon_h = config['simulation_settings']['predict_horizon_h']
        time_resolution_h = config['simulation_settings']['time_resolution_h']
        update_interval_h = config['simulation_settings']['update_interval_h']

        # setup time
        self.endhour_predict = endhour + predict_horizon_h
        self.steps_per_hour = 1 / time_resolution_h

        timeperiod_h = endhour - starthour

        index_timesteps = [i for i in np.arange(starthour, endhour, time_resolution_h)]
        index_timesteps_incl_predict = [i for i in np.arange(starthour, self.endhour_predict, time_resolution_h)]
        update_timesteps = [step * update_interval_h
                            for step in range(int(timeperiod_h / update_interval_h))]

        if precalc:
            #starthour_pre = float(starthour - precalc * 24)  # attention: allowed to become negative value
            starthour_pre = int(starthour - precalc * 24)  # attention: allowed to become negative value

            #timeperiod_pre_h = float(starthour - starthour_pre)
            timeperiod_pre_h = int(starthour - starthour_pre)
            index_timesteps_pre = [i for i in np.arange(starthour_pre, endhour, time_resolution_h)]
            index_timesteps_pre_incl_predict = [i for i in np.arange(starthour_pre, self.endhour_predict,
                                                                     time_resolution_h)]
            update_timesteps_pre = [step * update_interval_h
                                    for step in range(int(timeperiod_pre_h / update_interval_h))]

            self.starthour_run = starthour_pre
            self.index_timesteps_run = index_timesteps_pre
            self.index_timesteps_incl_predict_run = index_timesteps_pre_incl_predict
            self.update_timesteps_run = update_timesteps_pre
            self.precalc_days = precalc

        else:
            self.starthour_run = starthour
            self.index_timesteps_run = index_timesteps
            self.index_timesteps_incl_predict_run = index_timesteps_incl_predict
            self.update_timesteps_run = update_timesteps
            self.precalc_days = 0
