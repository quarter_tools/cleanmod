import numpy as np


class BuildingComponent:
    """ Class for Building Component Thermal Model """

    def __init__(self, name, int_ext, component_type, thickness, conductivity, density, capacity, u_value,
                 sw, lw, alpha_int, alpha_ext, area, orient, inclin, surf_id):

        self.Component_name = name

        composition = self.create_ComponentComposition(int_ext, component_type, thickness, conductivity, density,
                                                       capacity, u_value,sw, lw, alpha_int, alpha_ext)

        self.IntExt = composition['IntExt']
        self.ComponentType = composition['ComponentType']
        self.Thickness = composition['Thickness']
        self.Conductivity = composition['Conductivity']
        self.Density = composition['Density']
        self.StorageCapacity = composition['StorageCapacity']
        self.SW = composition['SW']
        self.LW = composition['LW']
        self.alphaInt = composition['alphaInt']
        self.alphaExt = composition['alphaExt']
        self.U = composition['U']
        #
        self.Area = area
        self.Orient = orient
        self.Inclin = inclin
        #
        self.Surf_id = surf_id
        #
        self.acm_wbt = composition['acm_wbt']

        BC_Model2 = self.calc_3R2C_helper(self.acm_wbt, area, 2,self.Thickness,self.Conductivity)
        BC_Model7 = self.calc_3R2C_helper(self.acm_wbt, area, 7,self.Thickness,self.Conductivity)
        R1Rel = BC_Model2['R1'] / BC_Model7['R1']
        C1Rel = BC_Model2['C1'] / BC_Model7['C1']
        if (R1Rel > 0.99) and (C1Rel < 0.95):
            self.RC = BC_Model2
        elif (R1Rel < 0.95) and (C1Rel < 0.95) and (abs(R1Rel - C1Rel) > 0.3):
            self.RC = BC_Model2
        else:
            self.RC = BC_Model7
        # Calculating the coefficients for equivalent outside temperature for outside components
        if self.IntExt == 'Ext':
            # The Short Wave coefficient is the SW absorption coefficient of exterior surface (BACK)
            # divided by the exterior heat transfer coefficient (convective + radiative) [VDI(38)]
            # This is multiplied by Idir + Idif for the surface equivalent temperature
            self.SWCoeff = self.SW / (self.alphaExt + 5)
            # The formula for LW Radiation is given in [VDI(33)-VDI(37)]. The two components of LW
            # Coefficient is multiplied by the (Earth Temperature - Ambient Temperature)
            # and the (Atmospheric Temperature -Ambient Temperature) respectively
            a = self.LW * 5 / ((self.alphaExt + 5) * 0.93)
            if self.ComponentType == 'Wall':
                self.LWCoeff = [0.5 * a, 0.5 * a]
            elif self.ComponentType == 'Ceiling':
                self.LWCoeff = [0, a]
            elif self.ComponentType == 'Roof':
                self.LWCoeff = [0, a]
            elif self.ComponentType == 'Floor':
                self.LWCoeff = [a, 0]

    def create_ComponentComposition(self, int_ext, component_type, thickness, conductivity, density, capacity, u_value,
                                    sw, lw, alpha_int, alpha_ext):

        dict = {}
        dict['IntExt'] = int_ext
        dict['ComponentType'] = component_type
        dict['Thickness'] = thickness
        dict['Conductivity'] = conductivity
        dict['Density'] = density
        dict['StorageCapacity'] = capacity
        dict['SW'] = sw
        dict['LW'] = lw
        dict['alphaInt'] = alpha_int
        dict['alphaExt'] = alpha_ext
        dict['U'] = u_value

        Alist = []
        for T in [2, 7]:
            wbt = 2 * np.pi / (86400 * T)
            A = np.eye(4)
            for i in range(len(thickness)):
                R = thickness[i] / conductivity[i]
                C = 1000 * capacity[i] * density[i] * thickness[i]

                sqterm1 = np.sqrt(0.5 * wbt * R * C)
                sqterm2 = np.sqrt(1 / (2 * wbt * R * C))

                Rea11 = np.cosh(sqterm1) * np.cos(sqterm1)
                Ima11 = np.sinh(sqterm1) * np.sin(sqterm1)
                Rea12 = R * sqterm2 * (np.cosh(sqterm1) * np.sin(sqterm1) +
                                       np.sinh(sqterm1) * np.cos(sqterm1))
                Ima12 = R * sqterm2 * (np.cosh(sqterm1) * np.sin(sqterm1) -
                                       np.sinh(sqterm1) * np.cos(sqterm1))
                Rea21 = (-1 / R) * sqterm1 * (np.cosh(sqterm1) * np.sin(sqterm1) -
                                              np.sinh(sqterm1) * np.cos(sqterm1))
                Ima21 = (1 / R) * sqterm1 * (np.cosh(sqterm1) * np.sin(sqterm1) +
                                             np.sinh(sqterm1) * np.cos(sqterm1))

                Av = np.array([[Rea11, Ima11, Rea12, Ima12],
                               [-Ima11, Rea11, -Ima12, Rea12],
                               [Rea21, Ima21, Rea11, Ima11],
                               [-Ima21, Rea21, -Ima11, Rea11]])
                A = A @ Av

            Alist.append(A)

        dict['acm_wbt'] = Alist

        return dict

    def __str__(self):
        return self.ComponentType + " Component"

    # noinspection PyPep8Naming
    def calc_3R2C_helper(self, acm_wbt, Area, T,Thickness,Conductivity):
        """ Calculate the 3R2C model for the specified time period (2 or 7 days)."""

        if T == 2:
            Acm = acm_wbt[0]
        elif T == 7:
            Acm = acm_wbt[1]

        wbt = 2 * np.pi / (86400 * T)
        Rea11 = Acm[0, 0]
        Ima11 = Acm[0, 1]
        Rea12 = Acm[0, 2]
        Ima12 = Acm[0, 3]
        # Rea21 = Acm[2, 0]
        # Ima21 = Acm[2, 1]
        Rea22 = Acm[2, 2]
        Ima22 = Acm[2, 3]

        BC_Model = {}

        BC_Model['R1'] = float((1 / Area) * (((Rea22 - 1) * Rea12 + Ima22 * Ima12) / ((Rea22 - 1) ** 2 + Ima22 ** 2)))
        BC_Model['R2'] = float((1 / Area) * (((Rea11 - 1) * Rea12 + Ima11 * Ima12) / ((Rea11 - 1) ** 2 + Ima11 ** 2)))
        BC_Model['C1'] = float(Area * ((Rea22 - 1) ** 2 + Ima22 ** 2) / (wbt * (Rea12 * Ima22 - (Rea22 - 1) * Ima12)))
        BC_Model['C2'] = float(Area * ((Rea11 - 1) ** 2 + Ima11 ** 2) / (wbt * (Rea12 * Ima11 - (Rea11 - 1) * Ima12)))
        BC_Model['Rw'] = float((1 / Area) * np.sum(np.array([Thickness]) / Conductivity))
        BC_Model['R3'] = float(BC_Model['Rw'] - BC_Model['R1'] - BC_Model['R2'])

        BC_Model['Ckorr'] = float((Area * (wbt * BC_Model['R1'] * Area) ** -1 * (
                    BC_Model['Rw'] * Area - Rea12 * Rea22 - Ima12 * Ima22) / (Rea22 * Ima12 - Rea12 * Ima22)))

        return BC_Model


class WindowComponent:
    """Class for Window Component Thermal Model."""

    def __init__(self, name, U, alphaInt, alphaExt, SW, LW, Area, Orient):
        self.Component_name = name
        self.ComponentType = 'Window'
        self.U = U
        self.alphaInt = alphaInt
        self.alphaExt = alphaExt
        self.SW = SW
        self.LW = LW
        self.IntExt = 'Ext'
        self.R1 = float((1 / U + 1 / alphaInt + 1 / alphaExt) / (6 * Area))
        self.Area = Area
        self.Orient = Orient
        # The formula for LW Radiation is given in [VDI(33)-VDI(37)]. The two components of LW
        # Coefficient is multiplied by the (Earth Temperature - Ambient Temperature)
        # and the (Atmospheric Temperature -Ambient Temperature) respectively
        a = self.LW * 5 / ((self.alphaExt + 5) * 0.93)
        # a = self.LW*5/((self.alphaExt+5)*0.93)
        self.LWCoeff = [0.5 * a, 0.5 * a]  # assumption is that inclination of window is 90° e.g. vertical

    def __str__(self):
        return "Window Component"


