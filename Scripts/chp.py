__author__ = "Zyou"
__credits__ = []
__license__ = ""
__maintainer__ = "Zyou"
__email__ = "zhengjie.you@tum.de"

# system packages
import pandas as pd
import numpy as np
import string
from scipy import optimize
import os

# external/third-party packages
# our own modules/applications


class CombinedHeatPower:
    """
    Combined heat and power (CHP) model class, designed to calculate the outputs of various types of CHP systems.

    Methods
    -------
    __init__: Initializes an instance of the CHP class using pre-defined parameter groups for simulation.

    simulate: Executes the simulation of the CHP model.

    """

    def __init__(self, parameters: pd.DataFrame):
        self.eta_th = float(parameters['eta_th'].array[0])
        self.eta_el = float(parameters['eta_el'].array[0])
        self.eta_chp = float(parameters['eta_chp'].array[0])
        self.c_chp = float(parameters['c_chp'].array[0])
        self.P_th_ref = float(parameters['P_th_ref [W]'].array[0])
        self.P_el_ref = float(parameters['P_el_ref [W]'].array[0])
        self.P_fuel_ref = float(parameters['P_fuel_ref [W]'].array[0])

    def simulate(self) -> dict:
        """
        Performs the simulation of the combined heat and power (CHP) model.

        Parameters
        ----------
        None

        Returns
        -------
        result : dict
            Dictionary containing the results of the simulation.
        """

        # calculation (can be further modified, now just the same as reference value)
        p_th = self.P_th_ref
        p_el = self.P_el_ref
        p_fuel = self.P_fuel_ref

        # round
        result = dict()

        result['P_th'] = p_th
        result['P_el'] = p_el
        result['P_fuel'] = p_fuel

        return result


def get_parameters(manufacturer: str, model: str, p_th_ref: int = 0) -> pd.DataFrame:
    """
    Loads the data for a specific combined heat and power (CHP) model from the database and returns a DataFrame containing the CHP parameters.

    Parameters
    ----------
    manufacturer : str
        Name of the CHP manufacturer.

    model : str
        Name of the CHP model.

    p_th_ref : numeric, optional
        Reference thermal power output in Watts. Default is 0.

    Returns
    -------
    parameters : pd.DataFrame
        DataFrame containing the model parameters.
    """

    df = pd.read_csv(os.path.abspath(os.path.join(cwd(), r'../../Libraries/chp_data/chp_database.csv')),
                     delimiter=',')
    df = df.loc[df['Model'] == model]
    df = df.loc[df['Manufacturer'] == manufacturer]
    parameters = pd.DataFrame()
    parameters['Manufacturer'] = (df['Manufacturer'].values.tolist())
    parameters['Model'] = (df['Model'].values.tolist())

    parameters['eta_th'] = (df['n_t'].values.tolist())
    parameters['eta_el'] = (df['n_e'].values.tolist())
    parameters['eta_chp'] = (df['n_CHP'].values.tolist())
    parameters['c_chp'] = (df['C_CHP'].values.tolist())

    parameters['P_th_ref [W]'] = p_th_ref
    parameters['P_el_ref [W]'] = p_th_ref * parameters['c_chp']
    parameters['P_fuel_ref [W]'] = p_th_ref / parameters['eta_th']

    return parameters


def cwd():
    """
    Returns the parent directory.

    Returns
    -------
    parent_dir : str
        Path to the parent directory.
    """

    real_path = os.path.realpath(__file__)
    dir_path = os.path.dirname(real_path)
    return dir_path
