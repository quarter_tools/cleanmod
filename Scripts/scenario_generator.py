import pandas as pd
import os
import random


# Paths and potential class inputs for development. Keep until we are sure that the code works with the rest of modules
"""
name_model = 'suburban_test'
folder_model = '../Models/{0}'.format(name_model)
folder_buildings_scenarios = folder_model + '/Input/Buildings_scenarios'
folder_GH_output = folder_model + '/Input/GH_output'
file_geometry_zones = folder_model + '/Input/GH_output/Geometry_Zones.csv'
file_building_variant_compositions = '../Libraries/Building_variant_compositions.csv'
_file_scenarios = folder_model + '/Input/Scenarios.xlsx'
# _scenarios = ['Suburban_RED1_1', 'Suburban_BASE_1', 'Suburban_RED2_1']  # List of scenarios from config_cleanmod
_scenarios = ['Suburban_RED1_1']  # List of scenarios from config_cleanmod
"""

# ----------------------------------------------------------------------------------------------------------------------


def scenario_preprocessing(scenarios, file_scenarios):
    """
    It reads the file Scenarios.xlsx and prepare a data frame to be used in CleanMod. It also reads the list of
    scenarios defined by the user in config_cleanmod (note: the number of scenarios listed in config_cleanmod for the
    simulation can be equal or less than the number of scenarios defined in the file Scenarios.xlsx. Additional checks
    are also performed to make sure that the scenarios were correctly defined by the user in the excel file. Moreover,
    the list of scenarios is sorted in a way that cumulative scenarios (scenario chains) are simulated in order and the
    renovation state of the parent scenario is taken into account to apply the renovation at each scenario.

    Input:
        scenarios: list of scenarios ids to be taken into account
        file_scenarios: path of the file Scenarios.xlsx
    Output:
        scenario_df: data frame containing all scenarios and their parameter values
        sorted_scenarios: list of scenarios_ids in the right order for the simulation of cumulative scenarios

    """

    # Prepare a data frame from Scenarios.xlsx
    # # Read the file
    scenarios_df = pd.read_excel(file_scenarios, engine='openpyxl', header=None)
    # # Set header
    scenarios_df.columns = ['Category', 'Parameter', 'Description'] + list(scenarios_df.iloc[0, 3:])
    # # Fill in nan in Category column
    scenarios_df['Category'].fillna(method='ffill', inplace=True)
    # # Take the parameter rows and avoid rows with nan as the parameter
    scenarios_df = scenarios_df[scenarios_df.iloc[:, 1].notna()]

    # Performing some checks
    # ------------------------------------------------------------------------------------------------------------------
    # # Base list of scenarios:
    # # # Scenarios listed in config_cleanmode to be simulated
    scenarios_list = scenarios
    print('\n' + str(len(scenarios_list)) + ' scenarios listed in config_cleanmod for the simulation:')
    print(scenarios_list)
    # # # All scenarios in file Scenarios.xlsx
    scenarios_all = [i for i in scenarios_df.columns][3:]

    # # Check that No_parent_scenario is not used as the name of an scenario
    if 'No_parent_scenario' in scenarios_list:
        raise Exception('\nError: No_parent_scenario is included in the scenarios listed in config_cleanmod.' +
                        'No_parent_scenario cannot be used as the name of an scenario. Correct and start again.')

    # # Check that the scenarios listed in config_cleanmod exist in the file Scenarios.xlsx
    missing_scenarios = []
    for scenario in scenarios_list:
        if scenario not in scenarios_all:
            missing_scenarios.append(scenario)
    if len(missing_scenarios) > 0:
        print('\nWarning: the following scenarios listed in config_cleanmod are missing in Scenarios.xlsx:')
        for i, missed_scenario in enumerate(missing_scenarios):
            print(i+1, missed_scenario)
        if len(missing_scenarios) == 1:
            raise Exception('\n' + str(len(missing_scenarios)) +
                            ' scenario listed in config_cleanmod is missing in Scenarios.xlsx.' +
                            ' Correct and start again')
        else:
            raise Exception('\n' + str(len(missing_scenarios)) +
                            ' scenarios listed in config_cleanmod are missing in Scenarios.xlsx.' +
                            ' Correct and start again')
    else:
        print('\n' + 'All scenarios listed in config_cleamnod are also defined as scenarios in the file Scenarios.xlsx')

    # # Base list and df of pairs of scenario the corresponding parent renovation scenario
    # # # Scenarios listed in config_cleanmode to be simulated
    scenario_pairs = []
    for scenario in scenarios_list:
        parent_scenario = scenarios_df.at[
            scenarios_df[scenarios_df['Parameter'] == 'parent_renovation_scenario'].index.values.tolist()[0], scenario]
        scenario_pairs.append((scenario, parent_scenario))
    scenario_pairs_df = pd.DataFrame()
    scenario_pairs_df['scenario'] = [scenario for (scenario, parent_scenario) in scenario_pairs]
    scenario_pairs_df['parent_scenario'] = [parent_scenario for (scenario, parent_scenario) in scenario_pairs]
    # # # All scenarios in file Scenarios.xlsx
    scenario_pairs_all = []
    for scenario in scenarios_all:
        parent_scenario = scenarios_df.at[
            scenarios_df[scenarios_df['Parameter'] == 'parent_renovation_scenario'].index.values.tolist()[0], scenario]
        scenario_pairs_all.append((scenario, parent_scenario))
    scenario_pairs_all_df = pd.DataFrame()
    scenario_pairs_all_df['scenario'] = [scenario for (scenario, parent_scenario) in scenario_pairs_all]
    scenario_pairs_all_df['parent_scenario'] = [parent_scenario for (scenario, parent_scenario) in scenario_pairs_all]

    # # Check whether the scenarios to be simulated are unique the list of config_cleandmod
    ''' If any scenario is repeated the process will be stopped to allow the user to correct the input variable and, 
    this way, to avoid simulating one scenario more than once, thus, improving simulation time.'''
    repeated_scenarios_list = [scenario for scenario in scenarios_list if scenarios_list.count(scenario) > 1]
    if len(repeated_scenarios_list) > 0:
        print('\nWarning: the following scenarios are repeated within the scenarios listed in config_cleanmod:')
        for i, repeated_scenario in enumerate(repeated_scenarios_list):
            print(i+1, repeated_scenario)
        if len(repeated_scenarios_list) == 1:
            raise Exception('\n' + str(len(repeated_scenarios_list)) +
                            ' scenario listed in config_cleanmod is repeated. It should be unique.' +
                            ' Correct and start again')
        else:
            raise Exception('\n' + str(len(repeated_scenarios_list)) +
                            ' scenarios listed in config_cleanmod are repeated. They should be unique.' +
                            ' Correct and start again')
    else:
        print('\nAll scenarios listed in config_cleanmod are unique')

    # # Check whether the scenarios listed in config_cleanmod to be simulated are unique in Scenarios.xlsx
    ''' If any scenario is repeated the process will be stopped to allow the user to correct the input file and, 
    this way, to avoid cleanMod to crash due to having more than one set of parameter values for the same scenario.'''
    repeated_scenarios_all = [scenario for scenario in scenarios_list if scenarios_all.count(scenario) > 1]
    if len(repeated_scenarios_all) > 0:
        print('\nWarning: the following scenarios in Scenarios.xlsx are repeated:')
        for i, repeated_scenario in enumerate(repeated_scenarios_all):
            print(i+1, repeated_scenario)
        if len(repeated_scenarios_all) == 1:
            raise Exception('\n' + str(len(repeated_scenarios_all)) +
                            ' scenarios in Scenarios.xlsx is repeated. It should be unique.' +
                            ' Correct and start again')
        else:
            raise Exception('\n' + str(len(repeated_scenarios_all)) +
                            ' scenarios in Scenarios.xlsx are repeated. They should be unique.' +
                            ' Correct and start again')
    else:
        print('\nAll scenarios listed in config_cleanmod are also unique in the file Scenarios.xlsx')

    # # Check whether parent scenarios of the scenarios listed in config_cleanmod are missing in Scenario.xlsx
    ''' When the renovation of buildings is applied in an scenario, buildings to be renovated are randomly selected.
    Thus, in the case of scenarios that are set in a cumulative way - e.g. 2020 -> 2030 -> 2040 - the renovation is to
    be applied to the state of the next previous scenario, not to the first, base scenario in the chain in oder to be
    consistent with the random selection of buildings from the initial scenario onwards. The parent scenario serves to
    indicate the next previous scenario. The parent scenarios should be defined in the Scenarios.xlsx to be able to
    reproduce the chain and properly apply the random selection of buildings according to the scenario-specific
    renovation factors, thus, obtaining the state of each scenario according to the corresponding scenario chain.'''
    missing_parent_scenarios = []
    for (scenario, parent_scenario) in scenario_pairs:
        if parent_scenario == 'No_parent_scenario' or parent_scenario in scenarios_all:
            pass
        else:
            if parent_scenario not in missing_parent_scenarios:
                missing_parent_scenarios.append(parent_scenario)
    if len(missing_parent_scenarios) > 0:
        print('\nWarning: The following parent renovation scenarios of the scenarios listed in config_clenmod are' +
              ' missing in Scenarios.xlsx:')
        for i, missed_scenario in enumerate(missing_parent_scenarios):
            print(i+1, missed_scenario)
        if len(missing_parent_scenarios) == 1:
            raise Exception('\n' + str(len(missing_parent_scenarios)) +
                            ' parent renovation scenarios in Scenarios.xlsx is missing. Correct and start again')
        else:
            raise Exception('\n' + str(len(missing_parent_scenarios)) +
                            ' parent renovation scenarios in Scenarios.xlsx are missing. Correct and start again')
    else:
        print('\n' + 'All parent renovation scenarios of the scenarios listed in config_cleanmod are correctly' +
              '\n' + 'defined as scenarios in Scenarios.xlsx, as required to be able to implement the renovation of' +
              '\n' + 'buildings in each scenario based on the state of the parent scenario.')

    # # Check that the parent scenario of each scenario listed in config_cleanmod is not the scenario itself
    '''The parent renovation scenario of one scenario must be another scenario or No_parent_scenario. Scenarios with
    No_parent_scenario as the parent renovation scenario are the initial state of a possible chain of cumulative  
    renovation scenarios'''
    self_parent_scenarios = [scenario for (scenario, parent_scenario) in scenario_pairs
                             if scenario == parent_scenario]
    if len(self_parent_scenarios) > 0:
        print('\nWarning: renovation scenarios in which the parent renovation scenario is the scenario itself:')
        for i, scenario in enumerate(self_parent_scenarios):
            print(i+1, scenario)
        if len(self_parent_scenarios) == 1:
            raise Exception('\n' + str(len(self_parent_scenarios)) +
                            ' scenario listed in config_cleanmod in which the parent renovation scenario is the' +
                            ' scenario itself. Correct and start again')
        else:
            raise Exception('\n' + str(len(self_parent_scenarios)) +
                            ' scenarios listed in config_cleanmod in which the parent renovation scenario is the' +
                            ' scenario itself. Correct and start again')
    else:
        print('\n' + 'No scenario listed in config_cleanmod in which the parent renovation scenarios is the scenario ' +
              'itself,\nas required to be able to correctly configure the scenario for its simulation.')

    # # Check that the parent scenario of each scenario in Scenario.xlsx is not the scenario itself
    '''The parent renovation scenario of one scenario must be another scenario or No_parent_scenario. Scenarios with
    No_parent_scenario as the parent renovation scenario are initial state of a possible chain of cumulative  
    renovation scenarios'''
    self_parent_scenarios_all = [scenario for (scenario, parent_scenario) in scenario_pairs_all
                                 if scenario == parent_scenario]
    if len(self_parent_scenarios_all) > 0:
        print('\nWarning: renovation scenarios in which the parent renovation scenario is the scenario itself:')
        for i, scenario in enumerate(self_parent_scenarios_all):
            print(i+1, scenario)
        if len(self_parent_scenarios_all) == 1:
            raise Exception('\n' + str(len(self_parent_scenarios_all)) +
                            ' scenario in Scenarios.xlsx in which the parent renovation scenario is the' +
                            ' scenario itself. Correct and start again')
        else:
            raise Exception('\n' + str(len(self_parent_scenarios_all)) +
                            ' scenarios in Scenarios.xlsx in which the parent renovation scenario is the' +
                            ' scenario itself. Correct and start again')
    else:
        print('\n' + 'No scenarios in Scenarios.xlsx in which the parent renovation scenarios is the scenario ' +
              'itself,\nas required to be able to correctly configure the scenario for its simulation.')

    # Create the scenario chain to which the scenario belong, while ...
    # ... checking that the chain starts with a Non_parent_scenario and is not in loop
    '''All scenario chains must start with an scenario in which the parent renovation scenario is No_parent_scenario. 
    A scenario chain is in loop when any of its scenarios appears more than one in the chain and the chain does  not 
    start with an scenario in which the parent scenario is No_parent_scenario.'''
    scenario_chain_dict = {}
    non_valid_chains_scenarios = []
    non_valid_chains_parents = []
    # For each scenario listed in config_cleanmod
    for scenario in scenarios_list:
        searched_scenario = scenario
        chain = []
        parents = []
        loop_identified = False
        none_identified = False
        max_iterations = len(scenario_pairs_all)
        iteration = 0
        while iteration < max_iterations:
            chain.append(searched_scenario)
            parent_scenario = scenario_pairs_all_df[scenario_pairs_all_df['scenario'] ==
                                                    searched_scenario]['parent_scenario'].tolist()[0]
            parents.append(parent_scenario)
            if parent_scenario in chain:
                loop_identified = True
                chain.append(parent_scenario)
                parent_of_parent = scenario_pairs_all_df[scenario_pairs_all_df['scenario'] ==
                                                         parent_scenario]['parent_scenario'].tolist()[0]
                parents.append(parent_of_parent)
                break
            elif parent_scenario == 'No_parent_scenario':
                none_identified = True
                break
            else:
                searched_scenario = parent_scenario
            iteration += 1
        sorted_chain = chain.copy()
        sorted_chain.reverse()
        scenario_chain_dict[scenario] = sorted_chain
        sorted_parents = parents.copy()
        sorted_parents.reverse()
        if loop_identified is True or none_identified is False:
            non_valid_chains_scenarios.append(sorted_chain)
            non_valid_chains_parents.append(sorted_parents)

    if len(non_valid_chains_scenarios) > 0:
        print('\nWarning: The following scenario chains are not valid, their initial scenario don not have' +
              '\nNo_parent_scenario as the parent scenario and/or they are in loop (some scenario appears more than' +
              ' once in the chain):')
        for i, chain in enumerate(non_valid_chains_scenarios):
            line = '> Non-valid chain | Scenario (Parent): '
            chain_parents = non_valid_chains_parents[i]
            for ii, scenario in enumerate(chain[0:-1]):
                parent = chain_parents[ii]
                line += scenario + ' (' + parent + ')' + ' --> '
            last_chain_scenario = chain[-1]
            last_parent_scenario = chain_parents[-1]
            line += last_chain_scenario + ' (' + last_parent_scenario + ')'
            print('\n' + line)

        if len(non_valid_chains_scenarios) == 1:
            raise Exception('\n' + str(len(non_valid_chains_scenarios)) +
                            ' scenario chain is in loop and/or does not start with an initial scenario with' +
                            ' No_parent_scenario as the parent renovation scenario. Correct and start again')
        else:
            raise Exception('\n' + str(len(non_valid_chains_scenarios)) +
                            ' scenario chains are in loop and/or do not start with an initial scenario with' +
                            ' No_parent_scenario as the parent renovation scenario. Correct and start again')
    else:
        print(
            '\n' + 'All renovation scenario chains are valid and start with an initial scenario with' +
            ' No_parent_scenario as the parent renovation scenario,\nas required to be able to correctly configure' +
            ' the cumulative scenarios.')

    # # Check that the sum of percentages per building variant is 100
    unbalanced_scenarios = []
    for scenario in scenarios_list:
        for chain_scenario_ in scenario_chain_dict[scenario]:
            percentage_buildings_bv1 = scenarios_df[
                scenarios_df['Parameter'] == '%_buildings-building_variant_1'][chain_scenario_].tolist()[0]
            percentage_buildings_bv2 = scenarios_df[
                scenarios_df['Parameter'] == '%_buildings-building_variant_2'][chain_scenario_].tolist()[0]
            percentage_buildings_bv3 = scenarios_df[
                scenarios_df['Parameter'] == '%_buildings-building_variant_3'][chain_scenario_].tolist()[0]
            sum_percentages = percentage_buildings_bv1 + percentage_buildings_bv2 + percentage_buildings_bv3
            if sum_percentages != 100 and chain_scenario_ not in unbalanced_scenarios:
                unbalanced_scenarios.append(chain_scenario_)
    if len(unbalanced_scenarios) > 0:
        print('\nWarning: In the following scenarios from Scenario.xslx required to simulate the scenarios listed ' +
              ' in config_cleanmod,\nthe sum of percentages of buildings per building variant is not 100:')
        for i, scenario in enumerate(unbalanced_scenarios):
            print(i+1, scenario)
        if len(unbalanced_scenarios) == 1:
            raise Exception('\n' + str(len(unbalanced_scenarios)) +
                            ' scenario in which the the sum of percentages of buildings per building variant is not' +
                            ' 100. Correct and start again')
        else:
            raise Exception('\n' + str(len(unbalanced_scenarios)) +
                            ' scenarios in which the the sum of percentages of buildings per building variant is not' +
                            ' 100. Correct and start again')
    else:
        print('\n' + 'All scenarios from Scenario.xlsx required to simulate the scenarios listed in config_cleanmod' +
              '\nhave a sum of percentages of buildings per building variant equal to 100,' +
              '\nas required to be able to correctly configure the cumulative scenarios.')

    print('\nThe object scenarios will be created in the following order:')
    for i, scenario in enumerate(scenarios):
        print(str(i+1) + ':\t' + scenario)

    return scenarios_df, scenario_chain_dict

# ----------------------------------------------------------------------------------------------------------------------


class Scenario:
    """A model scenario to be be simulated"""

    def __init__(self, scenario_id):
        """Initialize scenario attributes"""

        # Scenario parameters and their values per parameter category (as in file Scenarios.xlsx):
        # -- General:
        self.scenario_id = scenario_id
        self.scenario_description = None
        # -- Climate:
        self.location_year = None
        # -- Envelope:
        self.percentage_WWR = None
        self.parent_renovation_scenario_id = None
        self.percentage_buildings_bv1 = None  # bv: building_variant
        self.percentage_buildings_bv2 = None
        self.percentage_buildings_bv3 = None
        # -- Heat supply:
        self.heat_supply_system_bv1 = None
        self.heat_supply_system_bv2 = None
        self.heat_supply_system_bv3 = None
        # -- Heat transmission:
        self.heat_transmission_system_bv1 = None
        self.heat_transmission_system_bv2 = None
        self.heat_transmission_system_bv3 = None
        # -- PV:
        self.percentage_PV_potential_used_roofs = None
        self.percentage_PV_potential_used_walls = None
        self.pv_minimum_radiation = None
        # -- EV:
        self.percentage_EV_per_household = None
        # -- Redensification:
        # TODO - Priority 3 (deadline: 01.09.2023) - Responsible "MB" -  comment by = MB - Implement the code account for this parameter
        self.percentage_renovated_buildings_with_new_level = None
        self.percentage_new_buildings_bv2 = None
        self.percentage_new_buildings_bv3 = None
        # -- Energy management:
        self.calc_type = None
        self.tariff = None

        # Additional attributes:
        self.scenario_epw_file = None  # Path of the file containing the weather data for the scenario
        self.scenario_direct_rad_file = None
        self.scenario_total_rad_file = None
        self.scenario_pv_generation_file = None
        self.buildings_df = None
        self.parent_renovation_scenario = None  # Object associated with the parent renovation scenario

        print('\nScenario object ' + self.scenario_id + ' has been initialized.')

    def configure_scenario(self, scenarios_df,
                           folder_buildings_scenarios, folder_gh_output, file_geometry_zones,
                           file_building_variant_compositions, scenario_objects, complete_building_csv_files=False,
                           export=True):
        """Run all class methods to assign a value to each object attribute"""
        print('\nScenario object ' + self.scenario_id + ' will be configured with data from Scenarios.xlsx.')
        self.load_parameter_values(scenarios_df)
        self.load_scenario_epw_file(folder_gh_output)
        self.load_scenario_gh_file(folder_gh_output)
        self.load_parent_renovation_scenario(scenario_objects)
        self.create_buildings_df(file_geometry_zones, folder_buildings_scenarios, file_building_variant_compositions,
                                 complete_building_csv_files, export)
        print('\nScenario object ' + self.scenario_id + ' has been configured with data from Scenarios.xlsx.')

    def load_parameter_values(self, scenarios_df):
        """Extract the values of each parameter from file Scenarios.xlsx and assign them to the object attributes"""
        # -- General:
        self.scenario_description = scenarios_df[
            scenarios_df['Parameter'] == 'scenario_description'][self.scenario_id].tolist()[0]
        # -- Climate:
        self.location_year = scenarios_df[
            scenarios_df['Parameter'] == 'location_year'][self.scenario_id].tolist()[0]
        # -- Envelope:
        self.percentage_WWR = scenarios_df[
            scenarios_df['Parameter'] == '%_WWR'][self.scenario_id].tolist()[0]
        self.parent_renovation_scenario_id = scenarios_df[
            scenarios_df['Parameter'] == 'parent_renovation_scenario'][self.scenario_id].tolist()[0]
        self.percentage_buildings_bv1 = scenarios_df[
            scenarios_df['Parameter'] == '%_buildings-building_variant_1'][self.scenario_id].tolist()[0]
        self.percentage_buildings_bv2 = scenarios_df[
            scenarios_df['Parameter'] == '%_buildings-building_variant_2'][self.scenario_id].tolist()[0]
        self.percentage_buildings_bv3 = scenarios_df[
            scenarios_df['Parameter'] == '%_buildings-building_variant_3'][self.scenario_id].tolist()[0]
        # -- Heat supply:
        self.heat_supply_system_bv1 = scenarios_df[
            scenarios_df['Parameter'] == 'heat_supply-building_variant_1'][self.scenario_id].tolist()[0]
        self.heat_supply_system_bv2 = scenarios_df[
            scenarios_df['Parameter'] == 'heat_supply-building_variant_2'][self.scenario_id].tolist()[0]
        self.heat_supply_system_bv3 = scenarios_df[
            scenarios_df['Parameter'] == 'heat_supply-building_variant_3'][self.scenario_id].tolist()[0]
        # -- Heat transmission:
        self.heat_transmission_system_bv1 = scenarios_df[
            scenarios_df['Parameter'] == 'heat_transmission-building_variant_1'][self.scenario_id].tolist()[0]
        self.heat_transmission_system_bv2 = scenarios_df[
            scenarios_df['Parameter'] == 'heat_transmission-building_variant_2'][self.scenario_id].tolist()[0]
        self.heat_transmission_system_bv3 = scenarios_df[
            scenarios_df['Parameter'] == 'heat_transmission-building_variant_3'][self.scenario_id].tolist()[0]
        # -- PV:
        self.percentage_PV_potential_used_roofs = scenarios_df[
            scenarios_df['Parameter'] == '%_PV_potential_used_roofs'][self.scenario_id].tolist()[0]
        self.percentage_PV_potential_used_walls = scenarios_df[
            scenarios_df['Parameter'] == '%_PV_potential_used_walls'][self.scenario_id].tolist()[0]
        self.pv_minimum_radiation = scenarios_df[
            scenarios_df['Parameter'] == 'PV_minimum_radiation'][self.scenario_id].tolist()[0]
        # -- EV:
        self.percentage_EV_per_household = scenarios_df[
            scenarios_df['Parameter'] == '%_Evs_per_household'][self.scenario_id].tolist()[0]
        # -- Redensification:
        self.percentage_renovated_buildings_with_new_level = scenarios_df[
            scenarios_df['Parameter'] == '%_renov_buildings_with_new_level'][self.scenario_id].tolist()[0]
        self.percentage_new_buildings_bv2 = scenarios_df[
            scenarios_df['Parameter'] == '%_new_buildings-building_variant_2'][self.scenario_id].tolist()[0]
        self.percentage_new_buildings_bv3 = scenarios_df[
            scenarios_df['Parameter'] == '%_new_buildings-building_variant_3'][self.scenario_id].tolist()[0]
        # -- Energy management:
        self.calc_type = scenarios_df[
            scenarios_df['Parameter'] == 'control_type'][self.scenario_id].tolist()[0]
        self.tariff = scenarios_df[
            scenarios_df['Parameter'] == 'electricity_tariff'][self.scenario_id].tolist()[0]

    def load_scenario_epw_file(self, folder_gh_output):
        """"Retrieve the path of the epw file related to the scenario to be simulated"""
        folder_epw_file = folder_gh_output + '/' + self.location_year
        epw_files = []
        for file in os.listdir(folder_epw_file):
            if file.endswith('.epw'):
                epw_files.append(file)
        if len(epw_files) > 1:
            raise Exception('More than one epw files are available for scenario ' + self.scenario_id +
                            '. Correct and start again.')
        else:
            epw_file_name = epw_files[0]
            self.scenario_epw_file = folder_epw_file + '/' + epw_file_name
            print('\nScenario object ' + self.scenario_id + ' will use as epw: ' + epw_file_name)

    def load_scenario_gh_file(self, folder_gh_output):
        """"Retrieve the path of the epw file related to the scenario to be simulated"""
        folder_gh_file = folder_gh_output + '/' + self.location_year
        file_direct_rad_name = 'Radiation_DirectRad.csv'
        file_total_rad_name = 'Radiation_TotalRad.csv'
        file_pv_production_name = 'PV_ACenergyPerHour.csv'
        self.scenario_direct_rad_file = folder_gh_file + '/' + file_direct_rad_name
        self.scenario_total_rad_file = folder_gh_file + '/' + file_total_rad_name
        self.scenario_pv_generation_file = folder_gh_file + '/' + file_pv_production_name

    def load_parent_renovation_scenario(self, scenario_objects):
        """Load the object associated with the parent renovation scenario"""
        parent_renovation_scenario_id = self.parent_renovation_scenario_id
        if parent_renovation_scenario_id == 'No_parent_scenario' or parent_renovation_scenario_id is None:
            print('\nScenario object ' + self.scenario_id + ' does not have any parent scenario associated')
        else:
            self.parent_renovation_scenario = scenario_objects[self.parent_renovation_scenario_id]
            print('\nScenario object ' + self.scenario_id + ' has been extended with its parent scenario object:',
                  parent_renovation_scenario_id)

    def create_buildings_df(self, file_geometry_zones, folder_buildings_scenarios, file_building_variant_compositions,
                            complete_building_csv_files, export=True):
        """Create a buildings data frame, export it as csv and save it in the object attribute"""

        print('\nA building_df will be created for scenario ' + self.scenario_id)

        if export is False:
            """The scenario is not listed in config_cleanmod but it is part of the chain required for its simulation.
            Only scenarios listed in config_cleanmod are exported and have, therefore, a csv file available for reading
            and loading the data for the df. For the rest of scenarios in the required chain the reading process must
            be skipped"""
            read_csv = False
        else:
            read_csv = True

        if complete_building_csv_files and read_csv:
            building_csv_name = 'Buildings-' + self.scenario_id + '.csv'
            print('\nA complete building_csv file ' + building_csv_name + ' already exists and will be read')

            building_csv_path = folder_buildings_scenarios + '/' + building_csv_name
            buildings_df = pd.read_csv(building_csv_path)

            # Extract building_list
            buildings_list = buildings_df['building_code'].tolist()

            # Set building_code as the index
            buildings_df.index = buildings_list

            # assign the df to the object attribute
            self.buildings_df = buildings_df.copy()

        else:
            # Initialize df
            # ----------------------------------------------------------------------------------------------------------
            buildings_df = pd.DataFrame({'building_code': [],
                                         'renovation_variant': [],
                                         'heat_supply': [],
                                         'heat_transmission': [],
                                         'WWR_N': [],
                                         'WWR_E': [],
                                         'WWR_S': [],
                                         'WWR_W': []})

            # Create the content for the data frame
            # ----------------------------------------------------------------------------------------------------------
            # Check first whether a parent renovation scenario exists. If yes, use it as the basis
            parent_renovation_scenario_id = self.parent_renovation_scenario_id
            parent_renovation_scenario = self.parent_renovation_scenario
            if parent_renovation_scenario_id == 'No_parent_scenario' or parent_renovation_scenario_id is None:
                print('\nNo parent renovation scenario will be used as the basis for the buildings_df of scenario ' +
                      self.scenario_id)

                # Content generation NOT using a parent renovation scenario as the basis
                # ------------------------------------------------------------------------------------------------------
                # Content of column: building_code
                # # Read csv as df
                geometry_zones_df = pd.read_csv(file_geometry_zones, skiprows=[1])
                # # Add column building-ID
                geometry_zones_df['Building_ID'] = [i.split('-')[0] for i in geometry_zones_df['Zone-ID'].tolist()]
                # # Create list of unique buildings
                buildings_list = geometry_zones_df['Building_ID'].unique().tolist()

                # Content of column: renovation variant
                # # Fix a seed number for comparability
                random.seed(1)
                # # Summarize variants configuration
                variant_percentages = [self.percentage_buildings_bv1,
                                       self.percentage_buildings_bv2,
                                       self.percentage_buildings_bv3]
                # # Create a random distribution of renovation variants
                renovation_variant_list = random.choices([1, 2, 3], weights=variant_percentages, k=len(buildings_list))

            else:
                print('\nParent renovation scenario ' + parent_renovation_scenario_id +
                      ' will be used as the basis for the buildings_df of scenario ' + self.scenario_id)

                # Content generation using a parent renovation scenario as the basis
                # ------------------------------------------------------------------------------------------------------
                # Content of column: building_code
                # # Extract list of buildings from the parent renovation scenario as the buildings_list
                parent_buildings_list = parent_renovation_scenario.buildings_df['building_code']
                buildings_list = parent_buildings_list

                # Content of column: renovation variant
                # # Extract list of renovation variants from the parent renovation scenario
                p_buildings_df = parent_renovation_scenario.buildings_df.copy()
                parent_bv1 = p_buildings_df[p_buildings_df['renovation_variant'] == 1]['building_code'].tolist()
                parent_bv2 = p_buildings_df[p_buildings_df['renovation_variant'] == 2]['building_code'].tolist()
                parent_bv3 = p_buildings_df[p_buildings_df['renovation_variant'] == 3]['building_code'].tolist()

                # # Fix a seed number for comparability
                random.seed(1)
                # # Summarize desired variants configuration
                variant_percentages = [self.percentage_buildings_bv1,
                                       self.percentage_buildings_bv2,
                                       self.percentage_buildings_bv3]
                # # Create a random distribution of renovation variants
                should_renovation_var_l = random.choices([1, 2, 3], weights=variant_percentages, k=len(buildings_list))
                should_number_bv1 = should_renovation_var_l.count(1)
                should_number_bv2 = should_renovation_var_l.count(2)
                should_number_bv3 = should_renovation_var_l.count(3)

                # #  Check the change with respect the parent scenario
                change_number_bv1 = should_number_bv1 - len(parent_bv1)
                change_number_bv2 = should_number_bv2 - len(parent_bv2)
                change_number_bv3 = should_number_bv3 - len(parent_bv3)

                print('\nChanges in building variants with respect to the parent renovation scenario:')
                # -- bv1
                exchange_bv1 = []
                if change_number_bv1 == 0:  # No change, take all buildings from bv1 in the parent scenario
                    print('\nBuilding variant 1: no change')
                    scenario_bv1 = parent_bv1
                elif change_number_bv1 < 0:  # Reduction, take n buildings randomly to be renovated and keep the others
                    excess = change_number_bv1 * (-1)
                    print('\nBuilding variant 1: reduction of ' + str(excess) + ' building/s')
                    exchange_bv1 = []
                    scenario_bv1 = parent_bv1
                    for i in range(excess):
                        selected_building = random.choice(scenario_bv1)
                        exchange_bv1.append(selected_building)
                        scenario_bv1.remove(selected_building)
                else:  # Increment, error
                    increment = change_number_bv1
                    print('\nBuilding variant 1: increment of ' + str(increment) + ' building/s')
                    raise Exception('\nThe number of buildings with renovation variant 1 in the scenario cannot be' +
                                    ' greater than in the the parent scenario. Correct and start again')
                # -- bv2
                exchange_bv2 = []
                if change_number_bv2 == 0:  # No change, take all buildings from bv2 in the parent scenario
                    print('\nBuilding variant 2: no change')
                    scenario_bv2 = parent_bv2
                elif change_number_bv2 < 0:  # Reduction, take n buildings randomly to be renovated and keep the others
                    excess = change_number_bv2 * (-1)
                    print('\nBuilding variant 2: reduction of ' + str(excess) + ' building/s')
                    exchange_bv2 = []
                    scenario_bv2 = parent_bv2
                    for i in range(excess):
                        selected_building = random.choice(scenario_bv2)
                        exchange_bv2.append(selected_building)
                        scenario_bv2.remove(selected_building)
                else:  # Increment, take n buildings from bv1
                    increment = change_number_bv2
                    print('\nBuilding variant 2: increment of ' + str(increment) + ' building/s')
                    scenario_bv2 = parent_bv2
                    for i in range(increment):
                        selected_building = random.choice(exchange_bv1)
                        scenario_bv2.append(selected_building)
                        exchange_bv1.remove(selected_building)
                    print('(' + str(increment) + ' renovated from bv1 to bv2)')
                # -- bv3
                if change_number_bv3 == 0:  # No change, take all buildings from bv3 in the parent scenario
                    print('\nBuilding variant 3: no change')
                    scenario_bv3 = parent_bv3
                elif change_number_bv3 < 0:  # Reduction, error
                    excess = change_number_bv3 * (-1)
                    print('\nBuilding variant 3: reduction of ' + str(excess) + ' building/s')
                    raise Exception('\nThe number of buildings with renovation variant 3 in the scenario cannot be' +
                                    ' smaller than in the the parent scenario. Correct and start again')
                else:  # Increment, take n buildings from bv1 and/or bv2
                    increment = change_number_bv3
                    print('\nBuilding variant 3: increment of ' + str(increment) + ' building/s')
                    if increment == len(exchange_bv1) + len(exchange_bv2):  # Enough buildings to solve the update exist
                        scenario_bv3 = parent_bv3
                        if increment == len(exchange_bv1) or increment > len(exchange_bv1):
                            # Take all buildings form bv1
                            scenario_bv3 += exchange_bv1
                            exchange_bv1 = []
                            print('(' + str(increment - len(exchange_bv2)) + ' renovated from bv1 to bv3)')
                        if increment > len(exchange_bv1):  # Take the rest from bv2
                            print('(' + str(len(exchange_bv2)) + ' renovated from bv2 to bv3)')
                            scenario_bv3 += exchange_bv2
                            exchange_bv2 = []
                    elif increment > len(exchange_bv1) + len(exchange_bv2):
                        raise Exception('\nThe number of buildings to be renovate into bv3 exceed the number of' +
                                        ' buildings available from bv1 and bv2. Correct and start again')
                    else:  # if increment < len(exchange_bv1) + len(exchange_bv2):
                        raise Exception('\nThe number of buildings to be renovate into bv3 is smaller thant the' +
                                        ' number of buildings available from bv1 and bv2. It should be identical.' +
                                        ' Correct and start again')

                # # Summarize final scenario distribution
                scenario_bv1 = scenario_bv1
                scenario_bv2 = scenario_bv2
                scenario_bv3 = scenario_bv3

                # # Update renovation variants
                for scenario in scenario_bv1:
                    p_buildings_df.loc[scenario, 'renovation_variant'] = 1
                for scenario in scenario_bv2:
                    p_buildings_df.loc[scenario, 'renovation_variant'] = 2
                for scenario in scenario_bv3:
                    p_buildings_df.loc[scenario, 'renovation_variant'] = 3

                # # Final scenario variant list
                renovation_variant_list = p_buildings_df['renovation_variant'].tolist()

            # Content of column: heat_supply
            # # Heat supply system dictionary per renovation variant
            heat_supply_dict = {1: self.heat_supply_system_bv1,
                                2: self.heat_supply_system_bv2,
                                3: self.heat_supply_system_bv3}
            # # Link each renovation variant to its corresponding heat supply system
            heat_supply_list = [heat_supply_dict[rv] for rv in renovation_variant_list]

            # Content of column: heat_transmission
            # # Heat supply system dictionary per renovation variant
            heat_transmission_dict = {1: self.heat_transmission_system_bv1,
                                      2: self.heat_transmission_system_bv2,
                                      3: self.heat_transmission_system_bv3}
            # # Link each renovation variant to its corresponding heat supply system
            heat_transmission_list = [heat_transmission_dict[rv] for rv in renovation_variant_list]

            # Content of column: WWR_n
            if self.percentage_WWR == 'Tabula':
                # using WWR from Tabula
                wwr_list = []
                building_variant_composition_df = pd.read_csv(file_building_variant_compositions)
                for i, building_code in enumerate(buildings_list):
                    ren_var_num = renovation_variant_list[i]
                    ren_var_str = '0' + str(ren_var_num) if ren_var_num > 9 else '00' + str(ren_var_num)
                    building_variant_id = building_code.split('_')[0] + '.' + ren_var_str
                    wwr_val = building_variant_composition_df[building_variant_composition_df['building_variant'] ==
                                                              building_variant_id]['WWR'].tolist()[0]
                    wwr_list.append(wwr_val)
            else:
                # using a value from Scenario generator
                wwr_list = [self.percentage_WWR for building in buildings_list]

            # Fill in the df with values
            buildings_df['building_code'] = buildings_list
            buildings_df['renovation_variant'] = renovation_variant_list
            buildings_df['heat_supply'] = heat_supply_list
            buildings_df['heat_transmission'] = heat_transmission_list
            buildings_df['WWR_N'] = wwr_list
            buildings_df['WWR_E'] = wwr_list
            buildings_df['WWR_S'] = wwr_list
            buildings_df['WWR_W'] = wwr_list

            # Set building_code as the index
            buildings_df.index = buildings_list

            # assign the df to the object attribute
            self.buildings_df = buildings_df.copy()

            # Export the df as csv
            if export:
                export_file_name = 'Buildings-' + self.scenario_id + '.csv'
                export_file_path = folder_buildings_scenarios + '/' + export_file_name
                buildings_df.to_csv(export_file_path, index=False)
                print('\nScenario buildings_csv saved as:', export_file_name, '\nFile location:', export_file_path)


# ----------------------------------------------------------------------------------------------------------------------
# Test the progress. Just for development. Keep until we are sure that the code works with the rest of modules
"""
complete_building_csv_files = False

print('\n------- SCENARIO GENERATION STARTED:')
_scenarios_df, _scenario_chain_dict = scenario_preprocessing(_scenarios, _file_scenarios)

scenario_objects = {}
for scenario_id in _scenarios:
    print('\n' + scenario_id + '\n------------------------------')
    scenario_chain = _scenario_chain_dict[scenario_id]
    if len(scenario_chain) > 1:
        print('\nScenario chain associated with focus scenario ' + scenario_id + ' :', scenario_chain)
        print('\nAll scenarios in the chain will be processed, '
              'although only a building csv will be exported for the focus scenario ' + scenario_id)
    for chain_scenario_id in scenario_chain:
        if len(scenario_chain) > 1:
            print('\n\t' + chain_scenario_id + '\n\t------------------------------')
        export_csv = True if chain_scenario_id == scenario_id else False
        # if chain_scenario_id in list(scenario_objects.keys()):
        #     print('\nThis scenario has been already processed')
        # else:
        # Initialize the scenario object for the scenario_id
        scenario_object = Scenario(chain_scenario_id)
        # Configure the scenario object with the specifications from the file Scenarios.xlsx
        scenario_object.configure_scenario(_scenarios_df, folder_buildings_scenarios, folder_GH_output,
                                           file_geometry_zones, file_building_variant_compositions,
                                           scenario_objects, complete_building_csv_files, export_csv)
        # Add the configured object to the dictionary (to be able to process scenarios with parent scenarios)
        scenario_objects[chain_scenario_id] = scenario_object

print('\n------- SCENARIO GENERATION FINISHED:')
"""