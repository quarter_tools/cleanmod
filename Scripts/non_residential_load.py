import pandas as pd
import os
import logging
import shutil
import datetime
from openpyxl import load_workbook
from ruamel.yaml import YAML


class NonResidentialComponent:
    """
    A class used to represent the NonResidentialComponent of CleanMod.

    Attributes
    ----------
    config : dict
        A dictionary that holds the configuration settings.
    name_model : str
        A string that represents the name of the model.
    elec_config : dict
        A dictionary that holds the simulation settings for the electric configuration.
    time_resolution_h : int
        An integer that represents the time resolution in hours for the simulation.
    df2 : pandas.DataFrame
        A pandas DataFrame that holds the non-residential areas for the model.
    path : str
        A string that represents the path for the non-residential directory.
    exe_time : str
        A string that represents the execution time for the simulation.
    directory : str
        A string that represents the directory path for the simulation.
    temp : str
        A string that represents the temporary directory path for the simulation.
    index : list
        A list that holds the row indexes for the DataFrame.
    dt : datetime.datetime
        A datetime object that represents the start date for the simulation.
    end : datetime.datetime
        A datetime object that represents the end date for the simulation.
    step : datetime.timedelta
        A timedelta object that represents the simulation timestep.
    L : list
        A list that holds the timesteps for the simulation.
    nr_load : pandas.DataFrame
        A pandas DataFrame that holds the non-residential load data.
    nr_gen : pandas.DataFrame
        A pandas DataFrame that holds the non-residential generation data.

    Methods
    -------
    run()
        Runs the NonResidentialComponent simulation.
    create_directories()
        Creates the non-residential directories for the simulation.
    calculate_loads()
        Calculates the non-residential loads for the simulation.
    calculate_timesteps()
        Calculates the timesteps for the simulation.
    final_calculations()
        Calculates the final non-residential load and generation data for the simulation.
    """

    def __init__(self):
        """
           Constructs all the necessary attributes for the NonResidentialComponent object.
           """
        with open(f"../Step_0_config.yaml") as config_file:
            self.config = YAML().load(config_file)
        self.name_model = self.config['project_settings']['name_model']
        self.elec_config = self.config['simulation_settings']['elec_config']
        self.time_resolution_h = self.config['simulation_settings']['time_resolution_h']
        self.df2 = pd.read_csv(f'../Models/{self.name_model}/Input/NR_areas.csv')
        self.path = os.path.join(os.getcwd(),
                                 '../Models/{0}/Output/Input_created/Non_residential'.format(self.name_model))
        self.exe_time = datetime.datetime.now().strftime('%y%m%d_%H%M%S')
        self.directory = os.path.join(self.path, self.exe_time)
        self.temp = os.path.join(self.directory, 'temp')
        self.index = list(self.df2.iterrows())
        self.dt = datetime.datetime(2022, 1, 1)
        self.end = datetime.datetime(2022, 12, 31, 23, 59, 59)
        self.step = datetime.timedelta(minutes=15)
        self.L = []
        self.nr_load = None
        self.nr_gen = None

    def run(self):
        """
          Runs the NonResidentialComponent simulation.
          """
        print('\n--> CALCULATING NON RESIDENTIAL COMPONENTS')
        logging.debug('CALCULATING NON RESIDENTIAL COMPONENTS')
        self.create_directories()
        self.calculate_loads()
        self.calculate_timesteps()
        self.final_calculations()

    def create_directories(self):
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
        if not os.path.exists(self.temp):
            os.makedirs(self.temp)

    def calculate_loads(self):
        """
          Calculates NonResidentialComponent loads.
          """
        for i in range(len(self.index)):
            use_case = self.df2['non_residential_use'][i]
            wb = load_workbook(f'../Libraries/Non_residential_loads/{use_case}.xlsx')
            for sheet_name in wb.sheetnames:
                df1 = pd.read_excel(f'../Libraries/Non_Residential_Loads/{use_case}_.xlsx',
                                    sheet_name=sheet_name, engine='openpyxl')
                df3 = df1.iloc[:, 1:] * (self.df2['surface_m2'].values[i])
                df3.columns = df3.columns.str.rstrip('_m2')
                b = use_case + '_' + sheet_name
                df3.to_csv(os.path.join(self.temp, b + '.csv'))

    def calculate_timesteps(self):
        """
          Creates a timestep based on datetime.
          """
        while self.dt < self.end:
            self.L.append(self.dt.strftime('%Y/%m/%d %H:%M:%S'))
            self.dt += self.step

    def final_calculations(self):

        """
           Performs final calculations on the input data to generate aggregate electricity consumption and production values
           for non-residential buildings, and saves the results to CSV files.

           Returns:
               aggregated_non_resi_load (DataFrame or str): If 'non_resi_load' is in self.elec_config, a DataFrame object
                   containing the aggregate electricity consumption values with timestep, otherwise 'None'.
               aggregated_non_resi_gen (DataFrame or str): If 'non_resi_gen' is in self.elec_config, a DataFrame object
                   containing the aggregate electricity production values with timestep, otherwise 'None'.
           """

        f_ext = ['power_cons_total.csv', 'power_prod_total.csv']
        for i, val in enumerate(f_ext):
            files = [os.path.join(self.temp, f) for f in os.listdir(self.temp) if f.endswith(val)]
            for index, filename in enumerate(files):
                if index == 0:
                    df4 = pd.read_csv(filename)
                    df4 = df4.drop(df4.columns[[0]], axis=1)
                else:
                    df3 = pd.read_csv(filename)
                    df3 = df3.drop(df3.columns[[0]], axis=1)
                    df4 = df4.add(df3)

            df4.insert(loc=0, column='timestep', value=self.L)  # Insert 15min timeseries to dataframe
            df4['timestep'] = pd.to_datetime(df4['timestep'])
            if 'non_resi_load' or 'non_resi_gen' in self.elec_config and self.time_resolution_h == 1:  # if time resolution is hourly
                df4 = df4.groupby(pd.Grouper(freq='H',
                                             key='timestep')).sum().reset_index()  # convert timeseries to hourly by sum of the 15min samples
            df4['timestep'] = df4['timestep'].dt.strftime(
                '%m/%d %H:%M:%S')  # drop year from the timeseries to create timestep format
            df4.to_csv(os.path.join(self.directory, val))
            # pass only the aggregate of consumption and production with timestep to new dataframes
            if val == 'power_cons_total.csv':
                nr_load = df4[['timestep', 'sum_Wh']]
            elif val == 'power_prod_total.csv':
                nr_gen = df4[['timestep', 'sum_Wh']]
            else:
                continue

        # Defining aggregated_non_resi_load
        if 'non_resi_load' in self.elec_config:
            aggregated_non_resi_load = nr_load
        else:
            aggregated_non_resi_load = 'None'

        # Defining aggregated_non_resi_gen
        if 'non_resi_gen' in self.elec_config:
            aggregated_non_resi_gen = nr_gen
        else:
            aggregated_non_resi_gen = 'None'

        # Delete the temporary files
        shutil.rmtree(self.temp)
        logging.debug('NON RESIDENTIAL COMPONENTS calculated successfully.')
        print('\n--> NON RESIDENTIAL COMPONENTS calculated successfully.')


# component = NonResidentialComponent()  # create an object of the NonResidentialComponent class
# component.run()  # call the run() method on the object
