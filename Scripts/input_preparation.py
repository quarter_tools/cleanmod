import math
import numpy as np
import pandas as pd
import os
import shutil
import pathlib
import interface_UEP as interface_UEP
import weather as weather
import logging
from ruamel.yaml import YAML
from subprocess import call
from distutils.dir_util import copy_tree
from scipy.io import loadmat
import random
from random import randint
from progress_bar import app

########################################################################################################################
# DATA AND FOLDER/FILE PATHS
########################################################################################################################

# Data from config
# ----------------------------------------------------------------------------------------------------------------------
with open("Step_0_config.yaml") as config_file:
    config = YAML().load(config_file)
# project settings:
name_model = config['project_settings']['name_model']
scenarios = config['project_settings']['scenarios']
complete_input_created_exists = config['project_settings']['complete_input_created_exists']
# simulation settings:
time_resolution_h = config['simulation_settings']['time_resolution_h']
show_progress = config['progress_bar_setting']['show_progress']

# Base data
# Library --------------------------------------------------------------------------------------------------------------
file_building_classes = 'Libraries/Building_classes.csv'
file_building_component_compositions = 'Libraries/Building_component_compositions.csv'
file_building_variant_compositions = 'Libraries/Building_variant_compositions.csv'
file_building_pv_potentials = 'Libraries/Building_pv_potentials.csv'
file_dhw = 'Libraries/DHW_demand.csv'
file_household_sizes = 'Libraries/Household_sizes.csv'
file_hp_lib_database = 'Libraries/HP_lib_database.csv'
file_window_types = 'Libraries/Window_types.csv'
folder_EV_profiles = 'Libraries/EV_profiles'
file_CO2_emi = 'Libraries/CO2_emission/CO2_emission_2021.xlsx'
file_renovation_costs = 'Libraries/Renovation_costs.csv'
# Model input ----------------------------------------------------------------------------------------------------------
folder_model = 'Models/{0}'.format(name_model)
file_scenarios = folder_model + '/Input/Scenarios.xlsx'
# Buildings_scenarios
folder_buildings_scenarios = folder_model + "/Input/Buildings_scenarios"
# GH_output
file_geometry_surfaces = folder_model + "/Input/GH_output/Geometry_Surfaces.csv"
file_geometry_zones = folder_model + "/Input/GH_output/Geometry_Zones.csv"
# UEP_output
file_occupancy_mat = folder_model + '/Input/UEP_output/occupancy.mat'
file_gains_lighting_mat = folder_model + '/Input/UEP_output/lighting.mat'
file_active_load_mat = folder_model + '/Input/UEP_output/active_load.mat'
file_occupancy_csv = folder_model + '/Input/UEP_output/occupancy.csv'
file_gains_lighting_csv = folder_model + '/Input/UEP_output/lighting.csv'
file_active_load_csv = folder_model + '/Input/UEP_output/active_load.csv'
dir_dhw = folder_model + '/Input/UEP_output/DHW_per_building'
# UEP interface
uep_mat_src = ("../../UrbanEnergyPro/")
uep_dir_input = '../../UrbanEnergyPro/input/SynCity'
uep_runme = "../../UrbanEnergyPro/runme.py"
uep_mat_dst = (folder_model + "/Input/UEP_output")
# Model output ---------------------------------------------------------------------------------------------------------
# Input_created
folder_input_created = folder_model + "/Output/Input_created"
# Additional variables -------------------------------------------------------------------------------------------------
UEP_region_name = name_model
########################################################################################################################

# TODO - (Decision pending) - Priority ? (deadline: ?) - Responsible "?" - comment by = MB - finalise reformat input function for adapting simulation resolution
#def reformat_input_to_match_time_resolution(input, reformat_type='copy'):
#    num_timesteps = int(8760 / time_resolution_h)
#    if len(input) != num_timesteps:
#        input_new = pd.DataFrame()
#        if len(input) > num_timesteps:  # then sum timesteps
#            factor = int(len(input) / num_timesteps)
#            # sum every factor rows
#            i=0
#        else:
#            factor = int(num_timesteps / len(input))
#            if reformat_type=='copy':
#                for i, row in input.iterrows():
#                    for f in range(factor):
#                        input_new.append(i, row)
#                        # index setzen!
#            elif reformat_type=='split':
#                for i, row in input.iterrows():
#                    for f in range(factor):
#                        input_new.append(i, row)
#                input_new.apply(lambda x: x/factor)
#
#         # index setzen!
#
#    # Unterschied: df -- series
#
#    return input_new
########################################################################################################################


# bid_UEP dictionary based on building_codes
def building_dict(file):
    """This function creates a dictionary of buildings which is used as an index for dataframe generations and as
     building identifier in Urban Energy Pro."""

    global bid_dict
    os.chdir("../CleanMod")
    df_4dict = pd.read_csv(file)
    bid_dict_init = df_4dict["building_code"].to_dict()
    bid_dict = dict([(value, key) for key, value in bid_dict_init.items()])
    return bid_dict


def clean_existing_folders(folder_path):
    """This function removes the referenced folder in order to remove all its contents and then recreates another folder
    with the same name for new files.

    Args:
        folder_path (list): paths of the folders to be cleaned."""

    print('\n    Cleaning folders started, recreating whole folder in case it already contains files')
    logging.debug('Cleaning folders started, recreating whole folder in case it already contains files')

    for folder in folder_path:
        if os.path.isdir(folder):
            print('    Removing folder: ' + folder)
            shutil.rmtree(folder)
            logging.debug('Removing folder: ' + folder)
    for folder in folder_path:
        if not os.path.isdir(folder):
            print('    Creating folder: ' + folder)
            logging.debug('Creating folder: ' + folder)
            os.makedirs(folder)

    print('    Cleaning folders finished, recreating whole folder in case it already contains files')
    logging.debug('Cleaning folders finished, recreating whole folder in case it already contains files')

    pass


def preparation_workthrough(scenario_object, c_type, j):
    """This function is executed once for each scenario. It restructures, combines inputs to match
    format requirements, and creates zone_id_list and building_id_list for each scenario.

    Args:
        scenario_object (class): object containing all scenario configuration.
        c_type (str): name of the objective to be implemented in the simulation. Used here only for tracking progress.

    Returns:
        building_id_list (list): building identification list received from function create_building_data().
        zone_id_list (list): zone identification list received from function create_zone_data().
    """

    scenario_id = scenario_object.scenario_id

    # Directories within input created folder
    dir_building = folder_input_created + '/' + scenario_id + '/Building_data'
    dir_zone = folder_input_created + '/' + scenario_id + '/Zone_data'
    dir_window = folder_input_created + '/' + scenario_id + '/Window_data'
    dir_profile = folder_input_created + '/' + scenario_id + '/Timeseries_data'
    dir_elec_hh_load = folder_input_created + '/' + scenario_id + '/Elec_hh_load'
    dir_solar_radiation_reformat = folder_input_created + '/' + scenario_id + '/Radiation_data'
    dir_bc = folder_input_created + '/' + scenario_id + '/Building_component_data'

    # scenario related input files
    file_buildings = folder_buildings_scenarios + '/' + 'Buildings-' + scenario_id + '.csv'
    file_epw = scenario_object.scenario_epw_file
    file_direct_rad = scenario_object.scenario_direct_rad_file
    file_total_rad = scenario_object.scenario_total_rad_file

    # Create building dictionary linking cleanmod's building_codes to building ids for UEP
    bid_dict = building_dict(file_buildings)

    if complete_input_created_exists:
        buildings_data_enriched = pd.read_csv(dir_building + '/Buildings_extended.csv', index_col=0)
        building_id_list = buildings_data_enriched.index.tolist()
        zones_data = pd.read_csv(dir_zone + '/Zones.csv', index_col=0)
        zone_id_list = zones_data.index.tolist()

    else:
        # Initiate input preparation
        ################################################################################################################
        # INPUT PREPARATION PART 1 of 6 (loading weather data)
        # --------------------------------------------------------------------------------------------------------------
        print('\n--> INPUT PREPARATION PART 1 of 6 (loading weather data) STARTED')
        logging.debug('--> INPUT PREPARATION PART 1 of 6 (loading weather data) STARTED')
        print('    (' + 'The scenario being processed is: ' + scenario_id + ' | ' + 'Calculation_type: ' + c_type + ')')

        weather_data = weather.Weather(file_epw, file_direct_rad, file_total_rad)

        print('\n--> INPUT PREPARATION PART 1 of 6 (loading weather data) FINISHED')
        logging.debug('--> INPUT PREPARATION PART 1 of 6 (loading weather data) FINISHED')
        if show_progress:
            app.update_progress(10, 'PART 1 of 6 FINISHED', 'INPUT PREPARATION' ,False, scenario_id, c_type, j)
        ################################################################################################################
        # INPUT PREPARATION PART 2 of 6 (enrichment of building and zone data)
        # --------------------------------------------------------------------------------------------------------------
        print('\n--> INPUT PREPARATION PART 2 of 6 (enrichment of building and zone data) STARTED')
        logging.debug('--> INPUT PREPARATION PART 2 of 6 (enrichment of building and zone data) STARTED')
        print('    (' + 'The scenario being processed is: ' + scenario_id + ' | ' + 'Calculation_type: ' + c_type + ')')

        # check if final files do already exist and read in the ones necessary for other input preparation steps
        try:
            buildings_data_enriched = pd.read_csv(dir_building + '/Buildings_extended.csv', index_col=0)
            building_id_list = buildings_data_enriched.index.tolist()
            zones_data = pd.read_csv(dir_zone + '/Zones.csv', index_col=0)
            zone_id_list = zones_data.index.tolist()
            print('\n  > Files already exist and data has been loaded')
            logging.debug('  > Files already exist and data has been loaded')
        except:
            print('\n  > Files do no exist yet and will be created')
            logging.debug('  > Files do no exist yet and will be created')
            try:
                # Clean folders if they are not empty yet
                clean_existing_folders([dir_building, dir_zone])
            except:
                # Pass if the folders are already empty
                pass

            buildings_data = create_building_data(file_buildings, file_building_variant_compositions,
                                                  file_geometry_surfaces, bid_dict, weather_data)
            building_id_list = buildings_data.index.tolist()

            zones_data = create_zone_data(file_geometry_zones, file_household_sizes, buildings_data, bid_dict)
            zone_id_list = zones_data.index.tolist()

            new_columns_list = ['occupants', 'dwellings', 'storey_area', 'ev_num']
            ev_share = scenario_object.percentage_EV_per_household
            buildings_data_enriched = enrich_buildings_data(buildings_data, zones_data, new_columns_list, ev_share)

            # write created data to csv files
            buildings_data_enriched.to_csv(dir_building + '/Buildings_extended.csv')
            zones_data.to_csv(dir_zone + '/Zones.csv')

        print('\n--> INPUT PREPARATION PART 2 of 6 (enrichment of building and zone data) FINISHED')
        logging.debug('--> INPUT PREPARATION PART 2 of 6 (enrichment of building and zone data) FINISHED')
        if show_progress:
            app.update_progress(20, 'PART 2 of 6 FINISHED', 'INPUT PREPARATION' ,False, scenario_id, c_type, j)
        ################################################################################################################
        # INPUT PREPARATION PART 3 of 6 (creation of building_component data)
        # --------------------------------------------------------------------------------------------------------------
        print('\n--> INPUT PREPARATION PART 3 of 6 (creation of building_component_data) STARTED')
        logging.debug('--> INPUT PREPARATION PART 3 of 6 (creation of building_component_data) STARTED')
        print('    (' + 'The scenario being processed is: ' + scenario_id + ' | ' + 'Calculation_type: ' + c_type + ')')

        # check if final files do already exist and read in the ones necessary for other input preparation steps
        try:
            building_component_data_dict = {}
            for zid in zone_id_list:
                building_component_data_dict[zid] = pd.read_csv(dir_bc + '/bc_zid{0}.csv'.format(zid),skiprows=[1])
            print('\n  > Files already exist and data has been loaded')
            logging.debug('  > Files already exist and data has been loaded')
        except:
            print('\n  > Files do no exist yet and will be created')
            logging.debug('  > Files do no exist yet and will be created')
            try:
                # Clean folders if they are not empty yet
                clean_existing_folders([dir_bc])
            except:
                # Pass if the folders are already empty
                pass

            window_types_data = create_window_types_df(file_window_types)

            building_component_data_dict = create_building_component_data_per_zone(file_geometry_surfaces,
                                                                                   file_building_component_compositions,
                                                                                   file_building_variant_compositions,
                                                                                   buildings_data_enriched,
                                                                                   zones_data,
                                                                                   window_types_data)

            # write created data to csv files
            print("\n  > Saving created data to csv files - STARTED")
            for zid, bc_df in building_component_data_dict.items():
                bc_df.to_csv(dir_bc + '/bc_zid{0}.csv'.format(zid), index=False)
            for zid in zone_id_list:
                building_component_data_dict[zid] = pd.read_csv(dir_bc + '/bc_zid{0}.csv'.format(zid), skiprows=[1])
            print("\n  > Saving created data to csv files - FINISHED")

        print('\n--> INPUT PREPARATION PART 3 of 6 (creation of building_component_data) FINISHED')
        logging.debug('INPUT PREPARATION PART 3 of 6 (creation of building_component_data) FINISHED')
        if show_progress:
            app.update_progress(30, 'PART 3 of 6 FINISHED','INPUT PREPARATION' , False, scenario_id, c_type, j)
        ################################################################################################################
        # INPUT PREPARATION PART 4 of 6 (data form UEP)
        # --------------------------------------------------------------------------------------------------------------
        print('\n--> INPUT PREPARATION PART 4 of 6 (data form UEP) STARTED')
        logging.debug('INPUT PREPARATION PART 4 of 6 (data form UEP) STARTED')

        # check if final files already exist
        uep_check_list = []
        print('\n  > Checking if all UEP files have been saved in the input folders of the CleanMod Model')
        logging.debug('Checking if all UEP files have been saved in the input folders of the CleanMod Model')
        file_check_uep = [uep_mat_dst + '/active_load.mat',
                          uep_mat_dst + '/lighting.mat',
                          uep_mat_dst + '/occupancy.mat',
                          uep_mat_dst + '/DHW_per_building']

        for filename_uep in list(file_check_uep):
            file_uep = pathlib.Path(filename_uep)
            if file_uep.exists():
                print('    ' + filename_uep + "\n    exists")
                uep_check_list.append(True)
            else:
                print('    ' + filename_uep + "\n    doesn't exist")
                uep_check_list.append(False)

        # If any UEP file is missing
        if not (all(uep_check_list)):
            print("\n  > One or more UEP files are missing. Cleaning UEP's output source and destination folders")
            logging.debug("One or more UEP files are missing. Cleaning UEP's output source and destination folders")
            # Wipe existing uep folders
            if os.path.exists(uep_dir_input + '/SynHouseholds_' + UEP_region_name + '.csv'):
                os.remove(uep_dir_input + '/SynHouseholds_' + UEP_region_name + '.csv')
            if os.path.exists(uep_dir_input + '/SynCity_' + UEP_region_name + '.csv'):
                os.remove(uep_dir_input + '/SynCity_' + UEP_region_name + '.csv')
            clean_existing_folders([uep_mat_dst])
            clean_existing_folders([uep_mat_src +"Results"])
            # Delete former UEP '.mat' files in UEP
            for filename in os.listdir(uep_mat_src):
                if filename.endswith('.mat'):
                    os.remove(uep_mat_src + filename)
                    print('\n  > ' + filename + ' has been removed from UEP Source')
                    logging.debug(filename + ' has been removed from UEP Source')

            # Create synhouseholds
            interface_UEP.create_synhouseholds(zones_data, UEP_region_name, uep_dir_input)
            # Create syncity
            interface_UEP.calc_and_create_syncity(buildings_data_enriched, building_component_data_dict,
                                                  zones_data, UEP_region_name, uep_dir_input, file_dhw)
            print('\n  > syncity & synhousehold files for UEP have been created')
            logging.debug('syncity & synhousehold files for UEP have been created')

            # Run UEP
            call(['python', uep_runme, name_model])
            print('\n  > UEP has been called and is running')
            logging.debug('UEP has been called and is running')
            if show_progress:
                app.UEP_Progress(True)

            # Copy results of UEP-mat files to Model
            for filename in os.listdir(uep_mat_src):
                if filename.endswith('.mat'):
                    shutil.copy(uep_mat_src + filename, uep_mat_dst)
                    print('\n  > ' + filename + ' has been copied as Model Input')
                    logging.debug(filename + ' has been copied as Model Input')

            # Assign source and destination folders for UEP-DHW files
            uep_dhw_results = str(uep_mat_src + 'Results')
            uep_dhw_dst = (uep_mat_dst + "/DHW_per_building")

            # Get the latest DHW results from UEP
            uep_dhw_src = (max([os.path.join(uep_dhw_results, d) for d in os.listdir(uep_dhw_results)],
                               key=os.path.getmtime)) + "/0/time_series_per_building"

            # Copy results UEP-DHW files to Model Input
            copy_tree(uep_dhw_src, uep_dhw_dst)
            print('\n  > DHW_per_building has been copied as Model Input')
            logging.debug('DHW_per_building has been copied as Model Input')

        else:
            print('\n  > All UEP related files exist. Skipping UEP simulation')
            logging.debug('All UEP related files exist. Skipping UEP simulation')

        if show_progress:
            app.UEP_Progress(False)
        print('\n--> INPUT PREPARATION PART 4 of 6 (data form UEP) FINISHED')
        logging.debug('INPUT PREPARATION PART 4 of 6 (data form UEP) FINISHED')
        if show_progress:
            app.update_progress(40, 'PART 4 of 6 FINISHED', 'INPUT PREPARATION' ,False, scenario_id, c_type, j)
        ################################################################################################################
        # INPUT PREPARATION PART 5 of 6 (solar irradiation and window transmission)
        # --------------------------------------------------------------------------------------------------------------
        print('\n--> INPUT PREPARATION PART 5 of 6 (solar irradiation and window transmission) STARTED')
        logging.debug('INPUT PREPARATION PART 5 of 6 (solar irradiation and window transmission) STARTED')

        # check if final files do already exist and read in the ones necessary for other input preparation steps
        try:
            for zid in zone_id_list:
                pd.read_csv(dir_solar_radiation_reformat + '/Radiation_DirectRad_zid{0}.csv'.format(zid), index_col=0)
            angular_SHGC_timeseries_data = pd.read_csv(dir_window + '/angular_SHGC_timeseries.csv', index_col=0)
            pd.read_csv(dir_window + '/angular_SHGC.csv', index_col=0)
            factor_selfshadow_data = pd.read_csv(dir_window + '/factor_selfshadowing.csv', index_col=0)
            print('\n  > Files already exist and data has been loaded')
            logging.debug('  > Files already exist and data has been loaded')
        except:
            print('\n  > Files do no exist yet and will be created')
            logging.debug('  > Files do no exist yet and will be created')
            try:
                # Clean folders if they are not empty yet
                clean_existing_folders([dir_solar_radiation_reformat, dir_window])
            except:
                # Pass if the folders are already empty
                pass

            reformat_solar_radiation_write_new_files(weather_data, dir_solar_radiation_reformat, scenario_id, c_type, j)

            window_types_data = create_window_types_df(file_window_types)

            # process only used windows, not the whole library
            focus_building_vars = buildings_data_enriched['building_variant'].unique()
            building_variant_compositions_df = pd.read_csv(file_building_variant_compositions)
            focus_windows = []
            for focus_building_var in focus_building_vars:
                used_window = building_variant_compositions_df[building_variant_compositions_df['building_variant'] ==
                                                               focus_building_var]['window_type'].tolist()[0]
                if used_window not in focus_windows:
                    focus_windows.append(used_window)
            used_window_types_data = window_types_data[window_types_data['wid'].isin(focus_windows)]

            angular_SHGC_data = create_g_angular_dataframe(used_window_types_data)

            angular_SHGC_timeseries_data = create_g_angular_timeseries_dataframe(used_window_types_data,
                                                                                 angular_SHGC_data,
                                                                                 weather_data)

            factor_selfshadow_data = create_shadowing_dataframe(used_window_types_data, weather_data)

            # write created data to csv files
            angular_SHGC_data.to_csv(dir_window + '/angular_SHGC.csv')
            angular_SHGC_timeseries_data.to_csv(dir_window + '/angular_SHGC_timeseries.csv')
            factor_selfshadow_data.to_csv(dir_window + '/factor_selfshadowing.csv')

        print('\n--> INPUT PREPARATION PART 5 of 6 (solar irradiation and window transmission) FINISHED')
        logging.debug('INPUT PREPARATION PART 5 of 6 (solar irradiation and window transmission) FINISHED')
        if show_progress:
            app.update_progress(50, 'PART 5 of 6 FINISHED', 'INPUT PREPARATION', False, scenario_id, c_type, j)
        ################################################################################################################
        # INPUT PREPARATION, PART 6 of 6 (profile time series per zone)
        # --------------------------------------------------------------------------------------------------------------
        print('\n--> INPUT PREPARATION, PART 6 of 6 (profile time series per zone) STARTED')
        logging.debug('INPUT PREPARATION, PART 6 of 6 (profile time series per zone) STARTED')

        # check if final files do already exist
        try:
            pd.read_csv(dir_elec_hh_load + '/Elec_hh_load.csv')
            for zid in zone_id_list:
                pd.read_csv(dir_profile + '/profiles_zid{0}.csv'.format(zid))
            print('\n  > Files already exist and data has been loaded')
            logging.debug('  > Files already exist and data has been loaded')
        except:
            print('\n  > Files do no exist yet and will be created')
            logging.debug('  > Files do no exist yet and will be created')
            try:
                # Clean folders if they are not empty yet
                clean_existing_folders([dir_profile, dir_elec_hh_load])
            except:
                # Pass if the folders are already empty
                pass

            print('\n  > Reading UEP mat files started')
            logging.debug('Reading UEP mat files started')
            occupancy_data = create_df_from_matlab_output(zone_id_list, file_occupancy_mat,
                                                          'occupancy')
            gains_lighting_data = create_df_from_matlab_output(zone_id_list, file_gains_lighting_mat,
                                                               'lighting', unit_conversion='kW_in_W')
            active_load_data = create_df_from_matlab_output(zone_id_list, file_active_load_mat,
                                                            'active_load', unit_conversion='kW_in_W')
            print('\n  > Reading UEP mat files finished')
            logging.debug('Reading UEP mat files finished')

            # saving content of mat files in csv format
            print('\n  > Saving content of UEP mat files in csv format started')
            logging.debug('Saving content of UEP mat files in csv format started')
            occupancy_data.to_csv(file_occupancy_csv)
            gains_lighting_data.to_csv(file_gains_lighting_csv)
            active_load_data.to_csv(file_active_load_csv)
            active_load_data.to_csv(dir_elec_hh_load + '/Elec_hh_load.csv')
            print('\n  > Saving content of UEP mat files in csv format finished')
            logging.debug('Saving content of UEP mat files in csv format finished')

            # create time series for simulation
            print('\n  > Create time series for simulation started')
            logging.debug('Create time series for simulation started')
            if show_progress:
                app.update_progress(70, 'create time series for simulation started', 'INPUT PREPARATION' , False, scenario_id, c_type, j)
            timeseries_data_dict = create_timeseries_inputs_per_zone(zones_data, occupancy_data,
                                                                     building_component_data_dict,
                                                                     angular_SHGC_timeseries_data,
                                                                     factor_selfshadow_data,
                                                                     dir_solar_radiation_reformat,
                                                                     gains_lighting_data,
                                                                     active_load_data,
                                                                     weather_data, scenario_id, c_type, j)
            print('\n  > Create time series for simulation finished')
            logging.debug('Create time series for simulation finished')

            # write created data to csv files
            print('\n  > Creating Elec_hh_load.csv started')
            logging.debug('Creating Elec_hh_load.csv started')
            active_load_data.to_csv(dir_elec_hh_load + '/Elec_hh_load.csv')
            print('\n  > Creating Elec_hh_load.csv finished')
            logging.debug('Creating Elec_hh_load.csv finished')

            print('\n  > Creating time series profile files started')
            logging.debug('Creating time series profile files started')
            for zid, timeseries_df in timeseries_data_dict.items():
                timeseries_df.to_csv(dir_profile + '/profiles_zid{0}.csv'.format(zid))
            print('\n  > Creating time series profile files finished')
            logging.debug('Creating time series profile files finished')

        print('\n--> INPUT PREPARATION, PART 6 of 6 (profile time series per zone) FINISHED')
        logging.debug('INPUT PREPARATION, PART 6 of 6 (profile time series per zone) FINISHED')
        if show_progress:
            app.update_progress(99.9, 'PART 6 of 6 FINISHED', 'INPUT PREPARATION' , False, scenario_id, c_type, j)

    return building_id_list, zone_id_list


def create_g_angular_dataframe(wtypes_df):
    """Create a dataframe of angular g_values (separate ones for all specified irradiation components)
    according to VDI 6007-3 2015-06 based on the given list of window_types.

    Args:
        wtypes_df (dataframe): information on window_types.

    Returns:
        dataframe with multi-index irradiation_angle_deg(1st) and irrad_componenttype(2nd), window_types as columns.
    """

    angle_irr_lst = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]  # in degrees
    irr_comp_lst = ['dir', 'diff']  # zipped as list to enable later refinement of diffuse components

    # transmission constants
    A0 = 0.918
    A1 = 2.21 * 10 ** -4
    A2 = -2.75 * 10 ** -5
    A3 = -3.82 * 10 ** -7
    A4 = 5.83 * 10 ** -8
    A5 = -1.15 * 10 ** -9
    A6 = 4.74 * 10 ** -12

    def calc_g_angular(wtype_info):
        """Create a list of angular g_values (separate ones for all specified irradiation components)
        based on the given window_type."""
        win_id = wtype_info['wid']
        U = wtype_info['UVALUE']
        panels = wtype_info['Glass_panels']
        g_angular_lst = []

        for angle_irr in angle_irr_lst:
            # VDI(62)(64)
            tau_dir = ((((((A6 * angle_irr + A5)
                           * angle_irr + A4)
                          * angle_irr + A3)
                         * angle_irr + A2)
                        * angle_irr + A1)
                       * angle_irr + A0)
            if tau_dir < 0: tau_dir = 0
            # tau_diff = 0.84   # simplification of diff-radiation: value for uniformly overcast sky assumed
            tau_diff = 0.8  # simplification: value for uniformly overcast sky assumed, slightly corrected

            # VDI(66)(67)
            tau_i_dir = 0.907 ** (1 / math.sqrt(1 - (math.sin(math.radians(angle_irr)) / 1.515) ** 2))
            tau_i_diff = 0.903

            tau_lst = [tau_dir, tau_diff]
            tau_i_lst = [tau_i_dir, tau_i_diff]

            for i, value in enumerate(irr_comp_lst):
                tau = tau_lst[i]
                tau_i = tau_i_lst[i]

                tau1 = tau * tau_i

                # reflection VDI(69)
                rhoT1 = 1 - tau
                rho11 = rhoT1 / (2 - rhoT1)
                rho1 = rho11 + (((1 - rho11) * tau_i) ** 2 * rho11) / (1 - (rho11 * tau_i) ** 2)

                # absorption VDI(70)
                abs1 = 1 - tau1 - rho1

                # constants for calcs below
                xN2 = 1 - rho1 ** 2
                if xN2 == 0: xN2 = 10 ** (-20)
                tau2 = tau1 ** 2 / xN2

                if panels == 1:
                    # single-panel clear glass VDI(71)(73)
                    q_sek1 = abs1 * (7.7 / (7.7 + 25))
                    # kor_g = (tau1 + q_sek1)/g_dir0_1
                    g_angular = (tau1 + q_sek1)  # difference to original VDI procedure
                elif panels == 2 or panels == 9999999:
                    if panels == 9999999:
                        print('Warning: number of panels is undefined (9999999) for window ' + win_id +
                              ' . 2 panels will be used in calc_g_angular')
                    # two-panel clear glass VDI(74)
                    q21 = abs1 * (1 + (tau1 * rho1 / xN2)) * U / 25
                    q22 = abs1 * (tau1 / xN2) * (1 - U / 7.7)
                    q_sek2 = q21 + q22
                    # kor_g = (tau2+q_sek2)/g_dir0_2
                    g_angular = (tau2 + q_sek2)  # difference to original VDI procedure
                elif panels == 3:
                    # three-panel clear glass VDI(76)
                    xN3 = xN2 ** 2 - (tau1 * rho1) ** 2
                    if xN3 == 0: xN3 = 10 ** (-20)
                    tau3 = tau1 ** 3 / xN3
                    q31 = abs1 * (1 + (tau1 * rho1 * (xN2 + tau1 ** 2)) / xN3) * U / 25
                    q32 = abs1 * (tau1 * (xN2 + tau1 * rho1) / xN3) * 0.5 * (1 - U / 7.7 + U / 25)
                    q33 = abs1 * (tau1 ** 2 / xN3) * (1 - U / 7.7)
                    q_sek3 = q31 + q32 + q33
                    # kor_g = (tau3+q_sek3)/g_dir0_3
                    g_angular = (tau3 + q_sek3)  # difference to original VDI procedure
                else:
                    raise Exception('number of panels is not in [1,2,3]')

                g_angular_lst.append(g_angular)

        return g_angular_lst

    g_angular_lstlst = []

    print('\nCreating angular g values:')
    n = len(wtypes_df['wid'].tolist())
    count = 0
    for wtype, row in wtypes_df.iterrows():
        count += 1
        win_id = row['wid']
        print('Angular g values', count, 'of', n, 'for', win_id)
        g_angular_wtype = calc_g_angular(row)
        g_angular_lstlst.append(g_angular_wtype)

    # create dataframe from data, indices and column names
    g_angular_array = np.array(g_angular_lstlst).transpose()
    indexlists = [angle_irr_lst, irr_comp_lst]
    indices = pd.MultiIndex.from_product(indexlists, names=['angle_irr', 'irr_comp'])
    col = wtypes_df['wid'].values
    angular_SHGC_df = pd.DataFrame(g_angular_array, indices, col)

    return angular_SHGC_df


def create_g_angular_timeseries_dataframe(wtypes_df, angular_SHGC_df, weather_data):
    """Create timeseries of angular g_values based on the given weather file (sun position).

    Args:
        wtypes_df (dataframe): information on window_types.
        angular_SHGC_df (dataframe): dataframe with multi-index(irrad_angle_deg, irrad_comp) and wtypes as columns.
        weather_csv (str): file with solar azimuth and solar height.

    Returns:
        angular_SHGC_timeseries_df: dataframe with angular g_value timeseries per window_type x 4 orientations x (dir, diff).
    """

    wtypes_here_df = wtypes_df.set_index('wid')
    gam_f_deg = 90
    gam_f = np.radians(gam_f_deg)

    a_f_deg_dict = {'N': 0, 'E': 90, 'S': 180, 'W': 270,}
    a_f_rad_dict = {}
    for key, i in a_f_deg_dict.items():
        a_f_rad = np.radians(i)
        a_f_rad_dict[key] = a_f_rad

    # assigning variables, recalculating degrees in radians
    time = weather_data.HOY
    a_s_deg = weather_data.solar_azimuth
    a_s = np.radians(a_s_deg)

    # adjust degree classification from (90: parallel to horizont, 0: vertical to horizont) to \
    # VDI classification (0: parallel to horizont, 90: vertical to horizont
    gam_s_deg = [-(90 -i) for i in weather_data.solar_zenith]
    gam_s = np.radians(gam_s_deg)

    angular_SHGC_timeseries_df = pd.DataFrame(time, columns=['time'])
    angular_SHGC_timeseries_df.set_index('time', inplace=True)

    irr_comp_lst = ['dir', 'diff']  # zipped as list to enable later refinement of diffuse components

    def calc_g_angular_timeseries_dataframe(wtype_name):
        """Create timeseries of angular g_values based on given weather file (sun position)."""

        def find_corresponding_SHGC(irrad_type, wtype, SHGC_df):
            def find_by_xi_value(xi_value_deg):
                return SHGC_df.loc[(xi_value_deg, irrad_type), wtype]

            return find_by_xi_value

        # initialise new df for each irradiation_component (e.g. 'dir','diff') by copying angular_SHGC_timeseries_df
        angular_SHGC_wintype_tot = pd.DataFrame(time, columns=['time'])
        angular_SHGC_wintype_tot.set_index('time', inplace=True)

        for irr_comp in irr_comp_lst:
            # initialise new df for each window_type by copying angular_SHGC_timeseries_df
            angular_SHGC_wintype = pd.DataFrame(time, columns=['time'])
            angular_SHGC_wintype.set_index('time', inplace=True)

            for key, i in a_f_rad_dict.items():
                # i_deg = int(np.rad2deg(i))
                temp = pd.DataFrame(time, columns=['time'])
                # VDI(11)
                cos_xi = np.sin(gam_s) * np.cos(gam_f) + np.cos(gam_s) * np.sin(gam_f) * np.cos(abs(i - a_s))
                # prohibit negative values:
                cos_xi = np.where(cos_xi < 0, 0, cos_xi)
                xi = np.round(np.rad2deg(np.arccos(cos_xi)), -1).astype(int)
                temp[str(wtype_name) + '_' + key + '_' + irr_comp] = xi
                temp.set_index('time', inplace=True)
                angular_SHGC_wintype = angular_SHGC_wintype.join(temp, on='time')

            angular_SHGC_wintype = angular_SHGC_wintype.applymap(find_corresponding_SHGC(irr_comp, wtype_name,
                                                                                         angular_SHGC_df))

            angular_SHGC_wintype_tot = angular_SHGC_wintype_tot.join(angular_SHGC_wintype, on='time')

        return angular_SHGC_wintype_tot

    print('\nCreating angular g time series:')
    n = len(wtypes_df['wid'].tolist())
    count = 0
    for i, wtype in enumerate(wtypes_here_df.index.tolist()):
        count += 1
        win_id = wtype
        print('Angular g time series', count, 'of', n, 'for', win_id)
        angular_SHGC_new = calc_g_angular_timeseries_dataframe(wtype)
        angular_SHGC_timeseries_df = angular_SHGC_timeseries_df.join(angular_SHGC_new, on='time')

    return angular_SHGC_timeseries_df


def create_shadowing_dataframe(wtypes_df, weather_data):
    """
    Creates a dataframe of window shadowing factors for each timestamp in the weather data.

    Args:
        wtypes_df (pandas.DataFrame): A dataframe containing information on window types, with the following columns:
            - 'wid' (int): window ID.
            - 'Win_area' (float): window area (m^2).
            - 'Frame_fraction' (float): frame fraction, i.e., fraction of the total window area occupied by the frame.
            - 'Win_H' (float): window height (m).
            - 'Win_Soffit' (float): window soffit (m).
        weather_data (pandas.DataFrame): A dataframe containing solar azimuth and solar height for each timestamp, with the
            following columns:
            - 'HOY' (int): hour of the year.
            - 'solar_azimuth' (float): solar azimuth angle in degrees.
            - 'solar_zenith' (float): solar zenith angle in degrees.

    Returns:
        pandas.DataFrame: A dataframe with the following columns:
            - 'time' (int): hour of the year.
            - 'wid' (int): window ID.
            - 'shading_factor' (float): shading factor for the given window at the given timestamp.

    Notes:
        The function calculates the shading factors for each window type in the input `wtypes_df`, using the solar azimuth
        and solar height values in the input `weather_data`. The shading factor represents the percentage of the window area
        that is shaded at the given timestamp. The calculation is based on the VDI standard.
    """

    wtypes_here_df = wtypes_df.set_index('wid')
    gam_f_deg = 90
    gam_f = np.radians(gam_f_deg)

    a_f_deg_dict = {'N': 0, 'E': 90, 'S': 180, 'W': 270}
    a_f_rad_dict = {}
    for key, i in a_f_deg_dict.items():
        a_f_rad = np.radians(i)
        a_f_rad_dict[key] = a_f_rad

    # assigning variables, recalculating degrees in radians
    time = weather_data.HOY
    a_s_deg = weather_data.solar_azimuth
    a_s = np.radians(a_s_deg)

    # adjust degree classification from (90: parallel to horizont, 0: vertical to horizont) to \
    # VDI classification (0: parallel to horizont, 90: vertical to horizont
    gam_s_deg = [-(90 -i) for i in weather_data.solar_zenith]
    gam_s = np.radians(gam_s_deg)

    angular_shading_df = pd.DataFrame(list(zip(a_s, gam_s)), columns=['a_s', 'gam_s'])

    shading_factor = pd.DataFrame(time, columns=['time'])

    def calc_data_for_wtype(wtype_name, wtype_info):
        # TODO - Priority 1 (deadline: until 19.05.2023) - Responsible "MB" -  comment by = MB - check whether units are correctly applied
        A = wtype_info['Win_area']
        frame = wtype_info['Frame_fraction']
        H = wtype_info['Win_H']
        c1 = wtype_info['Win_Soffit']
        B = A / H
        A_w = A * frame
        b1 = (math.sqrt(A_w) - B) / 2

        for key, i in a_f_rad_dict.items():
            angular_shading_df['cos_xi'] = np.sin(gam_s) * np.cos(gam_f) + np.cos(gam_s) * np.sin(gam_f) \
                                           * np.cos(abs(i - a_s))
            cos_xi = np.round(angular_shading_df['cos_xi'], 3)

            # calculating shading effect
            # VDI suggests to take 10**20 when there is no direct sunlight on the surface
            angular_shading_df['e_h'] = np.where(cos_xi <= 0, 10 ** 20,
                                                 ((np.sin(i - a_s) * np.cos(gam_s)) / cos_xi))

            # correct the value, max can only be the full width of the window+frame
            angular_shading_df['e_h'] = np.where(angular_shading_df['e_h'] != 10 ** 20,
                                                 np.where(angular_shading_df['e_h'] > (B + 2 * b1), (B + 2 * b1),
                                                          angular_shading_df['e_h']), 10 ** 20)
            angular_shading_df['e_v'] = np.where(cos_xi <= 0, 10 ** 20, ((np.sin(gam_s) * np.sin(gam_f)
                                                                          - np.cos(gam_s) * np.cos(gam_f)
                                                                          * np.cos(i - a_s)) / cos_xi))

            # correct the value, max can only be the full height of the window+frame
            angular_shading_df['e_v'] = np.where(angular_shading_df['e_v'] != 10 ** 20,
                                                 np.where(angular_shading_df['e_v'] > (H + 2 * c1), H + 2 * c1,
                                                          angular_shading_df['e_h']), 10 ** 20)

            angular_shading_df['s_h'] = np.where(angular_shading_df['e_h'] >= 0,
                                                 angular_shading_df['e_h'] * c1 - b1,
                                                 -angular_shading_df['e_h'] * c1 - b1)
            angular_shading_df['s_h'] = np.where(angular_shading_df['s_h'] < 0, 0, angular_shading_df['s_h'])
            angular_shading_df['s_h'] = np.where(angular_shading_df['e_h'] * c1 + b1 < 0,
                                                 (angular_shading_df['s_h'] - (
                                                             angular_shading_df['e_h'] * c1) - b1),
                                                 angular_shading_df['s_h'])
            angular_shading_df['s_h'] = np.where((-angular_shading_df['e_h'] * c1 + b1) < 0,
                                                 (angular_shading_df['s_h'] + angular_shading_df['e_h'] * c1 - b1),
                                                 angular_shading_df['s_h'])

            angular_shading_df['s_v'] = angular_shading_df['e_v'] * c1 - b1
            angular_shading_df['s_v'] = np.where(angular_shading_df['s_v'] < 0, 0, angular_shading_df['s_v'])
            angular_shading_df['s_v'] = np.where(angular_shading_df['e_v'] * c1 + b1 < 0,
                                                 angular_shading_df['s_v'] - angular_shading_df['e_v'] * c1 - b1,
                                                 angular_shading_df['s_v'])

            angular_shading_df['fac_rest'] = (
                        1 - ((B * angular_shading_df['s_v'] + H * angular_shading_df['s_h'] - (
                        angular_shading_df['s_h'] * angular_shading_df['s_v'])) / A))
            angular_shading_df['fac_rest'] = np.where(angular_shading_df['fac_rest'] > 1, 0,
                                                      angular_shading_df['fac_rest'])
            angular_shading_df['fac_rest'] = np.where(angular_shading_df['fac_rest'] < 0, 0,
                                                      angular_shading_df['fac_rest'])

            # adding the shading factor to the df
            shading_factor_lst = angular_shading_df['fac_rest'].tolist()
            shading_factor[str(wtype_name) + '_' + key] = shading_factor_lst

        return shading_factor

    print('\nCreating shading factors:')
    n = len(wtypes_df['wid'].tolist())
    count = 0
    for wtype, row in wtypes_here_df.iterrows():
        count += 1
        win_id = wtype
        print('Shading factors', count, 'of', n, 'for', win_id)
        shading_factor = calc_data_for_wtype(wtype, row)

    return shading_factor


def create_building_data(file_buildings, file_building_variant_compositions, file_geometry_surfaces, bid_dict,
                         weather_data):
    """Create dataframe of buildings from given csv and enrich with default data necessary for UEP.

    Args:
        file_buildings (str): filepath of raw input csv of buildings.
        file_building_variant_compositions (str): filepath of input csv of building variant compositions.

    Returns:
        dataframe with enriched building information.
    """

    # create pd.df from file
    geometry_surfaces_df = pd.read_csv(file_geometry_surfaces, skiprows=[1])
    building_variant_compositions_df = pd.read_csv(file_building_variant_compositions)
    building_variant_compositions_df.set_index("building_variant", inplace=True)
    buildings_df = pd.read_csv(file_buildings)
    building_codes = buildings_df["building_code"].to_list()
    renovation_variants = []
    for i in buildings_df["renovation_variant"]:
        i = int(i)
        if i > 9:
            renovation_variants.append("0"+str(i))
        else:
            renovation_variants.append("00" + str(i))
    building_types = [i.split("_")[0] for i in building_codes]
    building_variants = [".".join([building_types[i], renovation_variants[i]]) for i, building_code in
                         enumerate(building_codes)]
    buildings_df["building_variant"] = building_variants
    buildings_df.set_index("building_code", inplace=True)

    # add heat_level_sh and heat_level_dhw
    buildings_df['heat_level_sh'] = 'Heat55'
    buildings_df['heat_level_dhw'] = 'Heat65'

    # calculate and add footprint area and nr of floors
    surf_ids = [i for i in geometry_surfaces_df["Surf-ID"]]
    surf_bid = [i.split("-")[0] for i in surf_ids]
    geometry_surfaces_df["surf_bid"] = surf_bid
    # footprint area
    for index in buildings_df.index:
        footprint_area_vals = geometry_surfaces_df[(geometry_surfaces_df["surf_bid"] == index) &
                                                   (geometry_surfaces_df["Surf-Boundary"] == "Ground")]["Surf-Area"]
        footprint_area = sum([i for i in footprint_area_vals])
        buildings_df.loc[index, "footprint_area"] = footprint_area
    # nr of floors
    for index in buildings_df.index:
        height_vals = geometry_surfaces_df[(geometry_surfaces_df["surf_bid"] == index) &
                                           (geometry_surfaces_df["Surf-Type"] == "Wall")]["Surf-Height"].unique()
        nr_of_floors = len([i for i in height_vals])
        buildings_df.loc[index, "floors"] = nr_of_floors

    # add building class
    for index in buildings_df.index:
        building_class = index.split(".")[2]
        buildings_df.loc[index, 'building_class'] = building_class

    # add columns with values depending on energy standard of the building
    # initialisation
    buildings_df['spec_heat_transfer_power'] = 0
    buildings_df['window_type'] = 0
    buildings_df['internal_mass_factor'] = 0
    buildings_df['thermal_bridge_factor'] = 0
    for index in buildings_df.index:
        buildings_df.loc[index, 'spec_heat_transfer_power'] = building_variant_compositions_df.loc[
            buildings_df.loc[index, 'building_variant'],
            'design_heating_load']
        buildings_df.loc[index, 'window_type'] = building_variant_compositions_df.loc[
            buildings_df.loc[index, 'building_variant'],
            'window_type']
        buildings_df.loc[index, 'internal_mass_factor'] = building_variant_compositions_df.loc[
            buildings_df.loc[index, 'building_variant'],
            'internal_mass_factor']
        buildings_df.loc[index, 'thermal_bridge_factor'] = building_variant_compositions_df.loc[
            buildings_df.loc[index, 'building_variant'],
            'thermal_bridge_factor']

    # Dictionaries for linking codes of CleanMod and UEP for Tabula building year classes
    year_class_dict = {'... 1859': 0,
                       '1860 ... 1918': 1,
                       '1919 ... 1948': 2,
                       '1949 ... 1957': 3,
                       '1958 ... 1968': 4,
                       '1969 ... 1978': 5,
                       '1979 ... 1983': 6,
                       '1984 ... 1994': 7,
                       '1995 ... 2001': 8,
                       '2002 ... 2009': 9,
                       '2010 ... 2015': 9,
                       '2016 ...': 9}

    size_class_dict = {"SFH": 0,
                       "TH": 1,
                       "MFH": 2,
                       "AB": 3}

    building_use_dict = {'Commercial': 0,
                         'Industrial': 1,
                         'Public': 2,
                         'Residential': 3}

    # Read file containing the relation of codes given for the Tabula building classes
    building_classes_df = pd.read_csv(file_building_classes)

    for i, index in enumerate(buildings_df.index):

        size_class_tabula = buildings_df.loc[index, "building_class"]  # index.split(".")[2]  # e.g. EFH
        size_class_uep = size_class_dict[size_class_tabula]

        tabula_code = index.split("Gen")[0]+"Gen"
        year_class_tabula = [i for i in building_classes_df[
                                        building_classes_df["TABULA_code"] == tabula_code]["Construction_period"]][0]
        year_class_uep = year_class_dict[year_class_tabula]

        buildings_df.loc[index, "use"] = 3  # 3: means residential buildings, for further explanation see UEP
        buildings_df.loc[index, "dist_to_heat_source"] = 0  # central heat production not calculated with UEP anyway
        buildings_df.loc[index, "free_walls"] = 4  # 4: means free-standing building # No need to adapt to real case situation in case of not for free_walls because we don't calculate space heating in UEP
        buildings_df.loc[index, 'lat'] = weather_data.location_latitude
        buildings_df.loc[index, 'lon'] = weather_data.location_longitude
        buildings_df.loc[index, 'year_class'] = year_class_uep  # e.g. 4 for 1958-1968. See dict above, and info in UEP
        buildings_df.loc[index, 'size_class'] = size_class_uep  # e.g. 2 for MFH. See dict above, and info in UEP
        buildings_df.loc[index, 'bid'] = bid_dict[index]  # an unique building number used as building id in UEP

    return buildings_df


def create_window_types_df(file_window_types):
    """Create dataframe of window_types from given csv."""

    window_types_df = pd.read_csv(file_window_types, skiprows=[1])

    # calc total UVALUE from glass and frame values and add the total value to the df
    window_types_df['UVALUE'] = ((1-window_types_df['Frame_fraction']) * window_types_df['Glass_UVALUE']
                                + window_types_df['Frame_fraction'] * window_types_df['Frame_UVALUE'])

    return window_types_df


def enrich_buildings_data(buildings_df, zones_df, columns_new_list, ev_share):
    """Sum values of corresponding zones(dwellings) and add in new column.

    This has to be a separate function from create_building_data because the zones need to be created
    and enriched inbetween in order to build the sum afterwards.

    Args:
        buildings_df (dataframe): buildings data.
        zones_df (dataframe): zones data.
        columns_new_list (list): list of column names to be added.

    Returns:
        enriched dataframe of buildings.
    """

    buildings_df_new = buildings_df

    for column in columns_new_list:
        buildings_df_new[column] = 0

    if 'dwellings' in columns_new_list:
        for bid_gh in buildings_df_new.index:
            buildings_df_new.loc[bid_gh, 'dwellings'] = len(zones_df[zones_df['building_code'] == bid_gh])

    if 'occupants' in columns_new_list:
        for bid_gh in buildings_df_new.index:
            buildings_df_new.loc[bid_gh, 'occupants'] = sum(
                zones_df[zones_df['building_code'] == bid_gh]['number_occupants'])

    if 'storey_area' in columns_new_list:
        for bid_gh in buildings_df_new.index:
            buildings_df_new.loc[bid_gh, 'storey_area'] = sum(zones_df[
                                                              zones_df['building_code'] == bid_gh]['Zone-Area'])

    if 'ev_num' in columns_new_list:
        for bid_gh in buildings_df_new.index:
            buildings_df_new.loc[bid_gh, 'ev_num'] = math.ceil(buildings_df_new.loc[bid_gh, 'dwellings'] * ev_share/100)

    return buildings_df_new


def create_zone_data(geometry_zones_csv, household_sizes_csv, buildings_df, bid_dict):
    """Enrich zone data with household size, building (building_code), heated area, energy standard, window_type.

    Args:
        geometry_zones_csv (str): name of input csv file from grasshopper script with geometries.
        household_sizes_csv (str): name of input csv file with household size distribution.
        buildings_df (dataframe): enriched buildings data.

    Returns:
        zone_data as dataframe.
    """
    # create pd.df from files
    zone_data = pd.read_csv(geometry_zones_csv, skiprows=[1])
    zone_data.set_index('Zone-ID', inplace=True)
    # household size
    # distribution of household sizes according to building type multifamily building taken from Project\
    # UrbanReNet(Hegger et al. 2012, S.119, Abb.100) based on data from Statistisches Bundesamt & \
    # their own calcs. Also used in (LowEx-Bestand Analyse Abschlussbericht zu AP 1.1 2018, S.28)

    def calculate_number_of_occupants_for_n_dwellings(n, file_household_distr):
        """
        Function to calculate the number of occupants for n dwellings based on the distribution of household sizes,
        independent of floor area.

        Args:
            n (int): The number of households.
            file_household_distr (str): The name of the input csv file with the household size distribution.

        Returns:
            list: A list of household sizes (occupants).
        """

        household_distr = pd.read_csv(file_household_distr)
        # Household sizes: numer of occupants per zon
        # x-values
        household_size = range(1, 6)
        # CFD (check family distribution). See library file with statistics for household sizes (nr of occupants)
        cfd = np.cumsum(household_distr['percentage'].values)

        occupants_list = []

        # Fix seed for comparability
        np.random.seed(1)
        # calculate number of occupants
        for dwelling in range(n):
            # get random_number and obtain x-value from cfd
            rand_num = np.random.uniform(0, 1, 1)
            hs_index = np.argmax(rand_num <= cfd)
            # get household size (number of occupants)
            occupants_in_dwelling = household_size[hs_index]
            occupants_list.append(occupants_in_dwelling)

        return occupants_list

    zone_data['number_occupants'] = calculate_number_of_occupants_for_n_dwellings(len(zone_data.index),
                                                                                  household_sizes_csv)
    # superordinate building
    zone_data['building_code'] = [i.split('-')[0] for i in zone_data.index]

    # heated area
    zone_data['heated_area'] = zone_data['Zone-Area']  # TODO - Priority 4 (deadline: 01.09.2023) - Responsible "MB" -  comment by = MB - ensure that this approach serves our simu goals! Option: Account for different heated areas as surface area. Check zone.py

    # energy standard
    zone_data['renovation_variant'] = 0
    for index in zone_data.index:
        zone_data.loc[index, 'renovation_variant'] = buildings_df.loc[zone_data.loc[index, 'building_code'],
                                                                      'renovation_variant']

    # window_type
    zone_data['window_type'] = 0
    for index in zone_data.index:
        zone_data.loc[index, 'window_type'] = buildings_df.loc[zone_data.loc[index, 'building_code'], 'window_type']

    zone_data['bid'] = [bid_dict[zone_data.loc[i, 'building_code']] for i in zone_data.index]
    zone_data['household_number'] = [int(i.split('_')[-1]) for i in zone_data.index]
    zone_data['hid'] = 0
    for index in zone_data.index:
        zone_data.loc[index, 'hid'] = '{0}_{1}'.format(zone_data.loc[index, 'bid'],
                                                       zone_data.loc[index, 'household_number'])

    building_variant_compositions_df = pd.read_csv(file_building_variant_compositions)
    zone_data['infiltration_rate'] = 0
    zone_data['ventilation_rate'] = 0
    for index in zone_data.index:
        building_variant_id = buildings_df.loc[zone_data.loc[index, 'building_code'], 'building_variant']
        acr_inf = building_variant_compositions_df[building_variant_compositions_df['building_variant'] ==
                                                   building_variant_id]['infiltration_rate'].tolist()[0]
        zone_data.loc[index, 'infiltration_rate'] = acr_inf
        acr_vent = building_variant_compositions_df[building_variant_compositions_df['building_variant'] ==
                                                   building_variant_id]['ventilation_rate'].tolist()[0]
        zone_data.loc[index, 'ventilation_rate'] = acr_vent

    return zone_data


def create_building_component_data_per_zone(file_geometry_surfaces, file_building_component_compositions,
                                            file_building_variant_compositions, buildings_df, zone_df, window_types_df):
    """
    This function creates zonewise building component data by combining geometry with material composition. The material composition selection is based on the energy standard and size-class of the building.

    Args:
        file_geometry_surfaces (str): Name of the input CSV from the Grasshopper script with surfaces.
        file_building_component_compositions (str): Name of the input CSV with building component composition typology.
        file_building_variant_compositions (str): Name of the input CSV of building variant compositions.
        buildings_df (dataframe): Enriched buildings data.
        zone_df (dataframe): Enriched zones data.
        window_types_df (dataframe): Window types.

    Returns:
        dict: A dictionary with zoneids as keys and dataframes with zone's components as values.
    """
    print("\n  > Creating building component data per zone - STARTED")

    building_variant_compositions_df = pd.read_csv(file_building_variant_compositions, skiprows=[1])
    building_variant_compositions_df.set_index("building_variant", inplace=True)

    # create pd.df from files
    geometry_data = pd.read_csv(file_geometry_surfaces, skiprows=[1])

    # set zid as new index
    index_geo = pd.Index([i.split('-')[0] + '-' + i.split('-')[1] for i in geometry_data['Surf-ID']])
    geometry_data.set_index(index_geo, inplace=True)

    building_component_compositions_df = pd.read_csv(file_building_component_compositions, skiprows=[1])
    # MB 28.04.2023: adapting format of read df
    # More info: building_component_composition has new format, easier for user. The next lines prepare the df so it has
    # the former structure, which is the one that is predefined by VDI for the RC models
    # First, re-arrange order of columns
    col_order = ['COMPONENT', 'LAYER_NR', 'LAYERS', 'THICKNESS', 'CONDUCTIVITY', 'DENSITY', 'CAPACITY', 'HBACK',
                 'HFRONT', 'UVALUE', 'LW-BACK', 'LW-FRONT', 'SW-BACK', 'SW-FRONT', 'note']
    former_building_component_compositions_df = building_component_compositions_df.loc[:, col_order]
    # Then, change positions of BACK values to the final layer
    col_component_ori = former_building_component_compositions_df['COMPONENT'].tolist()
    former_building_component_compositions_df.loc[:, 'COMPONENT'].fillna(method='ffill', inplace=True)
    unique_bc_names = [name for name in former_building_component_compositions_df.COMPONENT.unique()]
    for bc_name in unique_bc_names:
        bc_data = former_building_component_compositions_df[former_building_component_compositions_df['COMPONENT'] ==
                                                            bc_name]
        bc_indexes = bc_data.index.values.tolist()
        nr_of_layers = len(bc_data['LAYER_NR'].tolist())
        if nr_of_layers > 1:
            vals_h_back = bc_data['HBACK'].tolist()[::-1]  # [::-1]: reversing order using slicing operator
            vals_lw_back = bc_data['LW-BACK'].tolist()[::-1]
            vals_sw_back = bc_data['SW-BACK'].tolist()[::-1]
            former_building_component_compositions_df.loc[bc_indexes, 'HBACK'] = vals_h_back
            former_building_component_compositions_df.loc[bc_indexes, 'LW-BACK'] = vals_lw_back
            former_building_component_compositions_df.loc[bc_indexes, 'SW-BACK'] = vals_sw_back
    former_building_component_compositions_df['COMPONENT'] = col_component_ori
    # Then, set the df with the former structure as the df to be used in the rest of the code
    building_component_compositions_df = former_building_component_compositions_df
    # Finally, continue
    building_component_compositions_df.loc[:, 'COMPONENT'].fillna(method='ffill', inplace=True)

    map_boundary_dict = {'Surface': 'I', 'Ground': 'G', 'Outdoors': 'E'}
    map_type_dict = {'Wall': 'W', 'Floor': 'F', 'RoofCeiling': 'R'}

    building_components_df_with_units_dict = dict()

    for zid in zone_df.index:
        zid_building_components_df = pd.DataFrame()
        zid_surfaces = geometry_data.loc[zid]
        unique_name_dict = {}
        window_counter = 1

        for index, row in zid_surfaces.iterrows():
            bid = index.split('-')[0]
            building_variant = buildings_df.loc[bid, "building_variant"]
            bc_type = map_boundary_dict[row['Surf-Boundary']]
            bc_type += map_type_dict[row['Surf-Type']]
            bc_slope = float(row['Surf-Slope'])  # MB 08.03.2023: for vertical windows in roof surfaces (see also below)
            bc_area = row['Surf-Area']  # MB 08.03.2023: added to apply subtraction of window area

            # 'internal RoofCeiling' misleading and therefore changed to 'IC' (=internal ceiling):
            if bc_type == 'IR':
                bc_type = 'IC'

            # get identifier by extracting information from file_building_variant_compositions (e.g. EW_type)
            identifier = building_variant_compositions_df.loc[building_variant, '{0}_type'.format(bc_type)]

            # ensure that each building component associated with one surface has a unique name. This is important just/
            # in case one specific building component is assigned to two different surfaces (e.g. ..._EW_1 and ...EW_2 /
            # or ...W_1 and ...W_2)
            if identifier not in unique_name_dict.keys():
                unique_name_dict[identifier] = 1
            else:
                unique_name_dict[identifier] += 1

            # add new window if component is an external wall or external roof
            bc_win = False
            if bc_type == 'EW' or bc_type == 'ER':
                if bc_type == 'ER' and bc_slope == 0.0:  # Horizontal roof surface
                    pass
                else:
                    bc_win = True
                    info_window_type = window_types_df.loc[window_types_df['wid'] ==
                                                           buildings_df.loc[bid, 'window_type']].copy()
                    info_window_type.drop(['Glass_UVALUE', 'Glass_panels', 'Frame_type', 'Frame_UVALUE', 'Win_area',
                                          'Win_H'], axis=1, inplace=True)
                    win_id = info_window_type['wid'].to_list()[0]
                    win_name = win_id + '_' + f'W_{window_counter}'
                    win_area = bc_area * buildings_df.loc[bid, 'WWR_{0}'.format(row['Surf-Ori'].split('_')[0])] / 100
                    win_slope = 90 if bc_type == 'ER' else bc_slope
                    new_win = pd.DataFrame({'LAYER_NR': 1,
                                            'COMPONENT': win_name,
                                            'AREA': win_area,
                                            'DIRECTION': row['Surf-Ori'].split('_')[0],
                                            'ORIENTATION': row['Surf-Ori'].split('_')[1],
                                            'INCLINATION': win_slope},
                                           index=[0])
                    new_win = new_win.join(info_window_type.set_index('LAYER_NR'), on='LAYER_NR')
                    window_counter += 1
                    zid_building_components_df = zid_building_components_df.append(new_win)

            # add layers of the building component
            new_rows = building_component_compositions_df.loc[building_component_compositions_df['COMPONENT'] ==
                                                              identifier].copy()

            new_rows.loc[:, 'COMPONENT'] += '_' + bc_type + '_{0}'.format(unique_name_dict[identifier])
            new_area = bc_area if bc_win == False else bc_area - win_area
            info_gh = pd.DataFrame({'LAYER_NR': 1,
                                    'AREA': new_area,
                                    'DIRECTION': row['Surf-Ori'].split('_')[0],
                                    'ORIENTATION': row['Surf-Ori'].split('_')[1],
                                    'INCLINATION': row['Surf-Slope'], 'Surf_ID': row['Surf-ID']},
                                   index=[0])
            new_rows = new_rows.join(info_gh.set_index('LAYER_NR'), on='LAYER_NR')

            zid_building_components_df = zid_building_components_df.append(new_rows)

        unit_row = {'COMPONENT': '-',
                    'AREA': 'm2',
                    'DIRECTION': '-',
                    'ORIENTATION': 'DEG',
                    'INCLINATION': 'DEG',
                    'LAYER_NR': '-',
                    'LAYERS': '-',
                    'THICKNESS': 'm',
                    'CONDUCTIVITY': 'W/mK',
                    'DENSITY': 'kg/m3',
                    'CAPACITY': 'kJ/kgK',
                    'HBACK': 'W/m2K',
                    'HFRONT': 'W/m2K',
                    'UVALUE': 'W/m2K',
                    'Glass_SHGC': '%',
                    'LW-BACK': '%',
                    'LW-FRONT': '%',
                    'SW-BACK': '%',
                    'SW-FRONT': '%',
                    'wid': '-',
                    # 'panels': '-',
                    # 'Win_area': 'm2',
                    # 'H': 'm',
                    # 'c1': '-',
                    'Frame_fraction': '-'
                    }
        df_unit_row = pd.DataFrame(unit_row, index=['units'])
        zid_building_components_df_with_units = df_unit_row.append(zid_building_components_df)

        building_components_df_with_units_dict[zid] = zid_building_components_df_with_units

    print("\n  > Creating building component data per zone - FINISHED")

    return building_components_df_with_units_dict


def create_df_from_matlab_output(zid_list, profile_mat, name_profile, unit_conversion=False):
    """
    Converts Matlab output of building profiles to a pandas DataFrame.

    Args:
        zid_list (list): List of zone IDs.
        profile_mat (str): Path of the Matlab output file.
        name_profile (str): Name of the profile to extract.
        unit_conversion (bool, optional): Whether to convert units from kW to W. Defaults to False.

    Returns:
        pd.DataFrame: A pandas DataFrame containing the building profiles for each zone.
    """

    #profile_ndarray = loadmat(profile_mat)[name_profile][0:14, 0:8760]  # output from matlab already in matching timeresolution
    profile_ndarray = loadmat(profile_mat)[name_profile]
    profile_index = pd.Index(data=[i for i in np.arange(0.0, 8760.0, time_resolution_h)])
    profile_df_raw = pd.DataFrame(data=profile_ndarray.transpose(), index=profile_index, columns=zid_list)
    if unit_conversion == 'kW_in_W':
        profile_df_raw *= 1000
    profile_df = profile_df_raw.astype(np.int64)

    return profile_df


''' OLD FUNCTION - LEAVE THIS UNTIL FINAL CHECKS OF EFFECT OF DIFFERENT TIME RESOLUTION ARE DONE.
def create_df_from_matlab_output(zid_list, profile_mat, name_profile, resolution_minutes=1, unit_conversion=False):
    """Prepare profiles as dataframe from matlab output."""

    profile_ndarray = loadmat(profile_mat)[name_profile]  # in 1min-resolution
    if resolution_minutes != 1:
        profile_ndarray_short = profile_ndarray[:, 0:int(525600/resolution_minutes)]
        profile_ndarray = np.repeat(profile_ndarray_short, resolution_minutes, axis=1)
    profile_index = pd.RangeIndex(stop=525600)
    profile_df_raw = pd.DataFrame(data=profile_ndarray.transpose(), index=profile_index,
                                    columns=zid_list)
    if unit_conversion == 'kW_in_W':
        profile_df_raw *= 1000
    profile_df = profile_df_raw.groupby(by=lambda t: t // (time_resolution_h * 60)).mean().round().astype(
        np.int64)
    index_new = pd.Index(data=[i for i in np.arange(0.0, 8760.0, time_resolution_h)])
    profile_df.set_index(index_new, inplace=True)

    return profile_df'''


''' OLD APPROACH. KEEP UNTIL ZY INTEGRATES HIS NEW CODE FOR EV
def create_df_from_ev_load_program_output(zid_list, file_load, resolution_minutes=15, unit_conversion='kW_in_W'):
    """Prepare profiles as dataframe from csv from ev load program."""

    # read in the timeseries data from the csv as numpy array but use only as many rows as households needed (skipping \
    # as many rows at the bottom as are exceeding the needed number)
    profile_ndarray = np.genfromtxt(file_load,delimiter=';',skip_footer=2500-len(zid_list))

    profile_index = pd.RangeIndex(stop=int(8760*(60/resolution_minutes)))
    profile_df_raw = pd.DataFrame(data=profile_ndarray.transpose(), index=profile_index, columns=zid_list)
    if unit_conversion == 'kW_in_W':
        profile_df_raw *= 1000
    # output from EV load profile creator in 15min! --> create mean of 4 values if simulation is with hour-timesteps
    profile_df = profile_df_raw.groupby(by=lambda t: t // (time_resolution_h*60/resolution_minutes)).mean().round().astype(
        np.int64)
    index_new = pd.Index(data=[i for i in np.arange(0.0, 8760.0, time_resolution_h)])
    profile_df.set_index(index_new, inplace=True)

    return profile_df
'''

def create_timeseries_inputs_per_zone(zones_df, occupancy_df, bc_data_dict,
                                      g_angular_timeseries_df, factor_selfshadow_df, folder_solar_radiation,
                                      gains_lighting_df, active_load_df, weather_data , scenario_id, c_type, j):
    """
    Creates timeseries of disturbances consisting of internal gains and solar gains.

    Args:
        zones_df (pandas.DataFrame): DataFrame containing information about zones.
        occupancy_df (pandas.DataFrame): DataFrame containing information about occupancy.
        bc_data_dict (dict): Dictionary containing boundary condition data.
        g_angular_timeseries_df (pandas.DataFrame): DataFrame containing solar radiation data.
        factor_selfshadow_df (pandas.DataFrame): DataFrame containing self-shadow factor data.
        folder_solar_radiation (str): Path to the folder containing solar radiation data.
        gains_lighting_df (pandas.DataFrame): DataFrame containing lighting gains data.
        active_load_df (pandas.DataFrame): DataFrame containing active load data.
        weather_data (pandas.DataFrame): DataFrame containing weather data.
        scenario_id (int): ID of the simulation scenario.
        c_type (str): Type of the simulation scenario.
        j (int): Number of the simulation scenario.

    Returns:
        dict: Dictionary containing timeseries of disturbances for each zone.

    Raises:
        None.
    """

    zid_list = zones_df.index.tolist()
    timeseries_total_df_dict = dict()

    # create units row
    units = []
    units_dict = {'TIME': 'HOURS',
                  'T_amb': '°C',
                  'T_sky': '°C',
                  'T_supply_vent': '°C',
                  'acr_inf': '1/h',
                  'acr_vent': '1/h',
                  'occupants_present': '-',
                  'occupied': '-',
                  'sleep_time': '-',
                  'Q_int_pers': 'W/m2',
                  'Q_int_pers_rad': 'W/m2',
                  'Q_int_pers_conv': 'W/m2',
                  'Q_int_lighting': 'W/m2',
                  'Q_int_lighting_rad': 'W/m2',
                  'Q_int_lighting_conv': 'W/m2',
                  'Q_int_devices': 'W/m2',
                  'Q_int_devices_rad': 'W/m2',
                  'Q_int_devices_conv': 'W/m2',
                  'Q_int_tot': 'W/m2',
                  'Q_rad_tot': 'W/m2',
                  'Q_conv_tot': 'W/m2'}

    # TODO - Priority 1 (deadline: 19.05.2023) - Responsible "MB" -  comment by = MB 10.03.2023: Check wheter we need this and if not, take it out. Check also whether we use the window module to calculate the radiation inside.
    orientation_dict = {'S': 0, 'W': 90, 'N': 180, 'E': 270}  # use trnsys classification of degrees here
    for direction, orient in orientation_dict.items():
        units_dict['IT_{0}_{1}_90'.format(direction, orient)] = 'W/m2'
        units_dict['IB_{0}_{1}_90'.format(direction, orient)] = 'W/m2'
        units_dict['IT_INSIDE_{0}_{1}_90'.format(direction, orient)] = 'W/m2'
    units_dict['RADtot_H'] = 'W/m2'

    units.insert(0, units_dict)
    if show_progress:
        app.update_progress(0.25, 'create time series for simulation', 'Time series', False, scenario_id, c_type, j)
    total_zone_list = len(zid_list)
    zone_id_count = 0
    for zoneid in zid_list:
        timeseries_solar_gains_only_df = recombine_radiation_outside_and_calc_inside_one_zone(zoneid, bc_data_dict,
                                                                                              g_angular_timeseries_df,
                                                                                              factor_selfshadow_df,
                                                                                              folder_solar_radiation)
        timeseries_int_gains_only_df = calc_internal_gains_one_zone(zoneid, zones_df, occupancy_df, gains_lighting_df,
                                                                    active_load_df, weather_data)
        timeseries_total_df = timeseries_int_gains_only_df.join(timeseries_solar_gains_only_df, on='TIME')

        # add unit-row (in order to match uniform input format)
        timeseries_total_df_with_units = pd.concat([pd.DataFrame(units), timeseries_total_df])

        timeseries_total_df_dict[zoneid] = timeseries_total_df_with_units

        zone_id_count += 1
        print('processing zone', zone_id_count, 'of', total_zone_list,  ' zoneid {0}'.format(zoneid))
        if show_progress:
            app.update_progress(0.25, 'create time series for simulation', 'Time series', True, scenario_id, c_type, j)

    return timeseries_total_df_dict


def calc_internal_gains_one_zone(zid, zones_df, occupancy_df, gains_lighting_df, active_load_df, weather_data):
    """
    Function to calculate internal gains resulting from persons (based on occupancy), lighting and devices.

    Args:
        zid (int): Zone ID.
        zones_df (pandas.DataFrame): DataFrame containing the zones' data.
        occupancy_df (pandas.DataFrame): DataFrame containing the occupancy data.
        gains_lighting_df (pandas.DataFrame): DataFrame containing the lighting gains data.
        active_load_df (pandas.DataFrame): DataFrame containing the active load data.
        weather_data (pandas.Series): Pandas Series containing weather data.

    Returns:
        pandas.DataFrame: DataFrame containing the calculated internal gains data.

    Assumptions on internal gains:

        - persons: 80 W/person (0-6am), randint(80,170) W/person (6am-12pm), ratio radiative/convective: 50/50,
          limits taken from EN 13779-Aktivitätsgrad 1: zurückgelehnt; Aktivitätsgrad 4: stehend, leichte Tätigkeit.
          Simplification: all occupants of the dwelling with same random value.

        - For lighting and rest of devices the dissipated heat is assumed to equal the electrical power:

        - lighting: electrical profile taken from REM, ratio radiative/convective: 90/10.

        - rest of devices: total active load profile taken from REM and lighting+cooking subtracted,
          ratio radiative/convective: 5/95.

        - Ratio of heat transport mechanisms in general: radiative: 0.6; convective 0.4 (relative to sum of internal gains).

        Former approach (now commented):

        - Lighting: 4.5 W/m2 * area * randint(0.05,0.2).

        - Devices: 3 W/m2 * area * randint(0.5,0.8).

    The returned DataFrame contains the following columns:

        - occupied: binary occupancy column.
        - sleeptime: binary column that takes value 1 from 0am-6am.
        - Q_int_pers: internal gains resulting from persons.
        - Q_int_lighting_rad: radiative component of lighting gains.
        - Q_int_lighting_conv: convective component of lighting gains.
        - Q_int_devices_rad: radiative component of devices gains.
        - Q_int_devices_conv: convective component of devices gains.
        - Q_rad_tot: total radiative component of internal gains.
        - Q_conv_tot: total convective component of internal gains.
        - Q_int_tot: total internal gains.
        - Tamb: outdoor temperature.
        - Tsky: outdoor sky temperature.
        - Tsup_mv_A1: supply air temperature.
        - Acr_Inf_A1: ventilation rate
    """

    area_zone = zones_df.loc[zid, 'Zone-Area']

    # assumptions on internal gains:
    # persons: 80 W/person (0-6am), randint(80,170) W/person (6am-12pm), ratio radiative/convective: 50/50,  \
    # limits taken from EN 13779-Aktivitätsgrad 1: zurückgelehnt; Aktivitätsgrad 4: stehend, leichte Tätigkeit
    # !simplification: all occupants of the dwelling with same random value
    # for lighting and rest of devices the dissipated heat is assumed to equal the electrical power
    # lighting: electrical profile taken from REM, ratio radiative/convective: 90/10
    # rest of devices: total active load profile taken from REM and lighting+cooking subtracted, \
    #                  ratio radiative/convective: 5/95

    # # # # former approach:
    #                       lighting: 4.5 W/m2 * area * randint(0.05,0.2)
    #                       devices: 3 W/m2 * area * randint(0.5,0.8)
    #                       ratio of heat transport mechanisms in general: radiative: 0.6; convective 0.4 \
    #                       (relative to sum of internal gains)

    # create new dataframe for zid from corresponding column of occupancy_df
    occupants_present_df = occupancy_df[zid].to_frame(name='occupants_present')

    # starting timeseries_df
    timeseries_df = pd.DataFrame()

    # initialise new columns:
    column_names_new = ['TIME', 'T_amb', 'T_sky', 'T_supply_vent',
                        'acr_inf', 'acr_vent',
                        'occupants_present', 'occupied', 'sleep_time', 'Q_int_pers', 'Q_int_pers_rad', 'Q_int_pers_conv',
                        'Q_int_lighting', 'Q_int_lighting_rad', 'Q_int_lighting_conv',
                        'Q_int_devices', 'Q_int_devices_rad', 'Q_int_devices_conv',
                        'Q_int_tot', 'Q_rad_tot', 'Q_conv_tot']

    for col in column_names_new:
        timeseries_df[col] = np.zeros(len(occupants_present_df.index))

    # adding values of occupancy from occupancy_df from UEP
    timeseries_df['occupants_present'] = occupants_present_df['occupants_present']

    # create helper column with binary occupancy
    timeseries_df.loc[timeseries_df['occupants_present'] > 0, 'occupied'] = 1

    # create helper column with sleep time (takes value 1 from 0am-6am)
    timeseries_df.loc[timeseries_df.index % 24 < 6, 'sleep_time'] = 1

    # create internal gains columns
    #Persons
    timeseries_df.loc[:, 'Q_int_pers'] = 80  # W -> but later changed to W/m2
    num_awake = timeseries_df['sleep_time'].value_counts()[0]

    # Fix seed for comparability
    random.seed(1)
    Q_int_pers_list = [randint(80, 170) for i in range(num_awake)]

    timeseries_df.loc[timeseries_df['sleep_time'] == 0, 'Q_int_pers'] = Q_int_pers_list  # W -> but later changed to W/m2
    timeseries_df.loc[:, 'Q_int_pers'] *= timeseries_df['occupants_present']
    timeseries_df.loc[:, 'Q_int_pers'] /= area_zone  # from W to W/m2
    ratio_radiative_pers = 0.5
    timeseries_df['Q_int_pers_rad'] = ratio_radiative_pers * timeseries_df['Q_int_pers']
    timeseries_df['Q_int_pers_conv'] = (1 - ratio_radiative_pers) * timeseries_df['Q_int_pers']


    # lighting
    ratio_radiative_lighting = 0.9
    timeseries_df['Q_int_lighting'] = gains_lighting_df[zid] / area_zone  # in W/m2
    timeseries_df['Q_int_lighting_rad'] = ratio_radiative_lighting * timeseries_df['Q_int_lighting']
    timeseries_df['Q_int_lighting_conv'] = (1 - ratio_radiative_lighting) * timeseries_df['Q_int_lighting']

    # devices
    ratio_radiative_devices = 0.05
    rest_load = active_load_df[zid] - gains_lighting_df[zid]
    timeseries_df['Q_int_devices'] = rest_load / area_zone  # in W/m2
    timeseries_df['Q_int_devices_rad'] = ratio_radiative_devices * timeseries_df['Q_int_devices']
    timeseries_df['Q_int_devices_conv'] = (1 - ratio_radiative_devices) * timeseries_df['Q_int_devices']

    # summed internal gains
    timeseries_df.loc[:, 'Q_rad_tot'] = (timeseries_df['Q_int_pers_rad'] +
                                         timeseries_df['Q_int_lighting_rad'] +
                                         timeseries_df['Q_int_devices_rad'])
    timeseries_df.loc[:, 'Q_conv_tot'] = (timeseries_df['Q_int_pers_conv'] +
                                          timeseries_df['Q_int_lighting_conv'] +
                                          timeseries_df['Q_int_devices_conv'])
    timeseries_df.loc[:, 'Q_int_tot'] = timeseries_df['Q_rad_tot'] + timeseries_df['Q_conv_tot']

    timeseries_df['T_amb'] = weather_data.temperature_dry_bulb
    timeseries_df['T_sky'] = weather_data.temperature_sky
    timeseries_df['T_supply_vent'] = weather_data.temperature_dry_bulb

    acr_inf = zones_df.loc[zid, 'infiltration_rate']
    timeseries_df['acr_inf'] = [acr_inf for i in range (8760)]
    acr_vent = zones_df.loc[zid, 'ventilation_rate']
    timeseries_df['acr_vent'] = [acr_vent for i in range (8760)]

   # TODO - Priority 1 (deadline: until 19.05.2023) - Responsible "AN" -  comment by = MB - Check whether all the timeseries data are consistant in terms of indices (the code below starts with 0, the hours in the weather object start with 1)
    timeseries_df['TIME'] = [i for i in np.arange(0, 8760, time_resolution_h)]
    timeseries_df.set_index('TIME', inplace=True)

    return timeseries_df


def recombine_radiation_outside_and_calc_inside_one_zone(zid, bc_data_dict, g_angular_timeseries_df,
                                                         factor_selfshadow_df, folder_solar_radiation, validation=False):
    """
    Calculate solar radiation gains occurring inside the zone, i.e. after passing through windows.

    Args:
        - zid (str): the ID of the zone for which the solar radiation gains are calculated.
        - bc_data_dict (dict): a dictionary containing the boundary condition data for all zones.
        - g_angular_timeseries_df (pd.DataFrame): a DataFrame containing the g-value angular modifier as a function of sun incidence angle.
        - factor_selfshadow_df (pd.DataFrame): a DataFrame containing the self-shadowing factors.
        - folder_solar_radiation (str): the folder containing the solar radiation data.
        - validation (bool): a boolean flag indicating whether to use validation data or not.

    Returns:
        - timeseries_solar_gains (pd.DataFrame): a DataFrame containing the solar radiation gains occurring inside the zone, i.e. after passing through windows.

    Raises:
        - FileNotFoundError: if the solar radiation data for the specified zone is not found.
    """

    bc_data = bc_data_dict[zid]
    if validation:
        irrad_data = pd.read_csv('Tables_valid_7_2/vdi_import/VDI4710_rad_data_50degreeNorth_clearday.csv')
    else:
        # try:
        irrad_data_tot = pd.read_csv(folder_solar_radiation + f'/Radiation_TotalRad_zid{zid}.csv', skiprows=[1])
        irrad_data_dir = pd.read_csv(folder_solar_radiation + f'/Radiation_DirectRad_zid{zid}.csv', skiprows=[1])
        # except:
        #     Skip

    # initialisation
    timeseries_solar_gains = pd.DataFrame()
    timeseries_solar_gains['TIME'] = [i for i in np.arange(0, 8760, time_resolution_h)]
    timeseries_solar_gains.set_index('TIME', inplace=True)

    orientation_dict = {'S': 0, 'W': 90, 'N': 180, 'E': 270}  # use trnsys classification of degrees here
    for direction, orient in orientation_dict.items():
        timeseries_solar_gains['IT_{0}_{1}_90'.format(direction, orient)] = \
            np.zeros(int(8760 / time_resolution_h))
        timeseries_solar_gains['IB_{0}_{1}_90'.format(direction, orient)] = \
            np.zeros(int(8760 / time_resolution_h))
        timeseries_solar_gains['IT_INSIDE_{0}_{1}_90'.format(direction, orient)] = \
            np.zeros(int(8760/time_resolution_h))
    timeseries_solar_gains['RADtot_H'] = np.zeros(int(8760 / time_resolution_h))

    # recombine radiation outside
    area_ext_dict = {'H': 0, 'S': 0, 'W': 0, 'N': 0, 'E': 0}  # H ... horizontal surface (=roof)
    for index, row in bc_data.loc[bc_data.index != 'units'].iterrows():
        if row['COMPONENT'].split('_')[-2][0] == 'E' and row['LAYER_NR'] == 1:
            dir_ext = row['DIRECTION']
            if not dir_ext == 'H':
                ori_ext = orientation_dict[dir_ext]    # this is simplified to 4 main directions compared \
                # to original orientation in bc_data_table
            area_ext_dict[dir_ext] += row['AREA']
            irrad_tot = irrad_data_tot[row['Surf_ID']]
            irrad_dir = irrad_data_dir[row['Surf_ID']]
            if dir_ext == 'H':
                timeseries_solar_gains['RADtot_H'] += irrad_tot * row['AREA']
            else:
                timeseries_solar_gains['IT_{0}_{1}_90'.format(dir_ext, ori_ext)] += irrad_tot * row['AREA']
                timeseries_solar_gains['IB_{0}_{1}_90'.format(dir_ext, ori_ext)] += irrad_dir * row['AREA']

    # divide by total external component area of respective orientation because of later calc in disturbances
    for direction, orient in orientation_dict.items():
        timeseries_solar_gains['IT_{0}_{1}_90'.format(direction, orient)] /= area_ext_dict[direction]
        timeseries_solar_gains['IB_{0}_{1}_90'.format(direction, orient)] /= area_ext_dict[direction]
    timeseries_solar_gains['RADtot_H'] /= area_ext_dict['H']

    if validation:
        timeseries_solar_gains['IT_S_0_90'] = irrad_data['IT_S_0_90']
        timeseries_solar_gains['IB_S_0_90'] = irrad_data['IB_S_0_90']

    # calc radiation inside
    area_win_dict = {'S': 0, 'W': 0, 'N': 0, 'E': 0}
    for index, row in bc_data.loc[bc_data.index != 'units'].iterrows():
        if row['COMPONENT'].split('_')[-2][0] == 'W':
            dir_win = row['DIRECTION']
            ori_win = orientation_dict[dir_win]    # this is simplified to 4 main directions compared \
            # to original orientation in bc_data_table
            area_win_dict[dir_win] += row['AREA']
            col_name_selfshadow = '{0}_{1}'.format(row['wid'], dir_win)
            factor_selfshadow = factor_selfshadow_df[col_name_selfshadow]  # check that length of selfshadow timesteps is equal to length of solar radiation data
            g_angular_dir = g_angular_timeseries_df['{0}_{1}_dir'.format(row['wid'], dir_win)]
            g_angular_diff = g_angular_timeseries_df['{0}_{1}_diff'.format(row['wid'], dir_win)]
            irrad_dir = timeseries_solar_gains['IB_{0}_{1}_90'.format(dir_win, ori_win)]
            irrad_diff = timeseries_solar_gains['IT_{0}_{1}_90'.format(dir_win, ori_win)] - irrad_dir
            irrad_inside_dir = row['AREA'] * (1-row['Frame_fraction']) * irrad_dir * g_angular_dir * (1-factor_selfshadow)
            irrad_inside_diff = row['AREA'] * (1-row['Frame_fraction']) * irrad_diff * g_angular_diff
            irrad_inside_tot = irrad_inside_diff + irrad_inside_dir
            timeseries_solar_gains['IT_INSIDE_{0}_{1}_90'.format(dir_win, ori_win)] += irrad_inside_tot
            # addition for validation
            timeseries_solar_gains['IT_INSIDE_{0}_{1}_90_diff'.format(dir_win, ori_win)] = irrad_inside_diff
            timeseries_solar_gains['IT_INSIDE_{0}_{1}_90_dir'.format(dir_win, ori_win)] = irrad_inside_dir

    # divide by total window area of respective orientation because of later calc in disturbances
    for direction, orient in orientation_dict.items():
        timeseries_solar_gains['IT_INSIDE_{0}_{1}_90'.format(direction, orient)] /= area_win_dict[direction]

    timeseries_solar_gains[np.isnan(timeseries_solar_gains)] = 0  # catch all cells with nan values

    return timeseries_solar_gains


def reformat_solar_radiation_write_new_files(weather_data, dir_solar_radiation_reformat, scenario_id, c_type, j):
    """
    Reformat solar radiation data by splitting the input file into zonewise files, creating averages for areas with more than one sensor, and saving the result into a new directory.

    Args:
        weather_data: A pandas dataframe containing solar radiation data.
        dir_solar_radiation_reformat: A string specifying the directory to save the reformatted solar radiation files.
        scenario_id: An integer specifying the scenario id.
        c_type: A string specifying the calculation type.
        j: An integer specifying the progress bar value

    Returns:
        None
    """

    print('\ninput_creation: reformat_solar_radiation ....... started')
    logging.debug('input_creation: reformat_solar_radiation ....... started')

    for i, irrad_component in enumerate(['TotalRad', 'DirectRad']):

        irrad_data_raw = weather_data.solar_radiation_total if i == 0 else weather_data.solar_radiation_direct

        # reformat irrad_data:
        print('\nReformatting solar radiation:', irrad_component)
        irrad_data = irrad_data_raw.drop(['sensorString', 'sensorID', 'sensorNr', 'surfOri', 'surfOri_Name',
                                          'surfOri_Deg', 'surfSlope', 'sensorHeight'], axis=1)
        irrad_data['srfname_composed'] = irrad_data['buildingName'] + '-' + irrad_data['zoneNr'] + '-' + irrad_data[
            'faceNr']
        irrad_data['zid'] = irrad_data['buildingName'] + '-' + irrad_data['zoneNr']

        irrad_zonewise = irrad_data.groupby(by=['zid'])
        if show_progress:
            app.update_progress(0.5, 'reformating solar radiation', 'Reformating', False, scenario_id, c_type, j)
        n = len([i for i in irrad_zonewise.groups])
        for j, irrad_group in enumerate(irrad_zonewise.groups):
            irrad_zone_raw = irrad_zonewise.get_group(irrad_group)
            zid = irrad_zone_raw['zid'].iloc[0]
            irrad_reduced = irrad_zone_raw.drop(['buildingName', 'zoneNr', 'faceNr', 'zid'], axis=1)
            irrad_facewise = irrad_reduced.groupby(by=['srfname_composed'])

            result_zone = pd.DataFrame()
            if show_progress:
                app.update_progress(0.25, 'reformating solar radiation', 'Reformating', True, scenario_id, c_type, j)
            for irrad_group_face in irrad_facewise.groups:
                irrad_face = irrad_facewise.get_group(irrad_group_face)
                if len(irrad_face) == 1:
                    new = irrad_face
                    new.set_index('srfname_composed', inplace=True)
                else:
                    def give_weighted_mean(weightcolumn):
                        def f1(x):
                            return np.average(x, weights=irrad_face.loc[x.index, weightcolumn])

                        return f1

                    new = irrad_face.groupby('srfname_composed').agg(give_weighted_mean('sersorArea'))
                if result_zone.empty:
                    result_zone = new
                else:
                    result_zone = pd.concat([result_zone, new])

            # result_zone = result_zone.drop('sersorArea')
            result_zone['sersorArea'] = 'W/m2'
            result_trans = result_zone.transpose()

            result_trans.to_csv(dir_solar_radiation_reformat + f'/Radiation_{irrad_component}_zid{zid}.csv')
            print(irrad_component, j+1, 'of', n, 'reformatting for {0} finished'.format(irrad_group))
    if show_progress:
        app.update_progress(100, 'reformatting solar radiation', 'Reformatting', False, scenario_id, c_type, j)
    print('input_creation: reformat_solar_radiation ....... finished')
    logging.debug('input_creation: reformat_solar_radiation ....... finished')
    pass