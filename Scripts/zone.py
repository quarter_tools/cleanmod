import pandas as pd
import numpy as np
import logging
from types import SimpleNamespace
from scipy import signal
from . import building_component as building_component
from . import util as util


class Zone:
    """ Test."""  # #TODO - Priority 2 (deadline: 07.2023) - Responsible "AN" - comment by = MB . Add description i.e. docstrings

    def __init__(self, config, zoneid, zone_data, int_mass_factor, heat_transmission, time, input_data_loc,
                 pv_potential_roof, pv_potential_wall, thermal_bridge_factor, scenario_object, file_geometry_surfaces,
                 renovation_costs_df):

        self.zid = zoneid
        self.vol = zone_data['Zone-Volume']
        self.area = zone_data['Zone-Area']
        self.number_occupants = zone_data['number_occupants']
        # self.building_code = zone_data['building_code']
        self.heated_area = zone_data['heated_area']
        self.energy_standard = zone_data['renovation_variant']
        self.window_type = zone_data['window_type']

        # read config data
        self.name_model = config['project_settings']['name_model']
        self.time_resolution_h = config['simulation_settings']['time_resolution_h']
        self.predict_horizon_h = config['simulation_settings']['predict_horizon_h']
        self.update_interval_h = config['simulation_settings']['update_interval_h']
        self.elec_config = config['simulation_settings']['elec_config']
        self.heat_config = config['simulation_settings']['heat_config']
        self.Tz_avg = config['building_settings']['Tz_avg']
        self.start_temp_general = config['RC_settings']['start_temp_general']

        # building components
        building_components_file = input_data_loc + f'/Building_component_data/bc_zid{zoneid}.csv'
        building_components_data = self.read_building_component_data_csv(building_components_file)
        components = self.create_buildingcomponents(building_components_data, thermal_bridge_factor)
        self.ext_components = components['ext']
        self.int_components = components['int']
        self.windows = components['win']

        # renovation costs
        self.renovation_costs_construction = self.calculate_renovation_costs_construction(renovation_costs_df)

        if 'sh_load' in self.heat_config:
            self.internal_mass_factor = int_mass_factor
            self.heat_transmission = heat_transmission

            # initialize attributes
            self.area_int = 0
            self.area_ext = 0
            self.area_windows = 0
            self.area_win_south = 0
            self.area_win_west = 0
            self.area_win_north = 0
            self.area_win_east = 0
            self.area_floor = 0
            self.area_sum = 0
            self.sw_coeff_hor = 0
            self.sw_coeff_south = 0
            self.sw_coeff_west = 0
            self.sw_coeff_north = 0
            self.sw_coeff_east = 0
            self.lw_coeff_ext = []
            self.lw_coeff_win = []
            self.sg_coeff_int = {}
            self.sg_coeff_ext = {}
            # make calculations and update attributes above with these Results
            self.sum_area_for_radiation_distribution()

            # ----------------------------------------------------------------------------------------------------------
            # create initial Tz_avg_history
            self.Tz_avg = self.Tz_avg
            self.Tz_avg_past = [self.Tz_avg] * int(self.predict_horizon_h / self.update_interval_h - 1)

            # read time_series inputs and create disturbance timeseries
            time_series_file = input_data_loc + f'/Timeseries_data/profiles_zid{zoneid}.csv'
            gains, heat_cool_sys_data_raw, weather, radiation, programs = self.read_profiles_csv(time_series_file,
                                                                                                 time.starthour_run,
                                                                                                 time.endhour_predict,
                                                                                                 time.steps_per_hour,
                                                                                                 self.time_resolution_h)
            self.gains = gains
            self.acr_global = self.gains['ACR_ahu'].tolist()[0] + self.gains['ACR_infilt'].tolist()[0]
            #self.heat_cool_sys_data_raw = heat_cool_sys_data_raw
            self.weather = weather
            self.radiation = radiation
            self.programs = programs
            self.actives = self.create_disturbance_profiles(time.starthour_run)

            # make trnsys inputs absolute values for comparison
            #self.Qheat_trnsys = self.heat_cool_sys_data_raw.Qheat * self.area_floor
            #self.Qcool_trnsys = self.heat_cool_sys_data_raw.Qcool * self.area_floor

            # create start_dict and hc inputs
            self.start_dict = self.start_temp_general
            # ----------------------------------------------------------------------------------------------------------

            # initialize attributes
            self.RC = {}
            # make calculations and update attributes above with these Results
            self.create_thermal_circuit()

            # initialize attributes
            self.A_dis = 0
            self.B1_dis = 0
            self.B_dis = 0
            # make calculations and update attributes above with these Results
            self.create_statespace()

            # create heating_cooling_matrix -- B2_dis
            if self.heat_transmission == 'LT-RADIATOR' or 'RADIATOR' or 'Radiator' in self.heat_transmission:
                # HC_Tuple: FORMAT (x_h, x_c, i_h, i_c, e_h, e_c, r_h, r_c)
                # x: Convective fraction,
                # i: Surface heating internal components,
                # e: surface heating external components,
                # r: radiant fraction
                # _h: Heating,
                # _c: cooling
                hc_tuple = (0.5, 1, 0, 0, 0, 0, 0.5, 0)  # --> Heating by Radiators (50% convective + 50% radiative)
                                                         # and Cooling by Mech. air supply (100% convective)
            elif self.heat_transmission == 'FLOOR_HEATING' or 'Floor_heating' or 'Floor_Heating':
                # HC_Tuple: FORMAT (x_h, x_c, i_h, i_c, e_h, e_c, r_h, r_c)
                # x: Convective fraction,
                # i: Surface heating/cooling internal components,
                # e: surface heating/cooling external components,
                # r: radiant fraction
                # _h: Heating,
                # _c: cooling
                hc_tuple = (0, 1, 1, 0, 0, 0, 0, 0)  # --> Heating/cooling by Floor (100% internal component)

            else:
                raise util.InputError(0, 'heat_transmission of corresponding building has an invalid input value.')

            self.create_heating_cooling_matrix(hc_tuple)

        if 'pv_gen' in self.elec_config:
            self.pv_gen, self.pv_area = self.create_pv_gen(time.starthour_run, time.endhour_predict,
                                                           time.steps_per_hour, self.time_resolution_h,
                                                           time.index_timesteps_incl_predict_run,
                                                           pv_potential_roof, pv_potential_wall,
                                                           scenario_object.percentage_PV_potential_used_roofs,
                                                           scenario_object.percentage_PV_potential_used_walls,
                                                           scenario_object.scenario_pv_generation_file,
                                                           file_geometry_surfaces,
                                                           scenario_object.scenario_total_rad_file,
                                                           scenario_object.pv_minimum_radiation)

        if 'hh_load' in self.elec_config:
            hh_load_file = input_data_loc + '/Elec_hh_load/Elec_hh_load.csv'
            hh_load_df = pd.read_csv(hh_load_file)
            self.hh_load = self.select_hh_load_profile_uep(hh_load_df, time.starthour_run, time.endhour_predict,
                                                           time.steps_per_hour)

    @staticmethod
    def create_buildingcomponents(bc_data, thermal_bridge_f):
        ext_components = []
        int_components = []
        windows = []
        #component_dict = {'ext': ext_components, 'int': int_components, 'win': windows}

        unique_bc = bc_data['Name'].unique()

        bc_dict = {'IF': ['Int', 'Floor'],
                   'IC': ['Int', 'Ceiling'],
                   'ID': ['Int', 'Door'],
                   'IW': ['Int', 'Wall'],
                   'W': ['Ext', 'Window'],
                   'EW': ['Ext', 'Wall'],
                   'EF': ['Ext', 'Floor'],
                   'ER': ['Ext', 'Roof'],  # TODO - Priority 4 (deadline: 01.09.2023) - Responsible "MB" - comment by = MB- check whether it works with "Roof"
                   'GF': ['Ext', 'Floor']}  # TODO - Priority 4 (deadline: 01.09.2023) - Responsible "MB" - comment by = MB- Add 'Ground' Adjacency procedure and change 'Ext' to 'Ground' (Currently Ground Adjecency solved directly in Building_component_composition.csv)

        for name in unique_bc:
            t = list(bc_data[bc_data['Name'] == name].index)
            df = bc_data.loc[t, :]

            bc_id = name.split('_')[-2]
            int_ext, component_type = bc_dict[bc_id]

            lw_absorp_coeff = 0  # long wave absorption coefficient of exterior surface (BACK)
            sw_absorp_coeff = 0  # short wave absorption coefficient of exterior surface (BACK)

            temp = df['alpha_i'].dropna().astype(float).values
            alpha_int = temp[0]
            alpha_ext = None
            area = float(df['Area'].dropna().astype(float).values)
            temp2 = df['Orientation'].values
            orient = temp2[0]
            temp5 = df['Inclination'].values
            inclin = temp5[0]

            if int_ext == 'Ext':
                alpha_ext = df['alpha_a'].dropna().astype(float).values
                if component_type != 'Window':
                    temp3 = df['SW_back'].dropna().astype(float).values
                    sw_absorp_coeff = temp3[0]
                temp4 = df['LW_back'].dropna().astype(float).values
                lw_absorp_coeff = temp4[0]

            surf_id = None
            if component_type == 'Window':
                u_value = float(df['U'].values[0])

                bc = building_component.WindowComponent(name, u_value, alpha_int, alpha_ext,
                                                        sw_absorp_coeff, lw_absorp_coeff, area, orient)
                windows.append(bc)
            else:
                # Calculate U value
                if component_type == 'Ceiling':
                    u_value = 1 / (0.10 + 0.04 + (np.sum(np.divide(df['Thickness'].values, df['Conductivity']))))
                elif component_type == 'Floor':
                    u_value = 1 / (0.17 + 0.04 + (np.sum(np.divide(df['Thickness'].values, df['Conductivity']))))
                else:
                    u_value = 1 / (0.13 + 0.04 + (np.sum(np.divide(df['Thickness'].values, df['Conductivity']))))

                # Adding effect of thermal bridges
                if int_ext == 'Ext':
                    u_value += thermal_bridge_f
                    surf_id = df['Surf_ID'].dropna().tolist()[0]

                # Note: strange hierarchy where class is not inherited but the object is passed on
                bc = building_component.BuildingComponent(name, int_ext, component_type,
                                                          df['Thickness'].astype(float).values,
                                                          df['Conductivity'].astype(float).values,
                                                          df['Density'].astype(float).values,
                                                          df['Capacity'].astype(float).values,
                                                          u_value, sw_absorp_coeff, lw_absorp_coeff,
                                                          alpha_int, alpha_ext, area, orient, inclin, surf_id)

                if int_ext == 'Int':
                    int_components.append(bc)
                else:
                    ext_components.append(bc)

        component_dict = {'ext': ext_components, 'int': int_components, 'win': windows}

        return component_dict

    def sum_area_for_radiation_distribution(self):
        """Sums the Areas of a list of Building Component instances, and
        calculates the SW and LW equivalent temperature coefficients
        for the whole zone, as well as the coefficients for distributing solar gains"""

        AExt = 0
        AWin = 0
        AInt = 0
        BExt = 0
        BWin = 0
        AFloor = 0
        ARoof = 0
        AWest = 0
        ASouth = 0
        ANorth = 0
        AEast = 0
        AWinSouth, AWinWest, AWinNorth, AWinEast  = 0, 0, 0, 0

        for bc in self.ext_components:
            AExt += bc.Area
            BExt += bc.Area * bc.U
            if bc.ComponentType == 'Floor':
                AFloor += bc.Area
            # elif bc.ComponentType == 'Roof':   # distinction of this area is not necessary for the VDI procedure
                # ARoof += bc.Area
            elif bc.Orient > 315 or bc.Orient <= 45:  # attention: orientations differ from VDI
                ASouth += bc.Area
            elif bc.Orient <= 135:
                AWest += bc.Area
            elif bc.Orient <= 225:
                ANorth += bc.Area
            elif bc.Orient <= 315:
                AEast += bc.Area
        for bc in self.windows:
            AWin += bc.Area
            BWin += bc.Area * bc.U
            if bc.Orient > 315 or bc.Orient <= 45:  # attention: orientations differ from VDI
                ASouth += bc.Area
                AWinSouth += bc.Area
            elif bc.Orient <= 135:
                AWest += bc.Area
                AWinWest += bc.Area
            elif bc.Orient <= 225:
                ANorth += bc.Area
                AWinNorth += bc.Area
            elif bc.Orient <= 315:
                AEast += bc.Area
                AWinEast += bc.Area
        for bc in self.int_components:
            AInt += bc.Area
            if bc.ComponentType == 'Floor':
                AFloor += bc.Area

        self.area_int = AInt
        self.area_ext = AExt
        self.area_windows = AWin
        self.area_win_south = AWinSouth
        self.area_win_west = AWinWest
        self.area_win_north = AWinNorth
        self.area_win_east = AWinEast
        self.area_floor = AFloor
        Bsum = BExt + BWin
        ASum = AInt + AExt + AWin
        self.area_sum = ASum

        # coefficients for equivalent temperature resulting from radiation
        SW_Hor, SW_South, SW_West, SW_North, SW_East = 0, 0, 0, 0, 0
        LW0 = 0
        LW1 = 0
        # TODO - (Decision pending) Priority ? (deadline: ?) - Responsible "?" - comment by = ? - also new lw_coeff_PV per orientation should be initialized here (similar to LW0 and LW1, and to SW_hor etc.))
        for bc in self.ext_components:
            # TODO (Decision pending) - Priority ? (deadline: ?) - Responsible "?" - comment by = ?- when PV surfaces treated separately, insert if-clause here and set surfaces with PV to SWcoeff +=0
            # This is the third component of equation 32 in VDI 6007 --> kurzwellige Strahlung
            # We do want to keep the "langwellige Strahlungsteil" when the surface is covered by PV pannels, in order to
            # calculate the heat exchange between both surfaces (pv module and wall)
            if int(bc.Inclin) == 0:
                SW_Hor += bc.Area * bc.U * bc.SWCoeff / Bsum
            elif bc.Orient > 315 or bc.Orient <= 45:
                SW_South += bc.Area * bc.U * bc.SWCoeff / Bsum
            elif bc.Orient <= 135:
                SW_West += bc.Area * bc.U * bc.SWCoeff / Bsum
            elif bc.Orient <= 225:
                SW_North += bc.Area * bc.U * bc.SWCoeff / Bsum
            elif bc.Orient <= 315:
                SW_East += bc.Area * bc.U * bc.SWCoeff / Bsum
            # TODO - (Decision pending) - Priority ? (deadline: ?) - Responsible "?" - comment by = ? - when PV surfaces treated separately, LW0 and LW1 should be excluded (+=0) and instead new coefficients added
            # TODO - (Decision pending) - Priority ? (deadline: ?) - Responsible "?" - comment by = ? - new coeffs: self.lw_coeff_PV per orientation
            LW0 += bc.Area * bc.U * bc.LWCoeff[0] / Bsum
            LW1 += bc.Area * bc.U * bc.LWCoeff[1] / Bsum
            # LW_coef_PV_south += bc.Area * bc.U * (bc.LWCoeff[0]+bc.LWCoeff[1]) / Bsum
            # ...# TODO - (Decision pending) - Priority ? (deadline: ?) - Responsible "?" - comment by = ? - for LW_coeff_south ect, repeat if-elif from above to differentiate between bc.orients
        self.sw_coeff_hor = SW_Hor
        self.sw_coeff_south = SW_South
        self.sw_coeff_west = SW_West
        self.sw_coeff_north = SW_North
        self.sw_coeff_east = SW_East
        self.lw_coeff_ext = [LW0, LW1]
        # separate coefficients for equivalent temperature of windows to enable accounting for \
        # closed blinds ( = no longwave radiation)
        LW2 = 0
        LW3 = 0
        for bc in self.windows:
            LW2 += bc.Area * bc.U * bc.LWCoeff[0] / Bsum
            LW3 += bc.Area * bc.U * bc.LWCoeff[1] / Bsum
        self.lw_coeff_win = [LW2, LW3]

        # coefficients for distribution of solar gains on areas in zone [VDI(45),(46)]
        AExtWin = AExt + AWin

        # A_ExtWin? this constant has to be used for the shares below, because otherwise
        # negative values or values>1 can occur
        SG_IW_South = (ASum - AExtWin) / (ASum - ASouth)
        SG_IW_West = (ASum - AExtWin) / (ASum - AWest)
        SG_IW_North = (ASum - AExtWin) / (ASum - ANorth)
        SG_IW_East = (ASum - AExtWin) / (ASum - AEast)
        SG_EW_South = (AExtWin - ASouth) / (ASum - ASouth)
        SG_EW_West = (AExtWin - AWest) / (ASum - AWest)
        SG_EW_North = (AExtWin - ANorth) / (ASum - ANorth)
        SG_EW_East = (AExtWin - AEast) / (ASum - AEast)

        self.sg_coeff_int = {'S': SG_IW_South, 'W': SG_IW_West, 'N': SG_IW_North, 'E': SG_IW_East}
        self.sg_coeff_ext = {'S': SG_EW_South, 'W': SG_EW_West, 'N': SG_EW_North, 'E': SG_EW_East}

        # debugging
        """# Test with geosurf of trnsys example case
        self.SGcoeff_IW = {'S': 0.715, 'W': 0.715, 'N': 0.715, 'E': 0.715}  # anhand Tabelle 1 nachvollziehen!
        self.SGcoeff_EW = {'S': 0.285, 'W': 0.285, 'N': 0.285, 'E': 0.285}  # auch!"""

    def create_thermal_circuit(self):
        """ Calculates the Thermal Circuit including all RC Parameters for the building, returning a dictionary
        with all parameters. The Thermal Circuit is an adapted version of the model in VDI 6007-1, in which three
        additional capacitors of value = 1 have been included to make the model suitable for calculations with
        optimization. The dummy capacitors simplify the dynamic equations and optimization problem formulation."""

        def calc_RC_parallel(R1, C1, R2, C2):
            """Connect two components in parallel using the complex resistance approach
            [VDI 6007-1 Equations (23),(24)]"""

            wra = 2 * np.pi / (86400 * 5)  # Angular frequency of the room "RA", in l/s
            num = R1 * C1 ** 2 + R2 * C2 ** 2 + wra ** 2 * R1 * R2 * (R1 + R2) * C1 ** 2 * C2 ** 2  # Numerator in Eq.23
            den = (C1 + C2) ** 2 + wra ** 2 * (R1 + R2) ** 2 * C1 ** 2 * C2 ** 2  # Denominator in Eq.23
            R = num / den  # Eq.23

            num = (C1 + C2) ** 2 + wra ** 2 * (R1 + R2) ** 2 * C1 ** 2 * C2 ** 2  # Numerator in Eq.24
            den = C1 + C2 + wra ** 2 * (R1 ** 2 * C1 + R2 ** 2 * C2) * C1 * C2  # Denominator in Eq.24
            C = num / den  # Eq.24
            return [R, C]

        # RC: dict including 'Rrest', 'CEW', 'R1EW', 'RaEW', 'RaIW', 'R1IW', 'CIW', 'RaIL', 'Cz', 'Rvent' \
        # the implemented RC model is completed with C1, C2 and Cst, which are included later on through \
        # the method create_statespace. C1, C2 and Cst are the dummy capacitances with value = 1.
        RC = {}

        # Calculate the ventilation resistance (used in VDI 6007-1 Equation (75))
        c = 1.0061e3  # Specific heat capacity of air at 20 degrees [unit = J/kg K]
        rho = 1.2057  # density of air at 20 degrees [unit = kg/m3]
        Vdot = self.vol * self.acr_global / 3600
        if Vdot == 0:
            RC['Rvent'] = 10 ** 12  # [unit = W/K]
        else:
            RC['Rvent'] = (c * rho * Vdot) ** -1  # [unit = W/K]  Note: 1W = 1J/s

        # Calculate the thermal capacitance of the zone due to the air in the room
        # approach for room capacitance: zone thermal capacitance includes capacitance of air in room and capacitance \
        # added by internal walls within zone and furniture; calculation via internal_mass_factor related to floor area
        RC['Cz'] = c * rho * self.vol
        RC['Cz'] += self.internal_mass_factor * self.area_floor * 1000  # [J/K]

        # Calculates radiation between the outer walls and inner walls/surfaces
        # depending on which has the smallest total area [VDI 6007-1 Equation (29) or (31) with (30)]
        astr = 5  # VDI 6007-1 Equation (30) [W/m2K]
        R_EWIW = (astr * min(self.area_int, (self.area_ext + self.area_windows))) ** -1  # [W/K]

        # Calculates the resulting RC values for internal components,
        # by connecting the complex R1 and C1 values of the internal components in parallel,
        # using VDI 6007-1 Equation (23) and (24), implemented in method calc_RC_parallel,
        # the Equations are used iteratively to connect all components
        RIW = self.int_components[0].RC['R1']
        CIW = self.int_components[0].RC['C1']
        RkonIW = 1 / (self.int_components[0].alphaInt * self.int_components[0].Area)
        for bc in self.int_components[1:]:
            RIW, CIW = calc_RC_parallel(RIW, CIW, bc.RC['R1'], bc.RC['C1'])
            RkonIW = ((1 / RkonIW) + (bc.alphaInt * bc.Area)) ** -1
        RC['CIW'] = CIW
        RC['R1IW'] = RIW

        # Calculates the resulting RC values for External Components and Windows,
        # by connecting the complex R1 and C1 values of the internal components in parallel,
        # using VDI 6007-1 Equation (23) and (24), implemented in method calc_RC_parallel,
        # the Equations are used iteratively to connect all components
        REW = self.ext_components[0].RC['R1']
        CEW = self.ext_components[0].RC['C1']
        RkonEW = 1 / (self.ext_components[0].alphaInt * self.ext_components[0].Area)
        for bc in self.ext_components[1:]:
            REW, CEW = calc_RC_parallel(REW, CEW, bc.RC['R1'], bc.RC['C1'])
            RkonEW = ((1 / RkonEW) + (bc.alphaInt * bc.Area)) ** -1
        for bc in self.windows:
            REW = ((1 / REW) + (1 / bc.R1)) ** -1
            RkonEW = ((1 / RkonEW) + (bc.alphaInt * bc.Area)) ** -1
        RC['CEW'] = CEW
        RC['R1EW'] = REW

        # Calculate the Star to Delta zone resistances [VDI 6007-1 Equation (55), (56) and (57)]
        RC['RaIL'] = RkonIW * RkonEW / (RkonIW + RkonEW + R_EWIW)
        RC['RaEW'] = R_EWIW * RkonEW / (RkonIW + RkonEW + R_EWIW)
        RC['RaIW'] = R_EWIW * RkonIW / (RkonIW + RkonEW + R_EWIW)

        # Calculate the Rest resistance [VDI 6007-1 Equation (28)]
        UAges = 0
        InRalpha = 0
        for bc in self.ext_components:
            UAges += bc.U * bc.Area
            InRalpha += bc.alphaExt * bc.Area
        for bc in self.windows:
            UAges += bc.U * bc.Area
            InRalpha += bc.alphaExt * bc.Area
        Rges = max(1 / UAges, 1 / InRalpha)
        RC['Rrest'] = float(Rges - REW - (1 / RkonEW + 1 / R_EWIW) ** -1)

        # Assign RC to object
        self.RC = RC

    def create_statespace(self):
        """ Calculates the discrete State Space model for the Zone using the vectors:
        states x = [Tew, Tiw, Tz, T1, T2, Tst]' and disturbaces w = [Qew, Qiw, Qkon, Taeq, Tvent].
        Inputs (u) considered seperately as it depends on the heating

        For the purpose of this model continuous model inputs and disturbance are lumped together
        xdot = A_cont x + B_cont u

        For the discrete model they are seperated for the model:
        x(k+1) = A_dis x(k) + B1_dis w(k) + B2_dis u(k)

        B2_dis is derived from B1_dis, and is calculated in a seperate matrix
        Calculation of B2 depends on the heating technology
        """

        M = SimpleNamespace(**self.RC)
        C1 = 1
        C2 = 1
        Cst = 1
        R1 = M.CEW ** -1 * np.array([[-M.Rrest ** -1 - M.R1EW ** -1, 0, 0, M.R1EW ** -1, 0, 0]])
        R2 = M.CIW ** -1 * np.array([[0, -M.R1IW ** -1, 0, 0, M.R1IW ** -1, 0]])
        R3 = M.Cz ** -1 * np.array([[0, 0, -M.Rvent ** -1 - M.RaIL ** -1, 0, 0, M.RaIL ** -1]])
        R4 = C1 ** -1 * np.array([[M.R1EW ** -1, 0, 0, -M.R1EW ** -1 - M.RaEW ** -1, 0, M.RaEW ** -1]])
        R5 = C2 ** -1 * np.array([[0, M.R1IW ** -1, 0, 0, -M.RaIW ** -1 - M.R1IW ** -1, M.RaIW ** -1]])
        R6 = Cst ** -1 * np.array(
            [[0, 0, M.RaIL ** -1, M.RaEW ** -1, M.RaIW ** -1, -M.RaIL ** -1 - M.RaIW ** -1 - M.RaEW ** -1]])

        A_cont = np.vstack((R1, R2, R3, R4, R5, R6))
        B_cont = np.array([[0, 0, 0, M.CEW ** -1 * M.Rrest ** -1, 0],
                           [0, 0, 0, 0, 0],
                           [0, 0, M.Cz ** -1, 0, M.Cz ** -1 * M.Rvent ** -1],
                           [C1 ** -1, 0, 0, 0, 0],
                           [0, C2 ** -1, 0, 0, 0],
                           [0, 0, 0, 0, 0]])

        C_cont = np.array([])
        D_cont = np.array([])
        sysc = (A_cont, B_cont, C_cont, D_cont)
        dt = 3600 * self.time_resolution_h
        sysd = signal.cont2discrete(sysc, dt)
        self.A_dis = sysd[0]
        self.B1_dis = sysd[1]
        self.B_dis = sysd[1]

    def create_heating_cooling_matrix(self, hc_tuple):
        """
        Calculate input matrix for the input vector: u = [Heating, Cooling]'
        Based on the heating specified by HC_Tuple
        HC_Tuple : TUPLE WITH THE FORMAT (x_h, x_c, i_h, i_c, e_h, e_c, r_h, r_c)
        x: Convective fraction, i: Surface heating internal components, e: surface heating external components,
        r: radiant fraction
        _h: Heating, _c: cooling
        These 4 fractions have to add to 1 for heating case (resp. cooling case), see also fig.4 on p23 in VDI

        Assigns the input matrix B2_dis for the model in the form:
        x(k+1) = A_dis x(k) + B1_dis w(k) + B2_dis u(k)
        """

        x_h, x_c, i_h, i_c, e_h, e_c, r_h, r_c = hc_tuple

        # Distribute the heating and cooling to the gains: QEW, QIW, Qcon
        # Equations come from VDI6007 (52) and (53)

        F = np.array([[e_h + r_h * self.area_ext / self.area_sum, e_c + r_c * self.area_ext / self.area_sum],
                      [i_h + r_h * (self.area_sum - self.area_ext) / self.area_sum,
                       i_c + r_c * (self.area_sum - self.area_ext) / self.area_sum],
                      [x_h, x_c]])

        # Calculate the  # TODO - - Priority 2 (deadline: July 2023) - Responsible "AN" - comment by = ? - finish the description
        self.B2_dis = np.dot(self.B1_dis[:, 0:3], F)

    def create_pv_gen(self, my_start_hour, my_end_hour, steps_per_hour, my_time_resolution_h, index_time_steps,
                      pv_potential_roof, pv_potential_wall, pv_potential_used_roofs, pv_potential_used_walls,
                      file_pv_time_series, file_geometry_surfaces, file_total_radiation, pv_minimum_radiation):
        """Creates time series of pv generation in [W] for the focus zone, based on the zone surfaces, the pv potential
        of the building and the ratio of that potential used as defined through the factors pv_potential_used for walls
        and roofs

        Args:
            my_start_hour (float): start hour of time period being simulated
            my_end_hour (float): end hour of time period being simulated
            steps_per_hour (float): 1 if hourly, 4 if 15min.
            my_time_resolution_h (float): 1 if hourly, 0.25 if 15min.
            index_time_steps (list): time steps in simulated time period.
            pv_potential_roof (float): ratio of roof surfaces suitable for pv according to building type
            pv_potential_wall (float): ratio of wall surfaces suitable for pv according to building type
            pv_potential_used_roofs (float): percentage of pv_potential_roof used according to scenario settings
            pv_potential_used_walls (float): percentage of pv_potential_wall used according to scenario settings
            file_pv_time_series (str): path of the GH file containing the pv generation time series
            file_geometry_surfaces (str): path of the file containing all surfaces and their areas
            file_total_radiation (str): path of the file containing the total radiation at each surface
            pv_minimum_radiation (float): solar radiation level below which PV is excluded (in kW/m2)

        Returns:
            zone_pv_generation_sr_reduced (pd.Series): time series of pv generation from roof and wall surfaces
        """
        # create df total radiation
        total_radiation_df = pd.read_csv(file_total_radiation, skiprows=[1], delimiter=",", low_memory=False)

        # create df file_pv_time_series
        pv_time_series_df = pd.read_csv(file_pv_time_series, skiprows=[1], delimiter=",", low_memory=False)
        l_building_name = pv_time_series_df['buildingName'].tolist()
        l_zone_nr = pv_time_series_df['zoneNr'].tolist()
        l_face_nr = pv_time_series_df['faceNr'].tolist()
        l_face_id = []
        for i, name in enumerate(l_building_name):
            face_id = name + '-' + l_zone_nr[i] + '-' + l_face_nr[i]
            l_face_id.append(face_id)
        pv_time_series_df['face_id'] = l_face_id

        # load df of geometry surfaces
        geometry_surfaces_df = pd.read_csv(file_geometry_surfaces, skiprows=[1], delimiter=",")

        # initialize bc_pv_generation
        zone_pv_generation = pd.Series([0 for i in range(8760)])
        zone_pv_area = 0

        ts_cols = [str(i) for i in range(8760)]
        for bc in self.ext_components:
            bc_surf_id = bc.Surf_id
            hourly_total_radiation_sensor = 0
            if bc.ComponentType == 'Floor':
                continue  # This is for skip ground floor
            # Check whether the GH time series for the bc_surf_id exists and extract it if yes
            all_face_ids = pv_time_series_df['face_id'].tolist()
            if bc_surf_id not in all_face_ids:
                print('WARNING: the PV time series of the following Surf_ID is missing:', bc_surf_id)
                bc_pv_time_series = pd.Series([0 for i in range(8760)])
                bc_pv_potential_used = bc_pv_time_series
            else:
                # Extract only if minimum solar radiation criteria is achieved
                sensor_id = pv_time_series_df[pv_time_series_df['face_id'] == bc_surf_id]['sensorID'].tolist()[0]
                hourly_total_radiation_sensor = total_radiation_df[total_radiation_df['sensorID'] ==
                                                                   sensor_id].loc[:, ts_cols].copy()
                hourly_total_radiation_sensor_col = hourly_total_radiation_sensor.T
                hourly_total_radiation_sensor_list = [i for i in hourly_total_radiation_sensor_col.iloc[:, 0]]
                annual_total_radiation_sensor = sum(hourly_total_radiation_sensor_list)/1000  # kW/m2
                annual_total_radiation_criteria = pv_minimum_radiation  # kW/m2
                # TODO: change to annual_total_radiation_criteria, now set as 0 for testing
                if annual_total_radiation_sensor >= 0 and \
                        annual_total_radiation_sensor >= annual_total_radiation_criteria:
                    bc_pv_time_series_row = pv_time_series_df[pv_time_series_df['face_id'] ==
                                                              bc_surf_id].loc[:, ts_cols].copy()
                    bc_pv_time_series = bc_pv_time_series_row.T
                    bc_pv_time_series = bc_pv_time_series.iloc[:, 0].reset_index(drop=True)  # To convert it into pd.Series
                    # converting kWh into w/hm2. NOTE: No error here in word "sersorArea", it comes from gh pv files
                    gh_base_area = pv_time_series_df[pv_time_series_df['face_id'] ==
                                                     l_face_id]['sersorArea'].tolist()[0]
                    bc_pv_time_series = (bc_pv_time_series*1000) / gh_base_area

                    if bc.ComponentType == 'Roof':  # Assumption: pv panels are built parallel to surface
                        #bc_pv_potential = bc_pv_time_series * (pv_potential_roof/100)
                        bc_pv_potential = pv_potential_roof / 100
                        bc_pv_used = bc_pv_potential * (pv_potential_used_roofs / 100)
                    elif bc.ComponentType == 'Wall':  # Assumption: pv panels are built parallel to surface
                        # bc_pv_potential = bc_pv_time_series * (pv_potential_wall/100)
                        bc_pv_potential = pv_potential_wall / 100
                        bc_pv_used = bc_pv_potential * (pv_potential_used_walls/100)

                    bc_surf_id_area = geometry_surfaces_df[geometry_surfaces_df['Surf-ID'] ==
                                                           bc_surf_id]['Surf-Area'].tolist()[0]

                    bc_pv_area = bc_surf_id_area * bc_pv_used
                    zone_pv_area += bc_pv_area

                    bc_pv_potential_used = bc_pv_area * bc_pv_time_series

                else:
                    bc_pv_potential_used = 0

            zone_pv_generation = zone_pv_generation.add(bc_pv_potential_used, fill_value=0)

        # Converting ps.Series into pd.Dataframe to use existing code
        zone_pv_generation_df = zone_pv_generation.to_frame()

        # check whether total of rows matches 1 year in given time resolution
        if float(len(zone_pv_generation_df)) != 8760 * steps_per_hour:
            raise util.InputError(0, 'pv time series do not match given time_resolution.')

        # set new index and ignore given TIME column with the aim of avoiding format-problems
        index_matching_resolution = pd.Index([i for i in np.arange(0, 8760, my_time_resolution_h)])
        zone_pv_generation_df.set_index(index_matching_resolution, inplace=True)

        # prolong df if end_hour+predict_horizon exceeds length of data
        if my_end_hour >= 8760.0:
            overhang = float(my_end_hour - 8760)
            index_overhang = pd.Index([i for i in np.arange(8760, my_end_hour, my_time_resolution_h)])
            data_overhang = zone_pv_generation_df.loc[0:overhang - my_time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([zone_pv_generation_df, data_overhang])
            zone_pv_generation_df = new
        # add data to df if start_hour lies prior to start of df_in
        if my_start_hour < 0.0:
            overhang = float(-my_start_hour)
            index_overhang = pd.Index([i for i in np.arange(my_start_hour, 0, my_time_resolution_h)])
            data_overhang = zone_pv_generation_df.loc[8760 - overhang:8760 - my_time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([data_overhang, zone_pv_generation_df])
            zone_pv_generation_df = new

        zone_pv_generation_sr = zone_pv_generation_df.iloc[:, 0]

        zone_pv_generation_sr_reduced = zone_pv_generation_sr.loc[index_time_steps]

        return zone_pv_generation_sr_reduced, zone_pv_area

    def select_hh_load_profile_uep(self, elec_load_df, mystarthour, myendhour, steps_per_hour):
        """."""

        df_in = elec_load_df

        # check whether total of rows matches 1 year in given time resolution
        # if float(len(df_in)) != 8760 * steps_per_hour:
        #     raise util.InputError(0, 'input_profiles do not match given time_resolution.')

        # set new index and ignore given TIME column with the aim of avoiding format-problems
        # TODO -  - Priority 1 (deadline: 19.05.2023) - Responsible "AN" - comment by = ? - old code strictly requires 8760 time steps. New code apdat the length automatically. Need check if it raise some errors
        # index_matching_resolution = pd.Index([i for i in np.arange(0, 8760, time_resolution_h)])
        index_matching_resolution = pd.Index([i for i in np.arange(0, len(df_in.index), self.time_resolution_h)])
        df_in.set_index(index_matching_resolution, inplace=True)

        # prolong df_in if endhour+predict_horizon exceeds length of data
        if myendhour >= 8760.0:
            overhang = float(myendhour - 8760)
            index_overhang = pd.Index([i for i in np.arange(8760, myendhour, self.time_resolution_h)])
            data_overhang = df_in.loc[0:overhang - self.time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([df_in, data_overhang])
            df_in = new
        # add data to df_in if starthour lies prior to start of df_in
        if mystarthour < 0.0:
            overhang = float(-mystarthour)
            index_overhang = pd.Index([i for i in np.arange(mystarthour, 0, self.time_resolution_h)])
            data_overhang = df_in.loc[8760 - overhang:8760 - self.time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([data_overhang, df_in])
            df_in = new

        # reduce total timeseries to index_timesteps
        elec_load_zone = df_in.loc[mystarthour:myendhour, self.zid]

        return elec_load_zone

    def read_building_component_data_csv(self, filename):
        """ Read the building component data from csv.

        Args:
            filename (str): filename including relative path to file.

        Returns:
            dataframe.
        """

        building_data = pd.read_csv(filename, skiprows=[1], delimiter=',')

        # rename columns
        map_col_name = {'COMPONENT': 'Name',
                        'AREA': 'Area',
                        'DIRECTION': 'Direction',
                        'ORIENTATION': 'Orientation',
                        'INCLINATION': 'Inclination',
                        'LAYER_NR': 'Layer',
                        'LAYERS': 'Material',
                        'THICKNESS': 'Thickness',
                        'CONDUCTIVITY': 'Conductivity',
                        'DENSITY': 'Density',
                        'CAPACITY': 'Capacity',
                        'HBACK': 'alpha_a',
                        'HFRONT': 'alpha_i',
                        'UVALUE': 'U',
                        'SHGC': 'g_dir',
                        'LW-BACK': 'LW_back',
                        'LW-FRONT': 'LW_front',
                        'SW-BACK': 'SW_back',
                        'SW-FRONT': 'SW_front',
                        'note': 'note',
                        'Surf_ID': 'Surf_ID'}

        building_data = building_data.rename(columns=map_col_name)
        # print(building_data)

        # fill empty rows in 'Name' column
        building_data.loc[:, 'Name'].fillna(method='ffill', inplace=True)
        unique_bc = building_data['Name'].unique()

        # conversion of Conductivity from kJ/(hmK) to W/mK  # MB 28.04.2023: not required anymore
        # building_data.loc[:, 'Conductivity'] /= 3.6

        temp = list(building_data[building_data['Name'] == unique_bc[-1]].index)
        row = int(max(building_data.loc[temp, 'Layer'])) - 1
        building_data = building_data.loc[:temp[row], :]

        return building_data

    def read_profiles_csv(self, filename, my_start_hour, my_end_hour, steps_per_hour, my_time_resolution_h):
        """Read timeseries data.

        Args:
            filename (str): filename including relative path to file.
            my_start_hour (float): starthour of timeperiod to calculate.
            my_end_hour (float): endhour of timeperiod to calculate.
            steps_per_hour (float): 1 if hourly, 4 if 15min.
            my_time_resolution_h (float): 1 if hourly, 0.25 if 15min.

        Returns:
            Separate dataframes for the different types of inputs reduced to calculated timeperiod.
        """

        df_in = pd.read_csv(filename, skiprows=[1])  # Leave out the unit row

        # check whether total of rows matches 1 year in given time resolution
        if float(len(df_in)) != 8760 * steps_per_hour:
            raise util.InputError(0, 'input_profiles do not match given time_resolution.')

        # set new index and ignore given TIME column with the aim of avoiding format-problems
        index_matching_resolution = pd.Index([i for i in np.arange(0, 8760, my_time_resolution_h)])
        df_in.set_index(index_matching_resolution, inplace=True)

        # prolong df_in if end_hour+predict_horizon exceeds length of data
        if my_end_hour >= 8760.0:
            overhang = float(my_end_hour - 8760)
            index_overhang = pd.Index([i for i in np.arange(8760, my_end_hour, my_time_resolution_h)])
            data_overhang = df_in.loc[0:overhang - my_time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([df_in, data_overhang])
            df_in = new
        # add data to df_in if start_hour lies prior to start of df_in
        if my_start_hour < 0.0:
            overhang = float(-my_start_hour)
            index_overhang = pd.Index([i for i in np.arange(my_start_hour, 0, my_time_resolution_h)])
            data_overhang = df_in.loc[8760 - overhang:8760 - my_time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([data_overhang, df_in])
            df_in = new

        # print(df_in)

        # time = df_in.loc[start_hour:end_hour, 'TIME'].astype(float).values
        Tamb = df_in.loc[my_start_hour:my_end_hour, 'T_amb']
        Tsky = df_in.loc[my_start_hour:my_end_hour, 'T_sky']
        rad_tot_hor_0 = df_in.loc[my_start_hour:my_end_hour, 'RADtot_H']  # newly added to include zones with roof
        rad_tot_east_90 = df_in.loc[my_start_hour:my_end_hour, 'IT_E_270_90']
        rad_tot_inside_east_90 = df_in.loc[my_start_hour:my_end_hour, 'IT_INSIDE_E_270_90']
        rad_tot_west_90 = df_in.loc[my_start_hour:my_end_hour, 'IT_W_90_90']
        rad_tot_inside_west_90 = df_in.loc[my_start_hour:my_end_hour, 'IT_INSIDE_W_90_90']
        rad_tot_south_90 = df_in.loc[my_start_hour:my_end_hour, 'IT_S_0_90']
        rad_tot_inside_south_90 = df_in.loc[my_start_hour:my_end_hour, 'IT_INSIDE_S_0_90']
        rad_tot_north_90 = df_in.loc[my_start_hour:my_end_hour, 'IT_N_180_90']
        rad_tot_inside_north_90 = df_in.loc[my_start_hour:my_end_hour, 'IT_INSIDE_N_180_90']
        acr_infilt = df_in.loc[my_start_hour:my_end_hour, 'acr_inf']
        acr_ahu = df_in.loc[my_start_hour:my_end_hour, 'acr_vent']
        Tvent_ahu = df_in.loc[my_start_hour:my_end_hour, 'T_supply_vent']
        # Idir = df_in.loc[1:, 'RADdir'].astype(float).values
        # Idif = df_in.loc[1:, 'RADdif'].astype(float).values
        QconvInt = df_in.loc[my_start_hour:my_end_hour, 'Q_conv_tot']  # *area_floor now executed in create_disturbance_profiles
        QradInt = df_in.loc[my_start_hour:my_end_hour, 'Q_rad_tot']
        # Q_solair not read in; this convective part of solar gains is created in create_disturbance_profiles directly
        # QradExt = area_floor*df_in.loc[1:, 'Q_solabs_A1'].astype(float).values
        #Tsetheating = df_in.loc[my_start_hour:my_end_hour, 'Tset_ht_A1']
        #Tsetcooling = df_in.loc[my_start_hour:my_end_hour, 'Tset_cl_A1']
        #Qheat = df_in.loc[my_start_hour:my_end_hour, 'Q_heat_A1']
        #Qcool = df_in.loc[my_start_hour:my_end_hour, 'Q_cool_A1']
        #Tz_trnsys = df_in.loc[my_start_hour:my_end_hour, 'Tair_A1']
        #Top_trnsys = df_in.loc[my_start_hour:my_end_hour, 'Top_A1']
        Tearth = Tamb
        # MB 25.05.2023
        Q_int_pers = df_in.loc[my_start_hour:my_end_hour, 'Q_int_pers']   # in W/m2
        Q_int_lighting = df_in.loc[my_start_hour:my_end_hour, 'Q_int_lighting']  # in W/m2
        Q_int_devices = df_in.loc[my_start_hour:my_end_hour, 'Q_int_devices']  # in W/m2

        gains = pd.DataFrame({'QconvInt': QconvInt,  # in W/m2
                              'QradInt': QradInt,  # in W/m2
                              'ACR_infilt': acr_infilt,  # in 1/h
                              'ACR_ahu': acr_ahu,  # in 1/h
                              'Tamb_inf': Tamb,  # in C
                              'Tvent_ahu': Tvent_ahu,  # in C
                              'Q_int_pers': Q_int_pers,  # in W/m2
                              'Q_int_lighting': Q_int_lighting,  # in W/m2
                              'Q_int_devices': Q_int_devices})  # in W/m2

        # TODO - (Decision pending) - Priority ? (deadline: ?) - Responsible "?" - comment by = ? - add 'Neighbour_Temp', 'Tvent'(=T_supply_vent)

        heat_cool_sys_data = pd.DataFrame()
        #heat_cool_sys_data = pd.DataFrame({'Tsetheating': Tsetheating,
        #                                   'Tsetcooling': Tsetcooling,
        #                                   'Qheat': Qheat,  # fix input heating power of this timestep, not power limit
        #                                   'Qcool': Qcool})  # fix input cooling power of this timestep, not power limit

        # TODO - (Decision pending) - Priority ? (deadline: ?) - Responsible "?" - comment by = ? - add 'Heating_Conv', 'Cooling_Conv', 'Surface_Heating', 'Surface_Cooling', 'Heating_Lim', 'Cooling_Lim'

        weather = pd.DataFrame({'Tamb': Tamb,
                                'Tsky': Tsky,
                                'Tearth': Tearth})

        radiation = pd.DataFrame({'RadTot_Hor_0': rad_tot_hor_0,
                                  'RadTot_East_90': rad_tot_east_90,
                                  'RadTotInside_East_90': rad_tot_inside_east_90,
                                  'RadTot_West_90': rad_tot_west_90,
                                  'RadTotInside_West_90': rad_tot_inside_west_90,
                                  'RadTot_South_90': rad_tot_south_90,
                                  'RadTotInside_South_90': rad_tot_inside_south_90,
                                  'RadTot_North_90': rad_tot_north_90,
                                  'RadTotInside_North_90': rad_tot_inside_north_90})

        programs = pd.DataFrame()
        #programs = pd.DataFrame({'Tz_TRNSYS': Tz_trnsys,
        #                         'Top_TRNSYS': Top_trnsys})

        return gains, heat_cool_sys_data, weather, radiation, programs

    def create_disturbance_profiles(self, mystarthour):
        """Transform raw input in order to match the so-called 'actives' in RC model.

        Args:
             self (zone): respective zone to which the profiles belong.

        Returns:
            dictionary of actives QEW, QIW, Qcon, Taeq, Tvent each as pd.Series with str() as key.
        """

        # factor_shaded = 0.15   # try to automate later
        factor_conv_solar_gains = 0.1  # implicit assumption in trnsys (possibly reduce to 0.09 acc. to VDI)
        # ask Manuel for documentation on solar gains
        # sum convective gains (internal gains + solar gains)
        Qcon = self.gains['QconvInt'] * self.area_floor + factor_conv_solar_gains * (
                self.radiation['RadTotInside_South_90'] * self.area_win_south
                + self.radiation['RadTotInside_West_90'] * self.area_win_west
                + self.radiation['RadTotInside_North_90'] * self.area_win_north
                + self.radiation['RadTotInside_East_90'] * self.area_win_east)

        # simplified approach with 4 orientations only ['S', 'W', 'N', 'E']
        QradExt = {
            'S': self.radiation['RadTotInside_South_90'] * self.area_win_south * (1 - factor_conv_solar_gains),
            'W': self.radiation['RadTotInside_West_90'] * self.area_win_west * (1 - factor_conv_solar_gains),
            'N': self.radiation['RadTotInside_North_90'] * self.area_win_north * (1 - factor_conv_solar_gains),
            'E': self.radiation['RadTotInside_East_90'] * self.area_win_east * (1 - factor_conv_solar_gains)}
        # Attention! Q_secondaryHeatFlux (=Q_shf) as part of gains from radiation from external

        QradInt = self.gains['QradInt'] * self.area_floor

        # Calculate the gains on the external wall and internal wall
        # initialise Series
        QEW_ext = pd.Series(np.zeros(len(QradExt['S'])), index=QradExt['S'].index)
        QIW_ext = pd.Series(np.zeros(len(QradExt['S'])), index=QradExt['S'].index)

        for key, series in QradExt.items():
            QEW_ext += series * self.sg_coeff_ext[key]
            QIW_ext += series * self.sg_coeff_int[key]

        QEW = (QradInt * ((self.area_windows + self.area_ext) / self.area_sum) + QEW_ext)
        QIW = (QradInt * (self.area_int / self.area_sum) + QIW_ext)

        # Calculate equivalent outdoor temperature
        # Combination of equations 41, 42 and 32 of VDI 6007 Part I
        Taeq = (self.weather['Tamb']
                + self.sw_coeff_hor * self.radiation['RadTot_Hor_0']
                + self.sw_coeff_south * self.radiation['RadTot_South_90']
                + self.sw_coeff_west * self.radiation['RadTot_West_90']
                + self.sw_coeff_north * self.radiation['RadTot_North_90']
                + self.sw_coeff_east * self.radiation['RadTot_East_90']
                + (self.lw_coeff_ext[0] + self.lw_coeff_win[0]) * (
                            self.weather['Tearth'] - self.weather['Tamb'])
                + (self.lw_coeff_ext[1] + self.lw_coeff_win[1]) * (self.weather['Tsky'] - self.weather['Tamb'])
                # TODO - (Decision pending) - Priority ? (deadline: ?) - Responsible "?" - comment by = ? - + self.lw_coeff_PV_hor * ((self.PV_temp_csv['T_module_hor'] - self.weather['Tamb'])
                # + self.lw_coeff_PV_south * ((self.PV_temp_csv['T_module_south'] - self.weather['Tamb'])
                # + self.lw_coeff_PV_west * ((self.PV_temp_csv['T_module_west'] - self.weather['Tamb'])
                # ...
                )

        # find Tvent as mixed temperature from infiltration and air heating unit
        acr_total = self.gains.loc[mystarthour, 'ACR_ahu'] + self.gains.loc[mystarthour, 'ACR_infilt']

        #TODO - Priority 4 (deadline: 01.09.2023) - Responsible "?" - comment by = MB - Possiblity for improvement: to account for mechanical ventilation include calculation of consumed energy to pre-condition of Tvent
        Tvent = (self.gains['Tvent_ahu'] * self.gains.loc[mystarthour, 'ACR_ahu']
                + self.weather['Tamb'] * self.gains.loc[mystarthour, 'ACR_infilt']) / acr_total

        # print(QEW[0:24], QIW[0:24], Qcon[0:24], Taeq[0:24])

        # new parameters for plausibility check
        Qsolar = (self.radiation['RadTotInside_South_90'] * self.area_win_south
                  + self.radiation['RadTotInside_West_90'] * self.area_win_west
                  + self.radiation['RadTotInside_North_90'] * self.area_win_north
                  + self.radiation['RadTotInside_East_90'] * self.area_win_east)
        QconvInt = self.gains['QconvInt'] * self.area_floor
        QradInt = self.gains['QradInt'] * self.area_floor

        Qint_pers = self.gains['Q_int_pers'] * self.area_floor
        Qint_lighting = self.gains['Q_int_lighting'] * self.area_floor
        Qint_devices = self.gains['Q_int_devices'] * self.area_floor

        # C and rho used also above
        c = 1.0061e3  # Specific heat capacity of air at 20 degrees [unit = J/kg K]
        rho = 1.2057  # density of air at 20 degrees [unit = kg/m3]
        Qinf_Tinf = self.gains['ACR_infilt'] * self.vol * rho * c * (self.gains['Tamb_inf']) / 3600  # in J/s = W
        Qvent_Tvent = self.gains['ACR_ahu'] * self.vol * rho * c * (self.gains['Tvent_ahu']) / 3600  # in J/s = W
        Q_inf_vent_T_inlet = self.acr_global * self.vol * rho * c * (self.gains['Tvent_ahu']) / 3600  # in J/s = W
        Q_inf_vent_factor = self.acr_global * self.vol * rho * c / 3600  # J/s K = W/K

        Q_envelope_factor = 0
        for win in self.windows:
            Q_envelope_factor += win.U * win.Area
        for bc in self.ext_components:
            Q_envelope_factor += bc.U * bc.Area

        actives = {'Qcon': Qcon,
                   'QEW': QEW,
                   'QIW': QIW,
                   'Taeq': Taeq,
                   'Tvent': Tvent,
                   'Qsolar': Qsolar,
                   'QconvInt': QconvInt,
                   'QradInt': QradInt,
                   'Qint_pers': Qint_pers,
                   'Qint_lighting': Qint_lighting,
                   'Qint_devices': Qint_devices,
                   'Q_inf_vent_factor': Q_inf_vent_factor,
                   'Q_envelope_factor': Q_envelope_factor
                   }

        return actives

    def calculate_renovation_costs_construction(self, renovation_costs_df):
        zone_renovation_cost = 0
        # building_id = self.zid.split('-')[0]
        # building_code = building_id.split('_')[0]
        renovation_variant = self.energy_standard
        if renovation_variant == 1:
            pass
        elif renovation_variant == 2 or renovation_variant == 3:
            for bc in self.ext_components:
                component_name = bc.Component_name
                component_code = '_'.join(component_name.split('_')[:-2])
                if '_U_' in component_code:
                    component_code = component_code.split('_U_')[0]
                # Checking for type of measure and extracting the measure_id
                if '_&_' in component_code:  # Additional measure
                    measure_id = component_code.split('_&_')[-1]
                else:  # Replacing measure
                    measure_id = component_code
                # Checking if all cost data is available in the library
                if measure_id not in renovation_costs_df['measure_id'].tolist():
                    print('WARNING: in library renovation costs, measure_cost is missing for measure_id:', measure_id)
                else:
                    # Calculation of measure cost
                    bc_renovation_costs_specific = renovation_costs_df[renovation_costs_df['measure_id'] ==
                                                                       measure_id]['measure_cost'].tolist()[0]
                    bc_renovation_costs_total = bc_renovation_costs_specific * bc.Area

                    zone_renovation_cost += bc_renovation_costs_total

            for bc in self.windows:
                component_name = bc.Component_name
                component_code = component_name.split('_')[0]
                measure_id = component_code
                # Checking if all cost data is available in the library
                if measure_id not in renovation_costs_df['measure_id'].tolist():
                    print('WARNING: in library renovation costs, measure_cost is missing for measure_id:', measure_id)
                else:
                    # Calculation of measure cost
                    bc_renovation_costs_specific = renovation_costs_df[renovation_costs_df['measure_id'] ==
                                                                       measure_id]['measure_cost'].tolist()[0]
                    bc_renovation_costs_total = bc_renovation_costs_specific * bc.Area

                    zone_renovation_cost += bc_renovation_costs_total

        return zone_renovation_cost
