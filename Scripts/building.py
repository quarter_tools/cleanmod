import pandas as pd
import numpy as np
from . import zone as zone
from . import util as util
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)


class Building:
    """
    Represents a building with its attributes and methods to model energy-related operations.

    Attributes:
        Several attributes are set using the provided data and configurations.
        Important ones include building_code, zones, storey_area, and more.
    """
    def __init__(self, config, building_code, building_data, zones_data, time, input_data_loc,
                 file_pv_potentials, dir_dhw, scenario_object, file_geometry_surfaces, district_zones_count,
                 district_zones_nr, district_buildings_count, district_buildings_nr, renovation_costs_df):
        """
                Initializes a Building object with the provided data.

                Args:
                    config (dict): Configuration settings for the simulation.
                    building_code (str): Unique identifier code for the building.
                    building_data (dict): Data about the building attributes.
                    zones_data (DataFrame): Data about the zones within the building.
                    time (Time): Time attributes.
                    input_data_loc (str): Location for the input data.
                    file_pv_potentials (str): File path for PV potential data.
                    dir_dhw (str): Directory for domestic hot water data.
                    scenario_object (Scenario): Scenario-related attributes.
                    file_geometry_surfaces (str): File path for geometry surface data.
                    district_zones_count (int): Total count of zones in the district.
                    district_zones_nr (int): Zone number in the district.
                    district_buildings_count (int): Total number of buildings in the district.
                    district_buildings_nr (int): Building number in the district.
                    renovation_costs_df (DataFrame): Data about the renovation costs.
                """
        self.building_code = building_code
        self.footprint_area = building_data['footprint_area']
        self.size_class_gh = building_data['building_class']
        self.energy_standard = building_data['renovation_variant']

        # config settings
        self.config = config
        self.time_resolution_h = config['simulation_settings']['time_resolution_h']
        self.name_model = config['project_settings']['name_model']
        self.elec_config = config['simulation_settings']['elec_config']
        self.heat_config = config['simulation_settings']['heat_config']
        self.reduce_to_1_building_with_2_zones = config['project_settings']['reduce_to_1_building_with_2_zones']


        # self.generators = self.load_gen_technical_data_from_library(building_data, input_data_loc)
        self.heat_supply = building_data['heat_supply']
        self.heat_level_sh = building_data['heat_level_sh']
        # TODO: now the temperature level for dhw hard coded as Heat65
        self.heat_level_dhw = building_data['heat_level_dhw']
        self.heat_transmission = building_data['heat_transmission']
        self.WWR_N = building_data['WWR_N']
        self.WWR_E = building_data['WWR_E']
        self.WWR_S = building_data['WWR_S']
        self.WWR_W = building_data['WWR_W']
        self.window_type = building_data['window_type']
        self.internal_mass_factor = building_data['internal_mass_factor']
        self.thermal_bridge_f = building_data['thermal_bridge_factor']
        self.occupants = building_data['occupants']
        self.dwellings = building_data['dwellings']
        #self.storey_area = building_data['storey_area']

        self.use = building_data['use']
        self.dist2hp = building_data['dist_to_heat_source']
        self.free_walls = building_data['free_walls']
        self.lat = building_data['lat']
        self.lon = building_data['lon']
        self.year_class = building_data['year_class']
        self.size_class = building_data['size_class']
        self.bid_UEP = building_data['bid']

        pv_potentials_df = pd.read_csv(file_pv_potentials, skiprows=[1], delimiter=",")
        building_type_code = building_code.split('_')[0]
        pv_potential_roof = pv_potentials_df[pv_potentials_df['building_code'] ==
                                             building_type_code]['pv_potential_roof'].tolist()[0]
        pv_potential_wall = pv_potentials_df[pv_potentials_df['building_code'] ==
                                             building_type_code]['pv_potential_wall'].tolist()[0]

        self.zones = self.create_zones(zones_data, time, input_data_loc, pv_potential_roof, pv_potential_wall,
                                       scenario_object, file_geometry_surfaces,
                                       district_zones_count, district_zones_nr,
                                       district_buildings_count, district_buildings_nr, renovation_costs_df)

        self.storey_area = self.calculate_storey_area()
        self.total_design_heating_load = building_data['spec_heat_transfer_power']*self.storey_area/1000  # kW
        self.total_sizing_heating_load = 0

        self.n_ev = building_data['ev_num']

        if 'dhw_load' in self.heat_config:
            dhw_load_file = (dir_dhw + '/BuildingHeatDemand_' + f'{"{:.1f}".format(self.use)}' + '_' +
                             f'{"{:.1f}".format(self.year_class)}' + '_' + f'{"{:.1f}".format(self.size_class)}' +
                             '_bID-' + f'{"{:.1f}".format(self.bid_UEP)}' + '.csv')
            dhw_load_df = pd.read_csv(dhw_load_file, sep=';', usecols=['HotWaterD_W', 'HotWaterTank_m3'])
            # To make sure only data for two zones are loaded when variable reduce_to_1_building_with_2_zones = True
            if self.reduce_to_1_building_with_2_zones:
                # Factor according to sum of zone areas versus total storey area of complete building
                sum_zone_areas = sum(zones_data['Zone-Area'].tolist())
                dhw_factor = sum_zone_areas / building_data['storey_area']
                dhw_load_df['HotWaterD_W'] = dhw_load_df['HotWaterD_W']*dhw_factor
                dhw_load_df['HotWaterTank_m3'] = dhw_load_df['HotWaterTank_m3'] * dhw_factor
            self.dhw_load = self.assign_dhw_load_profile(dhw_load_df, time.starthour_run, time.endhour_predict,
                                                         time.steps_per_hour)
        else:
            dhw_load_df = pd.DataFrame()
            dhw_load_df['HotWaterD_W'] = [0 for i in range(8760)]
            self.dhw_load = self.assign_dhw_load_profile(dhw_load_df, time.starthour_run, time.endhour_predict,
                                                         time.steps_per_hour)

        if 'pv_gen' in self.elec_config:
            self.pv_gen = self.sum_zone_values(self.zones, 'pv_gen', time)
            self.pv_area = self.sum_pv_areas(self.zones)

        if 'hh_load' in self.elec_config:
            self.hh_load = self.sum_zone_values(self.zones, 'hh_load', time)

        # summation of all fix loads in 'elec_load_total'
        if 'hh_load' in self.elec_config:
            self.elec_load_total = self.hh_load

        self.renovation_costs_construction = self.sum_renovation_costs_construction(self.zones)
        self.renovation_costs_hp = self.calculate_cost_hp(self.total_design_heating_load) if \
            self.heat_supply == 'Heat_pump' else 0
        self.renovation_costs_pv = 0 if 'pv_gen' not in self.elec_config else self.calculate_cost_pv(self.pv_area)
        self.renovation_costs_total = sum([self.renovation_costs_construction,
                                           self.renovation_costs_hp,
                                           self.renovation_costs_pv])
        x = 0  # For debugging

    def create_zones(self, zones_data, time_input, input_data_location, pv_potential_roof, pv_potential_wall,
                     scenario_object, file_geometry_surfaces, district_zones_count, district_zones_nr,
                     district_buildings_count, district_buildings_nr, renovation_costs_df):
        """
             Create and return a list of Zone objects for the building.

             Args:
                 zones_data (DataFrame): Data about the zones within the building.
                 time_input (Time): Time attributes.
                 ... (more arguments as per method)

             Returns:
                 list: List of Zone objects.
             """
        zones = []
        building_zones_count = 0
        building_zones_nr = len(zones_data.iloc[:, 0].tolist())
        for zid, row in zones_data.iterrows():
            district_zones_count += 1
            building_zones_count += 1
            print(str(district_zones_count) + ' of ' + str(district_zones_nr) + ' zones in district | ' +
                  str(district_buildings_count) + ' of ' + str(district_buildings_nr) + ' buildings in district | ' +
                  str(building_zones_count) + ' of ' + str(building_zones_nr) + ' zones in building | ' +
                  'Zone_id:', zid)
            zone_new = zone.Zone(self.config, zid, row, self.internal_mass_factor, self.heat_transmission, time_input,
                                 input_data_location, pv_potential_roof, pv_potential_wall, self.thermal_bridge_f,
                                 scenario_object, file_geometry_surfaces, renovation_costs_df)
            zones.append(zone_new)
        return zones

    def assign_dhw_load_profile(self, load_df, mystarthour, myendhour, steps_per_hour):
        """
           Assigns the domestic hot water (DHW) load profile for the building.

           Args:
               load_df (DataFrame): DHW load data.
               mystarthour (float): Start hour for the simulation.
               myendhour (float): End hour for the simulation.
               steps_per_hour (int): Time steps per hour in the simulation.

           Returns:
               Series: DHW load profile for the building.
           """
        df_in = load_df

        # check whether total of rows matches 1 year in given time resolution
        if float(len(df_in)) != 8760 * steps_per_hour:
            raise util.InputError(0, 'input_profiles do not match given time_resolution.')

        # set new index and ignore given TIME column with the aim of avoiding format-problems
        index_matching_resolution = pd.Index([i for i in np.arange(0, 8760, self.time_resolution_h)])
        df_in.set_index(index_matching_resolution, inplace=True)

        # prolong df_in if end_hour + predict_horizon exceeds length of data
        if myendhour >= 8760.0:
            overhang = float(myendhour - 8760)
            index_overhang = pd.Index([i for i in np.arange(8760, myendhour, self.time_resolution_h)])
            data_overhang = df_in.loc[0:overhang - self.time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([df_in, data_overhang])
            df_in = new
        # add data to df_in if start_hour lies prior to start of df_in
        if mystarthour < 0.0:
            overhang = float(-mystarthour)
            index_overhang = pd.Index([i for i in np.arange(mystarthour, 0, self.time_resolution_h)])
            data_overhang = df_in.loc[8760 - overhang:8760 - self.time_resolution_h]
            data_overhang.set_index(index_overhang, inplace=True)
            new = pd.concat([data_overhang, df_in])
            df_in = new

        # reduce total time series to index time steps
        dhw_load_building = df_in.loc[mystarthour:myendhour, 'HotWaterD_W']

        return dhw_load_building

    @staticmethod
    def sum_zone_values(zones, attribute, time):
        """
        Sum values of a given attribute across all zones.

        Args:
            zones (list): List of Zone objects.
            attribute (str): Attribute to sum values of.
            time (Time): Time attributes.

        Returns:
            Series: Sum of values of the given attribute across all zones.
        """
        all_zones_values = pd.Series(np.zeros(len(time.index_timesteps_run)))
        for zone in zones:
            all_zones_values = pd.concat([all_zones_values, getattr(zone, attribute)], axis=1)
        building_values = all_zones_values.sum(axis=1)
        return building_values

    @staticmethod
    def sum_renovation_costs_construction(zones):
        """
           Calculate the total renovation construction costs for all zones.

           Args:
               zones (list): List of Zone objects.

           Returns:
               float: Total renovation construction costs.
           """
        building_renovation_costs = 0
        for zone in zones:
            building_renovation_costs += zone.renovation_costs_construction
        return building_renovation_costs

    @staticmethod
    def calculate_cost_hp(design_heating_load):
        """
         Calculate the cost of the heat pump based on its design heating load.

         Args:
             design_heating_load (float): Design heating load in kW.

         Returns:
             float: Cost of the heat pump.
         """
        # Prices from Baupreislexicon
        cost_hp_010_kw = 3283  # €/kW
        cost_hp_020_kw = 2298  # €/kW
        cost_hp_050_kw = 1581  # €/kW
        cost_hp_100_kw = 1343  # €/kW

        if design_heating_load <= 10:  # kW
            cost_hp_per_kw = cost_hp_010_kw  # €/kW
        elif design_heating_load == 20:  # kW
            cost_hp_per_kw = cost_hp_020_kw  # €/kW
        elif design_heating_load == 50:  # kW
            cost_hp_per_kw = cost_hp_050_kw  # €/kW
        elif design_heating_load >= 100:  # kW
            cost_hp_per_kw = cost_hp_100_kw  # €/kW
        else:
            if 10 < design_heating_load < 20:
                xp = [10, 20]
                fp = [cost_hp_010_kw, cost_hp_020_kw]
            elif 20 < design_heating_load < 50:
                xp = [20, 50]
                fp = [cost_hp_020_kw, cost_hp_050_kw]
            elif 50 < design_heating_load < 100:
                xp = [50, 100]
                fp = [cost_hp_050_kw, cost_hp_100_kw]
            cost_hp_per_kw = np.interp(design_heating_load, xp, fp)  # €/kW

        cost_hp = design_heating_load * cost_hp_per_kw  # €

        return cost_hp

    @staticmethod
    def sum_pv_areas(zones):
        """
          Calculate the total PV area for all zones.

          Args:
              zones (list): List of Zone objects.

          Returns:
              float: Total PV area in square meters.
          """
        building_pv_area = 0
        for zone in zones:
            building_pv_area += zone.pv_area
        return building_pv_area

    @staticmethod
    def calculate_cost_pv(pv_area):
        """
        Calculate the cost associated with the photovoltaic (PV) area.

        Args:
            pv_area (float): Total PV area in square meters.

        Returns:
            float: Cost associated with the PV area.
        """
        pv_module_efficiency = 0.15  # 15% from generic pv module in GH
        pv_specific_peak_power = 1000 * pv_module_efficiency  # Wp/m2
        pv_area_per_peak_power = (1/pv_specific_peak_power) * 1000  # m2/kWp
        cost_pv_per_peak_power = 2645  # €/kWp
        cost_pv_per_pv_area = cost_pv_per_peak_power / pv_area_per_peak_power  # €/m2
        cost_pv = pv_area * cost_pv_per_pv_area  # €
        return cost_pv

    def calculate_storey_area(self):
        """
            Calculate the total storey area based on all the zones.

            Returns:
                float: Total storey area in square meters.
            """
        total_storey_area = 0
        for zone in self.zones:
            total_storey_area += zone.area
        return total_storey_area
