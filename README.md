# CleanMod

## Introduction

Welcome to CleanMod, an advanced simulation tool designed for the strategic planning of neighborhoods transitioning towards sustainable and energy-efficient futures. CleanMod serves as an integral solution in decision-making, focusing not only on optimizing energy consumption but also on implementing eco-friendly and intelligent energy strategies. The tool transcends the mere integration of advanced technologies or sustainability trends, emphasizing the holistic operability and synergy of neighborhood systems.

## Description

CleanMod is an advanced, adaptable simulation suite developed to analyze diverse neighborhood types such as: urban, suburban, and rural. Positioned within the `CleanMod/Models` directory (e.g. suburban), these comprehensive models serve as foundational blueprints for investigating various environmental factors and neigborhood renovation strategies, each tailored to meet distinct project aims. CleanMod offers unparalleled flexibility, enabling users to adapt existing district neighborhood models or entirely re-envision them, aligning with specific simulation necessities and analytical ambitions. This process, however, necessitates a thorough preparation of building data and the development of accurate thermal models for the buildings.

To prepare new neighborhood models, the users can use tools like Grasshopper integrated within Rhinoceros 3D or Autodesk Revit. These applications facilitate the precise definition of geometric parameters for neighborhood buildings, encompassing zones, floors, and window areas, which are crucial for subsequent computations in CleanMod. Additionally, the user must empoloy Ladybug tools (including Honeybee and Radiance) for assessing solar radiation and generating rated PV production data. Building component specifications can be sourced from the TABULA library encompassing the physical attributes of walls, windows, ceilings, floors, and intricate layering details.

The suite evaluates a wide range of factors, encompassing both micro and macro aspects of a neighborhood. This includes, as mentioned, individual dwelling components like walls, ceilings, floors, windows, and other architectural features, as well as broader elements such as building infrastructure, heating systems, solar panel deployment, and electric vehicle adoption. The scalability of Photovoltaics, covering district levels from minimal to maximal deployment, is also a key feature. The model in `CleanMod/Models` folder includes these details for suburban neighborhoods for evaluations under various hypothetical scenarios to assess the impact of proposed modifications. Further neighborhoods like urban, rural, or any model can be created by advanced-user specificaitons following the Grasshopper workflow in the folder `CleanMod/GEO_RAD_PV`.

CleanMod is built in Python, leveraging the Gurobi optimizer through the Pyomo Solver. This optimization targets various goals, including maintaining set-point room temperatures, reducing electrical peak loads, minimizing CO2 emissions, and cutting overall energy costs. It employs an RC-model for district components, considering user presence and behavior in residential zones.

### Input/Output Folders

- **Input Folder**: This folder is a crucial for conducting simulations in CleanMod. It should contain the `UEP_output` and `GH_output` folders and a `Scenarios.xlsx` file. The `Scenarios.xlsx` file is specifically formatted to outline various simulation scenarios, which can differ based on factors such as geographic location, specific year, types of building renovations, energy management approaches, electricity pricing structures, and densities of electric cars (eCars).

CleanMod utilizes the Residential Electricity Demand Model (REM) and DHWcalc tools, part of the UrbanEnergyPro (UEP) suite, for the generation of hourly time series data relating to electrical demand and internal gains. These tools mainly focus on aspects like occupancy patterns, active electrical loads, lighting requirements, and Domestic Hot Water (DHW) consumption. The output from REM, essential for the CleanMod simulations, is typically found in the `CleanMod/Models/model name/Input/UEP_output` folder.

These time series data are automatically generated after CleanMod is executed, following the configurations specified in the `CleanMod/Step_0_config.yaml` file. The simulations also depend on additional inputs like EnergyPlus Weather (.epw) files (which are tailored to the Potsdam region for the years 2020, 2030, and 2045 for our models). Photovoltaic (PV) energy data, conforming to the format detailed in the `Model/…./Input/GH_output` folder, is also a necessary input for the simulations.

- **Output Folder**: Created post-simulation, it includes an `Input_created` folder with files needed for the simulations and a timestamped `output/Results` folder for each scenario run. The results encompass hourly timeseries calculations for buildings and districts, as well as annual district summaries which can be analyzed, compared, and graphically represented using postporcessing tools available within this suite.

## Conclusion

CleanMod stands as a versatile, powerful tool for architects, urban planners, and sustainability experts, offering insights and data-driven solutions for creating more sustainable and energy-efficient communities. For more detailed information on using CleanMod, please refer to the subsequent folders and files in this repository.

## Setting Up CleanMod

Before diving into simulations, ensure that CleanMod is properly set up by installing necessary dependencies and configuring your IDE to recognize the correct source folders.

### 1. Installing Dependencies

To install the required dependencies for CleanMod, follow these steps:

- Open your terminal.
- Navigate to the CleanMod directory.
- Run the following command:

  ```bash
  pip install -r Cleanvelope/CleanMod/requirements.txt
  ```

This command installs all the packages listed in the `requirements.txt` file located in the `Cleanvelope/CleanMod` directory.

### 2. Configuring Your IDE

For PyCharm users:

- Right-click on the 'CleanMod' folder.
- Select 'Mark Directory as'.
- Choose 'Sources Root'.

This action sets the 'CleanMod' folder as the source directory, making it easier for PyCharm to locate packages for import statements. The folder color will change to blue, indicating the change.

For users of other IDEs, ensure that the 'CleanMod' folder is added to the PYTHONPATH.

## Requirements for Simulating with CleanMod

### CleanMod/Models/Input Content

Successful simulations with CleanMod require specific contents in the `CleanMod/Models/Input` folder, including:

- `UEP_output` and `GH_output` folders.
- `Scenarios.xlsx` file, formatted for defining simulation scenarios.
- Necessary weather files and other inputs as detailed in the `CleanMod/Models/Input` folder.

### CleanMod/Libraries Folder

The `CleanMod/Libraries` folder contains critical files for simulation. If you modify these files, maintain the original naming conventions and content structure to ensure consistent and functional simulations.

# Running CleanMod

This section provides detailed instructions for a quick test run and a comprehensive main run of CleanMod simulations.

## Quick Test Run

To perform a shorter simulation with CleanMod, follow these steps:

1. **Prepare Input Data**: Ensure the input data folders for your neighborhood models contain all necessary files, following the pattern in `CleanMod/Models/Input`.

2. **Configure Model and Scenarios**: In `Step_0_config.yaml`, set `name_model` to `suburban` and `scenarios` to `[S0]`.

3. **Set Up Input Directory**: Activate the creation of a new `Input_created` folder by setting `complete_building_csv_files` and `complete_input_created_exists` to `false`.

4. **Adjust Simulation Parameters**: For a 10-day simulation, set `reduce_to_1_building_with_2_zones` to `true` and `endhour` to `240`.

5. **Run the Simulation**: Execute `Step_1_runme.py`. The first run may take longer due to data preprocessing.

6. **Review Results**: Find the simulation results in `Models/suburban/Output/Results_%time_stamp%`.

7. **Visualize Results**: Use `Step_7_analyser.py` in the `Post-processing` folder for result analysis and visualization.

## Main Run with Multi-Processing

For a full-scale simulation across various scenarios, follow these steps:

1. **Configure Parameters**: In `Step_0_config.yaml`, adjust parameters like `name_model`, `endhour`, `precalc_days`, `scenarios`, `complete_building_csv_files`, and `complete_input_created_exists`.

2. **Execute with Multi-Processing**: Start `Step_2_multi_processing.py`, setting `num_cores` according to your CPU.

3. **Generate Summary Tables**: Use `Step_9_show_res_table.py` in the `Post-processing` folder.

4. **Analyze Individual Scenarios**: Apply the plotting function from the Quick Test Run section for detailed scenario analysis.

### Tips and Steps

Ensure to read through the steps from `Step_0_config` to `Step_2_multi_processing` in the `CleanMod` folder and further steps in `CleanMod/Post-processing`. Always start simulations with `Step_0_config & Step_1_runme` or `Step_0_config & Step_2_multi_processing` for multiprocessing. Additional steps enable advanced post-processing capabilities.

## Authors

- Ahmad Saleem Nouman [A]
- Manuel de-Borja-Torrejon [A]
- Paulo Danzer [A]
- Zhengjie You [B]

## Affiliations

- **[A]** Chair of Building Technology and Climate Responsive Design, Technical University of Munich, Munich, Germany
- **[B]** Professorship of Energy Management Technologies, Technical University of Munich, Munich, Germany

## License

This work is licensed under a Creative Commons Attribution 4.0 International License.

### Creative Commons Attribution 4.0 International (CC BY 4.0)

You are free to:

- **Share** — copy and redistribute the material in any medium or format
- **Adapt** — remix, transform, and build upon the material for any purpose, even commercially.

Under the following terms:

- **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

#### More Information

For more information, please visit [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

## Citation

If you use the contents of this repository for your research, please cite the following paper:

1. 
   - **Title**: Cost-effective CO2 abatement in residential heating: A district-level analysis of heat pump deployment
   - **Authors**: Zhengjie You, Manuel de-Borja-Torrejon, Paulo Danzer, Ahmad Nouman, Claudia Hemmerle, Peter Tzscheutschler, Christoph Goebel
   - **Journal**: Energy & Buildings
   - **Year**: 2023
   - **DOI**: [10.1016/j.enbuild.2023.113644](https://doi.org/10.1016/j.enbuild.2023.113644)

## Contact Information

For any further queries or clarifications, feel free to reach out:
- Ahmad Saleem Nouman [ahmad.nouman@tum.de]

