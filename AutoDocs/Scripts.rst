CleanMod
========

.. figure:: /_images/image0.png
   :alt: CleanMod Overview
   :align: center
   :scale: 80%

Authors
-------
- Ahmad Saleem Nouman [A]
- Manuel de-Borja-Torrejon [A]
- Paulo Danzer [A]
- Zhengjie You [B]

Affiliations
------------
[A] Chair of Building Technology and Climate Responsive Design, Technical University of Munich, Munich, Germany

[B] Professorship of Energy Management Technologies, Technical University of Munich, Munich, Germany

Introduction
------------
Welcome to CleanMod, an advanced simulation tool designed for the strategic planning of neighborhoods transitioning towards sustainable and energy-efficient futures. CleanMod serves as an integral solution in decision-making, focusing not only on optimizing energy consumption but also on implementing eco-friendly and intelligent energy strategies. The tool transcends the mere integration of advanced technologies or sustainability trends, emphasizing the holistic operability and synergy of neighborhood systems.

Description
-----------
CleanMod is an advanced, adaptable simulation suite developed to analyze diverse neighborhood types such as: urban, suburban, and rural. Positioned within the ``CleanMod/Models`` directory (e.g. suburban), these comprehensive models serve as foundational blueprints for investigating various environmental factors and neigborhood renovation strategies, each tailored to meet distinct project aims. CleanMod offers unparalleled flexibility, enabling users to adapt existing district neighborhood models or entirely re-envision them, aligning with specific simulation necessities and analytical ambitions. This process, however, necessitates a thorough preparation of building data and the development of accurate thermal models for the buildings.

To prepare new neighborhood models, the users can use tools like Grasshopper integrated within Rhinoceros 3D or Autodesk Revit. These applications facilitate the precise definition of geometric parameters for neighborhood buildings, encompassing zones, floors, and window areas, which are crucial for subsequent computations in CleanMod. Additionally, the user must empoloy Ladybug tools (including Honeybee and Radiance) for assessing solar radiation and generating rated PV production data. Building component specifications can be sourced from the TABULA library encompassing the physical attributes of walls, windows, ceilings, floors, and intricate layering details.

The suite evaluates a wide range of factors, encompassing both micro and macro aspects of a neighborhood. This includes, as mentioned, individual dwelling components like walls, ceilings, floors, windows, and other architectural features, as well as broader elements such as building infrastructure, heating systems, solar panel deployment, and electric vehicle adoption. The scalability of Photovoltaics, covering district levels from minimal to maximal deployment, is also a key feature. The model in ``CleanMod/Models`` folder includes these details for suburban neighborhoods for evaluations under various hypothetical scenarios to assess the impact of proposed modifications. Further neighborhoods like urban, rural, or any model can be created by advanced-user specificaitons following the Grasshopper workflow in the folder ``CleanMod/GEO_RAD_PV``.

CleanMod is built in Python, leveraging the Gurobi optimizer through the Pyomo Solver. This optimization targets various goals, including maintaining set-point room temperatures, reducing electrical peak loads, minimizing CO2 emissions, and cutting overall energy costs. It employs an RC-model for district components, considering user presence and behavior in residential zones.

Input/Output Folders
--------------------
- **Input Folder**: This folder is a crucial for conducting simulations in CleanMod. It should contain the ``UEP_output`` and ``GH_output`` folders and a ``Scenarios.xlsx`` file. The ``Scenarios.xlsx`` file is specifically formatted to outline various simulation scenarios, which can differ based on factors such as geographic location, specific year, types of building renovations, energy management approaches, electricity pricing structures, and densities of electric cars (eCars).

CleanMod utilizes the Residential Electricity Demand Model (REM) and DHWcalc tools, part of the UrbanEnergyPro (UEP) suite, for the generation of hourly time series data relating to electrical demand and internal gains. These tools mainly focus on aspects like occupancy patterns, active electrical loads, lighting requirements, and Domestic Hot Water (DHW) consumption. The output from REM, essential for the CleanMod simulations, is typically found in the ``CleanMod/Models/model name/Input/UEP_output`` folder.

These time series data are automatically generated after CleanMod is executed, following the configurations specified in the ``CleanMod/Step_0_config.yaml`` file. The simulations also depend on additional inputs like EnergyPlus Weather (.epw) files (which are tailored to the Potsdam region for the years 2020, 2030, and 2045 for our models). Photovoltaic (PV) energy data, conforming to the format detailed in the ``Model/…./Input/GH_output`` folder, is also a necessary input for the simulations.

- **Output Folder**: Created post-simulation, it includes an ``Input_created`` folder with files needed for the simulations and a timestamped ``output/Results`` folder for each scenario run. The results encompass hourly timeseries calculations for buildings and districts, as well as annual district summaries which can be analyzed, compared, and graphically represented using postporcessing tools available within this suite.

Setting Up CleanMod
-------------------
Before diving into simulations, ensure that CleanMod is properly set up by installing necessary dependencies and configuring your IDE to recognize the correct source folders.

Installing Dependencies
-----------------------
To install the required dependencies for CleanMod, follow these steps:

- Open your terminal.
- Navigate to the CleanMod directory.
- Run the following command:

  .. code::

     pip install -r Cleanvelope/CleanMod/requirements.txt

This command installs all the packages listed in the ``requirements.txt`` file located in the ``Cleanvelope/CleanMod`` directory.

Configuring Your IDE
--------------------
For PyCharm users:

- Right-click on the 'CleanMod' folder.
- Select 'Mark Directory as'.
- Choose 'Sources Root'.

This action sets the 'CleanMod' folder as the source directory, making it easier for PyCharm to locate packages for import statements. The folder color will change to blue, indicating the change.

For users of other IDEs, ensure that the 'CleanMod' folder is added to the PYTHONPATH.

Requirements for Simulating with CleanMod
-----------------------------------------
Successful simulations with CleanMod require specific contents in the ``CleanMod/Models/model name/Input`` folder, including:

- ``UEP_output`` and ``GH_output`` folders.
- ``Scenarios.xlsx`` file, formatted for defining simulation scenarios.
- Necessary weather files and other inputs as detailed in the ``CleanMod/Models/Input`` folder.
- The ``CleanMod/Libraries`` folder contains critical files for simulation. If you modify these files, maintain the original naming conventions and content structure to ensure consistent and functional simulations.

Running CleanMod
================
This section provides detailed instructions for a quick test run and a comprehensive main run of CleanMod simulations.

Quick Test Run
--------------
To perform a shorter simulation with CleanMod, follow these steps:

#. **Prepare Input Data**: Ensure the input data folders for your neighborhood models contain all necessary files, following the pattern in ``CleanMod/Models/Input``.

#. **Configure Model and Scenarios**: In ``Step_0_config.yaml``, set ``name_model`` to ``suburban`` and ``scenarios`` to ``[S0]``.

#. **Set Up Input Directory**: Activate the creation of a new ``Input_created`` folder by setting ``complete_building_csv_files`` and ``complete_input_created_exists`` to ``false``.

#. **Adjust Simulation Parameters**: For a 10-day simulation, set ``reduce_to_1_building_with_2_zones`` to ``true`` and ``endhour`` to ``240``.

#. **Run the Simulation**: Execute ``Step_1_runme.py``. The first run may take longer due to data preprocessing.

#. **Review Results**: Find the simulation results in ``Models/suburban/Output/Results_%time_stamp%``.

#. **Visualize Results**: Use ``Step_7_analyser.py`` in the ``Post-processing`` folder for result analysis and visualization.

Main Run with Multi-Processing
------------------------------
For a full-scale simulation across various scenarios, follow these steps:

#. **Configure Parameters**: In ``Step_0_config.yaml``, adjust parameters like ``name_model``, ``endhour``, ``precalc_days``, ``scenarios``, ``complete_building_csv_files``, and ``complete_input_created_exists``.

#. **Execute with Multi-Processing**: Start ``Step_2_multi_processing.py``, setting ``num_cores`` according to your CPU.

#. **Generate Summary Tables**: Use ``Step_9_show_res_table.py`` in the ``Post-processing`` folder.

#. **Analyze Individual Scenarios**: Apply the plotting function from the Quick Test Run section for detailed scenario analysis.

.. image:: /_images/image4.PNG
   :alt: Image 4
   :caption: A sample from the output data

.. figure:: /_images/suburban_1.png
   :alt: CleanMod Overview
   :align: center
   :scale: 30%
   :caption: Graphical Analysis of the different scenarios

.. figure:: /_images/suburban_2.png
   :alt: CleanMod Overview
   :align: center
   :scale: 80%
   :caption: Graphical Analysis of the different scenarios

Tips
----
Ensure to read through the steps from ``Step_0_config`` to ``Step_2_multi_processing`` in the ``CleanMod`` folder and further steps in ``CleanMod/Post-processing``. Always start simulations with ``Step_0_config & Step_1_runme`` or ``Step_0_config & Step_2_multi_processing`` for multiprocessing. Additional steps enable advanced post-processing capabilities.

Scripts Package
===============

Scenario Generator Module
-------------------------
A crucial part of the ``scenario_generator`` , is purposefully designed to simulate various model scenarios. This class forms an integral part of the simulation process, enabling us to create, configure, and process a variety of scenarios for different use cases.

Parameters
^^^^^^^^^^^
- ``scenarios_df``
- ``folder_buildings_scenarios``
- ``folder_gh_output``
- ``file_geometry_zones``
- ``file_building_variant_compositions``
- ``scenario_objects``
- ``complete_building_csv_files``
- ``export``

Each of these parameters holds important information related to the scenarios being simulated, their location, composition, related files, and more.

Key Methods
^^^^^^^^^^^
- The ``configure_scenario`` function runs all class methods to assign a value to each object attribute.
- The ``create_buildings_df`` method creates a data frame for buildings, exports it as a CSV, and saves it in the object attribute.

Core Features
^^^^^^^^^^^^^^^
- The ``load_parameter_values`` method extracts the values of each parameter from the ``Scenarios.xlsx`` file and assigns them to the object attributes.
- The ``load_parent_renovation_scenario`` function loads the object associated with the parent renovation scenario.

Input/Output Handlers
^^^^^^^^^^^^^^^^^^^^^^
- The ``load_scenario_epw_file`` function retrieves the path of the EPW file related to the scenario to be simulated.
- The ``load_scenario_gh_file`` method retrieves the path of the GH file associated with the scenario to be simulated.

Input Preparation Module
-------------------------
The **input_preparation module** serves a crucial role in the Urban Energy Pro model, offering the necessary functions to prepare and handle input data appropriately for energy model computations. This module effectively functions as a data pre-processing and management tool for the model.

Functions
^^^^^^^^^
- ``input_preparation.building_dict(file)``
    Description: This function creates a dictionary of buildings which is used as an index for dataframe generations and as building identifier in Urban Energy Pro.
    Input: a file containing the buildings data.
    Output: a dictionary of buildings.

- ``input_preparation.calc_internal_gains_one_zone(zid, zones_df, occupancy_df, gains_lighting_df, active_load_df, weather_data)``
    Description: Function to calculate internal gains resulting from persons (based on occupancy), lighting and devices.
    Input: Zone ID, DataFrame containing the zones’ data, DataFrame containing the occupancy data, DataFrame containing the lighting gains data, DataFrame containing the active load data, Pandas Series containing weather data.
    Output: DataFrame containing the calculated internal gains data.

- ``input_preparation.clean_existing_folders(folder_path)``
    Description: This function removes the referenced folder in order to remove all its contents and then recreates another folder with the same name for new files.
    Input: paths of the folders to be cleaned.
    Output: None.

- ``input_preparation.create_building_component_data_per_zone(file_geometry_surfaces, file_building_component_compositions, file_building_variant_compositions, buildings_df, zone_df, window_types_df)``
    Description: This function creates zonewise building component data by combining geometry with material composition. The material composition selection is based on the energy standard and size-class of the building.
    Input: Name of the input CSV from the Grasshopper script with surfaces, Name of the input CSV with building component composition typology, Name of the input CSV of building variant compositions, Enriched buildings data, Enriched zones data, Window types.
    Output: A dictionary with zoneids as keys and dataframes with zone’s components as values.

- ``input_preparation.create_building_data(file_buildings, file_building_variant_compositions, file_geometry_surfaces, bid_dict, weather_data)``
    Description: Create dataframe of buildings from given csv and enrich with default data necessary for UEP.
    Input: filepath of raw input csv of buildings, filepath of input csv of building variant compositions.
    Output: dataframe with enriched building information.

- ``input_preparation.create_df_from_matlab_output(zid_list, profile_mat, name_profile, unit_conversion=False)``
    Description: Converts Matlab output of building profiles to a pandas DataFrame.
    Input: List of zone IDs, Path of the Matlab output file, Name of the profile to extract, Whether to convert units from kW to W.
    Output: A pandas DataFrame containing the building profiles for each zone.

UEP Interface Module
--------------------
The **interface_UEP module** provides the necessary functions to calculate building attributes, generate building data in a format compatible with UEP, and create files matching the UEP format of Syncity.

Functions
^^^^^^^^^
- ``interface_UEP.calc_and_create_syncity(buildings_df, buildingcomponent_df_dict, zones_df, uep_region_name, folder, file_dhw)``
    Description: Calculates missing building attributes and creates file with building data matching the UEP format of syncity.
    Parameters: buildings_df, buildingcomponent_df_dict, zones_df, uep_region_name, folder, file_dhw
    Returns: Nothing.

- ``interface_UEP.calc_average_dwelling_size(row_values)``
    Description: Calculates average dwelling_size by dividing storey_area by number of dwellings.
    Parameters: row_values
    Returns: Average dwelling size.

- ``interface_UEP.calc_dhw_parameters(heated_area, dhw_demand_file)``
    Description: Calculates parameters for energy demand resulting from domestic hot water. Procedure copied from UEP and remodelled within one single function.
    Parameters: heated_area, dhw_demand_file
    Returns: daily hot water demand and tank capacity of respective building.

- ``interface_UEP.calc_heated_area(building_code, zones_data)``
    Description: Calculates heated area of building as sum of heated area of zones.
    Parameters: building_code, zones_data
    Returns: Heated area of the building.

- ``interface_UEP.calc_params_dependent_on_buildingcomponents(building_code, building_component_data_dict, zones_data)``
    Description: Calculates parameters for 1R1C model of UEP incl. refurbishment levels.
    Parameters: building_code, building_component_data_dict, zones_data
    Returns: window_area, ref_levels, U, V, C, Tau.

- ``interface_UEP.create_synhouseholds(zones_df, uep_region_name, folder)``
    Description: Creates file with household data matching the UEP format of synhouseholds.
    Parameters: zones_df, uep_region_name, folder
    Returns: Nothing.

District Module
---------------
A crucial part of the ``class District`` namespace, the **district module** is designed to represent a district in the simulation model. It encapsulates all the buildings within the district and their related information and functionalities.

Parameters
^^^^^^^^^^^
- ``config``
- ``buildingids``
- ``zoneids``
- ``time``
- ``input_data_loc``
- ``nr_load``
- ``file_pv_potentials``
- ``dir_dhw``
- ``scenario_object``
- ``scenario_id``
- ``calc_type``
- ``j``
- ``file_geometry_surfaces``
- ``renovation_costs_df``

Key Methods
^^^^^^^^^^^
- The ``create_buildings_with_zones`` method forms the buildings with their respective zones.
- The ``create_dict_with_indexed_param`` function creates a dictionary from a raw numpy array using multiple keys.
- The ``create_pyomo_model`` function constructs a Pyomo optimization model to compute the optimized schedule.

Core Features
^^^^^^^^^^^^^^^
- The ``create_generator_info`` method generates information regarding the district.
- The ``prepare_non_resi`` function prepares non-residential load profiles.
- The ``select_hh_load_profile_uep`` method selects household load profiles.

Input/Output Handlers
^^^^^^^^^^^^^^^^^^^^^^
- The ``simulate`` method creates optimal schedules for the district and returns results.
- The ``solve_model`` function solves the optimization model multiple times according to the Model Predictive Control (MPC) approach.

Building Module
----------------
Nested within the ``class Building`` namespace, the **building module** incorporates an expansive range of functionalities for the computational modeling of various building systems. This class plays a pivotal role, serving as a tool for simulating and examining a variety of building attributes and performance metrics.

Parameters
^^^^^^^^^^^
The module is structured around a multitude of parameters:

- ``config``
- ``building_code``
- ``building_data``
- ``zones_data``
- ``time``
- ``input_data_loc``
- ``file_pv_potentials``
- ``dir_dhw``
- ``scenario_object``
- ``file_geometry_surfaces``
- ``district_zones_count``
- ``district_zones_nr``
- ``district_buildings_count``
- ``district_buildings_nr``
- ``renovation_costs_df``

Each of these parameters carries vital information related to the specific building or the district in which it is located.

Key Methods
^^^^^^^^^^^
- The ``assign_dhw_load_profile`` function assigns domestic hot water (DHW) load profiles, which provide insights into the energy consumption for heating water in the building.
- The ``calculate_cost_hp`` and ``calculate_cost_pv`` methods compute the costs linked to heat pumps (HP) and photovoltaics (PV), taking into account factors like design heating load and PV area.

Core Features
^^^^^^^^^^^^^^^
- The ``create_zones`` function identifies distinct zones within a building using parameters such as zones data, time input, and PV potentials, among others, to establish a comprehensive structural representation of the building.
- The ``calculate_storey_area`` method measures the area of each floor, providing an additional layer of detail in the building analysis.

Aggregate Values
^^^^^^^^^^^^^^^^^
In addition, the module also includes methods for determining aggregate values across zones:

- The total PV area is calculated by the ``sum_pv_areas`` method.
- The aggregate renovation costs for construction are computed by the ``sum_renovation_costs_construction`` method.
- The cumulative sum of specific zone attributes over time is found using the ``sum_zone_values`` method.

Building Component Module
-------------------------
The **building component module** is a fundamental part of the ``class BuildingComponent`` namespace. The module is used to model the thermal characteristics of different building components in the simulation model. This includes various parameters such as thickness, conductivity, density, and heat capacity.

Parameters
^^^^^^^^^^
- ``name``
- ``int_ext``
- ``component_type``
- ``thickness``
- ``conductivity``
- ``density``
- ``capacity``
- ``u_value``
- ``sw``
- ``lw``
- ``alpha_int``
- ``alpha_ext``
- ``area``
- ``orient``
- ``inclin``
- ``surf_id``

Key Methods
^^^^^^^^^^^^
.. function:: calc_3R2C_helper(acm_wbt, Area, T, Thickness, Conductivity)

   Calculates the 3R2C thermal model over a specified time period (2 or 7 days).

.. function:: create_ComponentComposition(int_ext, component_type, thickness, conductivity, density, capacity, u_value, sw, lw, alpha_int, alpha_ext)

   Creates a component composition.

Core Features
^^^^^^^^^^^^^^
- The ability to define and manage the thermal properties of various building components.
- Computation of complex thermal models such as the 3R2C model.

Code
----
This section provides an overview of the code for each module used in the project. Each module serves a specific function within the system, and their code can be viewed in detail here

Scenario Generator Module Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: scenario_generator
   :members:
   :undoc-members:
   :show-inheritance:

Input Preparation Module Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: input_preparation
   :members:
   :undoc-members:
   :show-inheritance:

UEP Interface Module Code
^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: interface_UEP
   :members:
   :undoc-members:
   :show-inheritance:

District Module Code
^^^^^^^^^^^^^^^^^^^^
.. automodule:: district
   :members:
   :undoc-members:
   :show-inheritance:

Building Module Code
^^^^^^^^^^^^^^^^^^^^
.. automodule:: building
   :members:
   :undoc-members:
   :show-inheritance:

Building Component Module Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: building_component
   :members:
   :undoc-members:
   :show-inheritance:

Weather Module Code
^^^^^^^^^^^^^^^^^^^
.. automodule:: weather
   :members:
   :undoc-members:
   :show-inheritance:

Window Module Code
^^^^^^^^^^^^^^^^^^
.. automodule:: window
   :members:
   :undoc-members:
   :show-inheritance:

Zone Module Code
^^^^^^^^^^^^^^^^
.. automodule:: zone
   :members:
   :undoc-members:
   :show-inheritance:

CHP Module Code
^^^^^^^^^^^^^^^
.. automodule:: chp
   :members:
   :undoc-members:
   :show-inheritance:

Electric Vehicle Module Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: electric_vehicle
   :members:
   :undoc-members:
   :show-inheritance:

Heat Pump Module Code
^^^^^^^^^^^^^^^^^^^^^
.. automodule:: heatpump
   :members:
   :undoc-members:
   :show-inheritance:
   :noindex:

Power Grid Module Code
^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: powergrid
   :members:
   :undoc-members:
   :show-inheritance:

Time Module Code
^^^^^^^^^^^^^^^^
.. automodule:: time
   :members:
   :undoc-members:
   :show-inheritance:

Non-Residential Component Module Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. figure:: /_images/Capture2.png
   :alt: Non-Residential Component Calculation
   :align: center
   :scale: 45%

   A Summary of Non-Residential Component

.. automodule:: non_residential_load_Simplified
   :members:
   :undoc-members:
   :show-inheritance:

Progress Bar Module Code
^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: progress_bar
   :members:
   :undoc-members:
   :show-inheritance:

Scenario Plotter Module Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: scenario_plotter
   :members:
   :undoc-members:
   :show-inheritance:

Sim Power Grid Module Code
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: sim_powergrid
   :members:
   :undoc-members:
   :show-inheritance:

Util Module Code
^^^^^^^^^^^^^^^^
.. automodule:: util
   :members:
   :undoc-members:
   :show-inheritance:

License
-------

This work is licensed under a Creative Commons Attribution 4.0 International License.

Creative Commons Attribution 4.0 International (CC BY 4.0)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You are free to:

- **Share** — copy and redistribute the material in any medium or format
- **Adapt** — remix, transform, and build upon the material for any purpose, even commercially.

Under the following terms:

- **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

More Information
^^^^^^^^^^^^^^^^

For more information, please visit `Creative Commons Attribution 4.0 International License <https://creativecommons.org/licenses/by/4.0/>`_.

Citation
--------
If you use the contents of this repository for your research, please cite the following papers:

1.
- **Title**: Cost-effective CO2 abatement in residential heating: A district-level analysis of heat pump deployment
- **Authors**: Zhengjie You, Manuel de-Borja-Torrejon, Paulo Danzer, Ahmad Nouman, Claudia Hemmerle, Peter Tzscheutschler, Christoph Goebel
- **Journal**: Energy & Buildings
- **Year**: 2023
- **DOI**: [10.1016/j.enbuild.2023.113644](https://doi.org/10.1016/j.enbuild.2023.113644)

Contact Information
-------------------
For any further queries or clarifications, feel free to reach out:
Ahmad Saleem Nouman [ahmad.nouman@tum.de]