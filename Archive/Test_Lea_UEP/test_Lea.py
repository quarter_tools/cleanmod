import os, sys, csv
import numpy as np
import pandas as pd
from datetime import datetime
from random import randint
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
from scipy.spatial.distance import squareform, pdist
from scipy.io import loadmat
import copy
import UrbanHeatPro.Classes.Simulation


def read_saved_syn_file_to_pd_df(syn_unit_filename, unit_type_building=True):
    """
    Reads existing syn city file
    """
    if unit_type_building:
        name = "synthetic buildings"
    else:
        name = "synthetic households"
    # Building data
    syn_units_df = pd.read_csv(syn_unit_filename, delimiter=";", skiprows=1)

    return syn_units_df

if __name__ == '__main__':
    syn_households_df = read_saved_syn_file_to_pd_df('SynHouseholds_Unterhaching_0.csv',
                                                          unit_type_building=False)

    household_size_list = list(syn_households_df['number_occupants'])

    household_size_distr = {}

    for i in range(1, 6):
        household_size_distr[i] = household_size_list.count(i)

    active_load_per_minute_one_year_each_household = loadmat('active_load_Feldmoching.mat')['active_load']

    active_load_sorted_hhsize = {}
    position = 0
    for i in range(1, 6):
        active_load_sorted_hhsize[i] = active_load_per_minute_one_year_each_household[position:position+household_size_distr[i]]
        position += household_size_distr[i]

    active_load_per_minute_one_year_each_household_sorted = np.empty_like(active_load_per_minute_one_year_each_household)

    counter_dict = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}
    for i, hhsize in enumerate(household_size_list):
        active_load_per_minute_one_year_each_household_sorted[i] = active_load_sorted_hhsize[hhsize][counter_dict[hhsize]]
        counter_dict[hhsize] += 1

    annual_load_hh = active_load_per_minute_one_year_each_household.sum(axis=1)*1/60
    print(annual_load_hh)

    annual_load_hh_sorted = active_load_per_minute_one_year_each_household_sorted.sum(axis=1) * 1 / 60
    print(annual_load_hh_sorted)

    syn_households_df["active_load_minutely"] = list(active_load_per_minute_one_year_each_household)

    #syn_households_df = pd.DataFrame
    #syn_households_df["active_load_minutely"] = list(active_load_per_minute_one_year_each_household)

    occupancy = loadmat('occupancy_Feldmoching.mat')['occupancy']
    print(occupancy)
    #annual_occupancy_hh.sum(axis=1) * 1 / 60
    #household_category = (occupancy_per_minute_one_year_each_household != 0).argmax(axis=1)
    hc = []
    for i in occupancy:
        hc.append(i[0])
    print(hc)

    allbuildings = annual_load_hh_sorted.sum()
    print(allbuildings)

    # initialize dictionaries for BID and corresponding hourly occupancy and hourly active load
    list_BID_residential = list(set(syn_households_df.bid))

    dict_bid_occupancy_hourly = {}
    dict_bid_active_load_hourly = {}

    load_annual_per_hh = []

    for current_bid in list_BID_residential:
        df_current_building_only = syn_households_df.loc[syn_households_df.bid == current_bid]

        # mean occupancy for each hour and building
        '''list_of_lists_current_building_hh_occupancies = list(df_current_building_only.occupancy_list_minutely.tolist())
        list_occupancy_each_minute_current_building = np.sum(list_of_lists_current_building_hh_occupancies, axis=0)
        list_occupancy_each_hour_current_building = np.mean(
            list_occupancy_each_minute_current_building.reshape(-1, config.resolution), axis=1)
        rounded_occupancy_each_hour_building = ['%.f' % elem for elem in list_occupancy_each_hour_current_building]

        dict_bid_occupancy_hourly[current_bid] = list(rounded_occupancy_each_hour_building)'''

        # add active load for each building - sum for the hour
        list_of_lists_current_building_hh_loads = list(df_current_building_only.active_load_minutely.tolist())
        active_load_each_minute_building = np.sum(list_of_lists_current_building_hh_loads, axis=0)
        active_load_each_hour_building = 1/60 * np.add.reduceat(active_load_each_minute_building,
                                                         np.arange(0, len(active_load_each_minute_building), 60))
        dict_bid_active_load_hourly[current_bid] = list(active_load_each_hour_building)
        load_annual_per_hh.append(active_load_each_hour_building.sum())
