Subroutine Type15  
!-----------------------------------------------------------------------------------------------------------------------
! This Type reads and interpolates weather data from TMY, TMY2, TMY3, IWEC, CWEC and EnergyPlus formatted
! data files. The data reading modes are as follows:
!
! Mode 1: TMY format  
! Mode 2: TMY2 format
! Mode 3: EnergyPlus format
! Mode 4: IWEC (International Weather for Energy Calculations)
! Mode 5: CWEC (Canadian Weather for Energy Calculations)
! Mode 6: METEONORM (TMY2 format)
! Mode 7: TMY3 format
! Mode 8: German TRY 2004 format
! Mode 9: German TRY 2010 format
!
!     LAST MODIFIED:
!       11/04 -- JWT - Re-coding from Types 89 and 16
!       12/04 -- JWT - Fixed the OCHECK declaration
!       03/04 -- JWT - Sky cover multipliers were wrong got CWEC and EnergyPlus formats
!       02/06 -- JWT - Fixed a bug in tracking modes 2 and 3 where the axis slope was not being reset.
!       02/06 -- JWT - Added the capability to take the slope and azimuth data as inputs to the model.
!       10/06 -- DEB/DAA - Opening a file with status 'NEW' created a conflict when the file already
!                      exists. Modified the opening status to 'UNKNOWN'.
!       09/07 -- JWT - Fixed the multipliers for illuminance and optical depth for the TMY-2 mode
!       09/07 -- JWT - Fixed the multipliers for wind velocity in the Energy+ mode
!       09/07 -- JWT - Fixed the high incidence angle check from 79.9 to 89.9 degrees.
!       03/08 -- DEB - Many Meteonorm files have a 999 in the snow depth field to indicate that there is no data. This 
!                       was causing the "snow covered" ground reflectance to be set all year.
!       04/08 -- JWT - Added the capability to read TMY3 files
!       06/09 -- TPM - fixed a small bug in the calculation of the effective sky temperature
!       06/09 -- TPM - increased the limit on the zenith angle from 87.5 degrees to 89.5 degrees
!       06/09 -- TPM - conversion to TRNSYS 17 coding standard
!       04/10 -- DEB - bugfix: if mode==5 instead of if mode==.5
!       06/10 -- TPM - bugfix to eosky equation (1000.d0 pressure offset instead of 1013.25d0)
!       07/10 -- TPM - bugfix to TRY mode (READ statement went the wrong place on END)
!       09/10 -- TPM - add calls to setRadiationData() for components that will get their solar data without user 
!                       connections in the input file.
!       03/11 -- MJD - bugfix calculating the minimum and maximum monthly average temperatures
!       05/11 -- CCF - Added TRY 2010 Format in mode 9 and fixed sky temp calculation for that mode
!       10/11 -- DEB - corrected units code for output 10 (pressure is in atm, not bar)
!       11/11 -- MJD - corrected the effective sky temperature to be a function of oqaque sky cover (not total)
!       11/11 -- DEB - handled ierror_rad = 15.
!       01/12 -- CFF - implemented calculation of cc variable for sky temp in modes 8 and 9 (TRY '04 and '10)
!       04/13 -- TPM - updated the mains water temperature calculation to the latest equations from NREL
!       02/14 -- TPM - bugfix: in TMY3 mode ground reflectance read from the file greater than 1 was not set to the 
!                       parameter value
!       02/14 -- TPM - bugfix: the variable xday in the mains water tempature calculation was not being set
!       05/14 -- JWT - bugfix: tracking mode 5 (slope and azimuth inputs) was not being set correctly with multiple 
!                       instances of Type15.
!       01/15 -- DEB - Added SSR code
!       07/15 -- TPM - changed the solar radiation calculations to use direct normal and horizontal diffuse radiation
!       08/16 -- TPM - modifed to read one more set of data so the solar radiation can be interpolated for sub-hourly timesteps
!       10/16 -- TPM - added calls to interpolation routine to dertmine sub-hourly values of horizontal diffsue and direct normal
!       10/16 -- TPM - changed radiation calls to the new GetHorizontalRadiation and GetTiltedRadiation routines
!       11/16 -- TPM - fixed an issue where n_items was not reset for types of different modes in EndofTimestep and LastCall routines
!    
!     REFERENCES:
!      1) Martin M.,Berdahl P., Characteristics of infrared sky radiation in the U.S.A.,Solar Energy, vol.33, No 3/4,p.321,1984
!      2) Walton G.,TARP Reference Manual,NBSIR 83-2655,1983
!-----------------------------------------------------------------------------------------------------------------------
! Copyright © 2015 Thermal Energy System Specialists, LLC. All rights reserved.

!export this subroutine for its use in external DLLs.
!DEC$ATTRIBUTES DLLEXPORT :: TYPE15

!-----------------------------------------------------------------------------------------------------------------------
 Use TrnsysConstants
 Use TrnsysFunctions 
!-----------------------------------------------------------------------------------------------------------------------

!Variable Declarations
Implicit None !force explicit declaration of local variables
Double Precision Time,Timestep
Integer nitems_max,ns_max,nout
Parameter (ns_max=99,nitems_max=36,nout=52+8*ns_max)                  

Integer i,j,k,j_month,j_spot,j_min,j_max,itemp1,itemp2,j_spot_I,iSlashSpot     !temporary variables
Integer lu                     !a variable for opening a temporary file
Integer luw                    !the logical unit for the list file
Integer commacntr                   
Integer ie                     !an indicator that there was an error in Psych
Integer ie_read                !an indicator that there was a data reading error
Integer n_items                !number of columns to be read from the data file
Integer headerskip             !number of header lines at top of the data file
Integer iskip                  !number of extra lines to skip if skip is on
Integer interp(nitems_max)     !indicator for interpolation for each value
Integer lu_data                !the logical unit for the data file
Integer site
Integer i_solartime            !1=data is in local time, -1=data in solar time
Integer ierror_rad             !an indicator if the radiation processor found problems
Integer mode                   !indicated which type of data file is to be read (1=TMY,2=TMY2,3=ENERGY+,4=IWEC,5=CWEC,6=METEONORM,7=TMY3)
Integer time_stamp(nitems_max) !the time base for the variable (0=average value reported at end of range, 1=instantaneous value at end of range)
Integer wmoregion,tmznhr,tmznmn
Integer ns                     !number of surfaces for radiation processing
Integer rad_mode               !diffuse radiation model (1=isotropic sky,2=Hay and Davies model,3=Reindl model,4=Perez model)
Integer shape_mode             !whether the radiation routines should shape the solar radiation or is it done by outside routines (0=don't shape;1=shape)
Integer track_mode(ns_max)     !tracking mode for the surface (1=fixed surface,2=one-axis tracking: fixed slope and variable azimuth,3=one-axis tracking: rotation abot a fixed axis,4=two-axis tracking)
Integer mode_horiz             !horizontal radiation calculation mode (always = 4 for weather files) 
Integer SPar                   !the starting parameter for the surface descriptions 
Integer nTRYreg                !the region number for the TRY file
Integer nTRYmon                !the month of the current read from the TRY file
Integer nerr                   !count of number of warnings during simulation from solar radiation routines
Integer CurrentUnit,CurrentType !the current unit and type number of the component

Character(LEN=MaxMessageLength) Message1,Message2,Message3,Message4,Message,Message6,Message7,Message8    !error and warning messages from this type
Character*1 tmzndir,latdir,elevdir
Character*22 City                   !the name of the city
Character*2 State                   !the name of the state
Character*1 longdir                 !indicates the direction of the longitude
Character*1 eplusheader(161)
Character*8 keyword
Character*22 source
Character*22 Country
Character*80 alphanum
Character*12 ICNTStr
      
Double Precision psydat(9)             !array holding Psych properties
Double Precision xmult(nitems_max)     !multiplier for read value
Double Precision xadd(nitems_max)      !addition factor for read value
Double Precision value_now(nitems_max) !the current value of the read variable
Double Precision value_next(nitems_max)!the next value of the read variable
Double Precision value_was(nitems_max) !the past value of the read variable
Double Precision value_future(nitems_max) !the future value of the read variable
Double Precision time_next             !the next time at which data should be read
Double Precision ratio                 !an iterpolation multiplier
Double Precision xnow                  !instantaneous value of function at end of current timestep
Double Precision xwas                  !instantaneous value of function at end of previous timestep
Double Precision xnext                 !instantaneous value of function at end of next timestep
Double Precision begin(nitems_max)     !the instantaneous value of the function at the beginning of the timestep
Double Precision ending(nitems_max)    !the instantaneous value of the function at the end of the timestep
Double Precision previous(nitems_max)  !the instantaneous value of the function at the end of the previoustimestep
Double Precision tmzonehr              !time zone for data city (in hours)
Double Precision tmzone                !time zone for data city
Double Precision latdeg                !latitude degrees for data city
Double Precision latmin                !latitude minutes for data city
Double Precision longdeg               !longitude degrees for data city
Double Precision longmin               !longitude minutes for data city
Double Precision lat                   !the latitude of data city
Double Precision long                  !the longitude of data city
Double Precision elev                  !elevation of data city
Double Precision shift                 !the shift in hour angle required to convert from LST to solar time
Double Precision rho                   !ground reflectance
Double Precision rhog                  !ground reflectance for non-snow covered periods
Double Precision rhog_snow             !ground reflectance for snow covered periods
Double Precision month_ave_T(0:13)     !the monthly average temperature
Double Precision month_min_T(0:13)     !the minimum monthly temperature
Double Precision month_max_T(0:13)     !the maximum monthly temperature
Double Precision annual_ave_T          !the annual average temperature
Double Precision annual_min_T          !the minimum annual temperature
Double Precision annual_max_T          !the maximum annual temperature
Double Precision annual_ave_I          !the annual average global horizontal irradiance
Double Precision length_month          !the length of each month in hours
Double Precision wdt_min               !critical minimum annual temperature for heating season
Double Precision T_ave_heat            !average monthly temperature below which heating if enabled for the month
Double Precision T_ave_cool            !average monthly temperature above which cooling if enabled for the month
Double Precision x_heat(0:13)          !heating season monthly indicator
Double Precision x_cool(0:13)          !cooling season monthly indicator
Double Precision x_swing(0:13)         !swing season temporary array
Double Precision offset,mains_ratio,xday,lag !used in the mains water calculation
Double Precision dt_max                !maximum monthly average temperature swing
Double Precision T_mains               !the mains water temperature
Double Precision td1                   !the time of the last data read
Double Precision td2                   !the time of the next data read
Double Precision slope(ns_max)         !slope of the surface
Double Precision axis_slope(ns_max)    !slope of the axis
Double Precision azimuth(ns_max)       !azimuth of the surface
Double Precision axis_azimuth(ns_max)  !azimuth of the axis
Double Precision ihd                   !global horizontal diffuse solar radiation
Double Precision idn                   !direct normal solar radiation
Double Precision solar(16)             !an array holding the solar radiation values
Double Precision ecld                  !emisivity of the clouds
Double Precision eosky,esky,tsky       !sky temperature variables
Double Precision xtemp1,xtemp2         !temporary variables
Double Precision out_temp(nout)        !a temporary output array required by the ordering of the outputs
Double Precision store(75+10*nitems_max)!storage array
Double Precision rad_input(10)         !array of values for radiation calculations
Double Precision SolConst              !solar constant
Double Precision TRY_T_Adj(14,15),TRY_h_Adj(14,15) !German TRY adjustment factors for T and h based on elevation

Double Precision Eglob                 !total radiation for sky coverage calculation
Double Precision E_dir                 !direct radiation for sky coverage calculation
Double Precision E_dif                 !diffuse radiation for sky coverage calculation
Double Precision cc					   !cloudiness cover
Double Precision help				   !help variable for cloudiness cover calculation

Double Precision StartTime, EndTime, HorNow, HorNext, HorStartP, HorEndP, HorMidP, HorPoint, IdnNow, IdnNext, IdnStartP, IdnEndP, IdnMidP, IdnPoint, time_rad       !values for sub-hourly interpolation of horizontal diffuse and direct normal
Integer HorSections, HorNum, HorMid, IdnSections, IdnNum, IdnMid
Logical RadErrors(8)  !log of warnings from solar radiation routines
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
Data SolConst /4871.d0/
Data TRY_T_Adj/7.0,    0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, &
               4.0,    0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, &
               13.0,   0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, &
               81.0, -0.89,-1.01,-1.24,-0.87,-0.84,-0.78,-0.88,-0.76,-0.94,-0.79,-0.76,-0.85,-0.88, &
               152.0,-0.78,-0.85,-1.05,-0.92,-0.85,-0.95,-0.93,-0.91,-0.91,-0.72,-0.69,-0.74,-0.86, &
               547.0,-1.77,-1.45,-1.10,-0.92,-0.85,-0.85,-0.94,-1.07,-1.28,-1.04,-0.95,-1.56,-1.15, &
               231.0,-0.92,-0.95,-1.04,-0.98,-0.88,-0.94,-0.89,-0.93,-1.02,-0.87,-0.74,-0.96,-0.93, &
               607.0,-1.54,-1.32,-1.09,-1.01,-1.08,-0.99,-1.04,-1.18,-1.32,-1.27,-1.10,-1.02,-1.16, &
               418.0,-1.13,-1.27,-1.32,-1.16,-1.04,-0.95,-0.87,-0.96,-1.15,-1.19,-1.06,-0.98,-1.09, &
               567.0,-1.63,-1.74,-1.56,-1.29,-1.00,-0.87,-0.74,-0.97,-1.09,-1.22,-1.20,-1.13,-1.20, &
               1213.0, 0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, &
               96.0, -0.98,-1.05,-1.05,-0.87,-0.77,-0.72,-0.81,-0.88,-0.94,-0.87,-0.99,-0.91,-0.90, &
               409.0,-1.38,-1.42,-1.28,-1.05,-0.82,-0.81,-0.75,-0.76,-0.83,-0.75,-0.99,-1.07,-0.99, &
               734.0, 0.43, 0.25,-0.08,-0.76,-0.82,-0.88,-1.01,-1.12,-0.35,-0.04, 0.00, 0.33,-0.34, &
               719.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0/
Data TRY_h_Adj/7.0,    0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, &
               4.0,    0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, &
               13.0,   0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, &
               81.0, -0.27,-0.34,-0.34,-0.22,-0.21,-0.23,-0.21,-0.18,-0.32,-0.25,-0.24,-0.28,-0.26, &
               152.0,-0.24,-0.28,-0.30,-0.27,-0.23,-0.26,-0.29,-0.27,-0.25,-0.23,-0.24,-0.19,-0.25, &
               547.0,-0.52,-0.44,-0.30,-0.30,-0.27,-0.28,-0.31,-0.37,-0.36,-0.29,-0.27,-0.51,-0.35, &
               231.0,-0.25,-0.23,-0.28,-0.26,-0.21,-0.23,-0.29,-0.27,-0.26,-0.28,-0.17,-0.30,-0.25, &
               607.0,-0.47,-0.40,-0.29,-0.34,-0.34,-0.25,-0.32,-0.39,-0.44,-0.41,-0.33,-0.30,-0.36, &
               418.0,-0.35,-0.37,-0.39,-0.38,-0.34,-0.24,-0.24,-0.27,-0.30,-0.33,-0.32,-0.27,-0.32, &
               567.0,-0.49,-0.59,-0.49,-0.38,-0.31,-0.21,-0.16,-0.27,-0.33,-0.40,-0.34,-0.33,-0.36, &
               1213.0, 0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, &
               96.0, -0.25,-0.35,-0.27,-0.28,-0.22,-0.20,-0.23,-0.20,-0.23,-0.25,-0.32,-0.29,-0.26, &
               409.0,-0.42,-0.40,-0.39,-0.30,-0.19,-0.26,-0.18,-0.17,-0.21,-0.16,-0.32,-0.30,-0.28, &
               734.0, 0.23, 0.11, 0.01,-0.19,-0.19,-0.25,-0.27,-0.34,-0.04, 0.05, 0.01, 0.12,-0.06, &
               719.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0/
Data Message1 /'Unable to correctly read from the supplied data file - please check the file and re-run the simulation.'/
Data Message2 /'End of file encountered'/
Data Message3 /'Total sky cover values for this time step were missing from the data file. The last read values were retained.'/
Data Message4 /'Opaque sky cover values for this time step were missing from the data file. The last read values were retained.'/
Data Message6 /'The calculated solar hour angle for the end of the data timestep has been modified from its original value.  This fix was added to account for sundown periods near midnight at high-latitude locations.  If this warning is noted for locations that do not meet this criteria, please contact your TRNSYS distributor.'/
Data Message7 /'The calculated value of the beam radiation exceeded the maximum possible beam radiation for this timestep.  The beam radiation has been set to the extraterrestrial radiation for this timestep.'/
Data Message8 /'Time inconsistency.'/
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Get the Global Trnsys Simulation Variables
 Time=getSimulationTime()
 Timestep=getSimulationTimeStep()
 luw=getListingFileLogicalUnit()
 CurrentUnit = getCurrentUnit()
 CurrentType = getCurrentType()
!-----------------------------------------------------------------------------------------------------------------------
 
!-----------------------------------------------------------------------------------------------------------------------
!Set the Version Number for This Type
 If(getIsVersionSigningTime()) Then
     Call SetTypeVersion(17)
     Return
 Endif
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the Last Call Manipulations Here
 If(getIsLastCallofSimulation()) Then
! Write count of times of errors
    mode = JFIX(getParameterValue(1)+0.5)
    If (mode==1) Then !TMY
       n_items = 36
    ElseIf ((mode==2).OR.(mode==6)) Then !TMY2
       n_items = 27
    ElseIf (mode==3) Then !ENERGY+
       n_items = 31
    ElseIf (mode==4) Then !IWEC
       n_items = 28
    ElseIf (mode==5) Then !CWEC
       n_items = 31
    ElseIf (mode==7) Then !TMY3
       n_items = 28
    ElseIf ((mode==8).OR.(mode==9)) Then !TRY 2004 or TRY 2010
       n_items = 19
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+1)+.5)
    If (nerr > 0) Then
       Write (ICNTStr,*) nerr
	   Message = 'The calculated horizontal beam radiation exceeded the horizontal extraterrestrial radiation and was set to the extraterrestrial for '//TRIM(ADJUSTL(ICNTStr))//' timesteps during the simulation.'
       Call Messages(-1,Message,'Warning',CurrentUnit,CurrentType)
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+2)+.5)
    If (nerr > 0) Then
       Write (ICNTStr,*) nerr
	   Message = 'The calculated horizontal diffuse radiation exceeded the horizontal extraterrestrial radiation and was set to the extraterrestrial for '//TRIM(ADJUSTL(ICNTStr))//' timesteps during the simulation.'
       Call Messages(-1,Message,'Warning',CurrentUnit,CurrentType)
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+3)+.5)
    If (nerr > 0) Then
       Write (ICNTStr,*) nerr
	   Message = 'The calculated total horizontal radiation exceeded the horizontal extraterrestrial radiation and was set to the extraterrestrial for '//TRIM(ADJUSTL(ICNTStr))//' timesteps during the simulation.'
       Call Messages(-1,Message,'Warning',CurrentUnit,CurrentType)
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+4)+.5)
    If (nerr > 0) Then
       Write (ICNTStr,*) nerr
	   Message = 'The total horizontal radiation calculated from the sum of the beam and diffuse horizontal radiation exceeded the horizontal extraterrestrial radiation and was set to the extraterrestrial. The beam and diffuse radiation on the horizontal were then calculated base on their original ratios for '//TRIM(ADJUSTL(ICNTStr))//' timesteps during the simulation.'
       Call Messages(-1,Message,'Warning',CurrentUnit,CurrentType)
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+5)+.5)
    If (nerr > 0) Then
       Write (ICNTStr,*) nerr
	   Message = 'The direct normal exceeded the Solar Constant and was set to the Solar Constant for '//TRIM(ADJUSTL(ICNTStr))//' timesteps during the simulation.'
       Call Messages(-1,Message,'Warning',CurrentUnit,CurrentType)
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+6)+.5)
    If (nerr > 0) Then
       Write (ICNTStr,*) nerr
	   Message = 'The calculated horizontal beam radiation exceeded the provided total horizontal radiation and was set to the total horizontal radiation for '//TRIM(ADJUSTL(ICNTStr))//' timesteps during the simulation.'
       Call Messages(-1,Message,'Warning',CurrentUnit,CurrentType)
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+7)+.5)
    If (nerr > 0) Then
       Write (ICNTStr,*) nerr
	   Message = 'The calculated horizontal beam radiation exceeded was less than zero and the horizontal diffuse radiation was set to the total horizontal radiation and the horizontal beam radiation was set to zero for '//TRIM(ADJUSTL(ICNTStr))//' timesteps during the simulation.'
       Call Messages(-1,Message,'Warning',CurrentUnit,CurrentType)
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+8)+.5)
    If (nerr > 0) Then
       Write (ICNTStr,*) nerr
	   Message = 'The weather data file contained positive solar radiation values when the sun was down for '//TRIM(ADJUSTL(ICNTStr))//' timesteps during the simulation.'
       Call Messages(-1,Message,'Warning',CurrentUnit,CurrentType)
    EndIf
    Return
 Endif
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Perform Any "End of Timestep" Manipulations That May Be Required
 If(getIsEndOfTimestep()) Then
    mode = JFIX(getParameterValue(1)+0.5)
    If (mode==1) Then !TMY
       n_items = 36
    ElseIf ((mode==2).OR.(mode==6)) Then !TMY2
       n_items = 27
    ElseIf (mode==3) Then !ENERGY+
       n_items = 31
    ElseIf (mode==4) Then !IWEC
       n_items = 28
    ElseIf (mode==5) Then !CWEC
       n_items = 31
    ElseIf (mode==7) Then !TMY3
       n_items = 28
    ElseIf ((mode==8).OR.(mode==9)) Then !TRY 2004 or TRY 2010
       n_items = 19
    EndIf
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+1)+.5)
    Do j = 1,8
       If (RadErrors(j)) Call SetStaticArrayValue(75+10*n_items+8+5+14+j, getStaticArrayValue(75+10*n_items+8+5+14+j) + 1.d0)
    EndDo
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+1)+.5)
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+2)+.5)
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+3)+.5)
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+4)+.5)
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+5)+.5)
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+6)+.5)
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+7)+.5)
    nerr = JFIX(getStaticArrayValue(75+10*n_items+8+5+14+8)+.5)
	Return
 Endif
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the "Very First Call of the Simulation Manipulations" Here
 If (getIsFirstCallofSimulation()) Then

! Set the mode of the data reader
    mode = JFIX(getParameterValue(1)+0.5)
    If ((mode < 1).or.(mode > 9)) Call FoundBadParameter(1,'Fatal','The weather data reader mode must be between 1 and 8.')

! Set the number of columns to be read from the data file
    If (mode==1) Then !TMY
       n_items = 36
    ElseIf ((mode==2).OR.(mode==6)) Then !TMY2
       n_items = 27
    ElseIf (mode==3) Then !ENERGY+
       n_items = 31
    ElseIf (mode==4) Then !IWEC
       n_items = 28
    ElseIf (mode==5) Then !CWEC
       n_items = 31
    ElseIf (mode==7) Then !TMY3
       n_items = 28
    ElseIf ((mode==8).OR.(mode==9)) Then !TRY 2004 or TRY 2010
       n_items = 19
    EndIf

! See how many surfaces are to be evaluated
    If ((mode /= 8).AND.(mode /= 9)) Then
       ns = JFIX(getParameterValue(6)+0.5)
       If ((ns <= 0).or.(ns > ns_max)) Call FoundBadParameter(6,'Fatal','The number of surfaces in the weather processor must be greater than 0 and less than the maximum number of surfaces allowed.')
       If (ErrorFound()) Return
    Else
       ns = JFIX(getParameterValue(9)+0.5)
       If ((ns <= 0).or.(ns > ns_max)) Call FoundBadParameter(9,'Fatal','The number of surfaces in the weather processor must be greater than 0 and less than the maximum number of surfaces allowed.')
       If (ErrorFound()) Return
    EndIf

! Set the required number of inputs and parameters
     If (mode == 1) Then
	    If (JFIX(getParameterValue(7)+0.5) == 5) Then
	       Call SetNumberofParameters(8)
	       Call SetNumberofInputs(2*ns)
	    Else
           Call SetNumberofParameters(7+3*ns)
	       Call SetNumberofInputs(0)
	    EndIf
     ElseIf ((mode == 8).OR.(mode == 9)) Then
	    If (JFIX(getParameterValue(10)+0.5) == 5) Then
	       Call SetNumberofParameters(10)
	       Call SetNumberofInputs(2*ns)
	    Else
           Call SetNumberofParameters(9+3*ns)
	       Call SetNumberofInputs(0)
	    EndIf
     Else
	    If (JFIX(getParameterValue(7)+0.5) == 5) Then
	       Call SetNumberofParameters(7)
	       Call SetNumberofInputs(2*ns)
	    Else
           Call SetNumberofParameters(6+3*ns)
	       Call SetNumberofInputs(0)
	    EndIf
     EndIf

!  Tell the TRNSYS Engine How This Type Works
    Call SetNumberofDerivatives(0)           
    Call SetNumberofOutputs(52+8*ns)           
    Call SetIterationMode(1)           
    Call SetNumberStoredVariables(75+10*n_items+8+5+14+8,0)

!  Set up this Type's entry in the SSR
    If (getIsIncludedInSSR()) Then
       Call setNumberOfReportVariables(0,0,0,2) !(nInt,nMinMax,nPars,nText)
    EndIf

!  Set the Correct Input and Output Variable Types
	If ((mode/=8).AND.(mode/=9)) Then
	   If (JFIX(getParameterValue(7)+0.5) == 5) Then
	      Do j=1,2*ns
	         Call SetInputUnits(j,'DG1')    
	      EndDo
	   EndIf
	Else
	   If (JFIX(getParameterValue(10)+0.5) == 5) Then
	      Do j=1,2*ns
	         Call SetInputUnits(j,'DG1')    
	      EndDo
	   EndIf
	EndIf
    Call SetOutputUnits(1,'TE1')    
    Call SetOutputUnits(2,'TE1')    
    Call SetOutputUnits(3,'TE1')    
    Call SetOutputUnits(4,'TE1')    
    Call SetOutputUnits(5,'TE1')    
    Call SetOutputUnits(6,'DM1')    
    Call SetOutputUnits(7,'PC1')    
    Call SetOutputUnits(8,'VE1')    
    Call SetOutputUnits(9,'DG1')    
    Call SetOutputUnits(10,'PR4')    
    Call SetOutputUnits(11,'DM1')    
    Call SetOutputUnits(12,'DM1')    
    Call SetOutputUnits(13,'IR1')    
    Call SetOutputUnits(14,'IR1')    
    Call SetOutputUnits(15,'IR1')    
    Call SetOutputUnits(16,'DG1')    
    Call SetOutputUnits(17,'DG1')    
    Call SetOutputUnits(18,'IR1')    
    Call SetOutputUnits(19,'IR1')    
    Call SetOutputUnits(20,'IR1')    
    Call SetOutputUnits(21,'IR1')    
    Call SetOutputUnits(22,'IR1')    
    Call SetOutputUnits(23,'DG1')    
    Do j=1,ns
	   Call SetOutputUnits(23+j,'IR1')
	   Call SetOutputUnits(23+ns+j,'IR1')
	   Call SetOutputUnits(23+2*ns+j,'IR1')
	   Call SetOutputUnits(23+3*ns+j,'IR1')
	   Call SetOutputUnits(23+4*ns+j,'IR1')
	   Call SetOutputUnits(23+5*ns+j,'DG1')
	   Call SetOutputUnits(23+6*ns+j,'DG1')
	   Call SetOutputUnits(23+7*ns+j,'DG1')
	EndDo
    Call SetOutputUnits(24+8*ns,'DG1')
	Call SetOutputUnits(25+8*ns,'DG1')
	Call SetOutputUnits(26+8*ns,'DG1')
	Call SetOutputUnits(27+8*ns,'LE1')
	Call SetOutputUnits(28+8*ns,'DM1')
	Call SetOutputUnits(29+8*ns,'DM1')
	Call SetOutputUnits(30+8*ns,'TE1')
	Call SetOutputUnits(31+8*ns,'TE1')
	Call SetOutputUnits(32+8*ns,'TE1')
	Call SetOutputUnits(33+8*ns,'TE1')
	Call SetOutputUnits(34+8*ns,'TE1')
	Call SetOutputUnits(35+8*ns,'TE1')
	Call SetOutputUnits(36+8*ns,'NAV')
	Call SetOutputUnits(37+8*ns,'NAV')
	Call SetOutputUnits(38+8*ns,'NAV')
	Call SetOutputUnits(39+8*ns,'NAV')
	Call SetOutputUnits(40+8*ns,'LE3')
	Call SetOutputUnits(41+8*ns,'LE1')
	Call SetOutputUnits(42+8*ns,'NAV')
	Call SetOutputUnits(43+8*ns,'NAV')
	Call SetOutputUnits(44+8*ns,'LE2')
	Call SetOutputUnits(45+8*ns,'DY1')
	Call SetOutputUnits(46+8*ns,'DM1')
	Call SetOutputUnits(47+8*ns,'TD1')
	Call SetOutputUnits(48+8*ns,'TD1')
	Call SetOutputUnits(49+8*ns,'DM1')
	Call SetOutputUnits(50+8*ns,'DM1')
	Call SetOutputUnits(51+8*ns,'DM1')

    Return

 EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!Do All of the First Timestep Manipulations Here - There Are No Iterations at the Initial Time
 If (getIsStartTime()) Then

!  Read in the Values of the Parameters from the Input File
    mode=JFIX(getParameterValue(1)+0.01)  
    lu_data=JFIX(getParameterValue(2)+0.01)
    rad_mode=JFIX(getParameterValue(3)+0.01)
	rhog=getParameterValue(4)
	rhog_snow=getParameterValue(5)

    If ((mode<1).or.(mode>9)) Call FoundBadParameter(1,'Fatal','The weather file mode must be between 1 and 7.')
    If ((rad_mode < 1).or.(rad_mode > 5)) Call FoundBadParameter(3,'Fatal','The diffuse radiation mode must be between 1 and 4.')
    If (mode /= 7) Then
       If ((rhog < 0.).or.(rhog > 1.)) Call FoundBadParameter(4,'Fatal','The ground reflectance must be between 0 and 1.')
       If ((rhog_snow < 0.).or.(rhog_snow > 1.)) Call FoundBadParameter(5,'Fatal','The snow-covered ground reflectance must be between 0 and 1.')
    Else
       If (rhog > 1.) Call FoundBadParameter(4,'Fatal','The no-albedo ground reflectance cannot be greater than 1.')
       If ((rhog < 1.).and.((rhog_snow < 0.).or.(rhog_snow > 1.))) Call FoundBadParameter(5,'Fatal','The constant ground reflectance must be between 0 and 1.')
    EndIf

    If ((mode == 8).OR.(mode == 9)) Then
       elev = getParameterValue(6)
       lat = getParameterValue(7)
       long = getParameterValue(8)
       SPar = 9
    Else
       SPar = 6
    EndIf
    ns=JFIX(getParameterValue(SPar)+0.5)
	If (JFIX(getParameterValue(SPar+1)+0.5) /= 5) Then
       Do j=1,ns
          track_mode(j)=JFIX(getParameterValue(SPar+3*(j-1)+1)+0.5)
          If ((track_mode(j) < 1).OR.(track_mode(j) > 4)) Call FoundBadParameter(SPar+j,'Fatal','The surface tracking mode must be between 1 and 4.')
	      slope(j)=getParameterValue(SPar+3*(j-1)+2)
          azimuth(j)=getParameterValue(SPar+3*(j-1)+3)
	      axis_slope(j)=getParameterValue(SPar+3*(j-1)+2)
          axis_azimuth(j)=getParameterValue(SPar+3*(j-1)+3)
       EndDo
    Else
       Do j=1,ns
          track_mode(j)=1
       EndDo
    EndIf

    If (mode==1) Then
	   If (JFIX(getParameterValue(7)+0.5) /= 5) Then
          lat=getParameterValue(7+3*ns)
       Else
          lat=getParameterValue(8)
       EndIf
       long=-99
       If ((lat < -90.).or.(lat > 90.)) Call FoundBadParameter(7+3*ns,'Fatal','The latitude must be between -90 and 90 degrees.')
    EndIf
    If (ErrorFound()) Return

!  Initialize SSR variables
    If (getIsIncludedInSSR()) Then
       If (mode == 1) Then !Mode 1: TRNSYS TMY format
          Call initReportText(1,'Weather Data File Format','TRNSYS TMY')
       ElseIf (mode == 2) Then ! Mode 2: TMY2 format
          Call initReportText(1,'Weather Data File Format','Typical Meteorological Year v2 (TMY2)')
       ElseIf (mode == 3) Then ! Mode 3: EnergyPlus format
          Call initReportText(1,'Weather Data File Format','EnergyPlus')
       ElseIf (mode == 4) Then ! Mode 4: IWEC (International Weather for Energy Calculations)
          Call initReportText(1,'Weather Data File Format','International Weather for Energy Calculations')
       ElseIf (mode == 5) Then ! Mode 5: CWEC (Canadian Weather for Energy Calculations)
          Call initReportText(1,'Weather Data File Format','Canadian Weather for Energy Calculations (EnergyPlus)')
       ElseIf (mode == 6) Then ! Mode 6: METEONORM (TMY2 format)
          Call initReportText(1,'Weather Data File Format','Meteonorm TMY2')
       ElseIf (mode == 7) Then ! Mode 7: TMY3 format
          Call initReportText(1,'Weather Data File Format','Typical Meteorological Year v3 (TMY3)')
       ElseIf (mode == 8) Then ! Mode 8: German TRY 2004 format
          Call initReportText(1,'Weather Data File Format','German TRY 2004')
       ElseIf (mode == 9) Then ! Mode 9: German TRY 2010 format
          Call initReportText(1,'Weather Data File Format','German TRY 2010')
       EndIf           
       message = trim(getLUFileName(lu_data))
       Do i=maxMessageLength,1,-1
          If ( message(i:i) == '\') Then
             iSlashSpot = i
             Exit
          EndIf
       EndDo
       message = trim( message(iSlashSpot+1:maxMessageLength))
       Call initReportText(2,'Weather Data File',message)
    EndIf

! Get the number of items to be read and set the interpolation and conversion factors
    If (mode == 1) Then !STANDARD TMY
       n_items=36
       headerskip=0
! Year
       interp(1)=-1 ; time_stamp(1)=0 ; xmult(1)=1.d0 ; xadd(1)=0.d0
! Month
       interp(2)=-2 ; time_stamp(2)=0 ; xmult(2)=1.d0 ; xadd(2)=0.d0
! Day
       interp(3)=-3 ; time_stamp(3)=0 ; xmult(3)=1.d0 ; xadd(3)=0.d0
! Hour
       interp(4)=-4 ; time_stamp(4)=1 ; xmult(4)=0.01d0 ; xadd(4)=0.d0
! Local standard time
       interp(5)=-5 ; time_stamp(5)=1 ; xmult(5)=0.01d0 ; xadd(5)=0.d0
! Extraterrestrial solar radiation
       interp(6)=-6 ; time_stamp(6)=0 ; xmult(6)=1.d0 ; xadd(6)=0.d0
! Direct normal solar radiation
       interp(7)=-7 ; time_stamp(7)=0 ; xmult(7)=1.d0 ; xadd(7)=0.d0
! Diffuse horizontal solar radiation
       interp(8)=-8 ; time_stamp(8)=0 ; xmult(8)=1.d0 ; xadd(8)=0.d0
! Net radiation
       interp(9)=-9 ; time_stamp(9)=0 ; xmult(9)=1.d0 ; xadd(9)=-5000.d0
! Global radiation on tilt
       interp(10)=-10 ; time_stamp(10)=0 ; xmult(10)=1.d0 ; xadd(10)=0.d0
! Observed Data
       interp(11)=-11 ; time_stamp(11)=0 ; xmult(11)=1.d0 ; xadd(11)=0.d0
! Engineering corrected data
       interp(12)=-12 ; time_stamp(12)=0 ; xmult(12)=1.d0 ; xadd(12)=0.d0
! Standard year corrected data
       interp(13)=-13 ; time_stamp(13)=0 ; xmult(13)=1.d0 ; xadd(13)=0.d0
! Additional radiation measurements
       interp(14)=-14 ; time_stamp(14)=0 ; xmult(14)=1.d0 ; xadd(14)=0.d0
! Minutes of sunshine
       interp(15)=-15 ; time_stamp(15)=0 ; xmult(15)=1.d0 ; xadd(15)=0.d0
! Time of TD 1440 measurements
       interp(16)=-16 ; time_stamp(16)=0 ; xmult(16)=1.d0 ; xadd(16)=0.d0
! Ceiling Height
       interp(17)=17 ; time_stamp(17)=1 ; xmult(17)=10.d0 ; xadd(17)=0.d0
! Sky condition indicator
       interp(18)=-18 ; time_stamp(18)=1 ; xmult(18)=1.d0 ; xadd(18)=0.d0
! Visibility
       interp(19)=19 ; time_stamp(19)=1 ; xmult(19)=0.1d0 ; xadd(19)=0.d0
! Occurrence of thunderstorm, tornado or squall
       interp(20)=-20 ; time_stamp(20)=1 ; xmult(20)=1.d0 ; xadd(20)=0.d0
! Occurrence of rain, rain showers or freezing rain
       interp(21)=-21 ; time_stamp(21)=1 ; xmult(21)=1.d0 ; xadd(21)=0.d0
! Occurrence of drizzle or freezing drizzle
       interp(22)=-22 ; time_stamp(22)=1 ; xmult(22)=1.d0 ; xadd(22)=0.d0
! Occurrence of snow, snow pellets or ice crystals
       interp(23)=-23 ; time_stamp(23)=1 ; xmult(23)=1.d0 ; xadd(23)=0.d0
! Occurrence of snow showers or snow grains
       interp(24)=-24 ; time_stamp(24)=1 ; xmult(24)=1.d0 ; xadd(24)=0.d0
! Occurrence of sleet, sleet showers or hail
       interp(25)=-25 ; time_stamp(25)=1 ; xmult(25)=1.d0 ; xadd(25)=0.d0
! Occurrence of fog, blowing dust or blowing sand
       interp(26)=-26 ; time_stamp(26)=1 ; xmult(26)=1.d0 ; xadd(26)=0.d0
! Occurrence of smoke, haze, dust, blowing snow or blowing spray
       interp(27)=-27 ; time_stamp(27)=1 ; xmult(27)=1.d0 ; xadd(27)=0.d0
! Sea level pressure
       interp(28)=28 ; time_stamp(28)=1 ; xmult(28)=0.01d0/101.325d0 ; xadd(28)=0.d0
! Station pressure
       interp(29)=29 ; time_stamp(29)=1 ; xmult(29)=0.01d0/101.325d0 ; xadd(29)=0.d0
! Dry bulb temperature
       interp(30)=30 ; time_stamp(30)=1 ; xmult(30)=0.1d0 ; xadd(30)=0.d0
! Dew point temperature
       interp(31)=31 ; time_stamp(31)=1 ; xmult(31)=0.1d0 ; xadd(31)=0.d0
! Wind direction
       interp(32)=32 ; time_stamp(32)=1 ; xmult(32)=1.d0 ; xadd(32)=0.d0
! Wind speed
       interp(33)=33 ; time_stamp(33)=1 ; xmult(33)=0.1d0 ; xadd(33)=0.d0
! Total sky cover
       interp(34)=34 ; time_stamp(34)=1 ; xmult(34)=10.d0 ; xadd(34)=0.d0
! Opaque sky cover
       interp(35)=35 ; time_stamp(35)=1 ; xmult(35)=10.d0 ; xadd(35)=0.d0
! Snow cover indicator
       interp(36)=-36 ; time_stamp(36)=1 ; xmult(36)=1.d0 ; xadd(36)=0.d0

    ElseIf ((mode==2).or.(mode==6)) Then !TMY2
       n_items=27
       headerskip=1

! Year
       interp(1)=-1 ; time_stamp(1)=0 ; xmult(1)=1.d0 ; xadd(1)=0.d0
! Month
       interp(2)=-2 ; time_stamp(2)=0 ; xmult(2)=1.d0 ; xadd(2)=0.d0
! Day
       interp(3)=-3 ; time_stamp(3)=0 ; xmult(3)=1.d0 ; xadd(3)=0.d0
! Hour
       interp(4)=-4 ; time_stamp(4)=1 ; xmult(4)=1.d0 ; xadd(4)=0.d0
! Extraterrestrial solar radiation
       interp(5)=-5 ; time_stamp(5)=0 ; xmult(5)=3.6d0 ; xadd(5)=0.d0
! Extraterrestrial direct normal solar radiation
       interp(6)=-6 ; time_stamp(6)=0 ; xmult(6)=3.6d0 ; xadd(6)=0.d0
! Global horizontal solar radiation
       interp(7)=-7 ; time_stamp(7)=0 ; xmult(7)=3.6d0 ; xadd(7)=0.d0
!       interp(7)=7 ; time_stamp(7)=1 ; xmult(7)=3.6d0 ; xadd(7)=0.d0
! Direct normal solar radiation
       interp(8)=-8 ; time_stamp(8)=0 ; xmult(8)=3.6d0 ; xadd(8)=0.d0
!       interp(8)=8 ; time_stamp(8)=1 ; xmult(8)=3.6d0 ; xadd(8)=0.d0
! Diffuse horizontal solar radiation
       interp(9)=-9 ; time_stamp(9)=0 ; xmult(9)=3.6d0 ; xadd(9)=0.d0
! Global horizontal illuminance
       interp(10)=-10 ; time_stamp(10)=0 ; xmult(10)=100.d0 ; xadd(10)=0.d0
! Direct normal illuminance
       interp(11)=-11 ; time_stamp(11)=0 ; xmult(11)=100.d0 ; xadd(11)=0.d0
! Diffuse horizontal illuminance
       interp(12)=-12 ; time_stamp(12)=0 ; xmult(12)=100.d0 ; xadd(12)=0.d0
! Zenith luminance
       interp(13)=-13 ; time_stamp(13)=0 ; xmult(13)=10.d0 ; xadd(13)=0.d0
! Total sky cover
       interp(14)=14 ; time_stamp(14)=1 ; xmult(14)=10.d0 ; xadd(14)=0.d0
! Opaque sky cover
       interp(15)=15 ; time_stamp(15)=1 ; xmult(15)=10.d0 ; xadd(15)=0.d0
! Dry bulb temperature
       interp(16)=16 ; time_stamp(16)=1 ; xmult(16)=0.1d0 ; xadd(16)=0.d0
! Dew point temperature
       interp(17)=17 ; time_stamp(17)=1 ; xmult(17)=0.1d0 ; xadd(17)=0.d0
! Relative humidity
       interp(18)=18 ; time_stamp(18)=1 ; xmult(18)=1.d0 ; xadd(18)=0.d0
! Atmospheric pressure
       interp(19)=19 ; time_stamp(19)=1 ; xmult(19)=0.001d0/1.01325d0 ; xadd(19)=0.d0
! Wind direction
       interp(20)=20 ; time_stamp(20)=1 ; xmult(20)=1.d0 ; xadd(20)=0.d0
! Wind speed
       interp(21)=21 ; time_stamp(21)=1 ; xmult(21)=0.1d0 ; xadd(21)=0.d0
! Visibility
       interp(22)=-22 ; time_stamp(22)=1 ; xmult(22)=0.1d0 ; xadd(22)=0.d0
! Ceiling height
       interp(23)=-23 ; time_stamp(23)=1 ; xmult(23)=1.d0 ; xadd(23)=0.d0
! Precipitable water
       interp(24)=24 ; time_stamp(24)=1 ; xmult(24)=1.d0 ; xadd(24)=0.d0
! Aerosol depth
       interp(25)=-25 ; time_stamp(25)=0 ; xmult(25)=1.d-3 ; xadd(25)=0.d0
! Snow depth
       interp(26)=-26 ; time_stamp(26)=1 ; xmult(26)=1.d0 ; xadd(26)=0.d0
! Days since last snowfall
       interp(27)=-27 ; time_stamp(27)=0 ; xmult(27)=1.d0 ; xadd(27)=0.d0

    ElseIf ((mode==3).or.(mode==5)) Then !ENERGY+, CWEC
       n_items=31
       headerskip=8

! Year
       interp(1)=-1 ; time_stamp(1)=0 ; xmult(1)=1.d0 ; xadd(1)=0.d0
! Month
       interp(2)=-2 ; time_stamp(2)=0 ; xmult(2)=1.d0 ; xadd(2)=0.d0
! Day
       interp(3)=-3 ; time_stamp(3)=0 ; xmult(3)=1.d0 ; xadd(3)=0.d0
! Hour
       interp(4)=-4 ; time_stamp(4)=1 ; xmult(4)=1.d0 ; xadd(4)=0.d0
! Minutes
       interp(5)=-5 ; time_stamp(5)=1 ; xmult(5)=1.d0 ; xadd(5)=0.d0
! Dry bulb temperature
       interp(6)=6 ; time_stamp(6)=1 ; xmult(6)=1.d0 ; xadd(6)=0.d0
! Dew point temperature
       interp(7)=7 ; time_stamp(7)=1 ; xmult(7)=1.d0 ; xadd(7)=0.d0
! Realtive humidity
       interp(8)=8 ; time_stamp(8)=1 ; xmult(8)=1.d0 ; xadd(8)=0.d0
! Atmospheric pressure
       interp(9)=9 ; time_stamp(9)=1 ; xmult(9)=0.000009869d0 ; xadd(9)=0.d0
! Extraterrestrial horizontal solar radiation
       interp(10)=-10 ; time_stamp(10)=0 ; xmult(10)=3.6d0 ; xadd(10)=0.d0
! Extraterrestrial direct normal solar radiation
       interp(11)=-11 ; time_stamp(11)=0 ; xmult(11)=3.6d0 ; xadd(11)=0.d0
! Horizontal infrared radiation
       interp(12)=-12 ; time_stamp(12)=0 ; xmult(12)=3.6d0 ; xadd(12)=0.d0
! Global horizontal solar radiation
       interp(13)=-13 ; time_stamp(13)=0 ; xmult(13)=3.6d0 ; xadd(13)=0.d0
! Direct normal solar radiation
       interp(14)=-14 ; time_stamp(14)=0 ; xmult(14)=3.6d0 ; xadd(14)=0.d0
! Diffuse horizontal solar radiation
       interp(15)=-15 ; time_stamp(15)=0 ; xmult(15)=3.6d0 ; xadd(15)=0.d0
! Global horizontal illuminance
       interp(16)=-16 ; time_stamp(16)=0 ; xmult(16)=1.d0 ; xadd(16)=0.d0
! Direct normal illuminance
       interp(17)=-17 ; time_stamp(17)=0 ; xmult(17)=1.d0 ; xadd(17)=0.d0
! Diffuse horizontal illuminance
       interp(18)=-18 ; time_stamp(18)=0 ; xmult(18)=1.d0 ; xadd(18)=0.d0
! Zenith luminance
       interp(19)=-19 ; time_stamp(19)=0 ; xmult(19)=1.d0 ; xadd(19)=0.d0
! Wind direction
       interp(20)=20 ; time_stamp(20)=1 ; xmult(20)=1.d0 ; xadd(20)=0.d0
! Wind speed
       interp(21)=21 ; time_stamp(21)=1 ; xmult(21)=1.d0 ; xadd(21)=0.d0
! Total sky cover
       interp(22)=22 ; time_stamp(22)=1 ; xmult(22)=10.d0 ; xadd(22)=0.d0
! Opaque sky cover
       interp(23)=23 ; time_stamp(23)=1 ; xmult(23)=10.d0 ; xadd(23)=0.d0
! Visibility
       interp(24)=-24 ; time_stamp(24)=1 ; xmult(24)=1.d0 ; xadd(24)=0.d0
! Ceiling height
       interp(25)=-25 ; time_stamp(25)=1 ; xmult(25)=1.d0 ; xadd(25)=0.d0
! Unused
       interp(26)=-26 ; time_stamp(26)=1 ; xmult(26)=1.d0 ; xadd(26)=0.d0
! Unused
       interp(27)=-27 ; time_stamp(27)=0 ; xmult(27)=1.d0 ; xadd(27)=0.d0
! Precipitable water
       interp(28)=28 ; time_stamp(28)=1 ; xmult(28)=1.d0 ; xadd(28)=0.d0
! Aerosol depth
       interp(29)=-29 ; time_stamp(29)=0 ; xmult(29)=1.d0 ; xadd(29)=0.d0
! Snow depth
       interp(30)=-30 ; time_stamp(30)=1 ; xmult(30)=1.d0 ; xadd(30)=0.d0
! Days since last snowfall
       interp(31)=-31 ; time_stamp(31)=0 ; xmult(27)=1.d0 ; xadd(31)=0.d0

    ElseIf (mode==4) Then !INTERNATIONAL WEATHER FOR ENERGY CALCULATIONS
       n_items=28
       headerskip=1
! Year
       interp(1)=-1 ; time_stamp(1)=0 ; xmult(1)=1.d0 ; xadd(1)=0.d0
! Month
       interp(2)=-2 ; time_stamp(2)=0 ; xmult(2)=1.d0 ; xadd(2)=0.d0
! Day of the month
       interp(3)=-3 ; time_stamp(3)=0 ; xmult(3)=1.d0 ; xadd(3)=0.d0
! Hour
       interp(4)=-4 ; time_stamp(4)=1 ; xmult(4)=1.d0 ; xadd(4)=0.d0
! Global extraterrestrial radiation (kJ/hr-m2)
       interp(5)=-5 ; time_stamp(5)=0 ; xmult(5)=3.6d0 ; xadd(5)=0.d0
! Extraterrestrial direct normal radiation (kJ/hr-m2)          
       interp(6)=-6 ; time_stamp(6)=0 ; xmult(6)=3.6d0 ; xadd(6)=0.d0
! Global horizontal solar radiation (kJ/m2)           
       interp(7)=-7 ; time_stamp(7)=0 ; xmult(7)=3.6d0 ; xadd(7)=0.d0
! Direct normal solar radiation (kJ/m2)           
       interp(8)=-8 ; time_stamp(8)=0 ; xmult(8)=3.6d0 ; xadd(8)=0.d0 
! Diffuse horizontal solar radiation (kJ/m2)           
       interp(9)=-9 ; time_stamp(9)=0 ; xmult(9)=3.6d0 ; xadd(9)=0.d0 
! Global horizontal illuminance (lux)           
       interp(10)=-10 ; time_stamp(10)=0 ; xmult(10)=100.d0 ; xadd(10)=0.d0 
! Direct normal illuminance (lux)           
       interp(11)=-11 ; time_stamp(11)=0 ; xmult(11)=100.d0 ; xadd(11)=0.d0 
! Diffuse horizontal illuminance (lux)           
       interp(12)=-12 ; time_stamp(12)=0 ; xmult(12)=100.d0 ; xadd(12)=0.d0 
! Zenith luminance (cd/m2)           
       interp(13)=-13 ; time_stamp(13)=0 ; xmult(13)=10.d0 ; xadd(13)=0.d0 
! Total sky cover (%)          
       interp(14)=14 ; time_stamp(14)=1 ; xmult(14)=10.d0  ; xadd(14)=0.d0
! Opaque sky cover (%)           
       interp(15)=15 ; time_stamp(15)=1 ; xmult(15)=10.d0 ; xadd(15)=0.d0 
! Dry bulb temperature (C)           
       interp(16)=16 ; time_stamp(16)=0 ; xmult(16)=0.1d0 ; xadd(16)=0.d0 
! Dew point temperature (C)          
       interp(17)=17 ; time_stamp(17)=0 ; xmult(17)=0.1d0 ; xadd(17)=0.d0 
! Relative humidity          
       interp(18)=18 ; time_stamp(18)=0 ; xmult(18)=1.d0 ; xadd(18)=0.d0
! Pressure (atm)
       interp(19)=19 ; time_stamp(19)=0 ; xmult(19)=0.0009869d0 ; xadd(19)=0.d0 
! Wind direction (deg)          
       interp(20)=-20 ; time_stamp(20)=1 ; xmult(20)=1.d0 ; xadd(20)=0.d0
! Windspeed (m/s)
       interp(21)=21 ; time_stamp(21)=1 ; xmult(21)=0.1d0 ; xadd(21)=0.d0 
! Visibility (km)          
       interp(22)=-22 ; time_stamp(22)=1 ; xmult(22)=0.1d0 ; xadd(22)=0.d0 
! Ceiling height (m)          
       interp(23)=-23 ; time_stamp(23)=1 ; xmult(23)=1.d0 ; xadd(23)=0.d0
! Unused
       interp(24)=-24 ; time_stamp(24)=0 ; xmult(24)=1.d0 ; xadd(24)=0.d0
! Precipitable weather (mm h2o)
       interp(25)=25 ; time_stamp(25)=1 ; xmult(25)=1.d0 ; xadd(25)=0.d0
! Aerosol optical depth (thousandths)
       interp(26)=-26 ; time_stamp(26)=0 ; xmult(26)=0.001d0 ; xadd(26)=0.d0 
! Snow depth (cm)
       interp(27)=-27 ; time_stamp(27)=1 ; xmult(27)=1.d0 ; xadd(27)=0.d0
! Days since last snowfall (days)
       interp(28)=-28 ; time_stamp(28)=0 ; xmult(28)=1.d0 ; xadd(28)=0.d0

     ElseIf (mode==7) Then !TMY3
        n_items=28
        headerskip=2
! Year
       interp(1)=-1 ; time_stamp(1)=0 ; xmult(1)=1.d0 ; xadd(1)=0.d0
! Month
       interp(2)=-2 ; time_stamp(2)=0 ; xmult(2)=1.d0 ; xadd(2)=0.d0
! Day
       interp(3)=-3 ; time_stamp(3)=0 ; xmult(3)=1.d0 ; xadd(3)=0.d0
! Hour
       interp(4)=-4 ; time_stamp(4)=1 ; xmult(4)=1.d0 ; xadd(4)=0.d0
! Extraterrestrial solar radiation
       interp(5)=-5 ; time_stamp(5)=0 ; xmult(5)=3.6d0 ; xadd(5)=0.d0
! Extraterrestrial direct normal solar radiation
       interp(6)=-6 ; time_stamp(6)=0 ; xmult(6)=3.6d0 ; xadd(6)=0.d0
! Global horizontal solar radiation
       interp(7)=-7 ; time_stamp(7)=0 ; xmult(7)=3.6d0 ; xadd(7)=0.d0
! Direct normal solar radiation
       interp(8)=-8 ; time_stamp(8)=0 ; xmult(8)=3.6d0 ; xadd(8)=0.d0
! Diffuse horizontal solar radiation
       interp(9)=-9 ; time_stamp(9)=0 ; xmult(9)=3.6d0 ; xadd(9)=0.d0
! Global horizontal illuminance
       interp(10)=-10 ; time_stamp(10)=0 ; xmult(10)=1.d0 ; xadd(10)=0.d0
! Direct normal illuminance
       interp(11)=-11 ; time_stamp(11)=0 ; xmult(11)=1.d0 ; xadd(11)=0.d0
! Diffuse horizontal illuminance
       interp(12)=-12 ; time_stamp(12)=0 ; xmult(12)=1.d0 ; xadd(12)=0.d0
! Zenith luminance
       interp(13)=-13 ; time_stamp(13)=0 ; xmult(13)=1.d0 ; xadd(13)=0.d0
! Total sky cover
       interp(14)=14 ; time_stamp(14)=1 ; xmult(14)=10.d0 ; xadd(14)=0.d0
! Opaque sky cover
       interp(15)=15 ; time_stamp(15)=1 ; xmult(15)=10.d0 ; xadd(15)=0.d0
! Dry bulb temperature
       interp(16)=16 ; time_stamp(16)=1 ; xmult(16)=1.0d0 ; xadd(16)=0.d0
! Dew point temperature
       interp(17)=17 ; time_stamp(17)=1 ; xmult(17)=1.0d0 ; xadd(17)=0.d0
! Relative humidity
       interp(18)=18 ; time_stamp(18)=1 ; xmult(18)=1.d0 ; xadd(18)=0.d0
! Atmospheric pressure
       interp(19)=19 ; time_stamp(19)=1 ; xmult(19)=0.001d0/1.01325d0 ; xadd(19)=0.d0
! Wind direction
       interp(20)=20 ; time_stamp(20)=1 ; xmult(20)=1.d0 ; xadd(20)=0.d0
! Wind speed
       interp(21)=21 ; time_stamp(21)=1 ; xmult(21)=1.d0 ; xadd(21)=0.d0
! Visibility
       interp(22)=-22 ; time_stamp(22)=1 ; xmult(22)=0.001d0 ; xadd(22)=0.d0
! Ceiling height
       interp(23)=-23 ; time_stamp(23)=1 ; xmult(23)=1.d0 ; xadd(23)=0.d0
! Precipitable water
       interp(24)=24 ; time_stamp(24)=1 ; xmult(24)=10.d0 ; xadd(24)=0.d0
! Aerosol depth
       interp(25)=-25 ; time_stamp(25)=0 ; xmult(25)=1.d0 ; xadd(25)=0.d0
! Albedo
       interp(26)=-26 ; time_stamp(26)=1 ; xmult(26)=1.d0 ; xadd(26)=0.d0
! Liquid precipitation depth
       interp(27)=-27 ; time_stamp(27)=0 ; xmult(27)=1.d0 ; xadd(27)=0.d0
! Liquid precipitation time
       interp(28)=-28 ; time_stamp(28)=1 ; xmult(28)=1.d0 ; xadd(28)=0.d0
     ElseIf (mode==8) Then !German TRY
        n_items=19
        headerskip=0
! TRY-Region
       interp(1)=-1 ; time_stamp(1)=0 ; xmult(1)=1.d0 ; xadd(1)=0.d0
! Standortinformation (Station Information)
       interp(2)=-2 ; time_stamp(2)=0 ; xmult(2)=1.d0 ; xadd(2)=0.d0
! Monat (Month)
       interp(3)=-3 ; time_stamp(3)=0 ; xmult(3)=1.d0 ; xadd(3)=0.d0
! Tag (Day)
       interp(4)=-4 ; time_stamp(4)=0 ; xmult(4)=1.d0 ; xadd(4)=0.d0
! Stunde (Hour)
       interp(5)=-5 ; time_stamp(5)=1 ; xmult(5)=1.d0 ; xadd(5)=0.d0
! Bedeckungsgrad (Sky Cover)
       interp(6)=6 ; time_stamp(6)=1 ; xmult(6)=12.5d0 ; xadd(6)=0.d0
! Windrichtung (Wind Direction)
       interp(7)=-7 ; time_stamp(7)=0 ; xmult(7)=1.d0 ; xadd(7)=0.d0
! Windgeschwindigkeit (Wind Speed)
       interp(8)=8 ; time_stamp(8)=1 ; xmult(8)=1.d0 ; xadd(8)=0.d0
! Temperatur (Temperature)
       interp(9)=9 ; time_stamp(9)=1 ; xmult(9)=1.d0 ; xadd(9)=0.d0
! Luftdruck in Stationshöhe (Pressure)
       interp(10)=10 ; time_stamp(10)=1 ; xmult(10)=0.001d0/1.01325d0 ; xadd(10)=0.d0
! Wasserdampfgehalt (Mischungsverhältnis) (Humidity Ratio)
       interp(11)=11 ; time_stamp(11)=1 ; xmult(11)=0.001d0 ; xadd(11)=0.d0
! Relative Feuchte (Relative Humidity)
       interp(12)=12 ; time_stamp(12)=1 ; xmult(12)=1.d0 ; xadd(12)=0.d0
! Wetterereignis (Weather Event)
       interp(13)=-13 ; time_stamp(13)=1 ; xmult(13)=1.d0 ; xadd(13)=0.d0
! Bestrahlungsstärke der direkten Sonnenstrahlung auf die horizontale Ebene (Horizontal Direct Solar)
       interp(14)=-14 ; time_stamp(14)=0 ; xmult(14)=3.6d0 ; xadd(14)=0.d0
! Bestrahlungsstärke der diffusen Sonnenstrahlung auf die horizontale Ebene (Horizontal Diffuse Solar)
       interp(15)=-15 ; time_stamp(15)=0 ; xmult(15)=3.6d0 ; xadd(15)=0.d0
! Information, ob B und/oder D Mess- oder Rechenwerte sind (radiation measured or calculated)
       interp(16)=-16 ; time_stamp(16)=1 ; xmult(16)=1.0d0 ; xadd(16)=0.d0
! Bestrahlungsstärke der Wärmestrahlung der Atmosphäre auf die horizontale Ebene (horizontal long-wave incident)
       interp(17)=-17 ; time_stamp(17)=0 ; xmult(17)=3.6d0 ; xadd(17)=0.d0
! spezifische Ausstrahlung der Wärmestrahlung der Erdoberfläche (long-wave emmitance of the earth)
       interp(17)=-18 ; time_stamp(18)=0 ; xmult(18)=3.6d0 ; xadd(18)=0.d0
! Qualitätsbit für die langwelligen Strahlungsgrößen (quality of the long-wave radiation)
       interp(19)=19 ; time_stamp(19)=1 ; xmult(19)=1.d0 ; xadd(19)=0.d0
     ElseIf (mode==9) Then !German TRY
        n_items=19
        headerskip=38
! TRY-Region
       interp(1)=-1 ; time_stamp(1)=0 ; xmult(1)=1.d0 ; xadd(1)=0.d0
! Standortinformation (Station Information)
       interp(2)=-2 ; time_stamp(2)=0 ; xmult(2)=1.d0 ; xadd(2)=0.d0
! Monat (Month)
       interp(3)=-3 ; time_stamp(3)=0 ; xmult(3)=1.d0 ; xadd(3)=0.d0
! Tag (Day)
       interp(4)=-4 ; time_stamp(4)=0 ; xmult(4)=1.d0 ; xadd(4)=0.d0
! Stunde (Hour)
       interp(5)=-5 ; time_stamp(5)=1 ; xmult(5)=1.d0 ; xadd(5)=0.d0
! Bedeckungsgrad (Sky Cover)
       interp(6)=6 ; time_stamp(6)=1 ; xmult(6)=12.5d0 ; xadd(6)=0.d0
! Windrichtung (Wind Direction)
       interp(7)=-7 ; time_stamp(7)=0 ; xmult(7)=1.d0 ; xadd(7)=0.d0
! Windgeschwindigkeit (Wind Speed)
       interp(8)=8 ; time_stamp(8)=1 ; xmult(8)=1.d0 ; xadd(8)=0.d0
! Temperatur (Temperature)
       interp(9)=9 ; time_stamp(9)=1 ; xmult(9)=1.d0 ; xadd(9)=0.d0
! Luftdruck in Stationshöhe (Pressure)
       interp(10)=10 ; time_stamp(10)=1 ; xmult(10)=0.001d0/1.01325d0 ; xadd(10)=0.d0
! Wasserdampfgehalt (Mischungsverhältnis) (Humidity Ratio)
       interp(11)=11 ; time_stamp(11)=1 ; xmult(11)=0.001d0 ; xadd(11)=0.d0
! Relative Feuchte (Relative Humidity)
       interp(12)=12 ; time_stamp(12)=1 ; xmult(12)=1.d0 ; xadd(12)=0.d0
! Wetterereignis (Weather Event)
       interp(13)=-13 ; time_stamp(13)=1 ; xmult(13)=1.d0 ; xadd(13)=0.d0
! Bestrahlungsstärke der direkten Sonnenstrahlung auf die horizontale Ebene (Horizontal Direct Solar)
       interp(14)=-14 ; time_stamp(14)=0 ; xmult(14)=3.6d0 ; xadd(14)=0.d0
! Bestrahlungsstärke der diffusen Sonnenstrahlung auf die horizontale Ebene (Horizontal Diffuse Solar)
       interp(15)=-15 ; time_stamp(15)=0 ; xmult(15)=3.6d0 ; xadd(15)=0.d0
! Information, ob B und/oder D Mess- oder Rechenwerte sind (radiation measured or calculated)
       interp(16)=-16 ; time_stamp(16)=1 ; xmult(16)=1.0d0 ; xadd(16)=0.d0
! Bestrahlungsstärke der Wärmestrahlung der Atmosphäre auf die horizontale Ebene (horizontal long-wave incident)
       interp(17)=-17 ; time_stamp(17)=0 ; xmult(17)=3.6d0 ; xadd(17)=0.d0
! spezifische Ausstrahlung der Wärmestrahlung der Erdoberfläche (long-wave emmitance of the earth)
       interp(17)=-18 ; time_stamp(18)=0 ; xmult(18)=3.6d0 ; xadd(18)=0.d0
! Qualitätsbit für die langwelligen Strahlungsgrößen (quality of the long-wave radiation)
       interp(19)=19 ; time_stamp(19)=1 ; xmult(19)=1.d0 ; xadd(19)=0.d0
    EndIf

! Open the data file
    Open (lu_data,err=2000)
            
! Read and process the header information at the top of the data file
    If (mode==1) Then !TMY
       shift=0.                 !tmy files are in solar time
    ElseIf ((mode==2).or.(mode==6)) THEN !TMY2
       Read (lu_data,5000,err=2000,end=2002) City,State,tmzonehr,latdir,latdeg,latmin,longdir,longdeg,longmin,elev
       lat = latdeg+latmin/60.
	   If (latdir == 'S') lat=-lat
       long = longdeg+longmin/60.
       tmzone = tmzonehr
	   If (longdir == 'W') Then
	      shift = -long-tmzone*15.0d0
	   Else
		  shift = long-tmzone*15.0d0
	   EndIf
   ElseIf ((mode==3).or.(mode==5)) Then !ENERGY+
       Read (lu_data,5001,err=2000,end=2000) (eplusheader(i),i=1,160)
       commacntr = 0
       Do i=1,160
          If (eplusheader(i)==',') commacntr=commacntr+1
          If ((eplusheader(i)==' ').and.(commacntr < 9)) Then
             eplusheader(i)='_'
          ElseIf ((eplusheader(i)=='/').and.(commacntr < 9)) Then
             eplusheader(i)='-'
          ElseIf ((eplusheader(i)==' ').and.(commacntr >= 9)) Then
             eplusheader(i)=','
          EndIf
       EndDo

       lu=getNextAvailableLogicalUnit()

! Write header info to a temp file
       Open (unit=lu,file='T15.TMP',status='UNKNOWN')
       Write (lu,5001) eplusheader
       Close (lu,status='KEEP')

! Now read the info into the proper variables
       Open (unit=lu,file='T15.TMP',status='OLD')
       Read (lu,*,err=2000,end=2000) keyword,City,State,Country,source,site,lat,long,tmzonehr,elev
       Close (lu,status='DELETE')

       tmzone = tmzonehr
       tmzone = -tmzone
       long = -long
       shift = (tmzone*15.)-long

! Now read the next seven header lines and ignore them
       Do j=1,7
          Read (lu_data,*)
       EndDo
    ElseIf (mode==4) Then !IWEC
       Read (lu_data,5003,err=2000,end=2000) City,State,Country,wmoregion,tmzndir,tmznhr,tmznmn,latdir,latdeg,latmin,longdir,longdeg,longmin,elevdir,elev
       lat = latdeg + latmin/60.d0
       long = longdeg +longmin/60.d0
       tmzone = tmznhr + tmznmn/60.d0
       If (tmzndir=='+') Then 
          tmzone = -tmzone
       EndIf
       If (latdir=='S') lat=-lat
       If (longdir=='E') long = -long
       shift = tmzone*15.d0 - long
    ElseIf (mode==7) Then !TMY3
       Read (lu_data,*,err=2000,end=2002) j,alphanum,State,tmzonehr,lat,long,elev
       Read (lu_data,*)
	   long=-long
	   tmzone=-tmzonehr
       shift=(tmzone*15.)-long
    ElseIf (mode==8) Then !German TRY 2004
       long = -long
       shift = -15+long
    ElseIf (mode==9) Then !German TRY 2010
       long = -long
       shift = -15+long
    ! Now read the header lines to skip and ignore them
       Do j=1,headerskip
          Read (lu_data,*)
       EndDo

    EndIf

! Initialize the monthly tmperature arrays
    month_ave_T=0.
    month_min_T=1.D+20
    month_max_T=-1.D+20

    annual_ave_T=0.
    annual_min_T=1.D+20
    annual_max_T=-1.D+20
    annual_ave_I=0.

! Read the initial value for the variables
     Do j=1,8760
        If (j<=744) Then
           length_month=744.
           j_month=1
        ElseIf ((j > 744).and.(j <= 1416)) Then
           length_month=672.
           j_month=2
        ElseIf ((j > 1416).and.(j <= 2160)) Then
           length_month=744.
           j_month=3
        ElseIf ((j > 2160).and.(j <= 2880)) Then
           length_month=720.
           j_month=4
        ElseIf ((j > 2880).and.(j <= 3624)) Then
           length_month=744.
           j_month=5
        ElseIf ((j > 3624).and.(j <= 4344)) Then
           length_month=720.
           j_month=6
        ElseIf ((j > 4344).and.(j <= 5088)) Then
           length_month=744.
           j_month=7
        ElseIf ((j > 5088).and.(j <= 5832)) Then
           length_month=744.
           j_month=8
        ElseIf ((j > 5832).and.(j <= 6552)) Then
           length_month=720.
           j_month=9
        ElseIf ((j > 6552).and.(j <= 7296)) Then
           length_month=744.
           j_month=10
        ElseIf ((j > 7296).and.(j <= 8016)) Then
           length_month=720.
           j_month=11
        Else
           length_month=744.
           j_month=12
        EndIf

        If (mode==1) Then  !STANDARD TMY
           Read (lu_data,6000,err=2000,end=2000) (begin(i),i=1,n_items)
           j_spot=30
           j_spot_I = 9
        ElseIf ((mode==2).OR.(mode==6)) Then       !TMY2
           Read (lu_data,4000,err=2000,end=2000) (begin(i),i=1,n_items)
           j_spot=16
           j_spot_I = 7
       ElseIf((mode==3).OR.(mode==5)) Then  !ENERGY+
           Read (lu_data,*,end=2000,err=2000) begin(1),begin(2),begin(3),begin(4),begin(5),alphanum,begin(6),begin(7),begin(8),begin(9),begin(10),begin(11),begin(12),begin(13),begin(14),begin(15),begin(16),begin(17),begin(18),begin(19),begin(20),begin(21),begin(22),begin(23),begin(24),begin(25),begin(26),begin(27),begin(28),begin(29),begin(30),begin(31)
           j_spot=6
           j_spot_I = 13
        ElseIf(mode==4) Then  !IWEC
           Read (lu_data,6001,err=2000,end=2000) (begin(i),i=1,n_items)
           j_spot=16
           j_spot_I = 7
       ElseIf(mode==7) Then       !TMY3
           Read (lu_data,5004,err=2000,end=2000,advance='NO') begin(1),begin(2),begin(3),begin(4)
           j_spot=16
           j_spot_I = 7
           Read (lu_data,*,err=2000,end=2000) begin(5),begin(6),begin(7),alphanum,alphanum,begin(8),alphanum,alphanum,begin(9),alphanum,alphanum,begin(10),alphanum,alphanum,begin(11),alphanum,alphanum,begin(12),alphanum,alphanum,begin(13),alphanum,alphanum,begin(14),alphanum,alphanum,begin(15),alphanum,alphanum,begin(16),alphanum,alphanum,begin(17),alphanum,alphanum,begin(18),alphanum,alphanum,begin(19),alphanum,alphanum,begin(20),alphanum,alphanum,begin(21),alphanum,alphanum,begin(22),alphanum,alphanum,begin(23),alphanum,alphanum,begin(24),alphanum,alphanum,begin(25),alphanum,alphanum,begin(26),alphanum,alphanum,begin(27),begin(28)
        ElseIf((mode==8)) Then       !German TRY 2004 
           j_spot=9
           j_spot_I = 17
           Read (lu_data,7001,err=2000,end=2000) (begin(i),i=1,n_items)
           nTRYreg = JFIX(begin(1))
           nTRYmon = JFIX(begin(3))
           begin(9) = begin(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
           begin(11) = begin(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
        ElseIf(mode==9) Then       !German TRY 2010
           j_spot=9
           j_spot_I = 17
           Read (lu_data,7001,err=2000,end=2000) (begin(i),i=1,n_items)
           nTRYreg = JFIX(begin(1))
           nTRYmon = JFIX(begin(3))
        EndIf

! Update the annual average temperature
        annual_ave_T=annual_ave_T+begin(j_spot)/8760.
! Calculate the monthly average, minimum and maximum temperatures
        If (begin(j_spot) < month_min_T(j_month)) month_min_T(j_month)=begin(j_spot)
        If (begin(j_spot) > month_max_T(j_month)) month_max_T(j_month)=begin(j_spot)
        If (begin(j_spot) < annual_min_T) annual_min_T=begin(j_spot)
        If (begin(j_spot) > annual_max_T) annual_max_T=begin(j_spot)
        month_ave_T(j_month)=month_ave_T(j_month)+begin(j_spot)/length_month
! Update the annual average global irradiance
        annual_ave_I = annual_ave_I+begin(j_spot_I)/365. 

     EndDo

! Replace some 'missing data' conditions in standard TMY files
     If (mode==1) Then         !STANDARD TMY
        Do i=1,n_items
           !replace 'missing radiation' values (9999) with zeros.
           If ((begin(i) > 9998.).and.(begin(i) < 10000.)) begin(i) = 0.d0
           !replace 'missing sky cover' values (99) with the last read value (0 in this case).
           If (((i==34).OR.(i==35)).and.(begin(i) > 98)) begin(i) = 0.d0
        EndDo
! Correct the precipitable water
     ElseIf (mode==7) Then
	    If (begin(27) < 0.) begin(27)=0.
	    If (begin(28) < 0.) begin(28)=0.
	 EndIf

! Use the interpolation factors to correct the averages
	 month_ave_T=month_ave_T*xmult(j_spot)+xadd(j_spot)
	 month_min_T=month_min_T*xmult(j_spot)+xadd(j_spot)
	 month_max_T=month_max_T*xmult(j_spot)+xadd(j_spot)

	 annual_ave_T=annual_ave_T*xmult(j_spot)+xadd(j_spot)
	 annual_min_T=annual_min_T*xmult(j_spot)+xadd(j_spot)
	 annual_max_T=annual_max_T*xmult(j_spot)+xadd(j_spot)
	 annual_ave_I=annual_ave_I*xmult(j_spot_I)+xadd(j_spot_I)

! Set the rollover month values
	 month_ave_T(0)=month_ave_T(12)
	 month_min_T(0)=month_min_T(12)
	 month_max_T(0)=month_max_T(12)

	 month_ave_T(13)=month_ave_T(1)
	 month_min_T(13)=month_min_T(1)
	 month_max_T(13)=month_max_T(1)

! Set some Parameters according to the EnergyGauge method of calculating the seasons
     wdt_min=(59.-32.)/1.8
	 T_ave_heat=(71.5-32.)/1.8
	 T_ave_cool=(66.-32.)/1.8

! Calculate the heating and cooling season indicators
     Do j=1,12
        If ((j==1).and.(annual_min_T < wdt_min)) Then
	       x_heat(j)=1.
        ElseIf ((j==12).and.(annual_min_T < wdt_min)) Then
	       x_heat(j)=1.
	    ElseIf (month_ave_T(j) <= T_ave_heat) Then
	       x_heat(j)=1.
	    Else
	       x_heat(j)=0.
	    EndIf

        If ((j==7).OR.(j==8)) Then
	       x_cool(j)=1.
	    ElseIf (month_ave_T(j) >= T_ave_cool) Then
	         x_cool(j)=1.
	    Else
	         x_cool(j)=0.
	    EndIf
	 EndDo

	 x_heat(0)=x_heat(12)
	 x_heat(13)=x_heat(1)

	 x_cool(0)=x_cool(12)
	 x_cool(13)=x_cool(1)

! Perform a check on the swing season months
     x_swing=0.
	 Do j=0,12
	    If ((x_heat(j) >= 0.5).and.(x_cool(j+1) >= 0.5)) Then
	       x_swing(j)=1.
	       x_swing(j+1)=1.
	    EndIf
	    If((x_cool(j) >= 0.5).and.(x_heat(j+1) >= 0.5)) Then
	       x_swing(j)=1.
	       x_swing(j+1)=1.
	    EndIf
	 EndDo

! Set the heating and cooling season indicators
	 Do j=0,12
	    x_heat(j)=DMAX1(x_heat(j),x_swing(j))
	    x_cool(j)=DMAX1(x_cool(j),x_swing(j))
	 EndDo

! Set the values at the current time
     Do j=1,n_items
           value_now(j)=begin(j)
     EndDo

!Rewind the file and read the header line
     Rewind (lu_data)
     Do j=1,headerskip
        Read (lu_data,*,err=2000,end=2000)
     EndDo

!Figure out how many lines to skip to find the right point in the data file to start reading
     iskip=MAX(0,JFIX(Time+0.5/3600.))

! Skip the right number of lines
     Do j=1,iskip
        If (mode==1) Then   ! TMY
           Read (lu_data,6000,err=2000,end=150) (value_now(i),i=1,n_items)
           If (value_now(i)==9999) value_now(i)=0.d0
        ElseIf ((mode==2).or.(mode==6)) Then        !TMY2
           Read (lu_data,4000,err=2000,end=150) (value_now(i),i=1,n_items)
        ElseIf ((mode==3).or.(mode==5)) Then   !ENERGY+
           Read (lu_data,*,end=150,err=2000) value_now(1),value_now(2),value_now(3),value_now(4),value_now(5),alphanum,value_now(6),value_now(7),value_now(8),value_now(9),value_now(10),value_now(11),value_now(12),value_now(13),value_now(14),value_now(15),value_now(16),value_now(17),value_now(18),value_now(19),value_now(20),value_now(21),value_now(22),value_now(23),value_now(24),value_now(25),value_now(26),value_now(27),value_now(28),value_now(29),value_now(30),value_now(31)
        ElseIf (mode==4) Then   !IWEC
           Read (lu_data,6001,err=2000,end=150) (value_now(i),i=1,n_items)
        ElseIf (mode==7) Then   !TMY3
           Read (lu_data,5004,err=2000,end=150,advance='NO') value_now(1),value_now(2),value_now(3),value_now(4)
           Read (lu_data,*,err=2000,end=150) value_now(5),value_now(6),value_now(7),alphanum,alphanum,value_now(8),alphanum,alphanum,value_now(9),alphanum,alphanum,value_now(10),alphanum,alphanum,value_now(11),alphanum,alphanum,value_now(12),alphanum,alphanum,value_now(13),alphanum,alphanum,value_now(14),alphanum,alphanum,value_now(15),alphanum,alphanum,value_now(16),alphanum,alphanum,value_now(17),alphanum,alphanum,value_now(18),alphanum,alphanum,value_now(19),alphanum,alphanum,value_now(20),alphanum,alphanum,value_now(21),alphanum,alphanum,value_now(22),alphanum,alphanum,value_now(23),alphanum,alphanum,value_now(24),alphanum,alphanum,value_now(25),alphanum,alphanum,value_now(26),alphanum,alphanum,value_now(27),value_now(28)
        ElseIf (mode==8) Then       !German TRY 2004
           Read (lu_data,7001,err=2000,end=150) (value_now(i),i=1,n_items)
           nTRYreg = JFIX(value_now(1))
           nTRYmon = JFIX(value_now(3))
           value_now(9) = value_now(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
           value_now(11) = value_now(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
        ElseIf (mode==9) Then       !German TRY 2010
           Read (lu_data,7001,err=2000,end=150) (value_now(i),i=1,n_items)
           nTRYreg = JFIX(value_now(1))
           nTRYmon = JFIX(value_now(3))
        EndIf
        goto 160

150     Call Rewind (Time,lu_data,headerskip,CurrentUnit,CurrentType,ie_read)
        If (mode==1) Then   ! TMY
           Read (lu_data,6000,err=2000,end=2000) (value_now(i),i=1,n_items)
        ElseIf ((mode==2).or.(mode==6)) Then        !TMY2
           Read (lu_data,4000,err=2000,end=2000) (value_now(i),i=1,n_items)
        ElseIf ((mode==3).or.(mode==5)) Then   !ENERGY+
           Read (lu_data,*,end=2000,err=2000) value_now(1),value_now(2),value_now(3),value_now(4),value_now(5),alphanum,value_now(6),value_now(7),value_now(8),value_now(9),value_now(10),value_now(11),value_now(12),value_now(13),value_now(14),value_now(15),value_now(16),value_now(17),value_now(18),value_now(19),value_now(20),value_now(21),value_now(22),value_now(23),value_now(24),value_now(25),value_now(26),value_now(27),value_now(28),value_now(29),value_now(30),value_now(31)
        ElseIf (mode==4) Then   !IWEC
           Read (lu_data,6001,err=2000,end=2000) (value_now(i),i=1,n_items)
        ElseIf (mode==7) Then   !TMY3
           Read (lu_data,5004,err=2000,end=2000,advance='NO') value_now(1),value_now(2),value_now(3),value_now(4)
           Read (lu_data,*,err=2000,end=2000) value_now(5),value_now(6),value_now(7),alphanum,alphanum,value_now(8),alphanum,alphanum,value_now(9),alphanum,alphanum,value_now(10),alphanum,alphanum,value_now(11),alphanum,alphanum,value_now(12),alphanum,alphanum,value_now(13),alphanum,alphanum,value_now(14),alphanum,alphanum,value_now(15),alphanum,alphanum,value_now(16),alphanum,alphanum,value_now(17),alphanum,alphanum,value_now(18),alphanum,alphanum,value_now(19),alphanum,alphanum,value_now(20),alphanum,alphanum,value_now(21),alphanum,alphanum,value_now(22),alphanum,alphanum,value_now(23),alphanum,alphanum,value_now(24),alphanum,alphanum,value_now(25),alphanum,alphanum,value_now(26),alphanum,alphanum,value_now(27),value_now(28)
        ElseIf (mode==8) Then       !German TRY 2004
           Read (lu_data,7001,err=2000,end=2000) (value_now(i),i=1,n_items)
           nTRYreg = JFIX(value_now(1))
           nTRYmon = JFIX(value_now(3))
           value_now(9) = value_now(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
           value_now(11) = value_now(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
        ElseIf (mode==9) Then       !German TRY 2010
           Read (lu_data,7001,err=2000,end=2000) (value_now(i),i=1,n_items)
           nTRYreg = JFIX(value_now(1))
           nTRYmon = JFIX(value_now(3))
        EndIf

! Replace some 'missing data' condition in standard TMY files
        If (mode==1) Then   ! TMY
           Do i=1,n_items
              !replace 'missing radiation' values (9999) with zeros.
              If ((value_now(i) > 9998.).and.(value_now(I) < 10000.)) value_now(i) = 0.d0
              !replace 'missing sky cover' values (99) with the last read value.
              If (((i==34).or.(i==35)).and.(value_now(i) > 98)) value_now(i) = value_was(i)
           EndDo
        ElseIf (mode==7) Then
	       If (value_now(27) < 0.) value_now(27)=0.
	       If (value_now(28) < 0.) value_now(28)=0.
        EndIf

160     If (ie_read > 0) goto 2000

! If interpolating average values, calculate the value at the end of the timestep
        Do k=1,n_items
           If ((interp(k) > 0).and.(time_stamp(k)==0)) Then
              begin(k)=2.d0*value_now(k)-begin(k)
           Else
              begin(k)=value_now(k)
           EndIf
        EndDo
     EndDo

! Determine the next reset time for absolute integrations
     i=JFIX((getSimulationStartTime()+Timestep/2.d0))
     time_next=DBLE(i)+1.d0

! Read the values at the current timestep
     If (mode==1) Then   !TMY
        Read (lu_data,6000,err=2000,end=190) (value_next(i),i=1,n_items)
     ElseIf ((mode==2).or.(mode==6)) Then        !TMY2
        Read (lu_data,4000,err=2000,end=190) (value_next(i),i=1,n_items)
     ElseIf ((mode==3).or.(mode==5)) Then   !ENERGYPLUS / CWEC
        Read (lu_data,*,end=190,err=2000) value_next(1),value_next(2),value_next(3),value_next(4),value_next(5),alphanum,value_next(6),value_next(7),value_next(8),value_next(9),value_next(10),value_next(11),value_next(12),value_next(13),value_next(14),value_next(15),value_next(16),value_next(17),value_next(18),value_next(19),value_next(20),value_next(21),value_next(22),value_next(23),value_next(24),value_next(25),value_next(26),value_next(27),value_next(28),value_next(29),value_next(30),value_next(31)
     ElseIf (mode==4) Then   !IWEC
        Read (lu_data,6001,err=2000,end=190) (value_next(i),i=1,n_items)
     ElseIf (mode==7) Then   !TMY3
        Read (lu_data,5004,err=2000,end=190,advance='NO') value_next(1),value_next(2),value_next(3),value_next(4)
        Read (lu_data,*,err=2000,end=190) value_next(5),value_next(6),value_next(7),alphanum,alphanum,value_next(8),alphanum,alphanum,value_next(9),alphanum,alphanum,value_next(10),alphanum,alphanum,value_next(11),alphanum,alphanum,value_next(12),alphanum,alphanum,value_next(13),alphanum,alphanum,value_next(14),alphanum,alphanum,value_next(15),alphanum,alphanum,value_next(16),alphanum,alphanum,value_next(17),alphanum,alphanum,value_next(18),alphanum,alphanum,value_next(19),alphanum,alphanum,value_next(20),alphanum,alphanum,value_next(21),alphanum,alphanum,value_next(22),alphanum,alphanum,value_next(23),alphanum,alphanum,value_next(24),alphanum,alphanum,value_next(25),alphanum,alphanum,value_next(26),alphanum,alphanum,value_next(27),value_next(28)
     ElseIf (mode==8) Then       !German TRY 2004
        Read (lu_data,7001,err=2000,end=190) (value_next(i),i=1,n_items)
        nTRYreg = JFIX(value_next(1))
        nTRYmon = JFIX(value_next(3))
        value_next(9) = value_next(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
        value_next(11) = value_next(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
     ElseIf (mode==9) Then       !German TRY 2010
        Read (lu_data,7001,err=2000,end=190) (value_next(i),i=1,n_items)
        nTRYreg = JFIX(value_next(1))
        nTRYmon = JFIX(value_next(3))
     EndIf

! Replace some 'missing data' condition in standard TMY files
     If (mode==1) Then         !TMY
        Do i=1,n_items
           !replace 'missing radiation' values (9999) with zeros.
           If ((value_next(i) > 9998.).and.(value_next(i) < 10000.)) value_next(i) = 0.d0
           !replace 'missing sky cover' values (99) with the last read value.
           If (((i==34).or.(i==35)).and.(value_next(i) > 98)) value_next(i) = value_now(i)
        EndDo
     ElseIf (mode==7) Then
	    If (value_next(27) < 0.) value_next(27)=value_now(27)
	    If (value_next(28) < 0.) value_next(28)=value_now(28)
     EndIf

     goto 200

190  Call Rewind (Time,lu_data,headerskip,CurrentUnit,CurrentType,ie_read)
     If (mode==1) Then   !TMY
        Read (lu_data,6000,err=2000,end=2000) (value_next(i),i=1,n_items)
     ElseIf ((mode==2).or.(mode==6)) Then        !TMY2
        Read (lu_data,4000,err=2000,end=2000) (value_next(i),i=1,n_items)
     ElseIf ((mode==3).or.(mode==5)) Then   !ENERGYPLUS / CWEC
        Read (lu_data,*,end=2000,err=2000) value_next(1),value_next(2),value_next(3),value_next(4),value_next(5),alphanum,value_next(6),value_next(7),value_next(8),value_next(9),value_next(10),value_next(11),value_next(12),value_next(13),value_next(14),value_next(15),value_next(16),value_next(17),value_next(18),value_next(19),value_next(20),value_next(21),value_next(22),value_next(23),value_next(24),value_next(25),value_next(26),value_next(27),value_next(28),value_next(29),value_next(30),value_next(31)
     ElseIf (mode==4) Then   !IWEC
        Read (lu_data,6001,err=2000,end=2000) (value_next(i),i=1,n_items)
     ElseIf (mode==7) Then   !TMY3
        Read (lu_data,5004,err=2000,end=2000,advance='NO') value_next(1),value_next(2),value_next(3),value_next(4)
        Read (lu_data,*,err=2000,end=2000) value_next(5),value_next(6),value_next(7),alphanum,alphanum,value_next(8),alphanum,alphanum,value_next(9),alphanum,alphanum,value_next(10),alphanum,alphanum,value_next(11),alphanum,alphanum,value_next(12),alphanum,alphanum,value_next(13),alphanum,alphanum,value_next(14),alphanum,alphanum,value_next(15),alphanum,alphanum,value_next(16),alphanum,alphanum,value_next(17),alphanum,alphanum,value_next(18),alphanum,alphanum,value_next(19),alphanum,alphanum,value_next(20),alphanum,alphanum,value_next(21),alphanum,alphanum,value_next(22),alphanum,alphanum,value_next(23),alphanum,alphanum,value_next(24),alphanum,alphanum,value_next(25),alphanum,alphanum,value_next(26),alphanum,alphanum,value_next(27),value_next(28)
     ElseIf (mode==8) Then       !German TRY 2004
        Read (lu_data,7001,err=2000,end=2000) (value_next(i),i=1,n_items)
        nTRYreg = JFIX(value_next(1))
        nTRYmon = JFIX(value_next(3))
        value_next(9) = value_next(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
        value_next(11) = value_next(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
     ElseIf (mode==9) Then       !German TRY 2010
        Read (lu_data,7001,err=2000,end=2000) (value_next(i),i=1,n_items)
        nTRYreg = JFIX(value_next(1))
        nTRYmon = JFIX(value_next(3))
     EndIf

! Replace some 'missing data' condition in standard TMY files
     If (mode==1) Then         !TMY
        Do i=1,n_items
           !replace 'missing radiation' values (9999) with zeros.
           If ((value_next(i) > 9998.).and.(value_next(i) < 10000.)) value_next(i) = 0.d0
           !replace 'missing sky cover' values (99) with the last read value.
           If (((i==34).or.(i==35)).and.(value_next(i) > 98)) value_next(i) = value_now(i)
        EndDo
     ElseIf (mode==7) Then
	    If (value_next(27) < 0.) value_next(27)=value_now(27)
	    If (value_next(28) < 0.) value_next(28)=value_now(28)
     EndIf

200  If (ie_read > 0) goto 2000

     ! Read the values at the future timestep
     If (mode==1) Then   !TMY
        Read (lu_data,6000,err=2000,end=310) (value_future(i),i=1,n_items)
     ElseIf ((mode==2).or.(mode==6)) Then        !TMY2
        Read (lu_data,4000,err=2000,end=310) (value_future(i),i=1,n_items)
     ElseIf ((mode==3).or.(mode==5)) Then   !ENERGYPLUS / CWEC
        Read (lu_data,*,end=310,err=2000) value_future(1),value_future(2),value_future(3),value_future(4),value_future(5),alphanum,value_future(6),value_future(7),value_future(8),value_future(9),value_future(10),value_future(11),value_future(12),value_future(13),value_future(14),value_future(15),value_future(16),value_future(17),value_future(18),value_future(19),value_future(20),value_future(21),value_future(22),value_future(23),value_future(24),value_future(25),value_future(26),value_future(27),value_future(28),value_future(29),value_future(30),value_future(31)
     ElseIf (mode==4) Then   !IWEC
        Read (lu_data,6001,err=2000,end=310) (value_future(i),i=1,n_items)
     ElseIf (mode==7) Then   !TMY3
        Read (lu_data,5004,err=2000,end=310,advance='NO') value_future(1),value_future(2),value_future(3),value_future(4)
        Read (lu_data,*,err=2000,end=310) value_future(5),value_future(6),value_future(7),alphanum,alphanum,value_future(8),alphanum,alphanum,value_future(9),alphanum,alphanum,value_future(10),alphanum,alphanum,value_future(11),alphanum,alphanum,value_future(12),alphanum,alphanum,value_future(13),alphanum,alphanum,value_future(14),alphanum,alphanum,value_future(15),alphanum,alphanum,value_future(16),alphanum,alphanum,value_future(17),alphanum,alphanum,value_future(18),alphanum,alphanum,value_future(19),alphanum,alphanum,value_future(20),alphanum,alphanum,value_future(21),alphanum,alphanum,value_future(22),alphanum,alphanum,value_future(23),alphanum,alphanum,value_future(24),alphanum,alphanum,value_future(25),alphanum,alphanum,value_future(26),alphanum,alphanum,value_future(27),value_future(28)
     ElseIf (mode==8) Then       !German TRY 2004
        Read (lu_data,7001,err=2000,end=310) (value_future(i),i=1,n_items)
        nTRYreg = JFIX(value_future(1))
        nTRYmon = JFIX(value_future(3))
        value_future(9) = value_future(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
        value_future(11) = value_future(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
     ElseIf (mode==9) Then       !German TRY 2010
        Read (lu_data,7001,err=2000,end=310) (value_future(i),i=1,n_items)
        nTRYreg = JFIX(value_future(1))
        nTRYmon = JFIX(value_future(3))
     EndIf

! Replace some 'missing data' condition in standard TMY files
     If (mode==1) Then         !TMY
        Do i=1,n_items
           !replace 'missing radiation' values (9999) with zeros.
           If ((value_future(i) > 9998.).and.(value_future(i) < 10000.)) value_future(i) = 0.d0
           !replace 'missing sky cover' values (99) with the last read value.
           If (((i==34).or.(i==35)).and.(value_future(i) > 98)) value_future(i) = value_next(i)
        EndDo
     ElseIf (mode==7) Then
	    If (value_future(27) < 0.) value_future(27)=value_next(27)
	    If (value_future(28) < 0.) value_future(28)=value_next(28)
     EndIf

     goto 320

310  Call Rewind (Time,lu_data,headerskip,CurrentUnit,CurrentType,ie_read)
     If (mode==1) Then   !TMY
        Read (lu_data,6000,err=2000,end=2000) (value_future(i),i=1,n_items)
     ElseIf ((mode==2).or.(mode==6)) Then        !TMY2
        Read (lu_data,4000,err=2000,end=2000) (value_future(i),i=1,n_items)
     ElseIf ((mode==3).or.(mode==5)) Then   !ENERGYPLUS / CWEC
        Read (lu_data,*,end=2000,err=2000) value_future(1),value_future(2),value_future(3),value_future(4),value_future(5),alphanum,value_future(6),value_future(7),value_future(8),value_future(9),value_future(10),value_future(11),value_future(12),value_future(13),value_future(14),value_future(15),value_future(16),value_future(17),value_future(18),value_future(19),value_future(20),value_future(21),value_future(22),value_future(23),value_future(24),value_future(25),value_future(26),value_future(27),value_future(28),value_future(29),value_future(30),value_future(31)
     ElseIf (mode==4) Then   !IWEC
        Read (lu_data,6001,err=2000,end=2000) (value_future(i),i=1,n_items)
     ElseIf (mode==7) Then   !TMY3
        Read (lu_data,5004,err=2000,end=2000,advance='NO') value_future(1),value_future(2),value_future(3),value_future(4)
        Read (lu_data,*,err=2000,end=2000) value_future(5),value_future(6),value_future(7),alphanum,alphanum,value_future(8),alphanum,alphanum,value_future(9),alphanum,alphanum,value_future(10),alphanum,alphanum,value_future(11),alphanum,alphanum,value_future(12),alphanum,alphanum,value_future(13),alphanum,alphanum,value_future(14),alphanum,alphanum,value_future(15),alphanum,alphanum,value_future(16),alphanum,alphanum,value_future(17),alphanum,alphanum,value_future(18),alphanum,alphanum,value_future(19),alphanum,alphanum,value_future(20),alphanum,alphanum,value_future(21),alphanum,alphanum,value_future(22),alphanum,alphanum,value_future(23),alphanum,alphanum,value_future(24),alphanum,alphanum,value_future(25),alphanum,alphanum,value_future(26),alphanum,alphanum,value_future(27),value_future(28)
     ElseIf (mode==8) Then       !German TRY 2004
        Read (lu_data,7001,err=2000,end=2000) (value_future(i),i=1,n_items)
        nTRYreg = JFIX(value_future(1))
        nTRYmon = JFIX(value_future(3))
        value_future(9) = value_future(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
        value_future(11) = value_future(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
     ElseIf (mode==9) Then       !German TRY 2010
        Read (lu_data,7001,err=2000,end=2000) (value_future(i),i=1,n_items)
        nTRYreg = JFIX(value_future(1))
        nTRYmon = JFIX(value_future(3))
     EndIf

! Replace some 'missing data' condition in standard TMY files
     If (mode==1) Then         !TMY
        Do i=1,n_items
           !replace 'missing radiation' values (9999) with zeros.
           If ((value_future(i) > 9998.).and.(value_future(i) < 10000.)) value_future(i) = 0.d0
           !replace 'missing sky cover' values (99) with the last read value.
           If (((i==34).or.(i==35)).and.(value_future(i) > 98)) value_future(i) = value_next(i)
        EndDo
     ElseIf (mode==7) Then
	    If (value_future(27) < 0.) value_future(27)=value_next(27)
	    If (value_future(28) < 0.) value_future(28)=value_next(28)
     EndIf

320  If (ie_read > 0) goto 2000

! Set the previous values as well
     Do j=1,n_items
        value_was(j)=value_now(j)
     EndDo

! Set the initial storage variables here                  
     store(1)=time_next
     Do j=1,n_items
        store(1+j)=value_was(j)
        store(1+n_items+j)=value_now(j)
        store(1+2*n_items+j)=value_next(j)
        store(1+3*n_items+j)=value_future(j)
        store(1+4*n_items+j)=begin(j)
        store(1+5*n_items+j)=begin(j)

        store(1+6*n_items+j)=DBLE(interp(j))
        store(1+7*n_items+j)=DBLE(time_stamp(j))
        store(1+8*n_items+j)=xmult(j)
        store(1+9*n_items+j)=xadd(j)
     EndDo

	 store(1+10*n_items+1)=annual_ave_T
	 store(1+10*n_items+2)=annual_min_T
	 store(1+10*n_items+3)=annual_max_T
	 store(1+10*n_items+4)=annual_ave_I

	 Do j=1,14
	    store(1+10*n_items+4+j)=month_ave_T(j-1)
	    store(1+10*n_items+4+14+j)=month_min_T(j-1)
	    store(1+10*n_items+4+28+j)=month_max_T(j-1)
	    store(1+10*n_items+4+42+j)=x_heat(j-1)
	    store(1+10*n_items+4+56+j)=x_cool(j-1)
	 EndDo
    
     Do j = 1,75+10*n_items
       Call SetStaticArrayValue(j,store(j))
     EndDo
     Do j = 1,7
       Call SetStaticArrayValue(75+10*n_items+j,0.d0)
     EndDo
     Do j = 1,14
        Call SetStaticArrayValue(75+10*n_items+8+5+j,0.d0)
     EndDo
     Do j = 1,8
        Call SetStaticArrayValue(75+10*n_items+8+5+14+j,0.d0)
     EndDo
      

! Set the value of the initial outputs
     Do j=1,n_items
! Interpolating of average values
        If ((interp(j) > 0).and.(time_stamp(j)==0)) Then
! Calculate the instantaneous value at the end of the data timestep
           ending(j)=2.*value_next(j)-begin(j)
! Calculate the interpolation ratio
           ratio=(time_next-Time)
! Calculate the instantaneous value at the beginning of the simulation
           xnow=begin(j)+(1.d0-ratio)*(ending(j)-begin(j))
! Set the instantaneous value at the end of the next timestep
           ratio=(time_next-Time-Timestep)
           xnext=begin(j)+(1.d0-ratio)*(ending(j)-begin(j))
           out_temp(j)=xnow*xmult(j)+xadd(j)
! Interpolating instantaneous values
        ElseIf (interp(j) > 0) Then
! Calculate the instantaneous value at the end of the data timestep
           ending(j)=value_next(j)
! Calculate the interpolation ratio
           ratio=(time_next-Time)
! Calculate the instantaneous value at the beginning of the simulation
           xnow=begin(j)+(1.d0-ratio)*(ending(j)-begin(j))
! Set the instantaneous value at the end of the next timestep
           ratio=(time_next-Time-Timestep)
           xnext=begin(j)+(1.d0-ratio)*(ending(j)-begin(j))
           out_temp(j)=xnow*xmult(j)+xadd(j)
! No interpolation of values
        Else
           If (Time > (time_next-1.d0+Timestep/2.)) Then
              out_temp(j)=value_next(j)*xmult(j)+xadd(j)
           Else
              out_temp(j)=value_now(j)*xmult(j)+xadd(j)
           EndIf
        EndIf
     EndDo
         
     td1=JFIX(Time-1./3600.d0/2.d0)
     td2=JFIX(Time-1./3600.d0/2.d0)+1.d0

! Calculate the current month
     If ((DMOD(Time,8760.d0)-Timestep/2.)<=744) Then
	    j_month=1
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=1416.) Then
	    j_month=2
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=2160.) Then
	    j_month=3
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=2880.) Then
	    j_month=4
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=3624.) Then
	    j_month=5
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=4344.) Then
	    j_month=6
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=5088.) Then
	    j_month=7
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=5832.) Then
	    j_month=8
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=6552.) Then
	    j_month=9
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=7296.) Then
	    j_month=10
     ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=8016.) Then
	    j_month=11
     Else 
	    j_month=12
	 EndIf

! Now set the outputs based on the mode
     If (mode==1) Then
! Month        
        Call SetOutputValue(46+8*ns,out_temp(2))
! Hour of the month
        Call SetOutputValue(47+8*ns,out_temp(4))
! IDN
        Call SetOutputValue(15,out_temp(7))
! Ihd
        Call SetOutputValue(14,out_temp(8))
! Tdb
        Call SetOutputValue(1,out_temp(30)) 
! Windspeed
        Call SetOutputValue(8,out_temp(33)) 
! Wind direction
        Call SetOutputValue(9,out_temp(32)) 
! Dew point
        Call SetOutputValue(2,out_temp(31)) 
! Ill
        Call SetOutputValue(36+8*ns,-99.d0) 
! Ill dn
        Call SetOutputValue(37+8*ns,-99.d0)
! Ill diff
        Call SetOutputValue(38+8*ns,-99.d0)
! Ill zen
        Call SetOutputValue(39+8*ns,-99.d0) 
! Sky cover
        Call SetOutputValue(11,out_temp(34)) 
! Opaque cover
        Call SetOutputValue(12,out_temp(35)) 
! Pressure
        Call SetOutputValue(10,out_temp(29)) 
! Horiz visibility
        Call SetOutputValue(40+8*NS,out_temp(19)) 
! Ceiling height
        Call SetOutputValue(41+8*NS,out_temp(17))
! Precip water
        Call SetOutputValue(42+8*NS,-99.d0) 
! Optical depth
        Call SetOutputValue(43+8*NS,-99.d0) 
! Snow depth
        Call SetOutputValue(44+8*NS,-99.d0) 
! Days since last snow
        Call SetOutputValue(45+8*NS,-99.d0) 
! W, Twb and %rh - calculated from Tdb, pressure and Tdp
        psydat(1)=getOutputValue(10)
        psydat(2)=getOutputValue(1)
        psydat(5)=getOutputValue(2)
        Call MoistAirProperties(CurrentUnit,CurrentType,1,3,1,psydat,0,ie)
        If (ErrorFound()) Return
        Call SetOutputValue(6,psydat(6))           !humidity ratio
        Call SetOutputValue(7,psydat(4)*100.d0)    !relative humidity
        Call SetOutputValue(3,psydat(3))

     ElseIf ((mode==2).or.(mode==6)) Then  !TMY2
! Month        
        Call SetOutputValue(46+8*ns,out_temp(2))
! Hour of the month
        Call SetOutputValue(47+8*ns,(out_temp(3)-1.d0)*24.d0+out_temp(4))
! Idn
        Call SetOutputValue(15,out_temp(8))
! Ihd
        Call SetOutputValue(14,out_temp(9))
! Tdb
        Call SetOutputValue(1,out_temp(16))
! Windspeed
        Call SetOutputValue(8,out_temp(21))
! Wind direction
        Call SetOutputValue(9,out_temp(20))
! Dew point
        Call SetOutputValue(2,out_temp(17))
! Relative humidity
        Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(18))))))
! Ill
        Call SetOutputValue(36+8*ns,out_temp(10))
! Ill dn
        Call SetOutputValue(37+8*ns,out_temp(11))
! Ill diff
        Call SetOutputValue(38+8*ns,out_temp(12))
! Ill zen
        Call SetOutputValue(39+8*ns,out_temp(13))
! Sky cover
        Call SetOutputValue(11,out_temp(14))
! Opaque cover
        Call SetOutputValue(12,out_temp(15))
! Pressure
        Call SetOutputValue(10,out_temp(19))
! Horiz visibility
        Call SetOutputValue(40+8*ns,out_temp(22))
! Ceiling height
        Call SetOutputValue(41+8*ns,out_temp(23))
! Precip water
        Call SetOutputValue(42+8*ns,out_temp(24))
! Optical depth
        Call SetOutputValue(43+8*ns,out_temp(25))
! Snow depth
        Call SetOutputValue(44+8*ns,out_temp(26))
! Days since last snow
        Call SetOutputValue(45+8*NS,out_temp(27))
! W and Twb - calculated from Tdb, pressure and %rh
        psydat(1)=getOutputValue(10)
        psydat(2)=getOutputValue(1)
        psydat(4)=getOutputValue(7)/100.
        Call MoistAirProperties(CurrentUnit,CurrentType,1,2,1,psydat,0,ie)
        If (ErrorFound()) Return
        Call SetOutputValue(6,psydat(6))
        Call SetOutputValue(3,psydat(3))

     ElseIf ((mode==3).or.(mode==5)) Then
! Month        
        Call SetOutputValue(46+8*ns,out_temp(2))
! Hour of the month
        Call SetOutputValue(47+8*ns,(out_temp(3)-1.d0)*24.d0+out_temp(4))
! Idn
        Call SetOutputValue(15,out_temp(14))
! Ihd
        Call SetOutputValue(14,out_temp(15))
! Tdb
        Call SetOutputValue(1,out_temp(6))
! Windspeed
        Call SetOutputValue(8,out_temp(21))
! Wind direction
        Call SetOutputValue(9,out_temp(20))
! Dew point
        Call SetOutputValue(2,out_temp(7))
! Relative humidity
        Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(8))))))
! Ill
        Call SetOutputValue(36+8*ns,out_temp(16))
! Ill dn
        Call SetOutputValue(37+8*ns,out_temp(17))
! Ill diff
        Call SetOutputValue(38+8*ns,out_temp(18))
! Ill zen
        Call SetOutputValue(39+8*ns,out_temp(19))
! Sky cover
        Call SetOutputValue(11,out_temp(22))
! Opaque cover
        Call SetOutputValue(12,out_temp(23))
! Pressure
        Call SetOutputValue(10,out_temp(9))
! Horiz visibility
        Call SetOutputValue(40+8*ns,out_temp(24))
! Ceiling height
        Call SetOutputValue(41+8*ns,out_temp(25))
! Precip water
        Call SetOutputValue(42+8*ns,out_temp(28))
! Optical depth
        Call SetOutputValue(43+8*ns,out_temp(29))
! Snow depth
        Call SetOutputValue(44+8*ns,out_temp(30))
! Days since last snow
        Call SetOutputValue(45+8*ns,out_temp(31))
! W and Twb - calculated from Tdb, pressure and %rh
        psydat(1)=getOutputValue(10)
        psydat(2)=getOutputValue(1)
        psydat(4)=getOutputValue(7)/100.
        Call MoistAirProperties(CurrentUnit,CurrentType,1,2,1,psydat,0,ie)
        If (ErrorFound()) Return
        Call SetOutputValue(6,psydat(6))
        Call SetOutputValue(3,psydat(3))

     ElseIf (mode==4) Then !IWEC
! Month
        Call SetOutputValue(46+8*ns,out_temp(2)) 
! Hour of the month
        Call SetOutputValue(47+8*ns,(out_temp(3)-1.d0)*24.d0+out_temp(4))
! Direct normal solar radiation
        Call SetOutputValue(15,out_temp(8)) 
! Global horizontal diffuse solar radiation
        Call SetOutputValue(14,out_temp(9)) 
! Dry bulb temperature           
        Call SetOutputValue(1,out_temp(16)) 
! Windspeed          
        Call SetOutputValue(8,out_temp(21)) 
! Wind direction
        Call SetOutputValue(9,out_temp(20)) 
! Dew point temperature
        Call SetOutputValue(2,out_temp(17)) 
! Relative humidity          
        Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(18))))))
! Global horizontal illuminance
        Call SetOutputValue(36+8*ns,out_temp(10)) 
! Direct normal illuminance           
        Call SetOutputValue(37+8*ns,out_temp(11)) 
! Diffuse horizontal illuminance           
        Call SetOutputValue(38+8*ns,out_temp(12)) 
! Zenith luminance          
        Call SetOutputValue(39+8*ns,out_temp(13)) 
! Total sky cover           
        Call SetOutputValue(11,out_temp(14))
! Opaque sky cover           
        Call SetOutputValue(12,out_temp(15))
! Pressure           
        Call SetOutputValue(10,out_temp(19)) 
! Visibility          
        Call SetOutputValue(40+8*ns,out_temp(22)) 
! Ceiling height          
        Call SetOutputValue(41+8*ns,out_temp(23)) 
! Precipitable water
        Call SetOutputValue(42+8*ns,out_temp(25)) 
! Aerosol optical depth
        Call SetOutputValue(43+8*ns,out_temp(26))
! Snow depth
        Call SetOutputValue(44+8*ns,out_temp(27)) 
! Days since last snowfall
        Call SetOutputValue(45+8*ns,out_temp(28)) 
! W - calculated from Tdb, pressure and rh          
        psydat(1)=getOutputValue(10)
        psydat(2)=getOutputValue(1)
        psydat(4)=getOutputValue(7)/100.
        Call MoistAirProperties(CurrentUnit,CurrentType,1,2,1,psydat,0,ie)
        If (ErrorFound()) Return
        Call SetOutputValue(6,psydat(6))
        Call SetOutputValue(3,psydat(3))

     ElseIf (mode==7) Then  !TMY3
! Month        
        Call SetOutputValue(46+8*ns,out_temp(1))
! Hour of the month
        Call SetOutputValue(47+8*ns,(out_temp(2)-1.d0)*24.d0+out_temp(4))
! Idn
        Call SetOutputValue(15,out_temp(8))
! Ihd
        Call SetOutputValue(14,out_temp(9))
! Tdb
        Call SetOutputValue(1,out_temp(16))
! Windspeed
        Call SetOutputValue(8,out_temp(21))
! Wind direction
        Call SetOutputValue(9,out_temp(20))
! Dew point
        Call SetOutputValue(2,out_temp(17))
! Relative humidity
        Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(18))))))
! Ill
        Call SetOutputValue(36+8*ns,out_temp(10))
! Ill dn
        Call SetOutputValue(37+8*ns,out_temp(11))
! Ill diff
        Call SetOutputValue(38+8*ns,out_temp(12))
! Ill zen
        Call SetOutputValue(39+8*ns,out_temp(13))
! Sky cover
        Call SetOutputValue(11,out_temp(14))
! Opaque cover
        Call SetOutputValue(12,out_temp(15))
! Pressure
        Call SetOutputValue(10,out_temp(19))
! Horiz visibility
        Call SetOutputValue(40+8*ns,out_temp(22))
! Ceiling height
        Call SetOutputValue(41+8*ns,out_temp(23))
! Precip water
        Call SetOutputValue(42+8*ns,out_temp(24))
! Optical depth
        Call SetOutputValue(43+8*ns,out_temp(25))
! Liquid precipitation depth
        Call SetOutputValue(44+8*ns,out_temp(27))
! Liquid preci[itation quantity
        Call SetOutputValue(45+8*ns,out_temp(28))
! W and Twb - calculated from Tdb, pressure and rh
        psydat(1)=getOutputValue(10)
        psydat(2)=getOutputValue(1)
        psydat(4)=getOutputValue(7)/100.
        Call MoistAirProperties(CurrentUnit,CurrentType,1,2,1,psydat,0,ie)
        If (ErrorFound()) Return
        Call SetOutputValue(6,psydat(6))
        Call SetOutputValue(3,psydat(3))
     ElseIf ((mode==8).OR.(mode==9)) Then  !German TRY 2004 or TRY 2010
! Month        
        Call SetOutputValue(46+8*ns,out_temp(3))
! Hour of the month
        Call SetOutputValue(47+8*ns,(out_temp(5)-1.d0)*24.d0+out_temp(4))
! Idn
        Call SetOutputValue(15,-99.d0)
! I
        Call SetOutputValue(14,-99.d0)
! Tdb
        Call SetOutputValue(1,out_temp(9))
! Windspeed
        Call SetOutputValue(8,out_temp(8))
! Wind direction
        Call SetOutputValue(9,out_temp(7))
! Relative humidity
        Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(12))))))
! Ill
        Call SetOutputValue(36+8*ns,-99.d0)
! Ill dn
        Call SetOutputValue(37+8*ns,-99.d0)
! Ill diff
        Call SetOutputValue(38+8*ns,-99.d0)
! Ill zen
        Call SetOutputValue(39+8*ns,-99.d0)
! Sky cover
        Call SetOutputValue(11,out_temp(6))
! Opaque cover
        Call SetOutputValue(12,-99.d0)
! Pressure
        Call SetOutputValue(10,out_temp(10))
! Horiz visibility
        Call SetOutputValue(40+8*ns,-99.d0)
! Ceiling height
        Call SetOutputValue(41+8*ns,-99.d0)
! Precip water
        Call SetOutputValue(42+8*ns,-99.d0)
! Optical depth
        Call SetOutputValue(43+8*ns,-99.d0)
! Liquid precipitation depth
        Call SetOutputValue(44+8*ns,-99.d0)
! Liquid preci[itation quantity
        Call SetOutputValue(45+8*ns,-99.d0)
! Humidity Ratio
        Call SetOutputValue(6,out_temp(11))
! Tdp and Twb - calculated from Tdb, pressure and W
        psydat(1)=getOutputValue(10)
        psydat(2)=getOutputValue(1)
        psydat(6)=getOutputValue(6)
        Call MoistAirProperties(CurrentUnit,CurrentType,1,4,1,psydat,0,ie)
        If (ErrorFound()) Return
        Call SetOutputValue(2,psydat(5))
        Call SetOutputValue(3,psydat(3))
     EndIf

! Calculate the max and min monthly average
	 j_min=1
	 j_max=1
	 Do j=2,12
	    If (month_ave_T(j) > month_ave_T(j_max)) j_max=j
	    If (month_ave_T(j) < month_ave_T(j_min)) j_min=j
	 EndDo

! Calculate the mains water temperature based on an algorithm by Craig Christensen and Jay Burch of NREL
     xday = DMOD(Time,8760.d0)/24.
	 T_mains = (annual_ave_T*1.8+32.) - 5.915 + 0.008422*(annual_ave_I)*0.947817/10.7639 + (-0.3112+0.01553*(annual_ave_T*1.8+32.))*(month_ave_T(j_max)-month_ave_T(j_min))*1.8/2.*DSIN(0.017453292*(0.986*xday-191.3+1.069*(annual_ave_T*1.8+32.)))
	 T_mains=(T_mains-32.)/1.8

! Set the latitude, longitude, elevation and shift
     Call SetOutputValue(24+8*ns,lat)
     Call SetOutputValue(25+8*ns,long)
     Call SetOutputValue(26+8*ns,shift)
     Call SetOutputValue(27+8*ns,elev)
! Set the monthly average air temperatures
	 Call SetOutputValue(30+8*ns,month_ave_T(j_month))
	 Call SetOutputValue(31+8*ns,month_min_T(j_month))
	 Call SetOutputValue(32+8*ns,month_max_T(j_month))
! Set the annual air temperatures
	 Call SetOutputValue(33+8*ns,annual_ave_T)
	 Call SetOutputValue(34+8*ns,annual_min_T)
	 Call SetOutputValue(35+8*ns,annual_max_T)
! Set the heating and cooling season outputs
     Call SetOutputValue(28+8*ns,x_heat(j_month))
     Call SetOutputValue(29+8*ns,x_cool(j_month))
! Set the mains water temperature
     Call SetOutputValue(5,T_mains)

!Calculate the ground reflectance
     If (mode==7) Then
        If (rhog < 0.0) Then
           rho = rhog_snow
	       Call SetOutputValue(51+8*ns,rho)
	    ElseIf ((out_temp(26) < 0.) .or. (out_temp(26) > 1.)) Then
	       Call SetOutputValue(51+8*ns,rhog)
	       rho=rhog
	    Else
	       Call SetOutputValue(51+8*ns,out_temp(26))
	       rho=out_temp(26)
	    EndIf
	 Else
        If ((getOutputValue(44+8*ns) > 0.).and.(getOutputValue(44+8*ns)/=999.)) Then  ! 999 indicates no data.
	       rho=rhog_snow
	    Else
	       rho=rhog
        EndIf
	    Call SetOutputValue(51+8*ns,rho)
	 EndIf

! Set the solar time indicator
     If (mode==1) Then
	    i_solartime=-1
	 Else
	    i_solartime=1
	 EndIf

     HorNow=getOutputValue(14)
	 IdnNow=getOutputValue(15)
     StartTime = Time
     EndTime = time_next
     HorEndP = 0.d0
     IdnEndP = 0.d0
     shape_mode = 0
	 
! Process the incident solar radiation
     itemp1=1
	 itemp2=3
     xtemp1=0.
	 xtemp2=0.
	 If ((mode /= 8).AND.(mode /= 9)) Then
	    mode_horiz = 6
        time_rad = -1
        HorNow=getOutputValue(14)
	    IdnNow=getOutputValue(15)
        If (mode == 1) Then
           HorNext = value_future(8)*xmult(8)+xadd(8)
           IdnNext = value_future(7)*xmult(7)+xadd(7)
        ElseIf ((mode == 3) .or. (mode == 5)) Then
           HorNext = value_future(15)*xmult(15)+xadd(15)
           IdnNext = value_future(14)*xmult(14)+xadd(14)
        Else
           HorNext = value_future(9)*xmult(9)+xadd(9)
           IdnNext = value_future(8)*xmult(8)+xadd(8)
        EndIf
        Call GetRadInterpolateValues(Time, StartTime, EndTime, timestep, HorNow, HorNext, HorStartP, HorMidP, HorEndP, HorNum, HorMid, HorSections, i_solartime, shift, lat)    
        Call GetRadInterpolateValues(Time, StartTime, EndTime, timestep, IdnNow, IdnNext, IdnStartP, IdnMidP, IdnEndP, IdnNum, IdnMid, IdnSections, i_solartime, shift, lat)    
        Call GetRadPoint(Time, StartTime, EndTime, Timestep, HorStartP, HorMidP, HorEndP, HorSections, HorNum, HorMid, HorPoint)
        Call GetRadPoint(Time, StartTime, EndTime, Timestep, IdnStartP, IdnMidP, IdnEndP, IdnSections, IdnNum, IdnMid, IdnPoint)
	    rad_input(3) = HorPoint
	    rad_input(4) = IdnPoint
	 Else
	    mode_horiz = 3
	    rad_input(2) = out_temp(14)
	    rad_input(3) = out_temp(15)
	 EndIf
	 Call SetRadiationData(CurrentUnit,mode_horiz,shape_mode,rad_input,rho,itemp2,lat,elev,shift,i_solartime,SolConst,td1,td2)
	 Call GetHorizontalRadiation(Time,mode_horiz,shape_mode,rad_input,rho,xtemp1,xtemp2,itemp1,itemp2,lat,elev,shift,i_solartime,SolConst,td1,td2,solar,ierror_rad)
     Call SetStaticArrayValue(75+10*n_items+8+5+1,HorStartP)
     Call SetStaticArrayValue(75+10*n_items+8+5+2,HorMidP)
     Call SetStaticArrayValue(75+10*n_items+8+5+3,HorEndP)
     Call SetStaticArrayValue(75+10*n_items+8+5+4,HorSections)
     Call SetStaticArrayValue(75+10*n_items+8+5+5,HorNum)
     Call SetStaticArrayValue(75+10*n_items+8+5+6,HorMid)
     Call SetStaticArrayValue(75+10*n_items+8+5+7,IdnStartP)
     Call SetStaticArrayValue(75+10*n_items+8+5+8,IdnMidP)
     Call SetStaticArrayValue(75+10*n_items+8+5+9,IdnEndP)
     Call SetStaticArrayValue(75+10*n_items+8+5+10,IdnSections)
     Call SetStaticArrayValue(75+10*n_items+8+5+11,IdnNum)
     Call SetStaticArrayValue(75+10*n_items+8+5+12,IdnMid)
     Call SetStaticArrayValue(75+10*n_items+8+5+13,EndTime)
     Call SetStaticArrayValue(75+10*n_items+8+5+14,StartTime)
! Need to do the error checking
     Do j = 1,8
        RadErrors(j) = .false.
     EndDo
     If (ierror_rad == 1) Then
        Call Messages(-1,'An illegal tracking mode has been specified for the radiation calculation.  The mode must be between 1 and 4.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 4) Then
        Call Messages(-1,'An inconsistency has been encountered in the weather data file relating to the sun-up and sun-down hour angles calculations and the measured solar radiation data.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 11) Then
        Call Messages(-1,'An illegal tilted surface mode has been specified for the radiation calculation.  The mode must be between 1 and 4.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 12) Then
        Call Messages(-1,'An illegal value for the latitude has been specified for the radiation calculation.  The latitude must be between -90 and 90 degrees.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 13) Then
        Call Messages(-1,'An illegal value for the shift in solar time has been specified for the radiation calculation.  The shift must be between -360 and 360 degrees.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 14) Then
        Call Messages(-1,'An illegal value for the ground reflectance has been specified for the radiation calculation.  The reflectance must be between 0 and 1.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 2) Then
        Call Messages(-1,'The calculated solar hour angle for the end of the data timestep has been modified from its original value.  This fix was added to account for sundown periods near midnight at high-latitude locations.  If this warning is noted for locations that do not meet this criteria, please contact your TRNSYS distributor.','Warning',CurrentUnit,CurrentType)
     ElseIf (ierror_rad == 3) Then
        RadErrors(1) = .true.
     ElseIf (ierror_rad == 5) Then
        RadErrors(2) = .true.
     ElseIf (ierror_rad == 6) Then
        RadErrors(3) = .true.
     ElseIf (ierror_rad == 7) Then
        RadErrors(4) = .true.
     ElseIf (ierror_rad == 8) Then
        RadErrors(5) = .true.
     ElseIf (ierror_rad == 9) Then
        RadErrors(6) = .true.
     ElseIf (ierror_rad == 10) Then
        RadErrors(7) = .true.
     ElseIf (ierror_rad == 16) Then
        RadErrors(8) = .true.
     ElseIf (ierror_rad == 15) Then
        Call Messages(-1,'The simulation time is outside the data reading interval.','Fatal',CurrentUnit,CurrentType)
        Return
     EndIf
	 Call SetOutputValue(13,solar(1))
	 Call SetOutputValue(16,solar(2))
	 Call SetOutputValue(17,solar(3))
	 Call SetOutputValue(18,solar(10))
	 Call SetOutputValue(19,solar(11))
	 Call SetOutputValue(20,solar(12))
	 Call SetOutputValue(21,solar(13))
	 Call SetOutputValue(22,solar(12)+solar(13))
	 Call SetOutputValue(23,solar(6))
	   
	 Do j=1,ns
	    If ((track_mode(j)==2).or.(track_mode(j)==3)) THEN
	       slope(j)=axis_slope(j)
	       azimuth(j)=axis_azimuth(j)
	    EndIf
	    If ((mode /= 8).AND.(mode /= 9)) Then
	       mode_horiz = 6
           Call GetRadPoint(Time, StartTime, EndTime, Timestep, HorStartP, HorMidP, HorEndP, HorSections, HorNum, HorMid, HorPoint)
           Call GetRadPoint(Time, StartTime, EndTime, Timestep, IdnStartP, IdnMidP, IdnEndP, IdnSections, IdnNum, IdnMid, IdnPoint)
	       rad_input(3) = HorPoint
	       rad_input(4) = IdnPoint
	    Else
	       mode_horiz = 3
	       rad_input(2) = out_temp(14)
	       rad_input(3) = out_temp(15)
	    EndIf
	    Call SetRadiationData(CurrentUnit,mode_horiz,shape_mode,rad_input,rho,rad_mode,lat,elev,shift,i_solartime,SolConst,td1,td2)
        Call GetTiltedRadiation(Time,rho,slope(j),azimuth(j),track_mode(j),rad_mode,elev,SolConst,solar,ierror_rad)
! Need to do the error checking
        Do k = 1,8
           RadErrors(k) = .false.
        EndDo
        If (ierror_rad == 1) Then
           Call Messages(-1,'An illegal tracking mode has been specified for the radiation calculation.  The mode must be between 1 and 4.','FATAL',CurrentUnit,CurrentType)
	       Return
        ElseIf (ierror_rad == 4) Then
           Call Messages(-1,'The simulation timestep is greater than the data timestep for the radiation calculation.  The simulation timestep needs to be reduced.','FATAL',CurrentUnit,CurrentType)
	       Return
        ElseIf (ierror_rad == 11) Then
           Call Messages(-1,'An illegal tilted surface mode has been specified for the radiation calculation.  The mode must be between 1 and 4.','FATAL',CurrentUnit,CurrentType)
	       Return
        ElseIf (ierror_rad == 12) Then
           Call Messages(-1,'An illegal value for the latitude has been specified for the radiation calculation.  The latitude must be between -90 and 90 degrees.','FATAL',CurrentUnit,CurrentType)
	       Return
        ElseIf (ierror_rad == 13) Then
           Call Messages(-1,'An illegal value for the shift in solar time has been specified for the radiation calculation.  The shift must be between -360 and 360 degrees.','FATAL',CurrentUnit,CurrentType)
	       Return
        ElseIf (ierror_rad == 14) Then
           Call Messages(-1,'An illegal value for the ground reflectance has been specified for the radiation calculation.  The refelectance must be between 0 and 1.','FATAL',CurrentUnit,CurrentType)
	       Return
        ElseIf (ierror_rad == 2) Then
           Call Messages(-1,'The calculated solar hour angle for the end of the data timestep has been modified from its original value.  This fix was added to account for sundown periods near midnight at high-latitude locations.  If this warning is noted for locations that do not meet this criteria, please contact your TRNSYS distributor.','Warning',CurrentUnit,CurrentType)
        ElseIf (ierror_rad == 3) Then
           RadErrors(1) = .true.
        ElseIf (ierror_rad == 5) Then
           RadErrors(2) = .true.
        ElseIf (ierror_rad == 6) Then
           RadErrors(3) = .true.
        ElseIf (ierror_rad == 7) Then
           RadErrors(4) = .true.
        ElseIf (ierror_rad == 8) Then
           RadErrors(5) = .true.
        ElseIf (ierror_rad == 9) Then
           RadErrors(6) = .true.
        ElseIf (ierror_rad == 10) Then
           RadErrors(7) = .true.
         ElseIf (ierror_rad == 16) Then
           RadErrors(8) = .true.
        ElseIf (ierror_rad == 15) Then
           Call Messages(-1,'The simulation time is outside the data reading interval.','Fatal',CurrentUnit,CurrentType)
           Return
        EndIf
        Call SetOutputValue(23+j,solar(10))
        Call SetOutputValue(23+ns+j,solar(11))
        Call SetOutputValue(23+2*ns+j,solar(12))
        Call SetOutputValue(23+3*ns+j,solar(13))
        Call SetOutputValue(23+4*ns+j,solar(12)+solar(13))
        Call SetOutputValue(23+5*ns+j,solar(6))
        Call SetOutputValue(23+6*ns+j,solar(4))
        Call SetOutputValue(23+7*ns+j,solar(5))
	 EndDo

! Calculate the sky coverage for TRY data
 If ((mode==8).OR.(mode==9)) then
     E_dir  = GetOutputValue(19)
     E_dif  = GetOutputValue(20)
     Eglob  = E_dir + E_dif 
     If (Eglob < 150.d0) Then
	   cc = (getStaticArrayValue(74+10*n_items+8+1)+getStaticArrayValue(74+10*n_items+8+2)+getStaticArrayValue(74+10*n_items+8+3))/3.d0
       Do j=1,5
          Call SetStaticArrayValue(74+10*n_items+8+j,cc)
       EndDo
    Else
       help = (1.4286d0*(E_dif/Eglob)-0.3d0)
       If (help < 0.d0) help = 0.001d0
       cc = (help)**0.5d0
       IF (cc > 1.d0) cc = 1.d0
       If (Dabs(Time-JFIX(Time)) < Timestep/2.d0) Then
          Call SetStaticArrayValue(74+10*n_items+8+1,getStaticArrayValue(74+10*n_items+8+2))
          Call SetStaticArrayValue(74+10*n_items+8+2,getStaticArrayValue(74+10*n_items+8+3))
          Call SetStaticArrayValue(74+10*n_items+8+3,getStaticArrayValue(74+10*n_items+8+4))
          Call SetStaticArrayValue(74+10*n_items+8+4,getStaticArrayValue(74+10*n_items+8+5))
          Call SetStaticArrayValue(74+10*n_items+8+5,cc)
       EndIf
    EndIf
     ecld = 0.8 
     eosky = 0.711d0+0.56d0*(getOutputValue(2)/100.d0)+0.73d0*(getOutputValue(2)/100.d0)**2
     eosky = eosky+.013d0*DCOS(2.d0*3.141592654d0*(DMOD(Time,24.d0)/24.d0))
     eosky = eosky+.00012d0*((getOutputValue(10)*1013.25d0)-1000.d0)
     esky = eosky+(1.d0-eosky)*cc*ecld
     tsky = esky**0.25d0*(getOutputValue(1)+273.15d0)-273.15d0     
     Call SetOutputValue(4,tsky)
 Else
     cc = getOutputValue(12)/100.d0
     eosky=0.711d0+0.56d0*(getOutputValue(2)/100.d0)+0.73d0*(getOutputValue(2)/100.d0)**2
     eosky=eosky+.013d0*DCOS(2.d0*3.141592654d0*(DMOD(Time,24.d0)/24.d0))
     eosky = eosky+.00012d0*((getOutputValue(10)*1013.25d0)-1000.d0)
     esky=eosky+(1.d0-eosky)*(0.1508*getOutputValue(11)/100.d0 + 0.6328*getOutputValue(12)/100.d0)
     tsky=esky**0.25d0*(getOutputValue(1)+273.15d0)-273.15d0
     Call SetOutputValue(4,tsky)
EndIf

! Calculate some of the time variables
     Call SetOutputValue(48+8*ns,DMOD(Time,24.d0))
     Call SetOutputValue(49+8*ns,DBLE(JFIX((Time-Timestep/2.)/24.))+1.)
     Call SetOutputValue(50+8*ns,DBLE(JFIX(getOutputValue(47+8*ns)/24.))+1.)

! Print any warning messages from the radiation processor
	 If (ierror_rad==2) Then
	    Call MESSAGES(-1,Message6,'WARNING',CurrentUnit,CurrentType)
	 ElseIf (ierror_rad==3) Then
	    Call MESSAGES(-1,Message7,'WARNING',CurrentUnit,CurrentType)
	 ElseIf (ierror_rad==4) Then
	    Call MESSAGES(-1,Message8,'WARNING',CurrentUnit,CurrentType)
	 EndIf

     Return
 Endif
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
!ReRead the Parameters if Another Unit of This Type Has Been Called Last
 If(getIsReReadParameters()) Then
    mode=JFIX(getParameterValue(1)+0.01)  
    lu_data=JFIX(getParameterValue(2)+0.01)
    rad_mode=JFIX(getParameterValue(3)+0.01)
	rhog=getParameterValue(4)
	rhog_snow=getParameterValue(5)
    If ((mode == 8).OR.(mode == 9)) Then
       elev = getParameterValue(6)
       lat = getParameterValue(7)
       long = getParameterValue(8)
       SPar = 9
    Else
       SPar = 6
    EndIf
    ns=JFIX(getParameterValue(SPar)+0.5)
	If (JFIX(getParameterValue(SPar+1)+0.5) /= 5) Then
       Do j=1,ns
          track_mode(j)=JFIX(getParameterValue(SPar+3*(j-1)+1)+0.5)
	      slope(j)=getParameterValue(SPar+3*(j-1)+2)
          azimuth(j)=getParameterValue(SPar+3*(j-1)+3)
	      axis_slope(j)=getParameterValue(SPar+3*(j-1)+2)
          axis_azimuth(j)=getParameterValue(SPar+3*(j-1)+3)
       EndDo
    Else
       Do j=1,ns
          track_mode(j)=1
       EndDo
    EndIf
    If (mode==1) Then
	   If (JFIX(getParameterValue(7)+0.5) /= 5) Then
          lat=getParameterValue(7+3*ns)
       Else
          lat=getParameterValue(8)
       EndIf
       long=-99
    EndIf
      
! Get the number of items to be read
    If (mode==1) Then
       n_items=36
	   headerskip=0
    ElseIf ((mode==2).OR.(mode==6)) Then
       n_items=27
	   headerskip=1
    ElseIf ((mode==3).OR.(mode==5)) Then
       n_items=31
	   headerskip=8
    ElseIf (mode==4) Then
       n_items=28 
	   headerskip=1
    ElseIf (mode==7) Then
       n_items=28 
	   headerskip=2
    ElseIf (mode==8) Then !German TRY 2004
       n_items=19
       headerskip=0
    ElseIf (mode==9) Then !German TRY 2010
       n_items=19
       headerskip=38
    EndIf

 EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
! Check to see if this component is being called again due to an unstable controller state - if so, back-up one line
 If ((CheckStability() < 1).and.(getOutputValue(52+8*ns) > 0.)) Then
    Backspace(lu_data)
 EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
! Get the inputs at the current time
 If ((mode/=8).AND.(mode/=9)) Then
    If (JFIX(getParameterValue(7)+0.5) == 5) Then
	   Do j=1,ns
	      slope(j)=getInputValue(1+2*(j-1))
          azimuth(j)=getInputValue(2+2*(j-1))
	      axis_slope(j)=getInputValue(1+2*(j-1))
          axis_azimuth(j)=getInputValue(2+2*(j-1))
	   EndDo
	EndIf
 Else
    If (JFIX(getParameterValue(10)+0.5) == 5) Then
	   Do j=1,ns
	      slope(j)=getInputValue(1+2*(j-1))
          azimuth(j)=getInputValue(2+2*(j-1))
	      axis_slope(j)=getInputValue(1+2*(j-1))
          axis_azimuth(j)=getInputValue(2+2*(j-1))
	   EndDo
	EndIf
 EndIf
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
! Retrieve the values in the storage array for this iteration

! Reset a few of the location parameters
 lat=getOutputValue(24+8*ns)
 long=getOutputValue(25+8*ns)
 shift=getOutputValue(26+8*ns)
 elev=getOutputValue(27+8*ns)

 Do j = 1,74+9*n_items
    store(j)=getStaticArrayValue(j)
 EndDo

 time_next=store(1)
 Do j=1,n_items
    value_was(j)=store(1+j)
    value_now(j)=store(1+n_items+j)
    value_next(j)=store(1+2*n_items+j)
    value_future(j)=store(1+3*n_items+j)
    begin(j)=store(1+4*n_items+j)
    previous(j)=store(1+5*n_items+j)
    interp(j)=JFIX(store(1+6*n_items+j))
    time_stamp(j)=JFIX(store(1+7*n_items+j))
    xmult(j)=store(1+8*n_items+j)
    xadd(j)=store(1+9*n_items+j)
 EndDo

 annual_ave_T=store(1+10*n_items+1)
 annual_min_T=store(1+10*n_items+2)
 annual_max_T=store(1+10*n_items+3)
 annual_ave_I=store(1+10*n_items+4)
	
 Do j=1,14
    month_ave_T(j-1)=store(1+10*n_items+4+j)
    month_min_T(j-1)=store(1+10*n_items+4+14+j)
    month_max_T(j-1)=store(1+10*n_items+4+28+j)
    x_heat(j-1)=store(1+10*n_items+4+42+j)
    x_cool(j-1)=store(1+10*n_items+4+56+j)
 EndDo
 
 HorStartP = getStaticArrayValue(75+10*n_items+8+5+1)
 HorMidP = getStaticArrayValue(75+10*n_items+8+5+2)
 HorEndP = getStaticArrayValue(75+10*n_items+8+5+3)
 HorSections = jfix(getStaticArrayValue(75+10*n_items+8+5+4))
 HorNum = jfix(getStaticArrayValue(75+10*n_items+8+5+5))
 HorMid = jfix(getStaticArrayValue(75+10*n_items+8+5+6))
 IdnStartP = getStaticArrayValue(75+10*n_items+8+5+7)
 IdnMidP = getStaticArrayValue(75+10*n_items+8+5+8)
 IdnEndP = getStaticArrayValue(75+10*n_items+8+5+9)
 IdnSections = jfix(getStaticArrayValue(75+10*n_items+8+5+10))
 IdnNum = jfix(getStaticArrayValue(75+10*n_items+8+5+11))
 IdnMid = jfix(getStaticArrayValue(75+10*n_items+8+5+12))
 EndTime = getStaticArrayValue(75+10*n_items+8+5+13)
 StartTime = getStaticArrayValue(75+10*n_items+8+5+14)
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
! Main calculation algorithm

! Initialize the output to no-data-read
 Call SetOutputValue(52+8*ns,0.d0)

! Check to see if its time to read some data
 If ((Time-Timestep/2.) > time_next) Then

! Set the next read time
    time_next=time_next+1.d0
    Call SetOutputValue(52+8*ns,1.d0)

! Move the next values to the current values
    Do j=1,n_items
       value_was(j)=value_now(j)
       value_now(j)=value_next(j)
       value_next(j)=value_future(j)
    EndDo

! Time to read the data for the next timestep
    If (mode==1) Then !TMY
       Read (lu_data,6000,err=2000,end=210) (value_future(i),i=1,n_items)
    ElseIf ((mode==2).or.(mode==6)) Then
       Read (lu_data,4000,err=2000,end=210) (value_future(i),i=1,n_items)
    ElseIf ((mode==3).or.(mode==5)) Then
       Read (lu_data,*,end=210,err=2000) value_future(1),value_future(2),value_future(3),value_future(4),value_future(5),alphanum,value_future(6),value_future(7),value_future(8),value_future(9),value_future(10),value_future(11),value_future(12),value_future(13),value_future(14),value_future(15),value_future(16),value_future(17),value_future(18),value_future(19),value_future(20),value_future(21),value_future(22),value_future(23),value_future(24),value_future(25),value_future(26),value_future(27),value_future(28),value_future(29),value_future(30),value_future(31)
    ElseIf (mode==4) Then !IWEC
       Read (lu_data,6001,err=2000,end=210) (value_future(i),i=1,n_items)
    ElseIf (mode==7) Then   !TMY3
       Read (lu_data,5004,err=2000,end=210,advance='NO') value_future(1),value_future(2),value_future(3),value_future(4)
       Read (lu_data,*,err=2000,end=210) value_future(5),value_future(6),value_future(7),alphanum,alphanum,value_future(8),alphanum,alphanum,value_future(9),alphanum,alphanum,value_future(10),alphanum,alphanum,value_future(11),alphanum,alphanum,value_future(12),alphanum,alphanum,value_future(13),alphanum,alphanum,value_future(14),alphanum,alphanum,value_future(15),alphanum,alphanum,value_future(16),alphanum,alphanum,value_future(17),alphanum,alphanum,value_future(18),alphanum,alphanum,value_future(19),alphanum,alphanum,value_future(20),alphanum,alphanum,value_future(21),alphanum,alphanum,value_future(22),alphanum,alphanum,value_future(23),alphanum,alphanum,value_future(24),alphanum,alphanum,value_future(25),alphanum,alphanum,value_future(26),alphanum,alphanum,value_future(27),value_future(28)
    ElseIf (mode==8) Then !German TRY 2004
       Read (lu_data,7001,err=2000,end=210) (value_future(i),i=1,n_items)
       nTRYreg = JFIX(value_future(1))
       nTRYmon = JFIX(value_future(3))
       value_future(9) = value_future(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
       value_future(11) = value_future(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
    ElseIf (mode==9) Then !German TRY 2010
       Read (lu_data,7001,err=2000,end=210) (value_future(i),i=1,n_items)
       nTRYreg = JFIX(value_future(1))
       nTRYmon = JFIX(value_future(3))
    EndIf
! Replace some 'missing data conditions
    If (mode==1) Then
       Do i=1,n_items
          !replace 'missing radiation' values (9999) with zeros.
          If ((value_future(i) > 9998.).and.(value_future(i) < 10000.)) value_future(i) = 0.d0
          !replace 'missing sky cover' values (99) with the last read value.
          If (((i==34).or.(i==35)).and.(value_future(i) > 98)) Then
             value_future(i) = value_next(i)
             If (i==34) Then
                Call MESSAGES(-1,Message3,'notice',CurrentUnit,CurrentType)
             ElseIf (i==35) Then
                Call MESSAGES(-1,Message4,'notice',CurrentUnit,CurrentType)
             EndIf
          EndIf
       EndDo
    ElseIf (mode==7) Then
	   If (value_future(27) < 0.) value_future(27)=value_next(27)
	   If (value_future(28) < 0.) value_future(28)=value_next(28)
    EndIf
    goto 230

210 Call Rewind (Time,lu_data,headerskip,CurrentUnit,CurrentType,ie_read)
    If (mode==1) Then !TMY
       Read (lu_data,6000,err=2000,end=2000) (value_future(i),i=1,n_items)
    ElseIf ((mode==2).or.(mode==6)) Then
       Read (lu_data,4000,err=2000,end=2000) (value_future(i),i=1,n_items)
    ElseIf ((mode==3).or.(mode==5)) Then
       Read (lu_data,*,end=2000,err=2000) value_future(1),value_future(2),value_future(3),value_future(4),value_future(5),alphanum,value_future(6),value_future(7),value_future(8),value_future(9),value_future(10),value_future(11),value_future(12),value_future(13),value_future(14),value_future(15),value_future(16),value_future(17),value_future(18),value_future(19),value_future(20),value_future(21),value_future(22),value_future(23),value_future(24),value_future(25),value_future(26),value_future(27),value_future(28),value_future(29),value_future(30),value_future(31)
    ElseIf (mode==4) Then !IWEC
       Read (lu_data,6001,err=2000,end=2000) (value_future(i),i=1,n_items)
    ElseIf (mode==7) Then   !TMY3
       Read (lu_data,5004,err=2000,end=2000,advance='NO') value_future(1),value_future(2),value_future(3),value_future(4)
       Read (lu_data,*,err=2000,end=2000) value_future(5),value_future(6),value_future(7),alphanum,alphanum,value_future(8),alphanum,alphanum,value_future(9),alphanum,alphanum,value_future(10),alphanum,alphanum,value_future(11),alphanum,alphanum,value_future(12),alphanum,alphanum,value_future(13),alphanum,alphanum,value_future(14),alphanum,alphanum,value_future(15),alphanum,alphanum,value_future(16),alphanum,alphanum,value_future(17),alphanum,alphanum,value_future(18),alphanum,alphanum,value_future(19),alphanum,alphanum,value_future(20),alphanum,alphanum,value_future(21),alphanum,alphanum,value_future(22),alphanum,alphanum,value_future(23),alphanum,alphanum,value_future(24),alphanum,alphanum,value_future(25),alphanum,alphanum,value_future(26),alphanum,alphanum,value_future(27),value_future(28)
    ElseIf (mode==8) Then !German TRY 2004
       Read (lu_data,7001,err=2000,end=2000) (value_future(i),i=1,n_items)
       nTRYreg = JFIX(value_future(1))
       nTRYmon = JFIX(value_future(3))
       value_future(9) = value_future(9)+TRY_T_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_T_Adj(1,nTRYreg))/100.d0)
       value_future(11) = value_future(11)+TRY_h_Adj(nTRYmon+1,nTRYreg)*JFIX((elev-TRY_h_Adj(1,nTRYreg))/100.d0)
    ElseIf (mode==9) Then !German TRY 2010
       Read (lu_data,7001,err=2000,end=2000) (value_future(i),i=1,n_items)
       nTRYreg = JFIX(value_future(1))
       nTRYmon = JFIX(value_future(3))
    EndIf

! Replace some 'missing data conditions
    If (mode==1) Then
       Do i=1,n_items
          !replace 'missing radiation' values (9999) with zeros.
          If ((value_future(i) > 9998.).and.(value_future(i) < 10000.)) value_future(i) = 0.d0
          !replace 'missing sky cover' values (99) with the last read value.
          If (((i==34).or.(i==35)).and.(value_future(i) > 98)) value_future(i) = value_next(i)
       EndDo
    ElseIf (mode==7) Then
	   If (value_future(27) < 0.) value_future(27)=value_next(27)
	   If (value_future(28) < 0.) value_future(28)=value_next(28)
    EndIf

230 If (ie_read > 0) goto 2000

    Do j=1,n_items
       previous(j)=begin(j)
       If ((interp(j) > 0).and.(time_stamp(j)==0)) Then
          begin(j)=2.*value_now(j)-begin(j)
       Else
          begin(j)=value_now(j)
       EndIf
    EndDo
            
    If (mode == 1) Then
       HorNow = value_next(8)*xmult(8)+xadd(8)
	   IdnNow = value_next(7)*xmult(7)+xadd(7)
       HorNext = value_future(8)*xmult(8)+xadd(8)
       IdnNext = value_future(7)*xmult(7)+xadd(7)
    ElseIf ((mode == 3) .or. (mode == 5)) Then
       HorNow = value_next(15)*xmult(15)+xadd(15)
	   IdnNow = value_next(14)*xmult(14)+xadd(14)
       HorNext = value_future(15)*xmult(15)+xadd(15)
       IdnNext = value_future(14)*xmult(14)+xadd(14)
    Else
       HorNow = value_next(9)*xmult(9)+xadd(9)
	   IdnNow = value_next(8)*xmult(8)+xadd(8)
       HorNext = value_future(9)*xmult(9)+xadd(9)
       IdnNext = value_future(8)*xmult(8)+xadd(8)
    EndIf

    StartTime = EndTime
    EndTime = time_next
    HorStartP = HorEndP
    IdnStartP = IdnEndP
    Call GetRadInterpolateValues(Time, StartTime, EndTime, timestep, HorNow, HorNext, HorStartP, HorMidP, HorEndP, HorNum, HorMid, HorSections, i_solartime, shift, lat)    
    Call GetRadInterpolateValues(Time, StartTime, EndTime, timestep, IdnNow, IdnNext, IdnStartP, IdnMidP, IdnEndP, IdnNum, IdnMid, IdnSections, i_solartime, shift, lat)    
 EndIf
 
! Step through each variable
 Do j=1,n_items
! Interpolation is off or the values are average values and therfore cannot be interpolated
    If (interp(j) < 0) Then
! Set the outputs
       If ((Time-Timestep/2.) > (time_next-1.d0)) Then
          out_temp(j)=value_next(j)*xmult(j)+xadd(j)
       Else
          out_temp(j)=value_now(j)*xmult(j)+xadd(j)
       EndIf
! Interpolation is on and values in the file are average values over the timestep
    ElseIf (time_stamp(j)==0) Then
! Calculate the instantaneous value at the end of the data timestep
       ending(j)=2.*value_next(j)-begin(j)
! Calculate the interpolation ratio
       ratio=(time_next-Time)
! Calculate the instantaneous value at the end of the simulation timestep
       xnow=begin(j)+(1.d0-ratio)*(ending(j)-begin(j))
! Set the instantaneous value at the end of the next timestep
       ratio=(time_next-Time-Timestep)
       xnext=begin(j)+(1.d0-ratio)*(ending(j)-begin(j))
! Calcualte the instantaneous value of the function at the end of the previous timestep
       ratio=(time_next-Time+Timestep)
       xwas=begin(j)+DMAX1(0.,((1.d0-ratio)))*(ending(j)-begin(j))+DMAX1(0.,((ratio-1.d0)))*(previous(j)-begin(j))
! Set average over the timestep and calculate the outputs
       out_temp(j)=(xwas+xnow)/2.*xmult(j)+xadd(j)
! Interpolation is on and values in the file are instantaneous
    Else
! Instantaneous values - step through each one - remember that TRNSYS puts out the average value over the timestep and not the instantaneous value!
       If (time_stamp(j)==1) Then
! Calculate the instantaneous value of the function at the end of the timestep
          ratio=(time_next-Time)
          xnow=value_now(j)+(1.d0-ratio)*(value_next(j)-value_now(j))
! Calculate the instantaneous value of the function at the end of the previous timestep
          ratio=(time_next-Time+Timestep)
          xwas=value_now(j)+DMAX1(0.,((1.d0-ratio)))*(value_next(j)-value_now(j))+DMAX1(0.,((ratio-1.d0)))*(value_was(j)-value_now(j))
! Set the instantaneous value at the end of the next timestep
          ratio=(time_next-Time-Timestep)
          xnext=value_now(j)+(1.d0-ratio)*(value_next(j)-value_now(j))
! Set the averages over the timestep and calculate the outputs
          out_temp(j)=(xnow+xwas)/2.*xmult(j)+xadd(j)
       EndIf
    EndIf
 EndDo

! Calculate the current month
 If ((DMOD(Time,8760.d0)-Timestep/2.)<=744) Then
	j_month=1
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=1416.) Then
	j_month=2
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=2160.) Then
	j_month=3
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=2880.) Then
	j_month=4
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=3624.) Then
	j_month=5
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=4344.) Then
	j_month=6
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=5088.) Then
	j_month=7
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=5832.) Then
	j_month=8
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=6552.) Then
	j_month=9
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=7296.) Then
	j_month=10
 ElseIf ((DMOD(Time,8760.d0)-Timestep/2.)<=8016.) Then
	j_month=11
 Else 
	j_month=12
 EndIf

! Now set the outputs based on the mode
 If (mode==1) Then
! Month        
    Call SetOutputValue(46+8*ns,out_temp(2))
! Hour of the month
    Call SetOutputValue(47+8*ns,out_temp(4))
! IDN
    Call SetOutputValue(15,out_temp(7))
! Ihd
    Call SetOutputValue(14,out_temp(8))
! Tdb
    Call SetOutputValue(1,out_temp(30)) 
! Windspeed
    Call SetOutputValue(8,out_temp(33)) 
! Wind direction
    Call SetOutputValue(9,out_temp(32)) 
! Dew point
    Call SetOutputValue(2,out_temp(31)) 
! Ill
    Call SetOutputValue(36+8*ns,-99.d0) 
! Ill dn
    Call SetOutputValue(37+8*ns,-99.d0)
! Ill diff
    Call SetOutputValue(38+8*ns,-99.d0)
! Ill zen
    Call SetOutputValue(39+8*ns,-99.d0) 
! Sky cover
    Call SetOutputValue(11,out_temp(34)) 
! Opaque cover
    Call SetOutputValue(12,out_temp(35)) 
! Pressure
    Call SetOutputValue(10,out_temp(29)) 
! Horiz visibility
    Call SetOutputValue(40+8*NS,out_temp(19)) 
! Ceiling height
    Call SetOutputValue(41+8*NS,out_temp(17))
! Precip water
    Call SetOutputValue(42+8*NS,-99.d0) 
! Optical depth
    Call SetOutputValue(43+8*NS,-99.d0) 
! Snow depth
    Call SetOutputValue(44+8*NS,-99.d0) 
! Days since last snow
    Call SetOutputValue(45+8*NS,-99.d0) 
! W, Twb and %rh - calculated from Tdb, pressure and Tdp
    psydat(1)=getOutputValue(10)
    psydat(2)=getOutputValue(1)
    psydat(5)=getOutputValue(2)
    Call MoistAirProperties(CurrentUnit,CurrentType,1,3,1,psydat,0,ie)
    If (ErrorFound()) Return
    Call SetOutputValue(6,psydat(6))           !humidity ratio
    Call SetOutputValue(7,psydat(4)*100.d0)    !relative humidity
    Call SetOutputValue(3,psydat(3))

 ElseIf ((mode==2).or.(mode==6)) Then  !TMY2
! Month        
    Call SetOutputValue(46+8*ns,out_temp(2))
! Hour of the month
    Call SetOutputValue(47+8*ns,(out_temp(3)-1.d0)*24.d0+out_temp(4))
! Idn
    Call SetOutputValue(15,out_temp(8))
! Ihd
    Call SetOutputValue(14,out_temp(9))
! Tdb
    Call SetOutputValue(1,out_temp(16))
! Windspeed
    Call SetOutputValue(8,out_temp(21))
! Wind direction
    Call SetOutputValue(9,out_temp(20))
! Dew point
    Call SetOutputValue(2,out_temp(17))
! Relative humidity
    Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(18))))))
! Ill
    Call SetOutputValue(36+8*ns,out_temp(10))
! Ill dn
    Call SetOutputValue(37+8*ns,out_temp(11))
! Ill diff
    Call SetOutputValue(38+8*ns,out_temp(12))
! Ill zen
    Call SetOutputValue(39+8*ns,out_temp(13))
! Sky cover
    Call SetOutputValue(11,out_temp(14))
! Opaque cover
    Call SetOutputValue(12,out_temp(15))
! Pressure
    Call SetOutputValue(10,out_temp(19))
! Horiz visibility
    Call SetOutputValue(40+8*ns,out_temp(22))
! Ceiling height
    Call SetOutputValue(41+8*ns,out_temp(23))
! Precip water
    Call SetOutputValue(42+8*ns,out_temp(24))
! Optical depth
    Call SetOutputValue(43+8*ns,out_temp(25))
! Snow depth
    Call SetOutputValue(44+8*ns,out_temp(26))
! Days since last snow
    Call SetOutputValue(45+8*NS,out_temp(27))
! W and Twb - calculated from Tdb, pressure and %rh
    psydat(1)=getOutputValue(10)
    psydat(2)=getOutputValue(1)
    psydat(4)=getOutputValue(7)/100.
    Call MoistAirProperties(CurrentUnit,CurrentType,1,2,1,psydat,0,ie)
    If (ErrorFound()) Return
    Call SetOutputValue(6,psydat(6))
    Call SetOutputValue(3,psydat(3))

 ElseIf ((mode==3).or.(mode==5)) Then
! Month        
    Call SetOutputValue(46+8*ns,out_temp(2))
! Hour of the month
    Call SetOutputValue(47+8*ns,(out_temp(3)-1.d0)*24.d0+out_temp(4))
! Idn
    Call SetOutputValue(15,out_temp(14))
! Ihd
    Call SetOutputValue(14,out_temp(15))
! Tdb
    Call SetOutputValue(1,out_temp(6))
! Windspeed
    Call SetOutputValue(8,out_temp(21))
! Wind direction
    Call SetOutputValue(9,out_temp(20))
! Dew point
    Call SetOutputValue(2,out_temp(7))
! Relative humidity
    Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(8))))))
! Ill
    Call SetOutputValue(36+8*ns,out_temp(16))
! Ill dn
    Call SetOutputValue(37+8*ns,out_temp(17))
! Ill diff
    Call SetOutputValue(38+8*ns,out_temp(18))
! Ill zen
    Call SetOutputValue(39+8*ns,out_temp(19))
! Sky cover
    Call SetOutputValue(11,out_temp(22))
! Opaque cover
    Call SetOutputValue(12,out_temp(23))
! Pressure
    Call SetOutputValue(10,out_temp(9))
! Horiz visibility
    Call SetOutputValue(40+8*ns,out_temp(24))
! Ceiling height
    Call SetOutputValue(41+8*ns,out_temp(25))
! Precip water
    Call SetOutputValue(42+8*ns,out_temp(28))
! Optical depth
    Call SetOutputValue(43+8*ns,out_temp(29))
! Snow depth
    Call SetOutputValue(44+8*ns,out_temp(30))
! Days since last snow
    Call SetOutputValue(45+8*ns,out_temp(31))
! W and Twb - calculated from Tdb, pressure and %rh
    psydat(1)=getOutputValue(10)
    psydat(2)=getOutputValue(1)
    psydat(4)=getOutputValue(7)/100.
    Call MoistAirProperties(CurrentUnit,CurrentType,1,2,1,psydat,0,ie)
    If (ErrorFound()) Return
    Call SetOutputValue(6,psydat(6))
    Call SetOutputValue(3,psydat(3))

 ElseIf (mode==4) Then !IWEC
! Month
    Call SetOutputValue(46+8*ns,out_temp(2)) 
! Hour of the month
    Call SetOutputValue(47+8*ns,(out_temp(3)-1.d0)*24.d0+out_temp(4))
! Direct normal solar radiation
    Call SetOutputValue(15,out_temp(8)) 
! Global horizontal diffuse solar radiation
    Call SetOutputValue(14,out_temp(9)) 
! Dry bulb temperature           
    Call SetOutputValue(1,out_temp(16)) 
! Windspeed          
    Call SetOutputValue(8,out_temp(21)) 
! Wind direction
    Call SetOutputValue(9,out_temp(20)) 
! Dew point temperature
    Call SetOutputValue(2,out_temp(17)) 
! Relative humidity          
    Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(18))))))
! Global horizontal illuminance
    Call SetOutputValue(36+8*ns,out_temp(10)) 
! Direct normal illuminance           
    Call SetOutputValue(37+8*ns,out_temp(11)) 
! Diffuse horizontal illuminance           
    Call SetOutputValue(38+8*ns,out_temp(12)) 
! Zenith luminance          
    Call SetOutputValue(39+8*ns,out_temp(13)) 
! Total sky cover           
    Call SetOutputValue(11,out_temp(14))
! Opaque sky cover           
    Call SetOutputValue(12,out_temp(15))
! Pressure           
    Call SetOutputValue(10,out_temp(19)) 
! Visibility          
    Call SetOutputValue(40+8*ns,out_temp(22)) 
! Ceiling height          
    Call SetOutputValue(41+8*ns,out_temp(23)) 
! Precipitable water
    Call SetOutputValue(42+8*ns,out_temp(25)) 
! Aerosol optical depth
    Call SetOutputValue(43+8*ns,out_temp(26))
! Snow depth
    Call SetOutputValue(44+8*ns,out_temp(27)) 
! Days since last snowfall
    Call SetOutputValue(45+8*ns,out_temp(28)) 
! W - calculated from Tdb, pressure and rh          
    psydat(1)=getOutputValue(10)
    psydat(2)=getOutputValue(1)
    psydat(4)=getOutputValue(7)/100.
    Call MoistAirProperties(CurrentUnit,CurrentType,1,2,1,psydat,0,ie)
    If (ErrorFound()) Return
    Call SetOutputValue(6,psydat(6))
    Call SetOutputValue(3,psydat(3))

 ElseIf (mode==7) Then  !TMY3
! Month        
    Call SetOutputValue(46+8*ns,out_temp(1))
! Hour of the month
    Call SetOutputValue(47+8*ns,(out_temp(2)-1.d0)*24.d0+out_temp(4))
! Idn
    Call SetOutputValue(15,out_temp(8))
! Ihd
    Call SetOutputValue(14,out_temp(9))
! Tdb
    Call SetOutputValue(1,out_temp(16))
! Windspeed
    Call SetOutputValue(8,out_temp(21))
! Wind direction
    Call SetOutputValue(9,out_temp(20))
! Dew point
    Call SetOutputValue(2,out_temp(17))
! Relative humidity
    Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(18))))))
! Ill
    Call SetOutputValue(36+8*ns,out_temp(10))
! Ill dn
    Call SetOutputValue(37+8*ns,out_temp(11))
! Ill diff
    Call SetOutputValue(38+8*ns,out_temp(12))
! Ill zen
    Call SetOutputValue(39+8*ns,out_temp(13))
! Sky cover
    Call SetOutputValue(11,out_temp(14))
! Opaque cover
    Call SetOutputValue(12,out_temp(15))
! Pressure
    Call SetOutputValue(10,out_temp(19))
! Horiz visibility
    Call SetOutputValue(40+8*ns,out_temp(22))
! Ceiling height
    Call SetOutputValue(41+8*ns,out_temp(23))
! Precip water
    Call SetOutputValue(42+8*ns,out_temp(24))
! Optical depth
    Call SetOutputValue(43+8*ns,out_temp(25))
! Liquid precipitation depth
    Call SetOutputValue(44+8*ns,out_temp(27))
! Liquid preci[itation quantity
    Call SetOutputValue(45+8*ns,out_temp(28))
! W and Twb - calculated from Tdb, pressure and rh
    psydat(1)=getOutputValue(10)
    psydat(2)=getOutputValue(1)
    psydat(4)=getOutputValue(7)/100.
    Call MoistAirProperties(CurrentUnit,CurrentType,1,2,1,psydat,0,ie)
    If (ErrorFound()) Return
    Call SetOutputValue(6,psydat(6))
    Call SetOutputValue(3,psydat(3))
 ElseIf ((mode==8).OR.(mode==9)) Then  !German TRY 2004 or TRY 2010
! Month        
    Call SetOutputValue(46+8*ns,out_temp(3))
! Hour of the month
    Call SetOutputValue(47+8*ns,(out_temp(5)-1.d0)*24.d0+out_temp(4))
! Idn
    Call SetOutputValue(15,-99.d0)
! I
    Call SetOutputValue(14,-99.d0)
! Tdb
    Call SetOutputValue(1,out_temp(9))
! Windspeed
    Call SetOutputValue(8,out_temp(8))
! Wind direction
    Call SetOutputValue(9,out_temp(7))
! Relative humidity
    Call SetOutputValue(7,DMIN1(100.,(DMAX1(0.,(out_temp(12))))))
! Ill
    Call SetOutputValue(36+8*ns,-99.d0)
! Ill dn
    Call SetOutputValue(37+8*ns,-99.d0)
! Ill diff
    Call SetOutputValue(38+8*ns,-99.d0)
! Ill zen
    Call SetOutputValue(39+8*ns,-99.d0)
! Sky cover
    Call SetOutputValue(11,out_temp(6))
! Opaque cover
    Call SetOutputValue(12,-99.d0)
! Pressure
    Call SetOutputValue(10,out_temp(10))
! Horiz visibility
    Call SetOutputValue(40+8*ns,-99.d0)
! Ceiling height
    Call SetOutputValue(41+8*ns,-99.d0)
! Precip water
    Call SetOutputValue(42+8*ns,-99.d0)
! Optical depth
    Call SetOutputValue(43+8*ns,-99.d0)
! Liquid precipitation depth
    Call SetOutputValue(44+8*ns,-99.d0)
! Liquid preci[itation quantity
    Call SetOutputValue(45+8*ns,-99.d0)
! Humidity Ratio
    Call SetOutputValue(6,out_temp(11))
! Tdp and Twb - calculated from Tdb, pressure and W
    psydat(1)=getOutputValue(10)
    psydat(2)=getOutputValue(1)
    psydat(6)=getOutputValue(6)
    Call MoistAirProperties(CurrentUnit,CurrentType,1,4,1,psydat,0,ie)
    If (ErrorFound()) Return
    Call SetOutputValue(2,psydat(5))
    Call SetOutputValue(3,psydat(3))
 EndIf

 td1=JFIX(Time-1./3600.d0/2.d0)
 td2=JFIX(Time-1./3600.d0/2.d0)+1.d0

! Calculate the max and min monthly averages
 j_min=1
 j_max=1
 Do j=2,12
     If (month_ave_T(j) > month_ave_T(j_max)) j_max=j
	 If (month_ave_T(j) < month_ave_T(j_min)) j_min=j
 EndDo

! Calculate the mains water temperature based on an algorithm by Craig Christensen and Jay Burch of NREL
 xday = DMOD(Time,8760.d0)/24.
 T_mains = (annual_ave_T*1.8+32.) - 5.915 + 0.008422*(annual_ave_I)*0.947817/10.7639 + (-0.3112+0.01553*(annual_ave_T*1.8+32.))*(month_ave_T(j_max)-month_ave_T(j_min))*1.8/2.0*DSIN(0.017453292*(0.986*xday-191.3+1.069*(annual_ave_T*1.8+32.)))
 T_mains=(T_mains-32.)/1.8

! Set the latitude, longitude, elevation and shift
 Call SetOutputValue(24+8*ns,lat)
 Call SetOutputValue(25+8*ns,long)
 Call SetOutputValue(26+8*ns,shift)
 Call SetOutputValue(27+8*ns,elev)
! Set the monthly average air temperatures
 Call SetOutputValue(30+8*ns,month_ave_T(j_month))
 Call SetOutputValue(31+8*ns,month_min_T(j_month))
 Call SetOutputValue(32+8*ns,month_max_T(j_month))
! Set the annual air temperatures
 Call SetOutputValue(33+8*ns,annual_ave_T)
 Call SetOutputValue(34+8*ns,annual_min_T)
 Call SetOutputValue(35+8*ns,annual_max_T)
! Set the heating and cooling season outputs
 Call SetOutputValue(28+8*ns,x_heat(j_month))
 Call SetOutputValue(29+8*ns,x_cool(j_month))
! Set the mains water temperature
 Call SetOutputValue(5,T_mains)

!Calculate the ground reflectance
 If (mode==7) Then
    If (rhog < 0.0) Then
       rho = rhog_snow
	   Call SetOutputValue(51+8*ns,rho)
    ElseIf ((out_temp(26) < 0.) .or. (out_temp(26) > 1.)) Then
	   Call SetOutputValue(51+8*ns,rhog)
	   rho=rhog
	Else
	   Call SetOutputValue(51+8*ns,out_temp(26))
	   rho=out_temp(26)
	EndIf
 Else
    If ((getOutputValue(44+8*ns) > 0.).and.(getOutputValue(44+8*ns)/=999.)) Then  ! 999 indicates no data.
	   rho=rhog_snow
	Else
	   rho=rhog
    EndIf
	Call SetOutputValue(51+8*ns,rho)
 EndIf

! Set the solar time indicator
 If (mode==1) Then
    i_solartime=-1
 Else
    i_solartime=1
 EndIf

 ihd=getOutputValue(14)
 idn=getOutputValue(15)
 
! Process the incident solar radiation
 itemp1=1
 itemp2=3
 xtemp1=0.
 xtemp2=0.
 If ((mode /= 8).AND.(mode /= 9)) Then
	mode_horiz = 6
    Call GetRadPoint(Time, StartTime, EndTime, Timestep, HorStartP, HorMidP, HorEndP, HorSections, HorNum, HorMid, HorPoint)
    Call GetRadPoint(Time, StartTime, EndTime, Timestep, IdnStartP, IdnMidP, IdnEndP, IdnSections, IdnNum, IdnMid, IdnPoint)
    rad_input(3) = HorPoint
    rad_input(4) = IdnPoint
 Else
	mode_horiz = 3
	rad_input(2) = out_temp(14)
	rad_input(3) = out_temp(15)
 EndIf
 Call SetRadiationData(CurrentUnit,mode_horiz,shape_mode,rad_input,rho,itemp2,lat,elev,shift,i_solartime,SolConst,td1,td2)
 Call GetHorizontalRadiation(Time,mode_horiz,shape_mode,rad_input,rho,xtemp1,xtemp2,itemp1,itemp2,lat,elev,shift,i_solartime,SolConst,td1,td2,solar,ierror_rad)
! Need to do the error checking
 Do j = 1,8
    RadErrors(j) = .false.
 EndDo
 If (ierror_rad == 1) Then
    Call Messages(-1,'An illegal tracking mode has been specified for the radiation calculation.  The mode must be between 1 and 4.','FATAL',CurrentUnit,CurrentType)
    Return
 ElseIf (ierror_rad == 4) Then
    Call Messages(-1,'The simulation timestep is greater than the data timestep for the radiation calculation.  The simulation timestep needs to be reduced.','FATAL',CurrentUnit,CurrentType)
    Return
 ElseIf (ierror_rad == 11) Then
    Call Messages(-1,'An illegal tilted surface mode has been specified for the radiation calculation.  The mode must be between 1 and 4.','FATAL',CurrentUnit,CurrentType)
    Return
 ElseIf (ierror_rad == 12) Then
    Call Messages(-1,'An illegal value for the latitude has been specified for the radiation calculation.  The latitude must be between -90 and 90 degrees.','FATAL',CurrentUnit,CurrentType)
    Return
 ElseIf (ierror_rad == 13) Then
    Call Messages(-1,'An illegal value for the shift in solar time has been specified for the radiation calculation.  The shift must be between -360 and 360 degrees.','FATAL',CurrentUnit,CurrentType)
    Return
 ElseIf (ierror_rad == 14) Then
    Call Messages(-1,'An illegal value for the ground reflectance has been specified for the radiation calculation.  The refelectance must be between 0 and 1.','FATAL',CurrentUnit,CurrentType)
    Return
 ElseIf (ierror_rad == 2) Then
    Call Messages(-1,'The calculated solar hour angle for the end of the data timestep has been modified from its original value.  This fix was added to account for sundown periods near midnight at high-latitude locations.  If this warning is noted for locations that do not meet this criteria, please contact your TRNSYS distributor.','Warning',CurrentUnit,CurrentType)
 ElseIf (ierror_rad == 3) Then
    RadErrors(1) = .true.
 ElseIf (ierror_rad == 5) Then
    RadErrors(2) = .true.
 ElseIf (ierror_rad == 6) Then
    RadErrors(3) = .true.
 ElseIf (ierror_rad == 7) Then
    RadErrors(4) = .true.
 ElseIf (ierror_rad == 8) Then
    RadErrors(5) = .true.
 ElseIf (ierror_rad == 9) Then
    RadErrors(6) = .true.
 ElseIf (ierror_rad == 10) Then
    RadErrors(7) = .true.
 ElseIf (ierror_rad == 16) Then
    RadErrors(8) = .true.
 EndIf

 Call SetOutputValue(13,solar(1))
 Call SetOutputValue(16,solar(2))
 Call SetOutputValue(17,solar(3))
 Call SetOutputValue(18,solar(10))
 Call SetOutputValue(19,solar(11))
 Call SetOutputValue(20,solar(12))
 Call SetOutputValue(21,solar(13))
 Call SetOutputValue(22,solar(12)+solar(13))
 Call SetOutputValue(23,solar(6))
 
 Do j=1,ns
    If ((track_mode(j)==2).or.(track_mode(j)==3)) THEN
	   slope(j)=axis_slope(j)
	   azimuth(j)=axis_azimuth(j)
	EndIf
	If ((mode /= 8).AND.(mode /= 9)) Then
	   mode_horiz = 6
       Call GetRadPoint(Time, StartTime, EndTime, Timestep, HorStartP, HorMidP, HorEndP, HorSections, HorNum, HorMid, HorPoint)
       Call GetRadPoint(Time, StartTime, EndTime, Timestep, IdnStartP, IdnMidP, IdnEndP, IdnSections, IdnNum, IdnMid, IdnPoint)
       rad_input(3) = HorPoint
       rad_input(4) = IdnPoint
	Else
	   mode_horiz = 3
	   rad_input(2) = out_temp(14)
	   rad_input(3) = out_temp(15)
	EndIf
	Call SetRadiationData(CurrentUnit,mode_horiz,shape_mode,rad_input,rho,rad_mode,lat,elev,shift,i_solartime,SolConst,td1,td2)
    Call GetTiltedRadiation(Time,rho,slope(j),azimuth(j),track_mode(j),rad_mode,elev,SolConst,solar,ierror_rad)

! Need to do the error checking
     Do k = 1,8
        RadErrors(k) = .false.
     EndDo
     If (ierror_rad == 1) Then
        Call Messages(-1,'An illegal tracking mode has been specified for the radiation calculation.  The mode must be between 1 and 4.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 4) Then
        Call Messages(-1,'The simulation timestep is greater than the data timestep for the radiation calculation.  The simulation timestep needs to be reduced.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 11) Then
        Call Messages(-1,'An illegal tilted surface mode has been specified for the radiation calculation.  The mode must be between 1 and 4.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 12) Then
        Call Messages(-1,'An illegal value for the latitude has been specified for the radiation calculation.  The latitude must be between -90 and 90 degrees.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 13) Then
        Call Messages(-1,'An illegal value for the shift in solar time has been specified for the radiation calculation.  The shift must be between -360 and 360 degrees.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 14) Then
        Call Messages(-1,'An illegal value for the ground reflectance has been specified for the radiation calculation.  The refelectance must be between 0 and 1.','FATAL',CurrentUnit,CurrentType)
	    Return
     ElseIf (ierror_rad == 2) Then
        Call Messages(-1,'The calculated solar hour angle for the end of the data timestep has been modified from its original value.  This fix was added to account for sundown periods near midnight at high-latitude locations.  If this warning is noted for locations that do not meet this criteria, please contact your TRNSYS distributor.','Warning',CurrentUnit,CurrentType)
     ElseIf (ierror_rad == 3) Then
        RadErrors(1) = .true.
     ElseIf (ierror_rad == 5) Then
        RadErrors(2) = .true.
     ElseIf (ierror_rad == 6) Then
        RadErrors(3) = .true.
     ElseIf (ierror_rad == 7) Then
        RadErrors(4) = .true.
     ElseIf (ierror_rad == 8) Then
        RadErrors(5) = .true.
     ElseIf (ierror_rad == 9) Then
        RadErrors(6) = .true.
     ElseIf (ierror_rad == 10) Then
        RadErrors(7) = .true.
     ElseIf (ierror_rad == 16) Then
        RadErrors(8) = .true.
     EndIf
    Call SetOutputValue(23+j,solar(10))
    Call SetOutputValue(23+ns+j,solar(11))
    Call SetOutputValue(23+2*ns+j,solar(12))
    Call SetOutputValue(23+3*ns+j,solar(13))
    Call SetOutputValue(23+4*ns+j,solar(12)+solar(13))
    Call SetOutputValue(23+5*ns+j,solar(6))
    Call SetOutputValue(23+6*ns+j,solar(4))
    Call SetOutputValue(23+7*ns+j,solar(5))
 EndDo

! Calculate the sky coverage for TRY data
 If ((mode==8).OR.(mode==9)) then
     E_dir  = GetOutputValue(19)
     E_dif  = GetOutputValue(20)
     Eglob  = E_dir + E_dif 
     If (Eglob < 150.d0) Then
	   cc = (getStaticArrayValue(74+10*n_items+7+1)+getStaticArrayValue(74+10*n_items+7+2)+getStaticArrayValue(74+10*n_items+7+3))/3.d0
       Do j=1,5
          Call SetStaticArrayValue(74+10*n_items+7+j,cc)
       EndDo
    Else
       help = (1.4286d0*(E_dif/Eglob)-0.3d0)
       If (help < 0.d0) help = 0.001d0
       cc = (help)**0.5d0
       IF (cc > 1.d0) cc = 1.d0
       If (Dabs(Time-JFIX(Time)) < Timestep/2.d0) Then
          Call SetStaticArrayValue(74+10*n_items+7+1,getStaticArrayValue(74+10*n_items+7+2))
          Call SetStaticArrayValue(74+10*n_items+7+2,getStaticArrayValue(74+10*n_items+7+3))
          Call SetStaticArrayValue(74+10*n_items+7+3,getStaticArrayValue(74+10*n_items+7+4))
          Call SetStaticArrayValue(74+10*n_items+7+4,getStaticArrayValue(74+10*n_items+7+5))
          Call SetStaticArrayValue(74+10*n_items+7+5,cc)
       EndIf
    EndIf
     ecld = 0.8 
     eosky = 0.711d0+0.56d0*(getOutputValue(2)/100.d0)+0.73d0*(getOutputValue(2)/100.d0)**2
     eosky = eosky+.013d0*DCOS(2.d0*3.141592654d0*(DMOD(Time,24.d0)/24.d0))
     eosky = eosky+.00012d0*((getOutputValue(10)*1013.25d0)-1000.d0)
     esky = eosky+(1.d0-eosky)*cc*ecld
     tsky = esky**0.25d0*(getOutputValue(1)+273.15d0)-273.15d0     
     Call SetOutputValue(4,tsky)
 Else
     cc = getOutputValue(12)/100.d0
 eosky=0.711d0+0.56d0*(getOutputValue(2)/100.d0)+0.73d0*(getOutputValue(2)/100.d0)**2
 eosky=eosky+.013d0*DCOS(2.d0*3.141592654d0*(DMOD(Time,24.d0)/24.d0))
 eosky = eosky+.00012d0*((getOutputValue(10)*1013.25d0)-1000.0d0)
 esky=eosky+(1.d0-eosky)*(0.1508*getOutputValue(11)/100.d0 + 0.6328*getOutputValue(12)/100.d0)
 tsky=esky**0.25d0*(getOutputValue(1)+273.15d0)-273.15d0
 Call SetOutputValue(4,tsky)
 EndIf
! Calculate some of the time variables
 Call SetOutputValue(48+8*ns,DMOD(Time,24.d0))
 Call SetOutputValue(49+8*ns,DBLE(JFIX((Time-Timestep/2.)/24.))+1.)
 Call SetOutputValue(50+8*ns,DBLE(JFIX(getOutputValue(47+8*ns)/24.))+1.)

! Print any warning messages from the radiation processor
 If (ierror_rad==2) Then
    Call MESSAGES(-1,Message6,'WARNING',CurrentUnit,CurrentType)
 ElseIf (ierror_rad==3) Then
    Call MESSAGES(-1,Message7,'WARNING',CurrentUnit,CurrentType)
 ElseIf (ierror_rad==4) Then
    Call MESSAGES(-1,Message8,'WARNING',CurrentUnit,CurrentType)
 EndIf

! Store the values
 store(1)=time_next
 Do j=1,n_items
    store(1+J)=value_was(j)
    store(1+n_items+j)=value_now(j)
    store(1+2*n_items+j)=value_next(j)
    store(1+3*n_items+j)=value_future(j)
    store(1+4*n_items+j)=begin(j)
    store(1+5*n_items+j)=previous(j)
 EndDo
 Do j = 1,1+6*n_items
    Call SetStaticArrayValue(j,store(j))
 EndDo
    Call SetStaticArrayValue(75+10*n_items+8+5+1,HorStartP)
    Call SetStaticArrayValue(75+10*n_items+8+5+2,HorMidP)
    Call SetStaticArrayValue(75+10*n_items+8+5+3,HorEndP)
    Call SetStaticArrayValue(75+10*n_items+8+5+4,Dble(HorSections))
    Call SetStaticArrayValue(75+10*n_items+8+5+5,Dble(HorNum))
    Call SetStaticArrayValue(75+10*n_items+8+5+6,Dble(HorMid))
    Call SetStaticArrayValue(75+10*n_items+8+5+7,IdnStartP)
    Call SetStaticArrayValue(75+10*n_items+8+5+8,IdnMidP)
    Call SetStaticArrayValue(75+10*n_items+8+5+9,IdnEndP)
    Call SetStaticArrayValue(75+10*n_items+8+5+10,Dble(IdnSections))
    Call SetStaticArrayValue(75+10*n_items+8+5+11,Dble(IdnNum))
    Call SetStaticArrayValue(75+10*n_items+8+5+12,dble(IdnMid))
    Call SetStaticArrayValue(75+10*n_items+8+5+13,EndTime)
    Call SetStaticArrayValue(75+10*n_items+8+5+14,StartTime)

 Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
! The model was unable to read from the data file - set an error and get out
2000 Call MESSAGES(-1,Message1,'FATAL',CurrentUnit,CurrentType)
 Return
2002 Call MESSAGES(-1,Message2,'FATAL',CurrentUnit,CurrentType)
 Return
!-----------------------------------------------------------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------
! Format for reading a TMY2 data file
4000 Format (1X,F2.0,F2.0,F2.0,F2.0,F4.0,F4.0,F4.0,2X,F4.0,2X,F4.0,2X,F4.0,2X,F4.0,2X,F4.0,2X,F4.0,2X,F2.0,2X,F2.0,2X,F4.0,2X,F4.0,2X,F3.0,2X,F4.0,2X,F3.0,2X,F3.0,2X,F4.0,2X,F5.0,12X,F3.0,2X,F3.0,2X,F3.0,2X,F2.0)
! Format for reading the first line (header) of a TMY2 data file
5000 Format (7X,A22,1X,A2,1X,F3.0,1X,A1,1X,F2.0,1X,F2.0,1X,A1,1X,F3.0,1X,F2.0,2X,F4.0)
! Format for reading the header of an EnergyPlus data file
5001 Format (160A1)
! Format for reading the header on an IWEC weather file
5003 Format (13X,A22,1X,A2,1X,A3,1X,I1,1X,A1,1X,I2,1X,I2,1X,A1,1X,F2.0,1X,F2.0,1X,A1,1X,F3.0,1X,F2.0,1X,A1,1X,F4.0)
! Format for reading the beginning section of a TMY3 file
5004 Format (F2.0,1X,F2.0,1X,F4.0,1X,F2.0,4X)
! Format for reading a data line in a standard TMY data file
6000 Format (5X,F2.0,F2.0,F2.0,F4.0,F4.0,F4.0,1X,F4.0,1X,F4.0,1X,F4.0,1X,F4.0,1X,F4.0,1X,F4.0,1X,F4.0,F10.0,F2.0,F2.0,F4.0,F5.0,F4.0,F1.0,F1.0,F1.0,F1.0,F1.0,F1.0,F1.0,F1.0,F5.0,F5.0,F4.0,F4.0,F3.0,F4.0,F2.0,F2.0,F1.0,F9.0)
! Format for reading a data line of an IWEC data file
6001 Format (1X,F4.0,F2.0,F2.0,F2.0,F4.0,F4.0,F4.0,2X,F4.0,2X,F4.0,2X,F4.0,2X,F4.0,2X,F4.0,2X,F4.0,2X,F2.0,2X,F2.0,2X,F4.0,2X,F4.0,2X,F3.0,2X,F4.0,2X,F3.0,2X,F3.0,2X,F4.0,2X,F5.0,2X,F4.0,2X,10X,F3.0,2X,F3.0,2X,F3.0,2X,F2.0,2X)
! Format for reading a data line of an German TRY data file
7001 Format (F4.0,2X,F2.0,2X,F2.0,2X,F2.0,2X,F2.0,2X,F1.0,2X,F3.0,2X,F6.1,2X,F6.1,2X,F7.1,2X,F6.1,2X,F3.0,2X,F2.0,2X,F4.0,2X,F4.0,1X,F1.0,2X,F4.0,2X,F5.0,2X,F1.0)
!-----------------------------------------------------------------------------------------------------------------------

End

