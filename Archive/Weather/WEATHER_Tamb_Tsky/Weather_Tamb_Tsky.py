# -*- coding: utf-8 -*-
"""
This code is meant:
    > to extract Dry Bulb Temperature from EPW
    > to calculate Tsky based on data from EPW and method from Trnsys
"""

import os
import math


"""----------------------------------------------------------------------------
INPUTS
----------------------------------------------------------------------------"""
folder_path = r"E:\MdBT_Docs\TUM_Cleanvelope_Local\_QUARTER_MODELLING_TOOLS\CLEANVELOPE_Models\CleanMod\Development\Weather_preparation"
epw_file_name = "Muenchen_Airp_2005_Hour.epw"
csv_file_name = "Tamb_Tsky.csv"
validation = True
ref_file_name = "Trnsys_model_for_validation\Model_for_validation.out"

"""----------------------------------------------------------------------------
CALCULATIONS
----------------------------------------------------------------------------"""
# Reading EPW
epw_file_path = os.path.join(folder_path, epw_file_name)
with open(epw_file_path,"r") as f:
    lines = f.readlines()
    # Removing \n at the end of the lines
    lines = [line.strip() for line in lines]
    # Skiping header
    data = lines[8:8768]  # 8768 is used to avoid leap years and ensure 8760 values per time serie

    
# Extracting required data from EWP
# Moth of year
MOY_L = [line.split(",")[1] for line in data]
# Day of Month
DOM_L = [line.split(",")[2] for line in data]
# Hour of day
HOD_L = [line.split(",")[3] for line in data]
# Dry bulb temperature
Tamb_L = [line.split(",")[6] for line in data]
# Dew point temperature
Tdew_L = [line.split(",")[7] for line in data]
# Atmospheric pressure
AtmP_L = [line.split(",")[9] for line in data]
# Total sky cover
TSC_L = [line.split(",")[22] for line in data]
# Opaque sky cover
OSC_L = [line.split(",")[23] for line in data]


# Checking if missing there are missing values in EPW
for OSC in OSC_L:
    if OSC == "99":
        raise Exception("Values for Opaque Sky Cover are missing in the EWP")
for AtmP in AtmP_L:
    if AtmP == "999999":
        raise Exception("Values for Atmospheric Pressure are missing in the EWP")
for TSC in TSC_L:
    if TSC == "99":
        raise Exception("Values for Total Sky Cover are missing in the EWP")



# For validation with Trnsys model
if validation == True:
    ref_file_path = os.path.join(folder_path, ref_file_name)
    # Read result file from Trnsys model
    with open(ref_file_path, "r") as f:
        lines = f.readlines()
        data = [line.strip().split("\t") for line in lines[2:8762]]
        Tamb_L = [float(line[1])for line in data]
        Tdew_L = [float(line[2])for line in data]
        AtmP_L = [float(line[3])for line in data]
        TSC_L = [float(line[4])for line in data]
        OSC_L = [float(line[5])for line in data]
        Tsky_L2 = [float(line[6])for line in data]


# Calculating Tsky based on method used in Trnsys
Tsky_L = []
# Adaptations of values according to Trnsys
if validation == False:
    OSC_L = [float(OSC)*10 for OSC in OSC_L]
    TSC_L = [float(TSC)*10 for TSC in TSC_L]
    AtmP_L = [float(AtmP)*0.000009869 for AtmP in AtmP_L]
# Equations from Trnsys
for i in range(8760):
    cc = float(OSC_L[i])/100
    eosky = 0.711 + 0.56*(float(Tdew_L[i])/100) + 0.73*(float(Tdew_L[i])/100)**2
    eosky = eosky + 0.013*math.cos(2*math.pi*((int(HOD_L[i])%24)/24))
    eosky = eosky + 0.00012*((float(AtmP_L[i])*1013.25)-1000)  # *1013.25 to convert from Pascals to Milibars
    esky = eosky + (1-eosky)*(0.1508*float(TSC_L[i])/100 + 0.6328*cc)
    Tsky = esky**0.25*(float(Tamb_L[i])+273.15)-273.15
    Tsky_L.append(round(Tsky,2))
# Checking results
print("\nTrnsys values:")
print("HOD \tTamb \tTdew \tAtmP \tTSC \tOSC \tTsky_ref")
i = 0
print(str(i+1) + " \t" + str(round(Tamb_L[i],2)) + " \t" + str(round(Tdew_L[i],2)) + " \t" + str(round(AtmP_L[i],2)) + " \t" + str(round(TSC_L[i],2)) + " \t" + str(round(OSC_L[i],2)) + " \t" + str(round(Tsky_L2[i],2)))

print("\nCalculated values:")
print("HOD \tTamb \tTsky \tTsky_ref")
for i in range(24):
    print(str(i+1) + " \t" + str(round(Tamb_L[i],2)) + " \t" + str(round(Tsky_L[i],2)) + " \t" + str(round(Tsky_L2[i],2)))

"""----------------------------------------------------------------------------
OUTPUTS
----------------------------------------------------------------------------"""
# Export results
csv_file_path = os.path.join(folder_path, csv_file_name)
header = ["HOY", "MOY", "DOM", "HOD","Tamb","Tsky"]
units = ["h", "-", "d", "h","C","C"]
with open(csv_file_path,"w") as f:
    f.write(",".join(header) + "\n")
    f.write(",".join(units) + "\n")
    for i in range(8760):
        data_line = [str(i+1), str(MOY_L[i]), str(DOM_L[i]), str(HOD_L[i]), str(Tamb_L[i]), str(Tsky_L[i])]
        f.write(",".join(data_line) + "\n")
        
























