import os
import pandas as pd
import numpy as np
import Scripts.util as util
import logging

if __name__ == '__main__':

    starthour = 0
    endhour = 8760
    time_resolution_h = float(1)
    index_timesteps = [i for i in np.arange(starthour, endhour, time_resolution_h)]
    steps_per_hour = 1 / time_resolution_h
    mydir = os.path.join(os.getcwd(), 'CleanMod/Results', '201112_192642')

    def create_elec_load_profile(bid, hid, starthour, endhour, index_timesteps, time_resolution_h, steps_per_hour, mydir,
                                 approach='VDI4655'):
        """Creates a pd.Series of the hourly electrical load of the household in [W]
        Pay attention that the year is assumed to start with Monday in this version and
        climatezone is Potsdam, persons per zone are 3.

        Args:
            index_timesteps (list): timesteps in calculated timeperiod.

        Returns:
            Dataframe of elec household load reduced to calculated timeperiod.
        """

        if approach == 'VDI4655':
            # assumptions
            # TODO: change assumptions to variables
            persons_per_zone = hdata['number_occupants']
            elec_load_annual = 3000 * 1000  # in [Wh], standard approach in VDI4655, independent of m2 or persons

            # load typedays
            mapping = {4.0: 'AB', 10.0: 'NB'}
            typedays = pd.read_csv('Results/201112_192642/Input_created/Weather_data/Typedays_'+mapping[bdata['year_class']]+'.csv',
                                   delimiter=',')

            # prolong typedays if endhour+predict_horizon exceeds length of data
            if endhour >= 8760.0:
                overhang_days = float((endhour - 8760) // 24)
                index_overhang = pd.Index([i for i in np.arange(365, 365 + overhang_days)])
                data_overhang = typedays.loc[0:overhang_days - 1]
                data_overhang.set_index(index_overhang, inplace=True)
                new = pd.concat([typedays, data_overhang])
                typedays = new
            # add data to pv_gen_per_kWp_rawdf if starthour lies prior to start of df_in
            if starthour < 0.0:
                overhang_days = float(np.ceil(-starthour // 24))
                index_overhang = pd.Index([i for i in np.arange(-overhang_days, 0)])
                data_overhang = typedays.loc[365 - overhang_days:365 - 1]
                data_overhang.set_index(index_overhang, inplace=True)
                new = pd.concat([data_overhang, typedays])
                typedays = new

            # print(typedays)

            # load profile factors
            F_el_TT = pd.read_csv('../Input/Residential_electricity/TRY04_VDI4655.csv', skiprows=1, nrows=1,
                                  delimiter=';')
            W_TT_dict = {}
            for col in F_el_TT.columns:
                W_TT_dict[col] = elec_load_annual * (1 / 365 + persons_per_zone * F_el_TT.loc[0, col])
            F_el_TT_t_15min = pd.read_csv('../Input/Residential_electricity/SLP_VDI4655.csv',
                                          skiprows=1, nrows=96, delimiter=',')
            df = F_el_TT_t_15min
            if time_resolution_h == 1.0:
                F_el_TT_t_matching_res = df.groupby(df.index // 4).agg('sum')
            elif time_resolution_h == 0.25:
                F_el_TT_t_matching_res = F_el_TT_t_15min
            else:
                raise util.InputError(0, 'no corresponding method in create_hh_load_profile_VDI4655 for given time_resolution')

            # index_matching_resolution = pd.Index([i for i in np.arange(0, 8760, time_resolution_h)])
            elec_load_zone = pd.Series(np.zeros(len(index_timesteps)), index=pd.Index(index_timesteps))
            for timestep in index_timesteps:
                identifier = typedays.loc[timestep // 24, 'typeday']
                elec_load_zone[timestep] = W_TT_dict[identifier] * F_el_TT_t_matching_res.loc[
                    int((timestep % 24) * steps_per_hour), identifier]

            # print(elec_load_zone)
        # elif approach == 'REM':

        return elec_load_zone


    buildings = pd.read_csv('new/City_LeaTest_new.csv', skiprows=1)
    buildings.set_index('bid', inplace=True)
    buildings.dropna(inplace=True)
    households = pd.read_csv('new/Households_LeaTest_new.csv', skiprows=1)
    households.set_index('hid', inplace=True)
    households.dropna(inplace=True)

    print(buildings)
    print(households)
    logging.debug('buildings: %s', buildings)
    logging.debug('households: %s', households)

    for bid, bdata in buildings.iterrows():
        print(bid)
        logging.debug(bid)
        for hid, hdata in households.loc[households['bid'] == bid].iterrows():
            load_hid = create_elec_load_profile(bid, hid, starthour, endhour, index_timesteps, time_resolution_h, steps_per_hour, mydir,
                                     approach='VDI4655')
            load_hid._set_name(hid, inplace=True)
            print('hid: ', hid)
            logging.debug('hid: %s', hid)
            #print('hdata :', hdata)
            if hdata['household_number'] == float(0):
                #print('New dataframe for bid_{0} created'.format(bid))
                load_bid = pd.DataFrame(load_hid)
                load_bid['building_sum'] = load_hid
            else:
                #print('else-entered')
                load_bid[hid] = load_hid
                load_bid['building_sum'] += load_hid
        print('arrived at saving line')
        logging.debug('arrived at saving line')
        load_bid.to_csv('elecload_{}.csv'.format(int(bid)))