from ruamel.yaml import YAML
import os
import shutil
import pandas as pd
import time
import numpy as np
import pandas as pd
from Cleanvelope.CleanMod.Step_1_runme import run_cleanmod


# this script run cleanmod iteratively with varying parameters
if __name__ == '__main__':
    # define the scenario name
    folder_name = 'conference_result'

    # read the results
    with open(f"Step_0_config.yaml") as config_file:
        config = YAML().load(config_file)
    name_model = config['project_settings']['name_model']  # 'Temp', 'peak_load', 'elec_bought', 'Qhc'
    mydir = os.path.join(os.getcwd(), 'Models/{0}/Output'.format(name_model))

    season = 'year'
    special_case = ''
    start_ts = 0
    days = 365
    # RB const
    scenario_1 = ["S1", "S2_1_1_a", "S2_2_1_a", "S3_1_1_a", "S3_2_1_a"]
    # RB dynamic
    scenario_2 = ["S2_1_2_a", "S2_2_2_a", "S3_1_2_a", "S3_2_2_a"]
    # emi const
    scenario_3 = ["S2_1_1_b", "S2_2_1_b", "S3_1_1_b", "S3_2_1_b"]  # S3_2_1_b fehlen
    # emi dynamic
    scenario_4 = ["S2_1_2_b", "S2_2_2_b", "S3_1_2_b", "S3_2_2_b"]
    scenarios = scenario_1 + scenario_2 + scenario_3 + scenario_4
    scenarios = ['S2_2_2_b']
    # scenarios = ["S2_2_1_b"]
    # heat_option = ['HP']
    heat_option = ['HP']
    bulding_list = range(22)
    scenario_ids = []
    calc_types = ['emi', 'peak_load']
    for heat_device in heat_option:
        for scenario in scenarios:
            for calc_type in calc_types:
                mydistrict = None  # after changing the time, mydistrict need to be reset
                # scenario list with RB control
                scenario_RB = scenario_1 + scenario_2
                # scenario list with constant tariff
                scenario_const = scenario_1 + scenario_3

                # decide heat generator
                if scenario == 'S1':
                    heat_device = 'GB'
                else:
                    heat_device = 'HP'

                # decide control mode
                # if scenario in scenario_RB:
                #     calc_type = 'RB'
                # else:
                #     calc_type = 'emi'

                # decide tariff type
                if scenario in scenario_const:
                    tariff = 'const'
                else:
                    tariff = 'dynamic'

                # create scenario identification number for checking purpose

                scenario_id = scenario + '_' + heat_device + '_' + calc_type + '_' + tariff
                scenario_ids.append(scenario_id)

                # change the parameters
                folder_name_extend = folder_name + '_' + heat_device + '_for_' + scenario + '_obj_' + calc_type

                res_dir = os.path.join(os.getcwd(), 'Models/{0}/Output'.format(name_model),
                                       folder_name_extend)

                with open("Step_0_config.yaml") as config_file:
                    config = YAML().load(config_file)

                config["project_settings"]["scenarios"] = [scenario]
                config["simulation_settings"]["heat_option"] = heat_device
                config["simulation_settings"]["starthour"] = start_ts
                config["simulation_settings"]["endhour"] = start_ts + days * 24
                config["simulation_settings"]["calc_type"] = [calc_type]
                config["simulation_settings"]["tariff"] = tariff
                # config["simulation_settings"]["building_num"] = building_num

                with open("Step_0_config.yaml", 'w+') as file:
                    YAML().dump(config, file)

                if mydistrict:
                    mydistrict.update_district(config)
                mydistrict = run_cleanmod(folder_name=folder_name_extend, mydistrict=mydistrict)
                error_list = [str(error_logging_row) for error_logging_row in mydistrict.error_logging]
                error_logging = np.array(error_list)
                pd.DataFrame(error_logging).to_csv(res_dir+"/error_logging.csv")

    # save info into the last scenario folder
    scenario_info = np.array(scenario_ids)
    pd.DataFrame(scenario_info).to_csv(res_dir + "/scenario_info.csv")
