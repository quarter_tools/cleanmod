from ruamel.yaml import YAML
import os
import shutil
import pandas as pd
import time
import numpy as np
import pandas as pd
from Cleanvelope.CleanMod.Step_1_runme import run_cleanmod


# this script run cleanmod iteratively with varying parameters
if __name__ == '__main__':
    # define the scenario name
    folder_name = 'conference_result'

    # read the results
    with open(f"Step_0_config.yaml") as config_file:
        config = YAML().load(config_file)
    name_model = config['project_settings']['name_model']  # 'Temp', 'peak_load', 'elec_bought', 'Qhc'
    mydir = os.path.join(os.getcwd(), 'Models/{0}/Output'.format(name_model))

    season = 'year'
    special_case = ''
    start_ts = 0
    days = 30

    # heat_option = ['HP']
    heat_option = ['HP']
    bulding_list = range(21)
    scenario_ids = []
    scenario = 'S3_2_2_b'

    for building in bulding_list:
        mydistrict = None  # after changing the time, mydistrict need to be reset

        # create scenario identification number for checking purpose

        # change the parameters
        folder_name_extend = folder_name + '_for_building_' + str(building)

        res_dir = os.path.join(os.getcwd(), 'Models/{0}/Output'.format(name_model),
                               folder_name_extend)

        with open("Step_0_config.yaml") as config_file:
            config = YAML().load(config_file)
        config["project_settings"]["scenarios"] = [scenario]
        config["simulation_settings"]["heat_option"] = 'HP'
        config["simulation_settings"]["starthour"] = start_ts
        config["simulation_settings"]["endhour"] = start_ts + days * 24
        # config["simulation_settings"]["calc_type"] = [calc_type]
        # config["simulation_settings"]["tariff"] = tariff
        config["simulation_settings"]["building_num"] = building

        with open("Step_0_config.yaml", 'w+') as file:
            YAML().dump(config, file)

        if mydistrict:
            mydistrict.update_district(config)
        mydistrict = run_cleanmod(folder_name=folder_name_extend, mydistrict=mydistrict)
        error_list = [str(error_logging_row) for error_logging_row in mydistrict.error_logging]
        error_logging = np.array(error_list)
        pd.DataFrame(error_logging).to_csv(res_dir+"/error_logging.csv")

# save info into the last scenario folder
scenario_info = np.array(scenario_ids)
pd.DataFrame(scenario_info).to_csv(res_dir + "/scenario_info.csv")
