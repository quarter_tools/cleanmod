import numpy as np

class Component_Composition:
    """ Generic Building Component Composition class """
    def __init__(self, IntExt, ComponentType, Thickness, Conductivity, Density, StorageCapacity, U, SW, LW, alphaInt, alphaExt):
        """ Building Component Constructor
        
        Taking over properties and calculating two chain matrices for a period of 2 days and 7 days
        VDI (2-9)
        """
        self.IntExt = IntExt
        self.ComponentType = ComponentType
        self.Thickness = Thickness
        self.Conductivity = Conductivity
        self.Density = Density
        self.StorageCapacity = StorageCapacity
        self.SW = SW
        self.LW = LW
        self.alphaInt = alphaInt
        self.alphaExt = alphaExt
        self.U = U
        Alist = []
        for T in [2, 7]:
            wbt = 2*np.pi/(86400*T)
            A = np.eye(4)
            for i in range(len(Thickness)):
                R = self.Thickness[i]/self.Conductivity[i]
                C = 1000*self.StorageCapacity[i]*self.Density[i]*self.Thickness[i]
                
                sqterm1 = np.sqrt(0.5*wbt*R*C)
                sqterm2 = np.sqrt(1/(2*wbt*R*C))

                Rea11 = np.cosh(sqterm1)*np.cos(sqterm1)
                Ima11 = np.sinh(sqterm1)*np.sin(sqterm1) 
                Rea12 = R*sqterm2*(np.cosh(sqterm1)*np.sin(sqterm1)
						            + np.sinh(sqterm1)*np.cos(sqterm1))
                Ima12 = R*sqterm2*(np.cosh(sqterm1)*np.sin(sqterm1)
				                    - np.sinh(sqterm1)*np.cos(sqterm1))
                Rea21 = (-1/R)*sqterm1*(np.cosh(sqterm1)*np.sin(sqterm1)
				                        - np.sinh(sqterm1)*np.cos(sqterm1))
                Ima21 = (1/R)*sqterm1*(np.cosh(sqterm1)*np.sin(sqterm1)
				                       + np.sinh(sqterm1)*np.cos(sqterm1))

                Av = np.array([[ Rea11, Ima11,  Rea12, Ima12],
                               [-Ima11, Rea11, -Ima12, Rea12],
                               [ Rea21, Ima21,  Rea11, Ima11],
                               [-Ima21, Rea21, -Ima11, Rea11]])
                A = A @ Av
            Alist.append(A)
        self.Acm_wbt = Alist

class Building_Component:
    """ Class for Building Component Thermal Model """
    def __init__(self, Composition, Area, Orient):
        """ Calculating the 3R2C Model using the helper to calculate for both time periods, and selecting the correct one
        VDI (10) (12-17)"""
        self.SW = Composition.SW
        self.LW = Composition.LW
        self.alphaInt = Composition.alphaInt
        self.alphaExt = Composition.alphaExt
        self.IntExt = Composition.IntExt
        self.ComponentType = Composition.ComponentType
        self.Area = Area
        self.U = Composition.U
        self.Orient = Orient
        
        BC_Model2 = self.calc3R2C_helper(Composition, Area, 2)
        BC_Model7 = self.calc3R2C_helper(Composition, Area, 7)
        R1Rel = BC_Model2['R1']/BC_Model7['R1']
        C1Rel = BC_Model2['C1']/BC_Model7['C1']
        if (R1Rel > 0.99) and (C1Rel < 0.95):
            self.RC = BC_Model2
        elif (R1Rel < 0.95) and (C1Rel < 0.95) and (abs(R1Rel - C1Rel) > 0.3):
            self.RC = BC_Model2
        else:
            self.RC = BC_Model7
        # Calculating the coefficients for equivalent outside temperature for outside components
        if self.IntExt == 'Ext':
            # The Short Wave coefficient is the SW absorption coefficient of exterior surface (BACK)
            # divided by the exterior heat transfer coefficient (convective + radiative) [VDI(38)]
            # This is multiplied by Idir + Idif for the surface equivalent temperature
            self.SWCoeff = self.SW/(self.alphaExt+5)
            # The formula for LW Radiation is given in [VDI(33)-VDI(37)]. The two components of LW 
            # Coefficient is multiplied by the (Earth Temperature - Ambient Temperature)
            # and the (Atmospheric Temperature -Ambient Temperature) respectively
            a = self.LW*5/((self.alphaExt+5)*0.93)
            #a = self.LW*5/((self.alphaExt+5)*0.93)
            if self.ComponentType == 'Wall':
                self.LWCoeff = [a, 0.5*a]
            elif self.ComponentType == 'Ceiling':
                self.LWCoeff = [a, a]
            elif self.ComponentType == 'Floor':
                self.LWCoeff = [a, 0]    
                
    def __str__(self):
        return self.ComponentType + " Component"
        
    def calc3R2C_helper(self, Composition, Area, T):
        """ Calculating the 3R2C Model for the specified Time Period \( 2 or 7 days\) """
        if T == 2:
            Acm = Composition.Acm_wbt[0]
        elif T == 7:
            Acm = Composition.Acm_wbt[1]
            
        wbt = 2*np.pi/(86400*T)
        Rea11 = Acm[0,0]
        Ima11 = Acm[0,1]
        Rea12 = Acm[0,2] 
        Ima12 = Acm[0,3] 
        Rea21 = Acm[2,0]
        Ima21 = Acm[2,1] 
        Rea22 = Acm[2,2]
        Ima22 = Acm[2,3]
        
        BC_Model = {}
        
        BC_Model['R1'] = float((1/Area)*(((Rea22-1)*Rea12+Ima22*Ima12)/((Rea22-1)**2+Ima22**2)))
        BC_Model['R2'] = float((1/Area)*(((Rea11-1)*Rea12+Ima11*Ima12)/((Rea11-1)**2+Ima11**2)))
        BC_Model['C1'] = float(Area*((Rea22-1)**2+Ima22**2)/(wbt*(Rea12*Ima22-(Rea22-1)*Ima12)))
        BC_Model['C2'] = float(Area*((Rea11-1)**2+Ima11**2)/(wbt*(Rea12*Ima11-(Rea11-1)*Ima12)))
        BC_Model['Rw'] = float((1/Area)*np.sum(np.array([Composition.Thickness])/Composition.Conductivity))
        BC_Model['R3'] = float(BC_Model['Rw']-BC_Model['R1'] - BC_Model['R2'])
        
        BC_Model['Ckorr'] = float((Area*(wbt*BC_Model['R1']*Area)**-1*(BC_Model['Rw']*Area-Rea12*Rea22-Ima12*Ima22)/(Rea22*Ima12-Rea12*Ima22)))
        return BC_Model
    
class Window_Composition:
    """ Generic Window Component Composition class """
    def __init__(self, U, alphaInt, alphaExt, SW, LW):
        self.ComponentType = 'Window'
        self.U = U
        self.alphaInt = alphaInt
        self.alphaExt = alphaExt 
        self.SW = SW
        self.LW = LW
        
class Window_Component:
    """ Class for Window Component Thermal Model """
    def __init__(self, Composition, Area, Orient):
        self.ComponentType = 'Window'
        self.IntExt = 'Ext'
        self.R1 = float((1/Composition.U + 1/Composition.alphaInt + 1/Composition.alphaExt)/(6*Area))
        self.SW = Composition.SW
        self.LW = Composition.LW
        self.alphaInt = Composition.alphaInt
        self.alphaExt = Composition.alphaExt
        self.Area = Area
        self.U = Composition.U
        self.Orient = Orient

        # The formula for LW Radiation is given in [VDI(33)-VDI(37)]. The two components of LW 
        # Coefficient is multiplied by the (Earth Temperature - Ambient Temperature)
        # and the (Atmospheric Temperature -Ambient Temperature) respectively
        a = self.LW*5/((self.alphaExt+5)*0.93)
        #a = self.LW*5/((self.alphaExt+5)*0.93)
        self.LWCoeff = [a, 0.5*a]   # assumption is that inclination of window is 90° e.g. vertical

    def __str__(self):
        return "Window Component"