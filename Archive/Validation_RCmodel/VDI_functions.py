import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import BuildingComponentClass


def Read_VDI(fname):
    """ Read the VDI Excel Tables"""
    # Read Table 1
    Table_headers = ['Name', 'Area', 'Layer', 'Material', 'Thickness', 'Conductivity', 'Density', 'Capacity', 'alpha_a',
                     'alpha_i', 'U', 'Inclination', 'Orientation', 'g_dir_shaded', 'g_diff_shaded', 'a_kon']

    Building_data = pd.read_excel(fname, skiprows=[0, 1], nrows=20, usecols=list(i for i in range(0, 16)),
                                  names=Table_headers)
    Building_data.loc[:, 'Name'].fillna(method='ffill', inplace=True)
    unique_BC = Building_data['Name'].unique()

    temp = list(Building_data[Building_data['Name'] == unique_BC[-1]].index)
    L1 = int(max(Building_data.loc[temp, 'Layer'])) - 1
    Building_data = Building_data.loc[:temp[L1], :]

    for name in unique_BC:
        t = list(Building_data[Building_data['Name'] == name].index)
        df = Building_data.loc[t, :]

    # Read Table 2
    Table_headers = ['Person_Gains', 'Person_Conv', 'Lighting_Gains', 'Lighting_Conv', 'Machine_Gains', 'Machine_Conv',
                     'Other_Gains', 'Other_Conv', 'Neighbour_Temp', 'Air_Exchange']
    Gains = pd.read_excel(fname, skiprows=list(range(29)), nrows=24, header=None, usecols='B:K', names=Table_headers)

    Table_headers = ['Set_Temp', 'Heating_Conv', 'Cooling_Conv', 'Surface_Heating', 'Surface_Cooling', 'Heating_Lim',
                     'Cooling_Lim']
    Thermal_Data = pd.read_excel(fname, skiprows=list(range(29)), nrows=24, header=None, usecols='L:R',
                                 names=Table_headers)

    # Read Table 3

    Table_headers = ['Outside_temp', 'LW_Rad', 'Atm_Rad']
    Weather = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='B:D', names=Table_headers)

    Table_headers = ['South_total', 'South_diff', 'West_total', 'West_diff', 'Win_South_total', 'Win_South_diff',
                     'Win_West_total', 'Win_West_diff']
    Radiation = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='E:L',
                              names=Table_headers)

    Table_headers = ['AirTemp', 'Felt_Temp', 'HeatingCooling']
    Day1Program1 = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='M:O',
                                 names=Table_headers)
    Day1Program2 = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='P:R',
                                 names=Table_headers)
    Day10Program1 = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='S:U',
                                  names=Table_headers)
    Day10Program2 = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='V:X',
                                  names=Table_headers)
    Day60Program1 = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='Y:AA',
                                  names=Table_headers)
    Day60Program2 = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='AB:AD',
                                  names=Table_headers)

    Programs = {'D1P1': Day1Program1,
                'D1P2': Day1Program2,
                'D10P1': Day10Program1,
                'D10P2': Day10Program2,
                'D60P1': Day60Program1,
                'D60P2': Day60Program2}

    Table_headers = ['Day1Temp', 'Day10Temp', 'Day60Temp', 'Day1HC', 'Day10HC', 'Day60HC']
    VDI6020 = pd.read_excel(fname, skiprows=list(range(59)), nrows=24, header=None, usecols='AE:AJ',
                            names=Table_headers)
    return Building_data, Gains, Thermal_Data, Weather, Radiation, Programs, VDI6020


def Plot_function(x, Validationbasis, title):
    if type(Validationbasis) == dict:
        plt.rcParams['figure.figsize'] = [10, 5]
        fig, axs = plt.subplots(1, 3)
        fig.suptitle(title + " - Air Temperatures [°C]")

        axs[0].plot(x[1:24])
        axs[0].plot(Validationbasis['D1P1']['AirTemp'].values)

        axs[1].plot(x[217:240])
        axs[1].plot(Validationbasis['D10P1']['AirTemp'])

        axs[2].plot(x[1417:1440], label='Calculated')
        axs[2].plot(Validationbasis['D60P1']['AirTemp'], label='Program1')
        axs[2].legend()
        axs[0].title.set_text('Day 1')
        axs[1].title.set_text('Day 10')
        axs[2].title.set_text('Day 60')
    else:
        plt.rcParams['figure.figsize'] = [10, 5]
        fig, axs = plt.subplots(1, 3)
        fig.suptitle(title + " - Air Temperatures [°C]")

        axs[0].plot(x[1:24])
        axs[0].plot(Validationbasis['Day1Temp'].values)

        axs[1].plot(x[217:240])
        axs[1].plot(Validationbasis['Day10Temp'])

        axs[2].plot(x[1417:1440], label='Calculated')
        axs[2].plot(Validationbasis['Day60Temp'], label='VDI6020')
        axs[2].legend()
        axs[0].title.set_text('Day 1')
        axs[1].title.set_text('Day 10')
        axs[2].title.set_text('Day 60')

    title_reduced = title.split('/')[1].split('.')[0]
    plt.savefig('Graphs/'+title_reduced+'_airtemp.png')

def Plot_function_Q(x, Validationbasis, title):
    if type(Validationbasis) == dict:
        plt.rcParams['figure.figsize'] = [10, 5]
        fig, axs = plt.subplots(1, 3)
        fig.suptitle(title + " - Heating/Cooling [W]")

        axs[0].plot(x[1:24])
        axs[0].plot(Validationbasis['D1P1']['HeatingCooling'].values)

        axs[1].plot(x[217:240])
        axs[1].plot(Validationbasis['D10P1']['HeatingCooling'])

        axs[2].plot(x[1417:1440], label='Calculated')
        axs[2].plot(Validationbasis['D60P1']['HeatingCooling'], label='Program1')
        axs[2].legend()
        axs[0].title.set_text('Day 1')
        axs[1].title.set_text('Day 10')
        axs[2].title.set_text('Day 60')
    else:
        plt.rcParams['figure.figsize'] = [10, 5]
        fig, axs = plt.subplots(1, 3)
        fig.suptitle(title + " - Heating/Cooling [W]")

        axs[0].plot(x[1:24])
        axs[0].plot(Validationbasis['Day1HC'].values)

        axs[1].plot(x[217:240])
        axs[1].plot(Validationbasis['Day10HC'])

        axs[2].plot(x[1417:1440], label='Calculated')
        axs[2].plot(Validationbasis['Day60HC'], label='VDI6020')
        axs[2].legend()
        axs[0].title.set_text('Day 1')
        axs[1].title.set_text('Day 10')
        axs[2].title.set_text('Day 60')
    title_reduced = title.split('/')[1].split('.')[0]
    plt.savefig('Graphs/'+title_reduced+'_Qhc.png')

def sort_BC(Building_data):
    Ext = []
    Int = []
    Win = []

    unique_BC = Building_data['Name'].unique()

    BC_dict = {'FB': ['Int', 'Floor'],
               'DE': ['Int', 'Ceiling'],
               'IT': ['Int', 'Door'],
               'IW': ['Int', 'Wall'],
               'AF': ['Ext', 'Window'],
               'AW': ['Ext', 'Wall']}

    for name in unique_BC:
        t = list(Building_data[Building_data['Name'] == name].index)
        df = Building_data.loc[t, :]

        IntExt, ComponentType = BC_dict[name[:2]]
        # LW = [0, 0]
        # SW = [0, 0]
        LW = 0
        SW = 0
        temp = df['alpha_i'].dropna().astype(float).values
        alphaInt = temp[0]
        alphaExt = None
        Area = float(df['Area'].dropna().astype(float).values)
        temp2 = df['Orientation'].values
        Orient = temp2[0]

        if IntExt == 'Ext':
            alphaExt = df['alpha_a'].dropna().astype(float).values
            SW = 0.7  # The Short Wave coefficient is the SW absorption coefficient of exterior surface (BACK), here the value is taken from example case 8 in VDI
            LW = 0.9  # LW absorption coefficient of exterior surface (BACK), the formula for LW Radiation is given in [VDI(33)-VDI(37)], here the value is taken from example case 8 in VDI

        if ComponentType == 'Window':
            U = float(df['U'].values)
            Comp = BuildingComponentClass.Window_Composition(U, alphaInt, alphaExt, SW, LW)
            BC = BuildingComponentClass.Window_Component(Comp, Area, Orient)
            Win.append(BC)
        else:
            # Calculate U value
            if ComponentType == 'Ceiling':
                U = 1 / (0.10 + 0.04 + (np.sum(np.divide(df['Thickness'].values, df['Conductivity']))))
            elif ComponentType == 'Floor':
                U = 1 / (0.17 + 0.04 + (np.sum(np.divide(df['Thickness'].values, df['Conductivity']))))
            else:
                U = 1 / (0.13 + 0.04 + (np.sum(np.divide(df['Thickness'].values, df['Conductivity']))))

            Comp = BuildingComponentClass.Component_Composition(IntExt, ComponentType, \
                                                                df['Thickness'].astype(float).values, \
                                                                df['Conductivity'].astype(float).values, \
                                                                df['Density'].astype(float).values, \
                                                                df['Capacity'].astype(float).values, \
                                                                U, SW, LW, alphaInt, alphaExt)

            BC = BuildingComponentClass.Building_Component(Comp, Area, Orient)

            if IntExt == 'Int':
                Int.append(BC)
            else:
                Ext.append(BC)

    return Ext, Int, Win
