import math
import numpy as np
import pandas as pd
import sys
import os
import shutil
import csv
from scipy.io import loadmat
from random import randint

from config_cleanmod import *
import Scripts.util as util
import Scripts.interface_UEP as UEP_interface


def create_df_from_matlab_output(zid_list, profile_mat, name_profile, unit_conversion=False):
    """Prepare profiles as dataframe from matlab output."""

    profile_ndarray = loadmat(profile_mat)[name_profile]   # output from matlab already in matching timeresolution
    profile_index = pd.Index(data=[i for i in np.arange(0.0, 8760.0, time_resolution_h)])
    profile_df_raw = pd.DataFrame(data=profile_ndarray.transpose(), index=profile_index, columns=zid_list)
    if unit_conversion == 'kW_in_W':
        profile_df_raw *= 1000
    profile_df = profile_df_raw.astype(np.int64)

    return profile_df

name_model_full = 'Models/{0}'.format(name_model)

file_dwellinglist = name_model_full + '/Results/Input_created/Building_data/Buildings_extended.csv'
dwellinglist = pd.read_csv(file_dwellinglist)['dwellings'].tolist()
names = pd.read_csv(file_dwellinglist)['building_code'].tolist()

file_occupancy = name_model_full + '/Input/RESIDENTIAL/Load_profiles/Consumption_UEP/occupancy.mat'
file_gains_lighting = name_model_full + '/Input/RESIDENTIAL/Load_profiles/Consumption_UEP/lighting.mat'
file_active_load = name_model_full + '/Input/RESIDENTIAL/Load_profiles/Consumption_UEP/active_load.mat'

file_zoneid_list = name_model_full + '/Results/Input_created/Zone_data/Zones.csv'
zoneid_list = pd.read_csv(file_zoneid_list)['Zone-ID'].tolist()

occupancy_data = create_df_from_matlab_output(zoneid_list, file_occupancy,
                                                              'occupancy')
gains_lighting_data = create_df_from_matlab_output(zoneid_list, file_gains_lighting,
                                                                   'lighting', unit_conversion='kW_in_W')
active_load_data = create_df_from_matlab_output(zoneid_list, file_active_load,
                                                                'active_load', unit_conversion='kW_in_W')

folder_dest = name_model_full + '/Input/RESIDENTIAL/Load_profiles/MK_Thesis'


def slice_total_df_buildingwise(folder_dest, profile_name, names, df):

    counter_start=0

    for i, num_dwellings in enumerate(dwellinglist):

        counter_end = counter_start + num_dwellings

        new_slice = df[df.columns[counter_start:counter_end]]
        new_slice.to_csv(folder_dest+'/'+profile_name+'_'+names[i]+'.csv')

        counter_start = counter_end

    pass

slice_total_df_buildingwise(folder_dest, 'occupancy', names, occupancy_data)
slice_total_df_buildingwise(folder_dest, 'lighting', names, gains_lighting_data)
slice_total_df_buildingwise(folder_dest, 'active_load', names, active_load_data)