import math
import numpy as np
import pandas as pd
import sys
import os
import shutil

zone_data = pd.DataFrame()
lst = [1,2,3,4,5]
mk_case = True
mz_bin = False
zid_df = pd.read_csv('/Users/MagaKono/PycharmProjects/quarter_tools/Cleanvelope/CleanMod/Models/Example/Results/210807_133009/Input_created/Zone_data/Zones.csv', usecols=[0])
zid_lst = zid_df['Zone-ID'].tolist()
file_occupancy = '/Users/MagaKono/PycharmProjects/quarter_tools/Cleanvelope/CleanMod/Libraries/MK_thesis_occupancy/occupancy_MFH_9.1.csv'


def calculate_number_of_occupants_for_n_dwellings(n, file_household_distr):
    """Calculate number of occupants based on distribution of household sizes, independent of floor area.

    Args:
        n (int): number of households.
        file_household_distr (str): name of input csv with household size distribution.
        if mk_case: file_household_distr (str): file with number of occupants for every zone
    Returns:
        list of household sizes (occupants).
    """
    if mk_case == False:
        household_distr = pd.read_csv(file_household_distr)
        # CFD for household size
        # x-values
        household_size = range(1, 6)
        # CFD
        cfd = np.cumsum(household_distr['percentage'].values)

    occupants_list = []

    # calculate number of occupants
    if mk_case == False:
        for dwelling in range(n):
            # get random_number and obtain x-value from cfd
            rand_num = np.random.uniform(0, 1, 1)
            index = np.argmax(rand_num <= cfd)
            # get household size (number of occupants)
            occupants_in_dwelling = household_size[index]
            occupants_list.append(occupants_in_dwelling)
    else:
        occupants_df = pd.read_csv(file_household_distr, usecols=[1])
        occupants_lst = occupants_df['number_occupants'].tolist()

        if mz_bin:
            occupants_list.append(occupants_lst)

        else:
            occupants_list_sum = sum(occupants_lst)
            occupants_list.append(occupants_list_sum)

    return occupants_list


if mk_case == False:
    zone_data['number_occupants'] = calculate_number_of_occupants_for_n_dwellings(24,
                                                                                  file_occupancy)
else:
    file_occupancy = '/Users/MagaKono/PycharmProjects/quarter_tools/Cleanvelope/CleanMod/Libraries/MK_thesis_occupancy/occupancy_MFH_9.1.csv'
    zone_data['number_occupants'] = calculate_number_of_occupants_for_n_dwellings(24,
                                                                                  file_occupancy)
