__author__ = "MB"
__credits__ = []
__license__ = ""
__maintainer__ = ""
__email__ = ""

# system packages
# import ...


class Building:
    """The Building class is used to model a building of the neighborhood.

    Attributes:
    ----------
    building_id : str
        unique name of the building

    building_use : str
        residential (both use of external time series or internally calculated values can be used) or non-residential \
        (only external time series can be used)

    building_standard : str
        id name of the energy efficiency standard (EES) of the building. The EES is defined in a library and contains \
        information about the constructive definition of the building (building components and windows)

    building_zones : list
        list containing all zone objects that compose the building. Zone objects include their single R-C model

    building_production_system : list
        list containing all production system objects that produce energy in the building

    building_storage_system : list
        list containing all storage system objects that storage energy in the building (excluded the building mass, \
        which is taken into account by the zones' R-C models)

    building_construction : list


    - Generic
    building_id : str
        unique name of the building

    - For UEP (i.e. external generation of household profiles of power load demand and user occupation)
    att : type
        description

    - For Zone (i.e R-C model)
    att : type
        description

    - For District (i.e. optimizer)
    att : type
        description

    Returns:
    --------


    Methods:
    --------

    __init__ :          ...

    Public method 1:    ...

    Public method 2:    ...
    """




    def __init__(self):
        """Initialization of Building class

        Parameters
        ----------
        self : Building object

            Object containing information on .... :

                Attribute 1:     ....
                Attribute 2:     ....
                Attribute 2:     ....

        Returns
        -------
        Output : dictionary

            The following are included:

                Data 1:        ...
                Data 2:        ...
                Data 3:        ...
        """

        self.building_id = ''
        self.footprint_area = ''
