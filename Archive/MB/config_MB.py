__author__ = "MB"
__credits__ = []
__license__ = ""
__maintainer__ = ""
__email__ = ""

"""This file corresponds to the user interface. Go from the top to the bottom defining the values of the different 
variables in order to configure a model and the simulation conditions. Finally, set the the simulation boolean to True
and run the script to run a simulation. 
"""


from Scripts.run_MB import simulate

# PROJECT SETTINGS
########################################################################################################################
"""PROJECT SETTINGS

    Parameters
    ----------
        model_name : str
            give a name for the model. It will also be use to name the folder assigned to the model.
"""
model_name = 'MB_04'


# BUILDING SETTINGS
########################################################################################################################
"""BUILDING SETTINGS

    Parameters
    ----------
        building_zoning_approach : str
            choose between 'single-zone' or 'multi-zone' or 'hybrid'
    
    Thermal comfort settings -------------------------------------------------------------------------------------------
        comfort_t_max_heating : str
            indicate the maximum temperature of the comfort band for winter
        comfort_t_min_heating : str
            indicate the minimum temperature of the comfort band for winter
        comfort_t_max_cooling : str :
            indicate the maximum temperature of the comfort band for summer
        comfort_t_min_cooling : str
            indicate the minimum temperature of the comfort band for summer
            Choose between:
                a float number as a str
                or one of the following options based on DIN EN 16798-1:
                    'PMV-I' or 'PMV-II' or 'PMV-III' or 'PMV-IV' or 'ACM-I' or 'ACM-II' or 'ACM-III'
                        PMV: model based on human heat balance
                        ACM: adaptive comfort model
                        I  : indoor environment category I   (high level of expectation)
                        II : indoor environment category II  (Normal level of expectation)
                        III: indoor environment category III (An acceptable, moderate level of expectation)
                        IV : indoor environment category IV  (Low level of expectation)
    
    Heating and cooling settings ---------------------------------------------------------------------------------------
        heating_t_set : str
            set the due room temperature during heating operation without e management
        cooling_t_set : str
            set the due room temperature during heating operation without e management
            Choose between:
                a float number as str
                or 'comfort_t_neutral' (mean of t_max and t_min for user-defined or PMV-based comfort bands,
                or comfort temperature according to DIN EN 16798-1 for ACM-based comfort band)
        heating_t_set_back : float
            indicate the temperature reduction from heating_t_set of the heating operation at night
        cooling_t_set_back : float
            indicate the temperature increment from cooling_t_set of the heating operation at night
        heating_t_set_back_start_hod : int
            indicate the start hour of day (hod) of the heating operation at night
        heating_t_set_back_stop_hod : int
            indicate the stop hour of day (hod) of the heating operation at night
        cooling_t_set_back_up_hod : int
            indicate the start hour of day (hod) of the cooling operation at night
        cooling_t_set_back_up_hod : int
            indicate the stop hour of day (hod) of the cooling operation at night
            Note: 1 = 00:00-01:00; 24 = 23:00-00:00
        heating_period_start_hoy : int
            indicate the start hour of year (hoy) of the heating period
        heating_period_stop_hoy : int
            indicate the stop hour of year (hoy) of the heating period
        cooling_period_start_hoy : int
            indicate the start hour of year (hoy) of the cooling period
        cooling_period_stop_hoy : int
            indicate the stop hour of year (hoy) of the cooling period
            Note: 1 = 1.Jan 00:00-01:00; 8760 = 31.Dec 23:00-00:00
    
    Gains settings -----------------------------------------------------------------------------------------------------
        solar_radiation_approach : str :
            indicate how to consider solar radiation in the model. Ensure consistency with pv_production_approach.
            Choose between:
            'EPW' (simplified calculation of solar radiation for orientations without considering self-shading or 
                shading from context using data of radiation on horizontal surface from an Energy Plus Weather (EPW)
                format file)
            or 'GH' (based on detailed values externally calculated with radiance taking into account self-shading and
                shading from context using ladybug tools in grasshopper (GH))
        internal_gains_approach : str
            indicate how to consider internal gains in the model. Ensure consistency with el_loads_hh_approach.
            Choose between:
            'DIN-EN-16798-1' (simplified calculation based on standard values according to method of DIN EN 16798-1)
            or 'REM' (based on detailed data externally calculated with tool REM using UrbanEnergyPro (UEP))
            
    Electrical loads settings ------------------------------------------------------------------------------------------
        el_loads_hh_approach : str :
            indicate how to consider the electrical loads of households (hh) in the model. Ensure consistency with 
            internal_gains_approach. Choose between:
            'DIN-EN-16798-1' (simplified calculation based on standard values according to method of DIN EN 16798-1)
            or 'VDI4655' (simplified internal calculation according to method of VDI 4655)
            or 'REM' (based on detailed data externally calculated with tool REM using UrbanEnergyPro (UEP))

"""
building_zoning_approach = 'multi-zone'  # 'single-zone' or 'multi-zone' or 'hybrid'
# Thermal comfort settings  --------------------------------------------------------------------------------------------
# TODO: implement the use of the new variables of thermal comfort settings in the other modules
comfort_t_max_heating = '25.0'  # °C
comfort_t_min_heating = '20.0'  # °C
comfort_t_max_cooling = '23.0'  # °C
comfort_t_min_cooling = '26.0'  # °C
# Heating and cooling settings  ----------------------------------------------------------------------------------------
# TODO: implement the use of the new variables of heating and cooling settings in the other modules
heating_t_set = '22.5'  # °C
cooling_t_set = '24.5'  # °C
heating_t_set_back = 0.0  # °C
cooling_t_set_back = 0.0  # °C
# Heating set-back and cooling set-up
heating_t_set_back_start_hod = 23  # hour of the day: 1 = 00:00-01:00; 24 = 23:00-00:00
heating_t_set_back_stop_hod = 7  # hour of the day: 1 = 00:00-01:00; 24 = 23:00-00:00
cooling_t_set_up_start_hod = 23  # hour of the day: 1 = 00:00-01:00; 24 = 23:00-00:00
cooling_t_set_up_stop_hod = 7  # hour of the day: 1 = 00:00-01:00; 24 = 23:00-00:00z
# Heating and cooling period
heating_period_start_hoy = 1  # hour of the year: 1 = 1.Jan 00:00-01:00; 8760 = 31.Dec 23:00-00:00
heating_period_stop_hoy = 8760  # hour of the year: 1 = 1.Jan 00:00-01:00; 8760 = 31.Dec 23:00-00:00
cooling_period_start_hoy = 1  # hour of the year: 1 = 1.Jan 00:00-01:00; 8760 = 31.Dec 23:00-00:00
cooling_period_stop_hoy = 8760  # hour of the year: 1 = 1.Jan 00:00-01:00; 8760 = 31.Dec 23:00-00:00
# Gains settings -------------------------------------------------------------------------------------------------------
solar_radiation_approach = 'GH'  # Note: currently only 'GH' is available but the term 'GH' is new
# TODO: implement the use of the new term 'GH' in the other modules
# TODO: implement solar_radiation_approach 'EPW'
internal_gains_approach = 'REM'  # Note: currently only 'REM' is available
# TODO: implement internal_gains_approach 'DIN-EN-16798-1'
# Electrical loads settings --------------------------------------------------------------------------------------------
el_loads_hh_approach = 'REM'  # Note: currently only 'VDI4655' and 'REM' are available
# TODO: implement internal_gains_approach 'DIN-EN-16798-1'


# PHOTOVOLTAICS (pv) SETTINGS
########################################################################################################################
"""PHOTOVOLTAICS (pv) SETTINGS

    Parameters
    ----------
        pv_production_approach : str :
            indicate how to consider the pv generation in the model. Ensure consistency with solar_radiation_approach.
            Choose between:
            'EPW' (simplified calculation of solar radiation according to orientation and inclination of panel without
                considering self-shading or shading from context using data of radiation on the horizontal surface from
                an Energy Plus Weather (EPW) format file)
            or 'GH' (based on detailed values externally calculated using results of solar radiation analysis with
                radiance taking into account self-shading and shading from context using ladybug tools in grasshopper 
                (GH))
            if pv_production_approach == 'GH':
                required_action = 'provide with the data exported from GH as input data for the model'
            if pv_production_approach == 'EPW':
                required_action = '...'
"""
# TODO: implement the use of the new term 'GH' in the other modules
# TODO: implement solar_radiation_approach 'EPW'
pv_production_approach = 'GH'  # Note: currently only 'GH' is available but the term 'GH' is new


# ELECTRIC VEHICLES (ev) SETTINGS
########################################################################################################################
"""ELECTRIC VEHICLES (ev) SETTINGS

    Parameters
    ----------
    ...
"""


# SIMULATION (sim) SETTINGS
########################################################################################################################
"""SIMULATION (sim) SETTINGS

    Parameters
    ----------
    sim_type : str
        choose between simulation 'without_e_management' or 'with_e_management'
    
    Simulation time settings -------------------------------------------------------------------------------------------
        sim_start : int
            set an hour of year (hoy) at which the sim should start (1: 1.Jan 00:00-01:00; 8760: 31.Dec 23:00-00:00)
        sim_stop : int
            set an hour of year (hoy) at which the sim should finish (1: 1.Jan 00:00-01:00; 8760: 31.Dec 23:00-00:00)
        sim_resolution : float
            set the dimension of the sim time steps in hours (1.0: 1 hour; 0.25: 15 min; max 1.0)
        sim_training_days : int
            set a nr of days before the start time step of the simulation to train the model (thermal mass)
        
    Optimization (opt) time settings -----------------------------------------------------------------------------------
        opt_horizon_h : int
            set the time span in hours for which the optimizer should create predictions
        opt_interval_h : int
            set the frequency (nr of hours, hs) with which the optimizer should create predictions
"""
sim_type = 'with_e_management'
# Simulation time settings ---------------------------------------------------------------------------------------------
sim_start_hoy = 1  # 1: 1.Jan 00:00-01:00; 8760: 31.Dec 23:00-00:00
sim_stop_hoy = 24  # 1: 1.Jan 00:00-01:00; 8760: 31.Dec 23:00-00:00
sim_resolution_h = 1.0  # 1.0: 1 hour; 0.25: 15 min; max 1.0
sim_training_days = 1
# Optimization (opt) time settings -------------------------------------------------------------------------------------
opt_horizon_h = 24
opt_interval_h = 12


# RUN SIMULATION
########################################################################################################################
"""RUN SIMULATION

    Parameters
    ----------
    sim_run : bool
        indicate whether the simulation should start when running this script or not. Choose between 'False' or 'True'.
"""
run_simulation = True
# DO NOT CHANGE THE FOLLOWING LINES ------------------------------------------------------------------------------------
# ...


# ...


########################################################################################################################
""" ####################################################################################################################
# COUPLING WITH OLD VERSION OF CONFIG.py
#################################################################################################################### """

# MODEL NAME -----------------------------------------------------------------------------------------------------------
# name_model = 'MB_04'
name_model = model_name

# TIME SETTINGS --------------------------------------------------------------------------------------------------------
# Simulation
# starthour = float(90*24)
# endhour = starthour + float(24*1)
# time_resolution_h = float(1)
# precalc_days = 1
starthour = sim_start_hoy
endhour = sim_stop_hoy
time_resolution_h = sim_resolution_h
precalc_days = sim_training_days

# Optimisation
# predict_horizon_h = float(24)
# update_interval_h = float(12)
predict_horizon_h = opt_horizon_h
update_interval_h = opt_interval_h

# Quarter specifications -----------------------------------------------------------------------------------------------
# TODO PD: add quarter specs:
# lat/long

# INPUT PREPARATION ----------------------------------------------------------------------------------------------------
# Does Input_created already exist and is it consistent with the case study?
folder_input_created = 'Models/{0}/Results/Input_created'.format(name_model)
# If True: input_preparation is skipped and existing files are read in
# If False: within input_preparation it is still checked for each input separately if there are already matching files
complete_input_created_exists = True

# idea --> to be able to specify the type of simulations: VDI examples or CleanMode study

# GEOMETRIES AND CONSTRUCTIONS:
# Define whether these inputs should be prepared based on the files from GH or TRNSYS or VDI.
# idea --> to substitute the 3 options below with "geo_bin" (0: gh_bin; 1: trnsys_bin; 2: vdi_bin)
gh_bin = True
trnsys_bin = False
vdi_bin = False  # NOTE: vdi_bin is not used in the current project.

# INTERNAL GAINS AND HOUSEHOLD ELECTRIC PROFILES:
# Define whether these inputs should be generated by UEP
# idea --> to create the option for choosing between UEP and VDI + occupation Profile
# If False, dummy profiles (default values: 1) will be created (e.g. for code development)
# If True, define "UEP_region_name" (NAME to be assigned to the files SynCity_NAME.csv and SynHouseholds_NAME.csv,
#   which are to be used by UEP to generate electric profiles)
UEP_bin = True
UEP_region_name = 'Feldmoching_0'  # NOTE: make sure the csv names match the corresponding "filename_" inputs in the
# config_cleanmod.py file of UEP

# this building_code - bid_UEP dictionary has to be updated with the corresponding bids from UEP
# bid_dict = {'MFH1': 0, 'MFH2': 1}
# idea --> create this dict directly in input_preparation.py:
#   1) read Geometry_Zones.csv
#   2) extract unique building names
#   3) assign indexes starting by 0
bid_dict = {'MFH1': 0, 'MFH2': 1, 'MFH3': 2, 'MFH4': 3, 'MFH5': 4,
            'MFH6': 5, 'MFH7': 6, 'MFH8': 7, 'MFH9': 8, 'MFH10': 9}

# approach for household electrical loads
# idea --> adapt script to set "REM" if UEP_bin = True and "VDI4655" if UEP_bin = False
# options: 'VDI4655', 'REM'
approach_hh_load = 'REM'

# approach for EV loads (electric vehicle loads)
# options: 'simple', 'even'
# simple strategy means charging with maximum power (currently 22 kW) as soon as car arrives back home
# even strategy means even charging throughout the whole timeslot when the car is at home (perfect prediction assumed)
approach_ev_load = 'simple'

# percentage of households with ev_load
# currently same percentage for all buildings
percentage_ev_load = 0.5

# ENERGY BALANCE -------------------------------------------------------------------------------------------------------
# idea --> adapt code to select between: 1) Sim w/ or w/o design load calculation; 2) Sim w/ or w/o optimisation. \
#   For w/ design load, in runme_cleanmode the lists of elec_config and heat_config should automatically be adapted \
#   to elec_config = [] ; heat_config = ['sh_load']
# Naming: Desing Load Sim (DLsim), Energy Balance Sim (EBsim), Energy Managament Sim (EMsim)
# For DLsim: elec_config = [] ; heat_config = ['sh_load']
# For EBsim or EMsim:
# # Min --> elec_config = ['hh_load'] ; heat_config = ['sh_load']
# # Max --> elec_config = ['hh_load', 'pv_gen'] ; heat_config = ['sh_load', 'dhw_load']

# COMPONETS CONSIDERED IN ENERGY BALANCE
# options elec_config:
#   'hh_load',
#   'pv_gen',
#   'ev_load' needs Models/lightcase/Results/Input_created/EV_load/EVload.csv
#   'non_resi_load'
#   'non_resi_gen'
# options heat_config: 'sh_load', 'dhw_load'
elec_config = ['hh_load', 'non_resi_gen']
heat_config = ['sh_load']

# optimization (pyomo)
# idea --> automatically create this tupple by reading Input\Generators\Generators.scv and extracting the relevant data
commodities = ('Gas', 'Elec', 'Heat70', 'Heat55', 'Cold')
# idea --> automatically create this list by reading Regional_data\Buildings.scv and extracting the relevant data
commodities_thermal = ['Heat70', 'Heat55', 'Cold']
# idea --> automatically create this dict
binary_heat_dict = {'Heat70': 1, 'Heat55': 1, 'Cold': -1}   # binary describing effect of commodity on heat balance

# approach for PV generation
# options: 'Trnsys190d', 'gh'  ---> gh still to be added
approach_pv_gen = 'Trnsys190d'
# only in case of 'Trnsys190d':
fname_local_generation = 'Models/Feldmoching/Input/RESIDENTIAL/Load_profiles/Production_PV/PV_production_1m2_light.csv'
# options pv_strategy: 'total_envelope','roof','roof+facade_South','facade', None
pv_strategy = 'roof'
# ^ obsolet, weil TRNSYS nicht mehr genutzt wird
# TODO: prozess umschreiben, damit die GH-Daten verwendet werden

# ----------------------------------------------------------------------------------------------------------------------
"""
NOTE: all this can be set as fixed values for CleanMode and, therefore, it could be placed in other location. In case
a different RC-Model configuration would be programmed, this values should be adapted. Thus, in theory, this inputs
could also be variables, but it will be very unlikely that we change them. 
"""
# RC model
temp_state_names = ['TEW', 'TIW', 'Tz', 'T1', 'T2']
temp_disturb_names = ['QEW', 'QIW', 'Qcon', 'Taeq', 'Tvent']
# temp_input_names = ['Heat70', 'Cold'] --> shifted to naming of Matrix B2 in "create_dict_with_indexed_param" \
#   in optimisation in order to enable zone-specific selection of heat commodity (different temperature levels)
start_temp_general = {'TEW': 18, 'TIW': 22, 'Tz': 22, 'T1': 22, 'T2': 22, 'Qh': 2000, 'Qc': 0}  # Warning: if the sim \
#   should start in summer, change Qh: 0 and Qc: 2000 and TEW: 25 for example, to avoid errors, otherwise, the first \
#   timesteps of the sim could result in indoor temps outside the comfort band --> error
# ----------------------------------------------------------------------------------------------------------------------
# THERMAL COMFORT
comfort_bands = {2: [20, 25], 3: [18, 25]}  # Comfort bands dictionary --> comfort_category: [Tmin, Tmax]
comfort_category = 2  # Comfort band from dictionary to be applied in the simulation
Tdelta_max = 2  # [K] | Maximum temperature increase/decrease per timestep
Tz_avg = 22.0  # [C] | In EBsim: Tset; In EMsim: average temperature to be met by the optimiser in 24h
# ----------------------------------------------------------------------------------------------------------------------
# heating system
# Enter Details on heating: TUPLE WITH THE FORMAT (x_h, x_c, i_h, i_c, e_h, e_c, r_h, r_c)
# x: Convective contribution, i: Surface heating internal components, e: surface heating external components,
# r: Radiative contribution
# _h: Heating, _c: cooling
# !if VDI_bin hc will be changed in runme!
hc_tuple_list_trnsys = (0, 0, 0, 0, 0, 0, 0, 0)
# hc_tuple_list_general = (0.5, 0.5, 0, 0, 0, 0, 0.5, 0.5)
# hc_power_general = {'Qmax': 0, 'Qmin': 0}
# hc_power_general = {'Qmax': 5000, 'Qmin': -2000}
hc_power_general = {'Qmax': 20000, 'Qmin': -100}

# users
acr_global = 0.47

# casename
casename = 'V0'

# ----------------------------------------------------------------------------------------------------------------------
# additions for MK thesis
''' did you delete the input_created folder??? '''
# concerns the choice of buildings.csv
mk_case = False
# if True: a selection of the case to simulate has to be made:
# ['2.1', '9.1', '16.5', '15.1', '16.7', '8.1', '4.1', '15.2', '16.6', '16.3', '16.2']
case_sim = '4.1'
# if True: simulating multizone, if False: simulating onezone
# concerns the data choice, for onezone all loads (occupancy, active, light) are summed
mz_bin = False
# if validation case: dummy profiles for occupancy, active load and lighting
valid_case = False


########################################################################################################################
simulate(name_model, precalc_days, elec_config, folder_input_created, time_resolution_h, temp_state_names,
         run_simulation)